package com.parser.service;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.parser.customError.CustomException;

/**
 * @author AmantyaTech
 */
public class SMTPPacketVerifier {
	/**
	 * Parses the hexdump of SMTPPacketVerifier
	 * 
	 * @request string
	 * @response LinkedHashMap<String, Object>
	 */
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SMTPPacketVerifier.class);
	
	private enum Command {
		EHLO, HELO, MAIL, RCPT, DATA, RSET, VRFY, EXPN, HELP, NOOP, QUIT;
	}

	private static Set<String> commands = new HashSet<String>();

	static {
		for (Command command : Command.values()) {
			commands.add(command.name());
		}
	}

	private static String checkForCommand(String line, LinkedHashMap<String, String> outputMap) {
		
		String[] splitBySpace = line.split(" ");
		String parsedString = new String();
		for (String term : splitBySpace) {
			if (commands.contains(term)) {
				outputMap.put("Command", term);
			} else {
				parsedString += " " + term;
			}
		}
		return parsedString.trim();
	}

	public static LinkedHashMap<String, String> parseSMTPPacket(String line) {
		LinkedHashMap<String, String> outputMap = new LinkedHashMap();
		outputMap.put("Line", line);
		line = checkForCommand(line, outputMap);
		String[] parts = (line.split(":"));
		if (parts.length == 2)
			outputMap.put(parts[0], parts[1]);

		else if (parts.length == 1) {
			LinkedList<String> numbers = new LinkedList<String>();
			Pattern p = Pattern.compile("\\d+");
			Matcher m = p.matcher(line);
			while (m.find()) {
				numbers.add(m.group());
			}
			if (numbers.size() > 0) {
				outputMap.put("Code", numbers.get(0));
			}
			String remaining = line;

			for (String number : numbers) {
				remaining = remaining.replace(number, "");

			}

			LinkedList<String> words = new LinkedList<String>();

			Pattern p1 = Pattern.compile("[A-Z]+\\s");
			Matcher m1 = p1.matcher(remaining);
			while (m1.find()) {
				words.add(m1.group());
			}

			if (words.size() > 0) {
				outputMap.put("Command", words.get(0));
				remaining = remaining.replace(words.get(0), "");
				if (remaining.trim().length() > 0)
					outputMap.put("Request Parameter:", remaining);
			} else {
				if (remaining.trim().length() > 0)
					outputMap.put("Response Parameter", remaining);
			}
		}
		LOGGER.info("smtp output "+outputMap);
		return outputMap;
	}

	public static List<LinkedHashMap> verifySMTPPacket(String hexdump) throws CustomException {
		LOGGER.info("smtp input"+hexdump);
		List<LinkedHashMap> smtplist = new LinkedList<LinkedHashMap>();
		try {
			if (hexdump == null)
				throw new CustomException("Hexdump can't be null, please try again!",400);
			String text = new String(javax.xml.bind.DatatypeConverter.parseHexBinary(hexdump), "UTF-8");
			String[] lines = (text.split("[\\r\\n]+"));
			for (int i = 0; i < lines.length; i++)
				smtplist.add(parseSMTPPacket(lines[i]));
		} catch (Exception e) {
			LinkedHashMap<String, Object> outputMapError = new LinkedHashMap<String, Object>();	
			outputMapError.put("Error", "SMTP Malformed Packet - " + e.getMessage());
			smtplist.add(outputMapError);
			LOGGER.error("error smtp "+e.getMessage());
		}
		return smtplist;
	}

}