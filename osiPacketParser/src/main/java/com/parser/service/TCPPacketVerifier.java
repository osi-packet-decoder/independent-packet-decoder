package com.parser.service;

import java.util.LinkedHashMap;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.parser.customError.CustomException;

      /**
       * @author AmantyaTech
       */

public class TCPPacketVerifier {
		
	/**
	 * Parses the hexdump of TCPPacket
	 * @request string
	 * @response LinkedHashMap<String, Object>
	 */
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TCPPacketVerifier.class);
	
	@SuppressWarnings("rawtypes")
	public static LinkedHashMap<String, Object> verifyTCPPacket(String hexdump)	throws CustomException {
		LOGGER.info("tcp input"+hexdump);
		String requestMethodStr = "";
		LinkedHashMap<String, Object> outputMap = new LinkedHashMap<String, Object>();
		LinkedHashMap<String, Object> outputMapError = new LinkedHashMap<String, Object>();

		try {
			isValidTCPPacket(hexdump);
			String sourcePort = isValidSourcePort(hexdump.substring(0, 4));
			outputMap.put("TCP Source Port", sourcePort);

			String destinationPort = isValidDestinationPort(hexdump.substring(4, 8));
			outputMap.put("Destination Port", destinationPort);

			String sequenceNumber = isVaildSequenceNo(hexdump.substring(8, 16));
			outputMap.put("Sequence Number", sequenceNumber);

			String acknowlegementNumber = isValidAcknowledgementNo(hexdump.substring(16, 24));
			outputMap.put("Acknowledgement Number", acknowlegementNumber);

			String dataOffsetHex = hexdump.substring(24, 25);
			int dataOffsetDec = Integer.parseInt(dataOffsetHex, 16); // header in words
			outputMap.put("Data Offset", "Hexadecimal: 0x" + dataOffsetHex + " ; Decimal: " + dataOffsetDec);
			
			int dataBeginsFromIndex = dataOffsetDec * 8;
			int lengthInBytes = 4 * dataOffsetDec;
			if (lengthInBytes < 20 || lengthInBytes > 60)
				throw new CustomException("Header length not valid.... " + dataOffsetHex,1201);
			outputMap.put("TCP Header Length", lengthInBytes + " Bytes");
			String combinedInfoHex = hexdump.substring(25, 28);
			int combinedInfoInteger = Integer.parseInt(combinedInfoHex, 16);
			String combinedInfoBinary = Integer.toBinaryString(combinedInfoInteger);
			while (combinedInfoBinary.length() < 12) {
				combinedInfoBinary = "0" + combinedInfoBinary;
			}

			String reservedBinary = combinedInfoBinary.substring(0, 6);
			outputMap.put("Reserved", "Binary: " + reservedBinary);

			String controlBitsBinary = combinedInfoBinary.substring(6, 12);
			outputMap.put("Control Bit Flags", "Binary: " + controlBitsBinary);

			String URGbinary = controlBitsBinary.substring(0, 1);
			boolean urgentPointerFieldSignificantFlag = "1".equals(URGbinary);
			outputMap.put("Urgent Pointer Field Significant Flag", urgentPointerFieldSignificantFlag);

			String ACKbinary = controlBitsBinary.substring(1, 2);
			boolean acknowledgementFieldSignificantFlag = "1".equals(ACKbinary);
			outputMap.put("Acknowledgement Field Significant Flag", acknowledgementFieldSignificantFlag);

			String PSHbinary = controlBitsBinary.substring(2, 3);
			boolean pushFunctionFlag = "1".equals(PSHbinary);
			outputMap.put("Push Function Flag", pushFunctionFlag);

			String RSTbinary = controlBitsBinary.substring(3, 4);
			boolean resetTheConnectionFlag = "1".equals(RSTbinary);
			outputMap.put("Reset The Connection Flag", resetTheConnectionFlag);

			String SYNbinary = controlBitsBinary.substring(4, 5);
			boolean synchronizeSequenceNumbersFlag = "1".equals(SYNbinary);
			outputMap.put("Synchronize Sequence Numbers Flag", synchronizeSequenceNumbersFlag);

			String FINbinary = controlBitsBinary.substring(5, 6);
			boolean noMoreDataFromSenderFlag = "1".equals(FINbinary);
			outputMap.put("No More Data From Sender Flag", noMoreDataFromSenderFlag);

			String windowSizeHex = hexdump.substring(28, 32);
			if (windowSizeHex.length() != 4)
				throw new CustomException("Window Size not valid...." + windowSizeHex,1201);
			int windowSizeDec = Integer.parseInt(windowSizeHex, 16);
			outputMap.put("Window Size", "Hexadecimal: 0x" + windowSizeHex + " ; Decimal: " + windowSizeDec);

			String checksumHex = hexdump.substring(32, 36);
			if (checksumHex.length() != 4)
				throw new CustomException("Checksum not valid...." + checksumHex,1201);
			outputMap.put("Checksum", "Hexadecimal: 0x" + checksumHex);

			String urgentPointerHex = hexdump.substring(36, 40);
			if (urgentPointerHex.length() != 4)
				throw new CustomException("Urgent Pointer not valid...." + urgentPointerHex,1201);
			int urgentPointerDec = Integer.parseInt(urgentPointerHex, 16);
			outputMap.put("Urgent Pointer", "Hexadecimal: 0x" + urgentPointerHex + " ; Decimal: " + urgentPointerDec);

			int payloadCharactersCount = hexdump.length() - dataBeginsFromIndex;
			outputMap.put("TCP Payload ", (payloadCharactersCount/2) +" bytes");
			
			String hexdumpRemaining=hexdump.substring(dataBeginsFromIndex);
     		String hexdumptls = hexdump.substring((int) dataBeginsFromIndex);

			try {
     			requestMethodStr =	new String(DatatypeConverter.parseHexBinary(hexdumptls), "UTF-8");
     		}catch (Exception e) {
     			//e.printStackTrace();
     		}
			 // TCP to HTTP
			if (sourcePort.equals("80") || destinationPort.equals("80") || requestMethodStr.contains("HTTP")) {
				LOGGER.info("calling http"+hexdump.substring(dataBeginsFromIndex));
				outputMap.put("Application Layer Protocol", "HTTP");
				if (hexdump.length() > 40 && hexdumpRemaining.length() > 0) {
					//LinkedHashMap<String, Object> map3 = new LinkedHashMap<String, Object>();
					LinkedHashMap<String, Object> map3 = HTTPPacketVerifier.verifyHTTPPacket(hexdumpRemaining);
					outputMap.putAll(map3);
				}
			}
			// TCP over SMTP
						else if (hexdump.length() > 40 && Integer.parseInt(destinationPort) == 25
								|| hexdump.length() > 40 && Integer.parseInt(sourcePort) == 25) {
							outputMap.put("Application Layer Protocol", "SMTP");
							LOGGER.info("calling smtp"+hexdump.substring(dataBeginsFromIndex));
							List<LinkedHashMap> smtpList = SMTPPacketVerifier.verifySMTPPacket(hexdump.substring(dataBeginsFromIndex));
							outputMap.put("SMTP", smtpList);
						}
						
			
			// TCP over Daimeter Protocol
			else if ((Integer.parseInt(sourcePort) == 6003 && Integer.parseInt(destinationPort) == 51742)
					|| (Integer.parseInt(sourcePort) == 3868 || Integer.parseInt(destinationPort) == 3868))
				 {
					outputMap.put("Application Layer Protocol", "Diameter Protocol");
					if (hexdump.length() > 64) {
						LOGGER.info("calling Diameter Protocol "+hexdumpRemaining);
						List<LinkedHashMap<String, Object>> daimeterlist = DiameterProtocol
								.verifyDiameterProtocol(hexdumpRemaining);
						outputMap.put("Diameter Protocol", daimeterlist);
					}

				}
			
			// TCP to FTP
			else if (Integer.parseInt(sourcePort) == 21  || Integer.parseInt(destinationPort) == 21 ) {
				LOGGER.info("calling ftp "+hexdumpRemaining);
				outputMap.put("Application Layer Protocol", "FTP");
				java.util.List<LinkedHashMap> ftplist = FTPPacketVerifier.verifyFTPPacket(hexdumpRemaining);
				outputMap.put("FTP", ftplist);
			}
			
			// TCP to TLS
				else if (destinationPort.equals("443") || sourcePort.equals("443")) {
	                if (hexdump.length() > 40 && ((hexdumptls.substring(0, 2).equals("14"))
	                        || (hexdumptls.substring(0,2).equals("15")) || (hexdumptls.substring(0, 2).equals("16"))
	                        || (hexdumptls.substring(0, 2).equals("17")) || (hexdumptls.substring(0, 2).equals("18")) )) {
	                   
	                    LOGGER.info("calling tls "+hexdump.substring(dataBeginsFromIndex));
	                    outputMap.put("Transport Layer Security", "TLS");
	                    LinkedHashMap<String, Object> sslObject = new LinkedHashMap<String, Object>();
	                    sslObject = TLSPacketVerifier.verifyTLSPacket(hexdump.substring(dataBeginsFromIndex));
	                    outputMap.putAll(sslObject);
	                }
	            }
			
			// TCP to SIP
			else if (Integer.parseInt(sourcePort) == 5060 || Integer.parseInt(destinationPort) == 5060  || Integer.parseInt(sourcePort) == 5061 || Integer.parseInt(destinationPort) == 5061) {
				outputMap.put("Application Layer Protocol", "SIP");
				
				if (hexdump.length() > 40) {
					LOGGER.info("calling sip "+hexdumpRemaining);
					LinkedHashMap<String, Object> map3 = new LinkedHashMap<String, Object>();
					map3 = SIPPacketVerifier.verifySIPPacket(hexdumpRemaining);
					outputMap.putAll(map3);
				}
			}
			else {	
				if(hexdumpRemaining.length() >0) {
					LinkedHashMap<String, Object> map3 = new LinkedHashMap<String, Object>();
					outputMap.put("Application Layer Protocol", "Unsupported");

				try {					
					throw new CustomException("Unsupported Protocol-Packet does not contained supported protocol" ,400);
				}catch (Exception e) {
					map3.put("Error", e.getMessage());
					outputMap.put("Unsupported",map3);
				}
			}
		}
			return outputMap;
		} catch (Exception e) {
			outputMapError.put("Error", "TCP Malformed Packet - " +e.getMessage());
			LOGGER.error("error tcp "+e.getMessage());
			return outputMapError;
		}
	}

	// used to verify if Destination Port is valid
	private static String isValidDestinationPort(String destinationPort) throws CustomException {
		if (destinationPort.length() != 4)
			throw new CustomException("DestinationPort not valid...." + destinationPort,1201);
		else {
			return Integer.parseInt(destinationPort, 16) + "";
		}
	}
	// used to verify if Source Port is valid
	private static String isValidSourcePort(String sourcePort) throws CustomException {
		if (sourcePort.length() != 4)
			throw new CustomException("SourcePort not valid...." + sourcePort,1201);
		else {
			return Integer.parseInt(sourcePort, 16) + "";
		}
	}
	// used to verify if Acknowledgement number is valid
		private static String isValidAcknowledgementNo(String acknowledgementNumber) throws CustomException {
			if (acknowledgementNumber.length() != 8)
				throw new CustomException("Acknowledgement No not valid...." + acknowledgementNumber,1201);
			return "0x" + acknowledgementNumber;
		}

		// used to verify if Sequence number is valid
		private static String isVaildSequenceNo(String sequenceNumber) throws CustomException {
			if (sequenceNumber.length() != 8)
				throw new CustomException("Sequence No not valid...." + sequenceNumber,1201);
			return "0x" + sequenceNumber;
		}
	// used to verify TCPPacket is valid
	private static String isValidTCPPacket(String hexdump) throws CustomException {
		if (hexdump.length() < 40)
			throw new CustomException("Invalid TCP Packet " + hexdump.substring(0),400);

		else
			return hexdump;
	}

}