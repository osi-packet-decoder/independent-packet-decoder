package com.parser.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.parser.customError.CustomException;
import com.parser.utility.JsonUtil;

public class ParserService {

	private static final Logger logger = LoggerFactory.getLogger(ParserService.class);

	private static JsonUtil<Map<?, ?>> mapJsonUtil = new JsonUtil<Map<?, ?>>();
	private static JsonUtil<List<?>> listJsonUtil = new JsonUtil<List<?>>();

	private static boolean isValid(String json) {
		return !json.contains("Error");
	}

	private static boolean isValid(Map<String, Object> outputMap) {
		logger.debug("isValid -> (outputMap) {}", outputMap);
		String json = mapJsonUtil.convertObjectToJson(outputMap);
		logger.debug("isValid -> (json) {}", json);
		return isValid(json);
	}

	private static boolean isValid(List<?> outputList) {
		logger.debug("isValid -> (outputList) {}", outputList);
		String json = listJsonUtil.convertObjectToJson(outputList);
		logger.debug("isValid -> (json) {}", json);
		return isValid(json);
	}

	@SuppressWarnings({ "rawtypes", "unused" })
	public static List<Object> getPossiblePackets(List<Object> toggleList,String hexdumpString) throws CustomException {
		if(toggleList.size()==0) {
          throw new CustomException("Please select the valid protocols from settings tab and try again!",400);		
		}
		
		StringBuilder stringBuild = new StringBuilder();
		toggleList.forEach(stringBuild::append);
		//String st = b.toString();           
         String activeSwitch = stringBuild.toString();
		if (hexdumpString != null) {
			String hexdump = null, errormsg = null;
			int counter = 0, errorCode = 0;
			LinkedHashMap<String, Object> rootOutputMap = null;
			LinkedHashMap<String, Object> outputMap = null;
			boolean flag = false;
			List<Object> ipList = null;
			List<Object> ipErrorList = null;
			String currentProtocol = null;

			String regx = "[\\|\t\n\r]+";
			List<String> list = Arrays.asList(hexdumpString.split(regx));
			// ******** Remove empty index from list
			list = list.stream().filter(s -> !s.isEmpty()).collect(Collectors.toList());
			ipList = new ArrayList<>();
			for (int i = 0; i < list.size(); i++) {
				hexdump = list.get(i).replaceAll("\\s", "");
				outputMap = new LinkedHashMap<String, Object>();
				rootOutputMap = new LinkedHashMap<String, Object>();
				String hexStart = "<<", hexEnd = ">>";
				try {

					if ((hexdump.matches("[0-9A-Fa-f<>]+"))) {
						counter++;
						if (((hexdump.startsWith(hexStart)) && (hexdump.endsWith(hexEnd)))) {
							hexdump = hexdump.replace(hexEnd, "");
							hexdump = hexdump.replace(hexStart, "");
						} else {
							hexdump = hexdump;
						}

						boolean isV4 = false, isV6 = false;
						String IPVer = null;
						if (hexdump.length() > 0) {
							IPVer = hexdump.substring(0, 1);
							if (IPVer.equals("4")) {
								isV4 = true;
							} else if (IPVer.equals("6")) {
								isV6 = true;
							}
						}

						outputMap = new LinkedHashMap<>();
						LinkedHashMap<String, Object> httpOutput =new LinkedHashMap<>();
						try {
							currentProtocol = "HTTP";
							 httpOutput = HTTPPacketVerifier.verifyHTTPPacket(hexdump);
							if (isValid(httpOutput)) {
								if((activeSwitch.contains("HTTP") || activeSwitch.contains("All"))) {
								outputMap.put(currentProtocol, httpOutput);
							}else {
								outputMap.put("Error", "Unsupported Protocol - " + "Packet does not contained supported protocol");	
							}
					    	}
						} catch (Exception e) {
							//logger.error("exception in {}", currentProtocol, e);
						}
						
						if(httpOutput.containsKey("Error")) {
							
							try {
								currentProtocol = "SIP";
								LinkedHashMap<String, Object> sipOutput = SIPPacketVerifier.verifySIPPacket(hexdump);
								if (isValid(sipOutput) && sipOutput.size()>0) {
									if (activeSwitch.contains("SIP") || activeSwitch.contains("All")) {
										outputMap.putAll(sipOutput);
								}else  {
									outputMap.put("Error", "Unsupported Protocol - " + "Packet does not contained supported protocol");	
								}
								}
							} catch (Exception e) {
								//logger.error("exception in {}", currentProtocol, e);
							}
						try {
							   currentProtocol = "IP";
	                            List<Object> ipOutput = IPPacketVerifier.verifyIPPacket(hexdump);
	                            if (ipOutput.size() >= 1) {
	                                if(ipOutput.get(0).toString().contains("version")) {
	                                if (activeSwitch.contains("IPv4") || activeSwitch.contains("All")) {
	                                    if(!activeSwitch.contains("SIP") || !activeSwitch.contains("HTTP")|| !activeSwitch.contains("Diameter")|| !activeSwitch.contains("FTP")|| !activeSwitch.contains("RTP")) {
	                                    	List<Object> ipPacket = ValidationProtocol.verifyProtocol( toggleList, hexdump);
			 	                            outputMap.put(currentProtocol, ipPacket);
			                            }
			                            else {
			                            	outputMap.put(currentProtocol, ipOutput);
			                            }
	                                    
	                                }else {
										outputMap.put("Error", "Unsupported Protocol - " + "Packet does not contained supported protocol");	
	                                }
	                                }
	                                else if(ipOutput.get(0).toString().contains("Version")) {
	                                if (activeSwitch.contains("IPv6") || activeSwitch.contains("All")) {
	                                	if(!activeSwitch.contains("SIP") || !activeSwitch.contains("HTTP")|| !activeSwitch.contains("Diameter")|| !activeSwitch.contains("FTP")|| !activeSwitch.contains("RTP")) {
			                            	List<Object> ipPacket = ValidationProtocol.verifyProtocol( toggleList, hexdump);
			 	                            outputMap.put(currentProtocol, ipPacket);
			                            }
			                            else {
			                            	outputMap.put(currentProtocol, ipOutput);
			                            }
	                                    
	                                }else {
										outputMap.put("Error", "Unsupported Protocol - " + "Packet does not contained supported protocol");	
	                                }
	                                }
	                                }
							
						} catch (Exception e) {
							//logger.error("exception in {}", currentProtocol, e);
							e.printStackTrace();
						}

						try {
							currentProtocol = "RTP";
							LinkedHashMap<String, Object> rtpOutput = RTPPacketVerifier.parseRTPPacket(hexdump);
							if (isValid(rtpOutput)) {
								if (activeSwitch.contains("RTP") || activeSwitch.contains("All")) {
								outputMap.put(currentProtocol, rtpOutput);
							}else {
								outputMap.put("Error", "Unsupported Protocol - " + "Packet does not contained supported protocol");	
							}
							}
						} catch (Exception e) {
							//logger.error("exception in {}", currentProtocol, e);
						}

						
						
						try {
							currentProtocol = "FTP";
							List<LinkedHashMap> ftpOutput = FTPPacketVerifier.verifyFTPPacket(hexdump);
							if (isValid(ftpOutput)) {
								if (activeSwitch.contains("FTP") || activeSwitch.contains("All")) {
								outputMap.put(currentProtocol,ftpOutput);
							}else {
								outputMap.put("Error", "Unsupported Protocol - " + "Packet does not contained supported protocol");	
							}
							}
						} catch (Exception e) {
							//logger.error("exception in {}", currentProtocol, e);
						}
						
						try {
							currentProtocol = "Diameter";
							List<LinkedHashMap<String, Object>> daimeterOutput = DiameterProtocol.verifyDiameterProtocol(hexdump);
							if(daimeterOutput.size() !=0 && !daimeterOutput.get(0).containsKey("Error")) {
								if (activeSwitch.contains("Diameter") || activeSwitch.contains("All")) {
								if(!daimeterOutput.get(0).containsKey("Error"))
									outputMap.put(currentProtocol,daimeterOutput);
							}else {
								outputMap.put("Error", "Unsupported Protocol - " + "Packet does not contained supported protocol");	
							}
							}
							
						} catch (Exception e) {
							//logger.error("exception in {}", currentProtocol, e);
						}
						
						
						
						}
						if(outputMap.isEmpty()) {
							if(list.size()>1)
								outputMap.put("Error", "Malformed Packet - No valid hexdump found, Please try again!! '" + hexdump);
                            else
                                outputMap.put("Error", "Malformed Packet - No valid hexdump found, Please try again!! " );
						}
						ipList.add(outputMap);
						flag = true;
					} else {
						
						throw new CustomException("Malformed Packet - Hexdump format is not valid '" + hexdump + "' ",400);
					}

				} catch (CustomException e) {
					
					errormsg = e.getMessage();
					errorCode = e.getCode();
					if (counter == 0)
						throw new CustomException("No valid hexdump found, Please try again!", 400);
				}
			}
			if (ipList != null) {
				if (ipList.size() == 1) {
					@SuppressWarnings("unchecked")
					Map<Object, Object> ipObject = (Map<Object, Object>) ipList.get(0);
					if (ipObject.size() == 1) {
						if (ipObject.containsKey("Error"))
							throw new CustomException(ipObject.get("Error").toString(), errorCode);
					} else
						return ipList;
				} else if (ipList.size() > 1) {
					ipErrorList = new ArrayList<>();
					for (int j = 0; j < ipList.size(); j++) {
						@SuppressWarnings("unchecked")
						LinkedHashMap<String, Object> rootOut = (LinkedHashMap<String, Object>) ipList.get(j);						
						if (rootOut.get("Error") != null ) {
							ipErrorList.add(ipList.get(j));
						}
					}
					if (ipList.size() == ipErrorList.size())
						throw new CustomException("No valid hexdump found, Please try again!!", 400);

					else
						return ipList;
				}

				if (counter == 0)
					throw new CustomException("No valid hexdump found, Please try again!", 400);
				else
					return ipList;
			} else
				throw new CustomException("Data could not be processed,please try again!", 400);
		} else
			throw new CustomException("No hexdump found, Please give the hexdump and try again!", 400);
	}
	}
	
		
	
	