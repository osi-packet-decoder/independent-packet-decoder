package com.parser.service;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.file.Files;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JToggleButton;
import javax.swing.JTree;
import javax.swing.Popup;
import javax.swing.PopupFactory;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.parser.commonFunction.CommonFunction;
import com.parser.customError.CustomException;

     /**
      * @author AmantyaTech
      */

class Parser extends JFrame implements ActionListener {
	 final Logger logger = LoggerFactory.getLogger(Parser.class);

// Components of the Form 
	private Container c;
	private JLabel title, footerLabel,all,tcp,udp,sip,diameter,tls,dtls,icmp,sctp,ftp,rtp,http,smtp,ipv4,ipv6,protocols;
    private JComboBox cb; 
	private JTextArea inputTextArea;
	private JTree jt;

	int clicked = 0,counter= 0;
    float wx = 1f, wy = 1f;
    Popup popup; 
	java.util.List<Object> toggleList = new ArrayList<Object>();
	java.util.List<Object> activeList = new ArrayList<Object>();


	private JButton decode, clear,upload,search,submit,cancel;
	JButton download = new JButton("Download Result");
	int c1;
	private JPanel titlePanel, footerPanel, mainPanel, buttonPanel,searchPanel,searchButtonPanel,westPanel,eastPanel;
	JFileChooser fileChooser = new JFileChooser();
	Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	int screenHeight = screenSize.height;
	int screenWidth = screenSize.width;
	java.util.List<Object> map = new ArrayList<Object>();
	JSONArray json = null;
	JPanel jtreePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
	String appIcon = "com/parser/icon.png";
	private BufferedImage verizonLogo,settingLogo;
	private static String exceptionMessage = null;
	JScrollPane jtreeScrollPane;
	ToggleSwitch tsTCP,tsUDP,tsSCTP,tsICMP,tsSIP,tsHTTP,tsDTLS,tsTLS,tsRTP,tsIPv4,tsIPv6,tsFTP,tsDiameter,tsSMTP,tsAll;
	private ToggleSwitch toggleSwitch = new ToggleSwitch();

	@SuppressWarnings({ "null", "unchecked" })
	public Parser() throws CustomException {

		setTitle("Protocol Decoder");
		setBounds(200, 0, screenWidth-500, screenHeight-40);
		setBackground(new Color(208, 224, 222));
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setResizable(true);
		setIconImage(createImage(appIcon).getImage());
		c = getContentPane();
		c.setLayout(new BorderLayout());

		c.setBackground(new Color(208, 224, 222));

		mainPanel = new JPanel();
		mainPanel.setBackground(new Color(208, 224, 222));
		westPanel = new JPanel();
		westPanel.setBackground(new Color(208, 224, 222));
		westPanel.setLayout(new FlowLayout(FlowLayout.LEFT));

		eastPanel = new JPanel();
		eastPanel.setBackground(new Color(208, 224, 222));
		eastPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));

		mainPanel.setLayout(new FlowLayout(FlowLayout.CENTER, screenWidth / 2, screenHeight / 114));


		try {
			verizonLogo = ImageIO.read(getClass().getResource("/com/parser/amantya.jpg"));
			settingLogo = ImageIO.read(getClass().getResource("/com/parser/settingAmantya.jpg"));

		} catch (IOException e2) {

		}
	   titlePanel = new JPanel();
	   settingLogo.setRGB(0, 0, 0);

	   titlePanel.setLayout(new GridBagLayout());
		GridBagConstraints gridBagConstraints = new GridBagConstraints();

		JLabel verizonLogoLabel = new JLabel(new ImageIcon(verizonLogo));
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.weightx = 1;
		gridBagConstraints.anchor = GridBagConstraints.NORTHWEST;
//1st cordinate in inset pixel from vertical, 2nd cordinate pixel from left(x-axis),3rd coordinate from downward vertical
        gridBagConstraints.insets = new Insets(4, 30, 0, 0);
		title = new JLabel("Protocol Decoder", SwingConstants.CENTER);
		title.setHorizontalAlignment(SwingConstants.CENTER);
		title.setFont(new Font("Arial", Font.PLAIN, 30));
		title.setPreferredSize(new Dimension(screenWidth, 50));
		title.setForeground(Color.BLACK);
		titlePanel.add(title);

        GridBagConstraints gbc= new GridBagConstraints();
      
		searchPanel = new JPanel();
		searchPanel.setBackground(new Color(208, 224, 222));
		searchPanel.setLayout(new FlowLayout(FlowLayout.CENTER));

		searchPanel.setBorder(BorderFactory.createLineBorder(Color.black, 1));

	  
		titlePanel.add(verizonLogoLabel, gridBagConstraints);


		titlePanel.setBackground(Color.WHITE);

		jtreePanel.setBackground(Color.WHITE);
		jtreeScrollPane = new JScrollPane(jtreePanel, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		jtreeScrollPane.setPreferredSize(new Dimension(screenWidth/2, screenHeight / 2));
		inputTextArea = new JTextArea("");
		inputTextArea.setLineWrap(true);
		JScrollPane inputTextScrollPane = new JScrollPane(inputTextArea, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		inputTextScrollPane.setPreferredSize(new Dimension(screenWidth/2, 100));
		mainPanel.add(inputTextScrollPane);


		tsAll = new ToggleSwitch();
		//tsAll.setActiveSwitch("All");
		tsAll.setActivated(true);
        toggleList.add("All");
		tsAll.setLocation(5, 135);
		tsAll.setPreferredSize(new Dimension(25, 15));
		tsAll.addMouseListener(new MouseAdapter() {
            public void mouseReleased(MouseEvent e) {
                if(tsAll.isActivated()) { //**Exception here**
//                	tsTCP.isActivated();
//                	tsTCP.setActivated(true);
//                	tsUDP.isActivated();
//                	tsUDP.setActivated(true);
//                	tsSCTP.isActivated();
//                	tsSCTP.setActivated(true);
//                	tsICMP.isActivated();
//                	tsICMP.setActivated(true);
                	tsSIP.isActivated();
                	tsSIP.setActivated(true);
                	tsHTTP.isActivated();
                	tsHTTP.setActivated(true);
//                	tsDTLS.isActivated();
//                	tsDTLS.setActivated(true);
//                	tsTLS.isActivated();
//                	tsTLS.setActivated(true);
                	tsRTP.isActivated();
                	tsRTP.setActivated(true);
                	tsIPv4.isActivated();
                	tsIPv4.setActivated(true);
                	tsIPv6.isActivated();
                	tsIPv6.setActivated(true);
                	tsFTP.isActivated();
                	tsFTP.setActivated(true);
                	tsDiameter.isActivated();
                	tsDiameter.setActivated(true);
                	repaint();
                    revalidate();

                } else  { //**or here**
                	tsIPv4.setActivated(false);
                	tsIPv6.setActivated(false);
                    tsSIP.setActivated(false);
                    tsHTTP.setActivated(false);
                    tsRTP.setActivated(false);
                    tsFTP.setActivated(false);
                    tsDiameter.setActivated(false);
                	repaint();
                    revalidate();
                }
            }
        });
		all = new JLabel("All");
		all.setPreferredSize(new Dimension(100, 15));
		
			protocols = new JLabel("Protocols", SwingConstants.CENTER);
			protocols.setHorizontalAlignment(SwingConstants.CENTER);
			protocols.setFont(new Font("Arial", Font.PLAIN, 20));
			protocols.setPreferredSize(new Dimension(180, 40));
			protocols.setLocation(100, 15);
			searchPanel.add(protocols);

			
			searchPanel.add(all);
			searchPanel.add(tsAll);
			
			tsSIP = new ToggleSwitch();
			tsSIP.setLocation(5, 135);
            toggleList.add("SIP");
			tsSIP.setActivated(true);
			tsSIP.setPreferredSize(new Dimension(25, 15));
			tsSIP.addMouseListener(new MouseAdapter() {
	            public void mouseReleased(MouseEvent e) {
	                if(tsSIP.isActivated()) { //**Exception here**
	                	if( tsFTP.isActivated() && tsHTTP.isActivated() &&
		        				  tsRTP.isActivated() && tsIPv4.isActivated() && tsIPv6.isActivated()
		        					&& tsDiameter.isActivated()) {
		        				tsAll.setActivated(true);
		        				tsAll.isActivated();
		        			}
	                    repaint();
	                    revalidate();

	                } else  { //**or here**
	                	tsAll.setActivated(false);
	                    repaint();
	                    revalidate();
	                }
	            }
	        });
			

			sip = new JLabel("SIP");
			sip.setPreferredSize(new Dimension(100, 15));
			
			 tsHTTP = new ToggleSwitch();
			tsHTTP.setLocation(5, 135);
			tsHTTP.setActivated(true);
            toggleList.add("HTTP");
			tsHTTP.setPreferredSize(new Dimension(25, 15));
			tsHTTP.addMouseListener(new MouseAdapter() {
	            public void mouseReleased(MouseEvent e) {
	                if(tsHTTP.isActivated()) { //**Exception here**
	                	if( tsSIP.isActivated() && tsFTP.isActivated() &&
		        				  tsRTP.isActivated() && tsIPv4.isActivated() && tsIPv6.isActivated()
		        					&& tsDiameter.isActivated()) {
		        				tsAll.setActivated(true);
		        				tsAll.isActivated();
		        			}
	                	repaint();
	                    revalidate();


	                } else  { //**or here**
	                	tsAll.setActivated(false);
	                    repaint();
	                    revalidate();
	                }
	            }
	        });
			http = new JLabel("HTTP");
			http.setPreferredSize(new Dimension(100, 15));


			 tsRTP = new ToggleSwitch();
			tsRTP.setLocation(5, 135);
			tsRTP.setActivated(true);
            toggleList.add("RTP");
			tsRTP.setPreferredSize(new Dimension(25, 15));
			tsRTP.addMouseListener(new MouseAdapter() {
	            public void mouseReleased(MouseEvent e) {
	                if(tsRTP.isActivated()) { //**Exception here**
	                	if( tsSIP.isActivated() && tsHTTP.isActivated() &&
		        				  tsFTP.isActivated() && tsIPv4.isActivated() && tsIPv6.isActivated()
		        					&& tsDiameter.isActivated()) {
		        				tsAll.setActivated(true);
		        				tsAll.isActivated();
		        			}
	                    repaint();
	                    revalidate();
	                } else  { //**or here**
	                    tsAll.setActivated(false);
	                    repaint();
	                    revalidate();
	                }
	            }
	        });
			rtp = new JLabel("RTP");
			rtp.setPreferredSize(new Dimension(100, 15));
			

			 tsIPv4 = new ToggleSwitch();
			tsIPv4.setLocation(5, 135);
            toggleList.add("IPv4");
			tsIPv4.setActivated(true);
			tsIPv4.setPreferredSize(new Dimension(25, 15));
			tsIPv4.addMouseListener(new MouseAdapter() {
	            public void mouseReleased(MouseEvent e) {
	                if(tsIPv4.isActivated()) { //**Exception here**
	                	if( tsSIP.isActivated() && tsHTTP.isActivated() &&
		        				  tsRTP.isActivated() && tsFTP.isActivated() && tsIPv6.isActivated()
		        					&& tsDiameter.isActivated()) {
		        				tsAll.setActivated(true);
		        				tsAll.isActivated();
		        			}
	                	repaint();
	                	revalidate();

	                } else  { //**or here**
	                    if(tsIPv6.isActivated()) {
		                    tsAll.setActivated(false);
	
	                    }else {
	                    tsTCP.setActivated(false);
	                    tsUDP.setActivated(false);
	                    tsSCTP.setActivated(false);
	                    tsICMP.setActivated(false);
	                    tsSIP.setActivated(false);
	                    tsHTTP.setActivated(false);
	                    tsDTLS.setActivated(false);
	                    tsTLS.setActivated(false);
	                    tsRTP.setActivated(false);
	                    tsFTP.setActivated(false);
	                    tsDiameter.setActivated(false);
	                    tsAll.setActivated(false);
	                    }
	                    repaint();
	                    revalidate();
	                }
	            }
	        });
			ipv4 = new JLabel("IPv4");
			ipv4.setPreferredSize(new Dimension(100, 15));
			
			searchPanel.add(ipv4);
			searchPanel.add(tsIPv4);

			tsIPv6 = new ToggleSwitch();
			tsIPv6.setLocation(5, 135);
			tsIPv6.setActivated(true);
            toggleList.add("IPv6");
			tsIPv6.setPreferredSize(new Dimension(25, 15));
			tsIPv6.addMouseListener(new MouseAdapter() {
	            public void mouseReleased(MouseEvent e) {
	                if(tsIPv6.isActivated()) { //**Exception here**
	                	if( tsSIP.isActivated() && tsHTTP.isActivated() &&
		        				  tsRTP.isActivated() && tsIPv4.isActivated() && tsFTP.isActivated()
		        					&& tsDiameter.isActivated()) {
		        				tsAll.setActivated(true);
		        				tsAll.isActivated();
		        			}
	                	repaint();
	                	revalidate();

	                } else  { //**or here**
	                    if(tsIPv4.isActivated()) {
		                    tsAll.setActivated(false);
	
	                    }else {
	                    	tsTCP.setActivated(false);
	                    tsUDP.setActivated(false);
	                    tsSCTP.setActivated(false);
	                    tsICMP.setActivated(false);
	                    tsSIP.setActivated(false);
	                    tsHTTP.setActivated(false);
	                    tsDTLS.setActivated(false);
	                    tsTLS.setActivated(false);
	                    tsRTP.setActivated(false);
	                    tsFTP.setActivated(false);
	                    tsDiameter.setActivated(false);
	                    tsAll.setActivated(false);
	                    }
	                    repaint();
	                    revalidate();
	                }
	            }
	        });
			ipv6 = new JLabel("IPv6");
			ipv6.setPreferredSize(new Dimension(100, 15));
			
			searchPanel.add(ipv6);
			searchPanel.add(tsIPv6);

			 tsFTP = new ToggleSwitch();
			tsFTP.setLocation(5, 135);
			tsFTP.setActivated(true);
            toggleList.add("FTP");
			tsFTP.setPreferredSize(new Dimension(25, 15));
			tsFTP.addMouseListener(new MouseAdapter() {
	            public void mouseReleased(MouseEvent e) {
	                if(tsFTP.isActivated()) { //**Exception here**

//	                	tsTCP.isActivated();
//	                	tsTCP.setActivated(true);
//	                	tsIPv4.isActivated();
//	                	tsIPv4.setActivated(true);
//	                	tsIPv6.isActivated();
//	                	tsIPv6.setActivated(true);
	                	if( tsSIP.isActivated() && tsHTTP.isActivated() &&
	        				  tsRTP.isActivated() && tsIPv4.isActivated() && tsIPv6.isActivated()
	        					&& tsDiameter.isActivated()) {
	        				tsAll.setActivated(true);
	        				tsAll.isActivated();
	        			}
	                    repaint();
	                    revalidate();

	                } else  { //**or here**
	                 
	                    tsAll.setActivated(false);
	                    repaint();
	                    revalidate();
	                }
	            }
	        });
			ftp = new JLabel("FTP");
			ftp.setPreferredSize(new Dimension(100, 15));
			
			
			 tsDiameter = new ToggleSwitch();
			tsDiameter.setLocation(5, 135);
			tsDiameter.setActivated(true);
            toggleList.add("Diameter");
			tsDiameter.setPreferredSize(new Dimension(25, 15));
			tsDiameter.addMouseListener(new MouseAdapter() {
	            public void mouseReleased(MouseEvent e) {
	                if(tsDiameter.isActivated()) { //**Exception here**
	                	if( tsSIP.isActivated() && tsHTTP.isActivated() &&
		        				  tsRTP.isActivated() && tsIPv4.isActivated() && tsIPv6.isActivated()
		        					&& tsFTP.isActivated()) {
		        				tsAll.setActivated(true);
		        				tsAll.isActivated();
		        			}
	                    repaint();
	                    revalidate();

	                } else  { //**or here**
	                    tsAll.setActivated(false);
	                    repaint();
	                    revalidate();
	                }
	            }
	        });
			diameter = new JLabel("Diameter");
			diameter.setPreferredSize(new Dimension(100, 15));

			searchPanel.add(http);
			searchPanel.add(tsHTTP);
			
			searchPanel.add(ftp);
			searchPanel.add(tsFTP);
	
			searchPanel.add(sip);
			searchPanel.add(tsSIP);
		
			searchPanel.add(rtp);
			searchPanel.add(tsRTP);			
			

			searchPanel.add(diameter);
			searchPanel.add(tsDiameter);

			
			searchButtonPanel = new JPanel();
			searchButtonPanel.setBackground(Color.WHITE);
			
		

			submit = new JButton("Submit");
			submit.setFont(new Font("Arial", Font.PLAIN, 15));
			submit.setForeground(Color.BLACK);
			submit.setBackground(Color.WHITE);
            Border borderSubmit = new LineBorder(Color.BLACK, 2, true);
            submit.setBorder(borderSubmit);

			submit.addActionListener(this);
			submit.setPreferredSize(new Dimension(90, 20));
			submit.setOpaque(true);
			//submit.setBorderPainted(false);
			searchButtonPanel.add(submit);
			
			cancel = new JButton("Cancel");
			cancel.setFont(new Font("Arial", Font.PLAIN, 15));
			cancel.setForeground(Color.BLACK);
			cancel.setBackground(Color.WHITE);
            Border borderCancel = new LineBorder(Color.BLACK, 2, true);
            cancel.setBorder(borderCancel);

			cancel.setPreferredSize(new Dimension(90, 20));
			cancel.setOpaque(true);
			cancel.addActionListener(this);
			//cancel.setBorderPainted(false);
			searchButtonPanel.add(cancel);
			
			searchPanel.add(searchButtonPanel,BorderLayout.CENTER);
	        searchPanel.setPreferredSize(new Dimension(200, 260));

	        
            Border borderSearch = new LineBorder(Color.BLACK, 2, true);                                
            searchPanel.setBorder(borderSearch);
            
			searchPanel.setBackground(Color.WHITE);
			GridBagConstraints gridBagConstraintsUDP = new GridBagConstraints();
			gridBagConstraintsUDP.gridx = 0;
			gridBagConstraintsUDP.gridy = 0;
			gridBagConstraintsUDP.weightx = 1;
			gridBagConstraintsUDP.anchor = GridBagConstraints.NORTHEAST;
	        //1st cordinate in inset pixel from vertical, 2nd cordinate pixel from left(x-axis),3rd coordinate from downward vertical
            gridBagConstraintsUDP.insets = new Insets(2, 0, 0, 8);
			Icon icon = new ImageIcon("/com/parser/setting.jpg");
			search = new JButton(new ImageIcon(settingLogo));
			search.setForeground(Color.WHITE);
			search.setBackground(Color.WHITE);
            Border borderSearchButton = new LineBorder(Color.WHITE, 2, true);
            search.setBorder(borderSearchButton);

			search.setToolTipText("Select Protocols");
			UIManager.put("ToolTip.background", Color.WHITE);
		      UIManager.put("ToolTip.foreground", Color.BLACK);
		      UIManager.put("ToolTip.font", new Font("Arial", Font.BOLD, 14));

		      //commented for hiding settingpanel
			search.addActionListener(new ActionListener() {
	            public void actionPerformed(ActionEvent e) {

	           		 eastPanel.add(searchPanel);
	           		searchPanel.repaint();
	    			searchPanel.revalidate();
	                    searchPanel.setVisible(true);
	                
	            }
	        });
			search.setVisible(true);
			search.setOpaque(true);
			
			titlePanel.add(search, gridBagConstraintsUDP);
            Border borderTitle = new LineBorder(Color.BLACK, 2, true);
            titlePanel.setBorder(borderTitle);

			c.add(titlePanel, BorderLayout.NORTH);

		



		JEditorPane jePane = new JEditorPane("text/html",
				"*Multiple hexdumps can be uploaded in a single text file separated by new line. <a href='txt'>Click here</a> to download sample file.");
		jePane.setEditable(false);
		jePane.setBackground(new Color(208, 224, 222));
		jePane.setForeground(Color.gray);

		jePane.addHyperlinkListener(new HyperlinkListener() {
			public void hyperlinkUpdate(HyperlinkEvent hle) {
				if (HyperlinkEvent.EventType.ACTIVATED.equals(hle.getEventType())) {
					if (hle.getDescription().equalsIgnoreCase("txt")) {
						String content;
						File file = null;
						String resource = "/com/parser/sample.txt";
						java.net.URL res = getClass().getResource(resource);
						if (res.getProtocol().equals("jar")) {
							try {
								InputStream inputStream = (InputStream) getClass().getResourceAsStream(resource);
								file = File.createTempFile("tempfile", ".tmp");
								OutputStream outputStream = new FileOutputStream(file);
								int read;
								byte[] bytes = new byte[1024];

								while ((read = inputStream.read(bytes)) != -1) {
									outputStream.write(bytes, 0, read);
								}
								outputStream.close();
								file.deleteOnExit();
							} catch (IOException ex) {
								// ex.printStackTrace();
							}
						} else {
							// this will probably work in your IDE, but not from a JAR
							file = new File(res.getFile());
						}
						try {
							content = new String(Files.readAllBytes(file.toPath()));
							String home = System.getProperty("user.home");
							@SuppressWarnings("resource")
							FileWriter fileWriter = new FileWriter(home + "/Downloads/sample.txt");
							fileWriter.write(content);
							fileWriter.flush();

						} catch (IOException e1) {

						}
					}
				}
			}
		});
		jePane.setOpaque(true);
		mainPanel.add(jePane);
		buttonPanel = new JPanel();
		buttonPanel.setBackground(new Color(208, 224, 222));
		

		decode = new JButton("Decode");
		decode.setFont(new Font("Arial", Font.PLAIN, 15));
		decode.setForeground(Color.BLACK);
		decode.setBackground(Color.WHITE);
        Border borderDecode = new LineBorder(Color.BLACK, 2, true);
        decode.setBorder(borderDecode);

		decode.addActionListener(this);
		decode.addActionListener(ev -> {
			String strCurrentLine = null;

			Scanner scanner = new Scanner(inputTextArea.getText());	
			StringBuilder fileBuilder = new StringBuilder();
			String inputstring = null;
			 while(scanner.hasNext()) {
				 strCurrentLine = scanner.nextLine();	
			    if(!strCurrentLine.contains("#")) {
			    	fileBuilder.append(strCurrentLine+"\n");
			    	if(fileBuilder!=null)
			    		inputstring = fileBuilder.toString();

			  }
  }	
			    scanner.close(); 
				logger.info("Parser takes input..."+inputstring);

			    if(map.size()!=0)
			    	map.clear();
			    try {
			    	
			    	map = ParserService.getPossiblePackets(toggleList, inputstring);

			logger.info("Parser output from ValidationProtocol..."+map);
             
			    }catch (CustomException e) {
					// TODO: handle exception

			    	if(strCurrentLine == null) {
						logger.info("Parser output from ValidationProtocol..."+"No Input, Please give the input and try again!");

			    		JOptionPane.showMessageDialog(null, "No Input, Please give the input and try again!",
								"Failure Message", JOptionPane.ERROR_MESSAGE);
			    	
			    		
			    	}else {
						logger.error("Parser output from ValidationProtocol..."+e.getMessage());
			    	JOptionPane.showMessageDialog(null, e.getMessage(),
							"Failure Message", JOptionPane.ERROR_MESSAGE);	
				}
			    }
		});

		decode.setPreferredSize(new Dimension(130, 30));
		decode.setOpaque(true);
		buttonPanel.add(decode);

		FileNameExtensionFilter extensionFilter = new FileNameExtensionFilter("TEXT FILES", "txt", "text");
		fileChooser.setFileFilter(extensionFilter);
		upload = new JButton("Upload");
		upload.setFont(new Font("Arial", Font.PLAIN, 15));
		upload.addActionListener(this);
		upload.addActionListener(ev -> {
			String strCurrentLine = null;
			int returnVal = fileChooser.showOpenDialog(c);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				File file = fileChooser.getSelectedFile();
				inputTextArea.setText("");

				if (file.getName().endsWith(".txt")) {
					BufferedReader input = null;
					try {
						input = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						//e.printStackTrace();
					}
					if (file.length() == 0) {
						if(map.size()!=0)
					    	map.clear();
						logger.error("Parser takes input..."+"Text file is empty!");
						JOptionPane.showMessageDialog(null, "Text file is empty!", "Failure Message",
								JOptionPane.ERROR_MESSAGE);
					} else {

						try {
					        Scanner scanner = new Scanner(input);	
					    	StringBuilder fileBuilder = new StringBuilder();
					    	String inputString = null;
					    	 while(scanner.hasNext()) {
					    		 strCurrentLine = scanner.nextLine();			  	  
						        if(!strCurrentLine.contains("#")) {
						        	if(strCurrentLine!=null)
								    	fileBuilder.append(strCurrentLine+"\n");
						        	inputString = fileBuilder.toString();
						      }
						  }	
						        scanner.close(); 
								logger.info("Parser takes input..."+inputString);

						        if(map.size()!=0)
							    	map.clear();
						    	map = ParserService.getPossiblePackets(toggleList, inputString);

						JOptionPane.showMessageDialog(null, "Your file is uploaded successfully!",
								"Success Message", JOptionPane.PLAIN_MESSAGE);
						logger.info("Your file is uploaded successfully!");
						logger.info("Parser output from ValidationProtocol..."+map);

					
						}catch (CustomException e) {
							// TODO: handle exception
							exceptionMessage = e.getMessage();
							if(exceptionMessage!=null) {
								logger.error("Parser output from ValidationProtocol..."+exceptionMessage);
							JOptionPane.showMessageDialog(null, exceptionMessage,
									"Failure Message", JOptionPane.ERROR_MESSAGE);
							}else {
								logger.error("Parser output from ValidationProtocol..."+"Hexdump format is not valid, Please check the format and try again!");

								JOptionPane.showMessageDialog(null, "Hexdump format is not valid, Please check the format and try again!",
										"Failure Message", JOptionPane.ERROR_MESSAGE);	
							}
						}
						}

				}

					else {
						map.clear();
						jtreeScrollPane.setVisible(false);
			            download.setVisible(false);
						JOptionPane.showMessageDialog(null, "Your file is not supported, Please upload the text file!", "Failure Message",
								JOptionPane.ERROR_MESSAGE);
						logger.error("File uploaded..."+"Your file is not supported, Please upload the text file!");

					}
			}

		});
		upload.setForeground(Color.BLACK);
		upload.setBackground(Color.WHITE);
        Border border = new LineBorder(Color.BLACK, 2, true);
        upload.setBorder(border);

		upload.setPreferredSize(new Dimension(130, 30));
		upload.setOpaque(true);
		buttonPanel.add(upload);
		mainPanel.add(buttonPanel);

		clear = new JButton("Clear");
		clear.setFont(new Font("Arial", Font.PLAIN, 15));
		clear.setForeground(Color.BLACK);
		clear.setBackground(Color.WHITE);
        Border borderClear = new LineBorder(Color.BLACK, 2, true);
        clear.setBorder(borderClear);

		clear.addActionListener(this);
		clear.setPreferredSize(new Dimension(130, 30));
		clear.setOpaque(true);
		buttonPanel.add(clear);
		
	
			repaint();
			revalidate();
			
				

		download.setFont(new Font("Arial", Font.PLAIN, 15));
		download.addActionListener(ev -> {
			try {
				List<Object> jsonOutputList = null;
				// Used Gson Pretty Printing for arranging fields in download file
				Gson gson = new GsonBuilder().setPrettyPrinting().create();
				String transportNodekey = null;
				Object applicationNodekey = null;
				String ipv6Nodekey = null;
				String ipVersion = null;
				String independentProtocol = null;
				String ipv6Version = null;
				String errorNodeKey = null;
				String downloadFileName = null;
				jsonOutputList = new ArrayList<Object>();
				for (int i = 0; i < map.size(); i++) {
					String errorMessage = null;
					Map rootObject = new LinkedHashMap<>();
					Map ipv4Object = new LinkedHashMap<>();
					Map ipPacketObject = new LinkedHashMap<>();
					Map ipv6Object = new LinkedHashMap<>();
					Map tcpObject = new LinkedHashMap<>();
					Map udpObject = new LinkedHashMap<>();
					Map httpObject = new LinkedHashMap<>();
					Map sctpObject = new LinkedHashMap<>();
					Map errorObject = new LinkedHashMap<>();
					Map icmpObject = new LinkedHashMap<>();
					Map otherIPObject = new LinkedHashMap<>();
					Map otherTransportObject = new LinkedHashMap<>();
					Map otherIPICMPObject = new LinkedHashMap<>();
					Map otherIPv4ICMPObject = new LinkedHashMap<>();
					Map otherIPv6ICMPObject = new LinkedHashMap<>();
					Map otherTransportICMPObject = new LinkedHashMap<>();

					int upperLayerProtocol = 0,ipv6LayerProtocol = 0;
					Map<String, Object> downloadMap = (Map<String, Object>) map.get(i);
					
					for (String key1 : downloadMap.keySet()) {
	                    
	                    if (key1.equals("SIP")) {
							c1 = 7;
							ipPacketObject.put(key1, downloadMap.get(key1));
							independentProtocol = "SIP";
						    rootObject.put("Packet " + (i + 1), ipPacketObject);
						    break;
						}
	                    if (key1.equals("FTP")) {
	                    	ipPacketObject.put(key1, downloadMap.get(key1));
	                    	independentProtocol = "FTP";
						    rootObject.put("Packet " + (i + 1), ipPacketObject);
							break;
						}
						
						if (key1.equals("Diameter")) {
							ipPacketObject.put(key1, downloadMap.get(key1));
							independentProtocol = "Diameter";
						    rootObject.put("Packet " + (i + 1), ipPacketObject);
							break;
						}
						
						if (key1.equals("RTP")) {
							ipPacketObject.put(key1, downloadMap.get(key1));
							independentProtocol = "RTP";
						    rootObject.put("Packet " + (i + 1), ipPacketObject);
							break;
						}
						
						if (key1.equals("HTTP")) {
							ipPacketObject.put(key1, downloadMap.get(key1));
							independentProtocol = "HTTP";
						    rootObject.put("Packet " + (i + 1), ipPacketObject);
							break;
						}
						
						if (key1.equals("Error") || key1.equals("Unsupported")) {
							ipPacketObject.put(key1, downloadMap.get(key1));
							 String errorString = (String) ipPacketObject.get("Error");
							 List<String> list = Arrays.asList(errorString.split("-"));
							 errorMessage = list.get(0);
							 independentProtocol = "Error";
						    //rootObject.put("Packet " + (i + 1), ipPacketObject);
                            break;
						}
						List<Object> tcpList = (List<Object>) downloadMap.get(key1);	
 					   Map<Object, Object> errorNodeTcp = (Map<Object, Object>) tcpList.get(0);						
						if (key1.equals("IP")) {
							
						
	     					for (Object key : errorNodeTcp.keySet()) {

						
						if (key.equals("version")) {
							c1 = 0;
						}
						if (key.equals("Version")) {
							c1 = 20;
							ipv6LayerProtocol = 8;

						}
						if (key.equals("Error") || key.equals("Unsupported")) {
							c1 = 9;
							upperLayerProtocol = 9;

						}
						if (key.equals("TCP Source Port")) {
							c1 = 1;
							upperLayerProtocol = 1;
						}
						if (key.equals("UDP Source Port")) {
							c1 = 4;
							upperLayerProtocol = 4;
						}
						if (key.equals("SCTP Source Port")) {
							c1 = 3;
							upperLayerProtocol = 3;
						}

						if (key.equals("Method")) {
							c1 = 2;
						}
						if (key.equals("Type")) {
							c1 = 5;
							upperLayerProtocol = 5;
						}
						if (key.equals("IP")) {
							c1 = 6;
						}

						if (c1 == 0) {
							ipv4Object.put(key, errorNodeTcp.get(key));
							if (ipv4Object.containsKey("version"))
								ipVersion = (String) ipv4Object.get("version");
							
						}
						if (c1 == 20) {
							
							ipv6Object.put(key, errorNodeTcp.get(key));
							if (ipv6Object.containsKey("Version"))
								ipv6Version = (String) ipv6Object.get("Version");
							if (ipv4Object.containsKey("Internet Protocol"))
								ipv6Nodekey = (String) ipv4Object.get("Internet Protocol");
						}
						if (c1 == 1) {
							tcpObject.put(key, errorNodeTcp.get(key));
							if (ipv4Object.containsKey("Transport Layer Protocol"))
								transportNodekey = (String) ipv4Object.get("Transport Layer Protocol");
							if (ipv6Object.containsKey("Transport Layer Protocol"))
								transportNodekey = (String) ipv6Object.get("Transport Layer Protocol");

						}
						if (c1 == 5) {
							icmpObject.put(key, errorNodeTcp.get(key));
							if (ipv4Object.containsKey("Transport Layer Protocol"))
								transportNodekey = (String) ipv4Object.get("Transport Layer Protocol");
							if (ipv6Object.containsKey("Transport Layer Protocol"))
								transportNodekey = (String) ipv6Object.get("Transport Layer Protocol");
						}

						if (c1 == 3) {
							sctpObject.put(key, errorNodeTcp.get(key));
							if (ipv4Object.containsKey("Transport Layer Protocol"))
								transportNodekey = (String) ipv4Object.get("Transport Layer Protocol");
							if (ipv6Object.containsKey("Transport Layer Protocol"))
								transportNodekey = (String) ipv6Object.get("Transport Layer Protocol");
						}
						if (c1 == 4) {
							udpObject.put(key, errorNodeTcp.get(key));
							if (ipv4Object.containsKey("Transport Layer Protocol"))
								transportNodekey = (String) ipv4Object.get("Transport Layer Protocol");
							if (ipv6Object.containsKey("Transport Layer Protocol"))
								transportNodekey = (String) ipv6Object.get("Transport Layer Protocol");
						}

						if (c1 == 2) {
							httpObject.put(key, errorNodeTcp.get(key));
						}
						if (c1 == 6) {
							List array = (List) errorNodeTcp.get(key);
							for (int l = 0; l < array.size(); l++) {
								Map<String, Object> mapOther = (Map<String, Object>) array.get(l);
								String nodeIPv4 = null;
								String nodeIpv6 = null;
                                String nodeTransportName = null;

                                int next = 0;            						
          							
            					for (String keyOther : mapOther.keySet()) {
            						if(keyOther.equals("version")) {
            							next = 0;
            						
            						}
            						if(keyOther.equals("Version")) {
            							next = 5;
            						
            						}
            						if(keyOther.equalsIgnoreCase("TCP Source Port")) {
            							next = 1;
            						
            						}
            						if(keyOther.equalsIgnoreCase("UDP Source Port")) {
            							next = 2;
            						
            						}
            						if(keyOther.equalsIgnoreCase("SCTP Source Port")) {
            							next = 3;
            						
            						}
            						if(keyOther.equalsIgnoreCase("Type")) {
            							next = 4;
            						
            						}

            						if(next == 0) {
          								otherIPICMPObject.put("IPv4",otherIPv4ICMPObject);

            							otherIPv4ICMPObject.put(keyOther , mapOther.get(keyOther));
            						}
                                     if(next == 5) {
                                    	   if(mapOther.containsKey("version") && mapOther.containsKey("Version")) {
                                    		   otherIPv4ICMPObject.put("IPv6",otherIPv6ICMPObject);
                  							}
                  							else {
                  								otherIPICMPObject.put("IPv6",otherIPv6ICMPObject);
                  							}
            							otherIPv6ICMPObject.put(keyOther , mapOther.get(keyOther));
            						}
                                     if(next == 1) {
                                    	  if(mapOther.containsKey("version")) {
                                    		  otherIPv4ICMPObject.put("TCP", otherTransportObject);
               							}
                                  	   if(mapOther.containsKey("Version")) {
                                 		  otherIPv6ICMPObject.put("TCP", otherTransportObject);
               							}

            							otherTransportObject.put(keyOther , mapOther.get(keyOther));
            						}
                                 if(next == 2) {
                                	 if(mapOther.containsKey("version")) {
                               		  otherIPv4ICMPObject.put("UDP", otherTransportObject);
          							}
                             	   if(mapOther.containsKey("Version")) {
                            		  otherIPv6ICMPObject.put("UDP", otherTransportObject);
          							}

        							otherTransportObject.put(keyOther , mapOther.get(keyOther));
         						}
                                 if(next == 3) {
                                	 if(mapOther.containsKey("version")) {
                               		  otherIPv4ICMPObject.put("SCTP", otherTransportObject);
          							}
                             	   if(mapOther.containsKey("Version")) {
                            		  otherIPv6ICMPObject.put("SCTP", otherTransportObject);
          							}

        							otherTransportObject.put(keyOther , mapOther.get(keyOther));
         						}
                                 if(next == 4) {
                                	 if(mapOther.containsKey("version")) {
                                  		  otherIPv4ICMPObject.put("ICMP", otherTransportObject);
             							}
                                	   if(mapOther.containsKey("Version")) {
                               		  otherIPv6ICMPObject.put("ICMP", otherTransportObject);
             							}
        							otherTransportObject.put(keyOther , mapOther.get(keyOther));
         						}
                                 if(keyOther.equalsIgnoreCase("Application Layer Protocol") || keyOther.equalsIgnoreCase("Security Layer Protocol") || keyOther.equalsIgnoreCase("Transport Layer Security"))
                                 	break;
             					
            					}
							}
						}
						if (c1 == 9) {
							errorObject.put(key, errorNodeTcp.get(key));
							
							if(tcpObject.containsKey("Application Layer Protocol") || udpObject.containsKey("Application Layer Protocol")) {
								if (ipv4Object.containsKey("Transport Layer Protocol"))
									transportNodekey = (String) ipv4Object.get("Transport Layer Protocol");
								if (ipv6Object.containsKey("Transport Layer Protocol"))
									transportNodekey = (String) ipv6Object.get("Transport Layer Protocol");
								
							}else {
							if (ipv4Object.containsKey("Transport Layer Protocol")) {
								if(ipv4Object.containsValue("Unsupported"))
								     transportNodekey = (String) ipv4Object.get("Transport Layer Protocol");
								else {
									if(errorObject.containsKey("Error")) {
										   String errorString = (String) errorObject.get("Error");
											List<String> list = Arrays.asList(errorString.split("-"));
														errorMessage = list.get(0);
		                                   if(errorMessage.contains("Malformed"))
		  								     transportNodekey = (String) ipv4Object.get("Transport Layer Protocol") + "(M)";
		                                   if(errorMessage.contains("Decoded"))
											     transportNodekey = (String) ipv4Object.get("Transport Layer Protocol") + "(N)";

											
										   }
									
							}
							}
							if (ipv6Object.containsKey("Transport Layer Protocol")) {
								if(ipv6Object.containsValue("Unsupported"))
								     transportNodekey = (String) ipv6Object.get("Transport Layer Protocol");
								else {
									if(errorObject.containsKey("Error")) {
										   String errorString = (String) errorObject.get("Error");
											List<String> list = Arrays.asList(errorString.split("-"));
														errorMessage = list.get(0);
		                                   if(errorMessage.contains("Malformed"))
		  								     transportNodekey = (String) ipv6Object.get("Transport Layer Protocol") + "(M)";
		                                   if(errorMessage.contains("Decoded"))
											     transportNodekey = (String) ipv6Object.get("Transport Layer Protocol") + "(N)";

											
										   }
									
							}
							}
							else if (ipv4Object.containsKey("Internet Protocol")) {
								if(ipv4Object.containsValue("Unsupported"))
								     transportNodekey = (String) ipv4Object.get("Internet Protocol");
								else {
									if(errorObject.containsKey("Error")) {
										   String errorString = (String) errorObject.get("Error");
											List<String> list = Arrays.asList(errorString.split("-"));
														errorMessage = list.get(0);
		                                   if(errorMessage.contains("Malformed"))
		  								     transportNodekey = (String) ipv4Object.get("Internet Protocol") + "(M)";
		                                   if(errorMessage.contains("Decoded"))
											     transportNodekey = (String) ipv4Object.get("Internet Protocol") + "(N)";

											
										   }
									
							}
							}
						}
						

					}

				
					if (ipv6LayerProtocol == 8) {
						if(ipv4Object.size() == 0) {
						ipPacketObject.put("IPv6", ipv6Object);
						}else {
							ipv4Object.put("IPv6", ipv6Object);
							ipPacketObject.put("IPv4", ipv4Object);

						}
						   if (upperLayerProtocol == 1) {
							ipv6Object.put("TCP", tcpObject);
							List list = null;
							 if(tcpObject.containsKey("SIP")) {
								   list = (List) tcpObject.get("SIP");
								   if(list.get(0).toString().contains("Error")) {
									   for (Object object : list) {
												Map<String, Object> ftpLineMap = (Map<String, Object>) object;
												String errorString = (String) ftpLineMap.get("Error");
												List<String> list1 = Arrays.asList(errorString.split("-"));
												errorMessage = list1.get(0);
									            
                                   if(errorMessage.contains("Malformed"))
										applicationNodekey = (String) tcpObject.get("Application Layer Protocol") + "(M)";
                                   if(errorMessage.contains("Decoded"))
										applicationNodekey = (String) tcpObject.get("Application Layer Protocol") + "(N)";

									}
								   }else {
								if (tcpObject.containsKey("Application Layer Protocol"))
									applicationNodekey = (String) tcpObject.get("Application Layer Protocol");
								 }
								 }
							 if(tcpObject.containsKey("SMTP")) {
								   list = (List) tcpObject.get("SMTP");
								   if(list.get(0).toString().contains("Error")) {
									   for (Object object : list) {
												Map<String, Object> ftpLineMap = (Map<String, Object>) object;
												String errorString = (String) ftpLineMap.get("Error");
												List<String> list1 = Arrays.asList(errorString.split("-"));
												errorMessage = list1.get(0);
									            
                                       if(errorMessage.contains("Malformed"))
										applicationNodekey = (String) tcpObject.get("Application Layer Protocol") + "(M)";
                                       if(errorMessage.contains("Decoded"))
   										applicationNodekey = (String) tcpObject.get("Application Layer Protocol") + "(N)";

									}
								   }else {
								if (tcpObject.containsKey("Application Layer Protocol"))
									applicationNodekey = (String) tcpObject.get("Application Layer Protocol");
								 }
								 }
							 if(tcpObject.containsKey("Diameter Protocol")) {
								   list = (List) tcpObject.get("Diameter Protocol");
										for (Object object : list) {
												Map<String, Object> diameterLineMap = (Map<String, Object>) object;
											if(diameterLineMap.keySet().size() < 2) {
												if(list.get(0).toString().contains("Error")) {
																Map<String, Object> ftpLineMap = (Map<String, Object>) object;
																String errorString = (String) ftpLineMap.get("Error");
																List<String> list1 = Arrays.asList(errorString.split("-"));
																errorMessage = list1.get(0);
													            
				                                       if(errorMessage.contains("Malformed"))
														applicationNodekey = (String) tcpObject.get("Application Layer Protocol") + "(M)";
				                                       if(errorMessage.contains("Decoded"))
				   										applicationNodekey = (String) tcpObject.get("Application Layer Protocol") + "(N)";

													
												   }
									   }
									 else {
										 errorMessage = null;
								if (tcpObject.containsKey("Application Layer Protocol"))
									applicationNodekey = (String) tcpObject.get("Application Layer Protocol");
								 }
								 }
							 }
							 if(tcpObject.containsKey("FTP")) {
								   list = (List) tcpObject.get("FTP");
								   if(list.get(0).toString().contains("Error")) {
									   for (Object object : list) {
												Map<String, Object> ftpLineMap = (Map<String, Object>) object;
												String errorString = (String) ftpLineMap.get("Error");
												List<String> list1 = Arrays.asList(errorString.split("-"));
												errorMessage = list1.get(0);
									            
                                   if(errorMessage.contains("Malformed"))
										applicationNodekey = (String) tcpObject.get("Application Layer Protocol") + "(M)";
                                   if(errorMessage.contains("Decoded"))
										applicationNodekey = (String) tcpObject.get("Application Layer Protocol") + "(N)";

									}
								   } else {
								if (tcpObject.containsKey("Application Layer Protocol"))
									applicationNodekey = (String) tcpObject.get("Application Layer Protocol");
								 }
								 }
							 if(tcpObject.containsKey("TLS")) {
								   list = (List) tcpObject.get("TLS");
								   if(list.get(0).toString().contains("Error")) {
									   for (Object object : list) {
												Map<String, Object> ftpLineMap = (Map<String, Object>) object;
												String errorString = (String) ftpLineMap.get("Error");
												List<String> list1 = Arrays.asList(errorString.split("-"));
												errorMessage = list1.get(0);
									            
                                   if(errorMessage.contains("Malformed"))
										applicationNodekey = (String) tcpObject.get("Transport Layer Security") + "(M)";
                                   if(errorMessage.contains("Decoded"))
										applicationNodekey = (String) tcpObject.get("Transport Layer Security") + "(N)";

									}
								   } else {
								if (tcpObject.containsKey("Transport Layer Security"))
									applicationNodekey = (String) tcpObject.get("Transport Layer Security");
								 }
								 }
						if (c1 == 2) {
							tcpObject.put("HTTP", httpObject);
							if (tcpObject.containsKey("Application Layer Protocol")) {
								applicationNodekey = (String) tcpObject.get("Application Layer Protocol");
						}
						}

					}
						 if (upperLayerProtocol == 3) {
							 ipv6Object.put("SCTP", sctpObject);
								if (sctpObject.containsKey("Application Layer Protocol"))
									applicationNodekey = (String) sctpObject.get("Application Layer Protocol");
								if (sctpObject.containsKey("Transport Layer Security"))
									applicationNodekey = (String) sctpObject.get("Transport Layer Security");
								if (c1 == 2)
									sctpObject.put("HTTP", httpObject);

							}
						 if (upperLayerProtocol == 5) {
							 ipv6Object.put("ICMP", icmpObject);
								if (c1 == 6) {
									icmpObject.put("Other Protocols", otherIPICMPObject);
								}
								if (c1 == 2)
									icmpObject.put("HTTP", httpObject);

							}
						 
						 if (upperLayerProtocol == 9) {
							 if (errorNodeTcp.containsKey("Application Layer Protocol")) {
								 if(errorNodeTcp.containsValue("TCP")) {
									ipv6Object.put("TCP", tcpObject);
									if(tcpObject.containsValue("Unsupported")) {
										applicationNodekey = (String) tcpObject.get("Application Layer Protocol");
									    tcpObject.putAll(errorObject);

									}else {
										if(errorNodeTcp.containsKey("Error")) {
											   String errorString = (String) errorNodeTcp.get("Error");
												List<String> list = Arrays.asList(errorString.split("-"));
											    errorMessage = list.get(0);
			                                   if(errorMessage.contains("Malformed"))
			   									applicationNodekey = (String) tcpObject.get("Application Layer Protocol") + "(M)";
			                                   if(errorMessage.contains("Decoded"))
			   									applicationNodekey = (String) tcpObject.get("Application Layer Protocol") + "(N)";
								    tcpObject.put("HTTP", errorObject);
									}
									}
								}
								 else if(errorNodeTcp.containsValue("UDP")) {
										ipv6Object.put("UDP", udpObject);
										if(udpObject.containsValue("Unsupported")) {
											applicationNodekey = (String) udpObject.get("Application Layer Protocol");
											udpObject.putAll(errorObject);

										}
								 }
							 }
								else {
									if (errorNodeTcp.containsKey("Transport Layer Protocol")) {
										if(errorNodeTcp.containsValue("Unsupported")) 
										       ipv6Object.putAll(errorObject);
										else
								               ipv6Object.put(errorNodeTcp.get("Transport Layer Protocol"), errorObject);
									}
									else if (errorNodeTcp.containsKey("Internet Protocol")) 
									       ipv6Object.put(errorNodeTcp.get("Internet Protocol"), errorObject);
								
							}
						 }
						 else if (upperLayerProtocol == 4) {
							 ipv6Object.put("UDP", udpObject);
							 List list = null;
							 LinkedHashMap<String, Object> rtpParsedMap = null;
							 if(udpObject.containsValue("Unsupported")) {
									applicationNodekey = (String) tcpObject.get("Application Layer Protocol");
									udpObject.putAll(errorObject);

								}
							 if(udpObject.containsKey("RTP")) {
								    rtpParsedMap = (LinkedHashMap<String, Object>) udpObject.get("RTP");
										if(rtpParsedMap.containsKey("Error")) {
											String errorString = (String) rtpParsedMap.get("Error");
											List<String> list1 = Arrays.asList(errorString.split("-"));		
											errorMessage = list1.get(0);
								            
			                                   if(errorMessage.contains("Malformed"))
													applicationNodekey = (String) udpObject.get("Application Layer Protocol") + "(M)";
			                                   if(errorMessage.contains("Decoded"))
													applicationNodekey = (String) udpObject.get("Application Layer Protocol") + "(N)";		
			                                   }else {
									if (udpObject.containsKey("Application Layer Protocol"))
										applicationNodekey = (String) udpObject.get("Application Layer Protocol");
								 }
								 }
							 if(udpObject.containsKey("SIP")) {
								   list = (List) udpObject.get("SIP");
								   if(list.get(0).toString().contains("Error")) {
									   for (Object object : list) {
												Map<String, Object> ftpLineMap = (Map<String, Object>) object;
												String errorString = (String) ftpLineMap.get("Error");
												List<String> list1 = Arrays.asList(errorString.split("-"));
												errorMessage = list1.get(0);
									            
                                   if(errorMessage.contains("Malformed"))
										applicationNodekey = (String) udpObject.get("Application Layer Protocol") + "(M)";
                                   if(errorMessage.contains("Decoded"))
										applicationNodekey = (String) udpObject.get("Application Layer Protocol") + "(N)";

									}
								   }else {
								if (udpObject.containsKey("Application Layer Protocol"))
									applicationNodekey = (String) udpObject.get("Application Layer Protocol");
								 }
								 }
							 if(udpObject.containsKey("DTLS")) {
								   list = (List) udpObject.get("DTLS");
								   if(list.get(0).toString().contains("Error")) {
									   for (Object object : list) {
												Map<String, Object> ftpLineMap = (Map<String, Object>) object;
												String errorString = (String) ftpLineMap.get("Error");
												List<String> list1 = Arrays.asList(errorString.split("-"));
												errorMessage = list1.get(0);
									            
                                   if(errorMessage.contains("Malformed"))
										applicationNodekey = (String) udpObject.get("Security Layer Protocol") + "(M)";
                                   if(errorMessage.contains("Decoded"))
										applicationNodekey = (String) udpObject.get("Security Layer Protocol") + "(N)";

									}
								   }  else {
							if (udpObject.containsKey("Security Layer Protocol"))
								applicationNodekey = (String) udpObject.get("Security Layer Protocol");
							 }
							 
						}
						 }
					}
					else {
						if(errorNodeTcp.size()< 2) {
							ipPacketObject.put("Error", errorObject);
						}
						else {
						ipPacketObject.put("IPv4", ipv4Object);
						if (upperLayerProtocol == 1) {
							ipv4Object.put("TCP", tcpObject);
							List list = null;
							 if(tcpObject.containsKey("SIP")) {
								   list = (List) tcpObject.get("SIP");
								   if(list.get(0).toString().contains("Error")) {
									   for (Object object : list) {
												Map<String, Object> ftpLineMap = (Map<String, Object>) object;
												String errorString = (String) ftpLineMap.get("Error");
												List<String> list1 = Arrays.asList(errorString.split("-"));
												errorMessage = list1.get(0);
									            
                                     if(errorMessage.contains("Malformed"))
										applicationNodekey = (String) tcpObject.get("Application Layer Protocol") + "(M)";
                                     if(errorMessage.contains("Decoded"))
 										applicationNodekey = (String) tcpObject.get("Application Layer Protocol") + "(N)";

									}
								   }else {
								if (tcpObject.containsKey("Application Layer Protocol"))
									applicationNodekey = (String) tcpObject.get("Application Layer Protocol");
								 }
								 }
							 if(tcpObject.containsKey("SMTP")) {
								   list = (List) tcpObject.get("SMTP");
								   if(list.get(0).toString().contains("Error")) {
									   for (Object object : list) {
												Map<String, Object> ftpLineMap = (Map<String, Object>) object;
												String errorString = (String) ftpLineMap.get("Error");
												List<String> list1 = Arrays.asList(errorString.split("-"));
												errorMessage = list1.get(0);
									            
                                     if(errorMessage.contains("Malformed"))
										applicationNodekey = (String) tcpObject.get("Application Layer Protocol") + "(M)";
                                     if(errorMessage.contains("Decoded"))
 										applicationNodekey = (String) tcpObject.get("Application Layer Protocol") + "(N)";

									}
								   }else {
								if (tcpObject.containsKey("Application Layer Protocol"))
									applicationNodekey = (String) tcpObject.get("Application Layer Protocol");
								 }
								 }
							 if(tcpObject.containsKey("Diameter Protocol")) {
								   list = (List) tcpObject.get("Diameter Protocol");
										for (Object object : list) {
												Map<String, Object> diameterLineMap = (Map<String, Object>) object;
											if(diameterLineMap.keySet().size() < 2) {
												if(list.get(0).toString().contains("Error")) {
																Map<String, Object> ftpLineMap = (Map<String, Object>) object;
																String errorString = (String) ftpLineMap.get("Error");
																List<String> list1 = Arrays.asList(errorString.split("-"));
																errorMessage = list1.get(0);
													            
				                                       if(errorMessage.contains("Malformed"))
														applicationNodekey = (String) tcpObject.get("Application Layer Protocol") + "(M)";
				                                       if(errorMessage.contains("Decoded"))
				   										applicationNodekey = (String) tcpObject.get("Application Layer Protocol") + "(N)";

													
												   }
									   }
									 else {
										 errorMessage = null;
								if (tcpObject.containsKey("Application Layer Protocol"))
									applicationNodekey = (String) tcpObject.get("Application Layer Protocol");
								 }
								 }
							 }
							 if(tcpObject.containsKey("FTP")) {
								   list = (List) tcpObject.get("FTP");
								   if(list.get(0).toString().contains("Error")) {
									   for (Object object : list) {
												Map<String, Object> ftpLineMap = (Map<String, Object>) object;
												String errorString = (String) ftpLineMap.get("Error");
												List<String> list1 = Arrays.asList(errorString.split("-"));
												errorMessage = list1.get(0);
									            
                                     if(errorMessage.contains("Malformed"))
										applicationNodekey = (String) tcpObject.get("Application Layer Protocol") + "(M)";
                                     if(errorMessage.contains("Decoded"))
 										applicationNodekey = (String) tcpObject.get("Application Layer Protocol") + "(N)";

									}
								   } else {
								if (tcpObject.containsKey("Application Layer Protocol"))
									applicationNodekey = (String) tcpObject.get("Application Layer Protocol");
								 }
								 }
							 if(tcpObject.containsKey("TLS")) {
								   list = (List) tcpObject.get("TLS");
								   if(list.get(0).toString().contains("Error")) {
									   for (Object object : list) {
												Map<String, Object> ftpLineMap = (Map<String, Object>) object;
												String errorString = (String) ftpLineMap.get("Error");
												List<String> list1 = Arrays.asList(errorString.split("-"));
												errorMessage = list1.get(0);
									            
                                   if(errorMessage.contains("Malformed"))
										applicationNodekey = (String) tcpObject.get("Transport Layer Security") + "(M)";
                                   if(errorMessage.contains("Decoded"))
										applicationNodekey = (String) tcpObject.get("Transport Layer Security") + "(N)";

									}
								   } else {
								if (tcpObject.containsKey("Transport Layer Security"))
									applicationNodekey = (String) tcpObject.get("Transport Layer Security");
								 }
								 }
						if (c1 == 2) {
							tcpObject.put("HTTP", httpObject);
							if (tcpObject.containsKey("Application Layer Protocol"))
								applicationNodekey = (String) tcpObject.get("Application Layer Protocol");
						}

					}
						 if (upperLayerProtocol == 3) {
							 ipv4Object.put("SCTP", sctpObject);
						if (sctpObject.containsKey("Application Layer Protocol"))
							applicationNodekey = (String) sctpObject.get("Application Layer Protocol");
						if (sctpObject.containsKey("Transport Layer Security"))
							applicationNodekey = (String) sctpObject.get("Transport Layer Security");
						if (c1 == 2)
							sctpObject.put("HTTP", httpObject);

					}
					 if (upperLayerProtocol == 5) {
						 ipv4Object.put("ICMP", icmpObject);
						if (c1 == 6) {
							icmpObject.put("Other Protocols", otherIPICMPObject);
						}
						if (c1 == 2)
							icmpObject.put("HTTP", httpObject);

					}
					 if (upperLayerProtocol == 9) {
							if (errorNodeTcp.containsKey("Application Layer Protocol")) {
								 if(errorNodeTcp.containsValue("TCP")) {
									 ipv4Object.put("TCP", tcpObject);
										if(tcpObject.containsValue("Unsupported")) {
											applicationNodekey = (String) tcpObject.get("Application Layer Protocol");
										    tcpObject.putAll(errorObject);

										}else {
											if(errorNodeTcp.containsKey("Error")) {
												   String errorString = (String) errorNodeTcp.get("Error");
													List<String> list = Arrays.asList(errorString.split("-"));
												    errorMessage = list.get(0);
				                                   if(errorMessage.contains("Malformed"))
				   									applicationNodekey = (String) tcpObject.get("Application Layer Protocol") + "(M)";
				                                   if(errorMessage.contains("Decoded"))
				   									applicationNodekey = (String) tcpObject.get("Application Layer Protocol") + "(N)";
									                tcpObject.put("HTTP", errorObject);
										}
										}
									}
									 else if(errorNodeTcp.containsValue("UDP")) {
										 ipv4Object.put("UDP", udpObject);
											if(udpObject.containsValue("Unsupported")) {
												applicationNodekey = (String) udpObject.get("Application Layer Protocol");
												udpObject.putAll(errorObject);

											}
									 }
							}
							else {
								if (errorNodeTcp.containsKey("Transport Layer Protocol")) {
									if(errorNodeTcp.containsValue("Unsupported")) 
									       ipv4Object.putAll(errorObject);
									else
							               ipv4Object.put(errorNodeTcp.get("Transport Layer Protocol"), errorObject);
								}else if (errorNodeTcp.containsKey("Internet Protocol")) {
								       ipv4Object.put(errorNodeTcp.get("Internet Protocol"), errorObject);
						        }
							}
						 
					 }
					else if (upperLayerProtocol == 4) {
								 ipv4Object.put("UDP", udpObject);
								 List list = null;
								 LinkedHashMap<String, Object> rtpParsedMap = null;
								 if(udpObject.containsKey("RTP")) {
									    rtpParsedMap = (LinkedHashMap<String, Object>) udpObject.get("RTP");
											if(rtpParsedMap.containsKey("Error")) {
												String errorString = (String) rtpParsedMap.get("Error");
												List<String> list1 = Arrays.asList(errorString.split("-"));		
												errorMessage = list1.get(0);
									            
				                                   if(errorMessage.contains("Malformed"))
														applicationNodekey = (String) udpObject.get("Application Layer Protocol") + "(M)";
				                                   if(errorMessage.contains("Decoded"))
														applicationNodekey = (String) udpObject.get("Application Layer Protocol") + "(N)";		
				                                   }else {
										if (udpObject.containsKey("Application Layer Protocol"))
											applicationNodekey = (String) udpObject.get("Application Layer Protocol");
									 }
									 }
								 if(udpObject.containsKey("SIP")) {
									   list = (List) udpObject.get("SIP");
									   if(list.get(0).toString().contains("Error")) {
										   for (Object object : list) {
													Map<String, Object> ftpLineMap = (Map<String, Object>) object;
													String errorString = (String) ftpLineMap.get("Error");
													List<String> list1 = Arrays.asList(errorString.split("-"));
													errorMessage = list1.get(0);
										            
	                                     if(errorMessage.contains("Malformed"))
											applicationNodekey = (String) udpObject.get("Application Layer Protocol") + "(M)";
	                                     if(errorMessage.contains("Decoded"))
	 										applicationNodekey = (String) udpObject.get("Application Layer Protocol") + "(N)";

										}
									   }else {
									if (udpObject.containsKey("Application Layer Protocol"))
										applicationNodekey = (String) udpObject.get("Application Layer Protocol");
									 }
									 }
								 if(udpObject.containsKey("DTLS")) {
									   list = (List) udpObject.get("DTLS");
									   if(list.get(0).toString().contains("Error")) {
										   for (Object object : list) {
													Map<String, Object> ftpLineMap = (Map<String, Object>) object;
													String errorString = (String) ftpLineMap.get("Error");
													List<String> list1 = Arrays.asList(errorString.split("-"));
													errorMessage = list1.get(0);
										            
	                                   if(errorMessage.contains("Malformed"))
											applicationNodekey = (String) udpObject.get("Security Layer Protocol") + "(M)";
	                                   if(errorMessage.contains("Decoded"))
											applicationNodekey = (String) udpObject.get("Security Layer Protocol") + "(N)";

										}
									   }  else {
								if (udpObject.containsKey("Security Layer Protocol"))
									applicationNodekey = (String) udpObject.get("Security Layer Protocol");
								 }
								 
							}
							 }
							}
				}
	     					}
						}
							}
					

					if(errorObject.containsKey("Error") || errorObject.containsKey("Unsupported")) {
						List<String> list = null;
					  if(errorObject.get("Error") != null) {
					     String errorString = (String) errorObject.get("Error");

					       list = Arrays.asList(errorString.split("-"));
					  }
					  if(errorObject.get("Unsupported") != null) {
						     LinkedHashMap unsupported = (LinkedHashMap) errorObject.get("Unsupported");
						     String errorString = (String) unsupported.get("Error");
						       list = Arrays.asList("Partially Unsupported Protocol");
						  }
					rootObject.put("Packet " + (i + 1) +" : " + list.get(0), ipPacketObject);
					String gsonToJson = gson.toJson(rootObject, LinkedHashMap.class);
					jsonOutputList.add(gsonToJson);
					}
					else {
						if(errorMessage == null) {
						rootObject.put("Packet " + (i + 1), ipPacketObject);
						}else {
							rootObject.put("Packet " + (i + 1) + " : " + errorMessage, ipPacketObject);
						}
					String gsonToJson = gson.toJson(rootObject, LinkedHashMap.class);
					jsonOutputList.add(gsonToJson);
					}
				}
				if (jsonOutputList.size() > 1) {
					downloadFileName = "IP" + "-HexDecode";
				} else {
					if (errorNodeKey != null && errorNodeKey.contains("Error")) {
						downloadFileName = "Error" + "-HexDecode";
					}
					else {
						if(ipVersion == null) {
							if(independentProtocol != null)
								downloadFileName = independentProtocol;	
							else if (transportNodekey == null && ipv6Version != null) {
							downloadFileName = "IPv"+ ipv6Version;
							}else {
								if(applicationNodekey ==null) {
								downloadFileName = "IPv" + ipv6Version + "-" +transportNodekey;
							     }else
								downloadFileName = "IPv" + ipv6Version + "-" + transportNodekey + "-" + applicationNodekey;
							}
						}else {
						if (transportNodekey == null) {
							 if(independentProtocol != null)
									downloadFileName = independentProtocol;	
							if(ipv6Nodekey == null && ipVersion != null) {
							downloadFileName = "IPv" + ipVersion;
							}else {
								downloadFileName = "IPv" + ipVersion + "-" +ipv6Nodekey;
							}
							
						}
						if (transportNodekey != null) {

							if (applicationNodekey == null) {
								if(ipv6Nodekey == null) {
								downloadFileName = "IPv" + ipVersion + "-" + transportNodekey;
								}else {
									downloadFileName = "IPv" + ipVersion + "-" + ipv6Nodekey + "-" +transportNodekey;
								}
							}
							if (applicationNodekey != null) {
								if(ipv6Nodekey == null) {
								downloadFileName = "IPv" + ipVersion + "-" + transportNodekey + "-" + applicationNodekey;
								}else {
									downloadFileName = "IPv" + ipVersion + "-" + ipv6Nodekey + "-" + transportNodekey + "-" + applicationNodekey;

								}
							}
						}
						}
					}
				}

				String home = System.getProperty("user.home");
				Date date = new Date();
				Format formatter = new SimpleDateFormat("YYYY-MM-dd_hh-mm-ss");
				@SuppressWarnings("resource")
				FileWriter filew = new FileWriter(
						home + "/Downloads/" + downloadFileName + "(" + formatter.format(date) + ")" + ".json");
				filew.write(jsonOutputList.toString());
				filew.flush();
				logger.info("Output result is downloaded successfully!");


			} catch (Exception e1) {
				logger.error("Output result is not downloaded");

				 //e1.printStackTrace();
			}

		});
		download.setPreferredSize(new Dimension(180, 30));
		download.setForeground(Color.BLACK);
		download.setBackground(Color.WHITE);
        Border borderDownload = new LineBorder(Color.BLACK, 2, true);
        download.setBorder(borderDownload);

		download.setOpaque(true);
		download.setVisible(true);

		c.add(mainPanel, BorderLayout.CENTER);
		c.add(westPanel, BorderLayout.WEST);
		c.add(eastPanel, BorderLayout.EAST);


		setVisible(true);

		footerPanel = new JPanel();

		footerPanel.setBorder(new BevelBorder(BevelBorder.LOWERED));
		footerPanel.setSize(this.getWidth(), 50);
		footerLabel = new JLabel("Copyright © 2020. All Rights Reserved.", SwingConstants.CENTER);
		footerLabel.setLocation(0, screenHeight - 20);
		footerLabel.setHorizontalAlignment(SwingConstants.CENTER);
		footerLabel.setForeground(Color.BLACK);
		footerLabel.setFont(new Font("Arial", Font.PLAIN, 12));
		footerLabel.setPreferredSize(new Dimension(screenWidth, 18));
		footerLabel.setVisible(true);
		footerPanel.setBackground(Color.WHITE);
        Border borderFooter = new LineBorder(Color.BLACK, 2, true);
        footerPanel.setBorder(borderFooter);

		footerPanel.add(footerLabel);
		c.add(footerPanel, BorderLayout.SOUTH);
        
	}

	public ImageIcon createImage(String path) {
		return new ImageIcon(getClass().getClassLoader().getResource(path));
	}

// used to get the action performed on decode , upload and clear button 
	public void actionPerformed(ActionEvent e) {
       
		if (e.getSource() == cancel) {
			ToggleSwitch toggleSwitchObj = new ToggleSwitch();

            if(toggleList.contains("SIP")) {
            	tsSIP.isActivated();
            tsSIP.setActivated(true);
            }if(!toggleList.contains("SIP")) {
                tsSIP.setActivated(false);
                }
            
            if(toggleList.contains("HTTP")) {
            	tsHTTP.isActivated();
            tsHTTP.setActivated(true);
            }if(!toggleList.contains("HTTP")) {
                tsHTTP.setActivated(false);
                }
            
            if(toggleList.contains("RTP")) {
            	tsRTP.isActivated();
            tsRTP.setActivated(true);
            }if(!toggleList.contains("RTP")) {
                tsRTP.setActivated(false);
                }
            
            if(toggleList.contains("IPv4")) {
            	tsIPv4.isActivated();
            tsIPv4.setActivated(true);
            }if(!toggleList.contains("IPv4")) {
                tsIPv4.setActivated(false);
                }
            
            if(toggleList.contains("IPv6")) {
            	tsIPv6.isActivated();
            tsIPv6.setActivated(true);
            }if(!toggleList.contains("IPv6")) {
                tsIPv6.setActivated(false);
                }
            
            if(toggleList.contains("FTP")) {
            	tsFTP.isActivated();
            tsFTP.setActivated(true);
            }if(!toggleList.contains("FTP")) {
                tsFTP.setActivated(false);
                }
            
            if(toggleList.contains("Diameter")) {
            	tsDiameter.isActivated();
            tsDiameter.setActivated(true);
            }if(!toggleList.contains("Diameter")) {
            tsDiameter.setActivated(false);
            }
            

            if(toggleList.contains("All")) {
            	tsAll.isActivated();
            tsAll.setActivated(true);
            }if(!toggleList.contains("All")) {
                tsAll.setActivated(false);
                }
           repaint();
           revalidate();
            searchPanel.setVisible(false);


		        repaint();
		        revalidate();
			

		}
		if (e.getSource() == submit) {
	        
	        
	        if(tsSIP.isActivated()) {
	        	if(!toggleList.contains("SIP"))
		        toggleList.add("SIP");
	        }if(!tsSIP.isActivated()) {
		        toggleList.remove("SIP");
	        }
	        
	        if(tsHTTP.isActivated()) {
	        	if(!toggleList.contains("HTTP"))
		        toggleList.add("HTTP");
	        }if(!tsHTTP.isActivated()) {
		        toggleList.remove("HTTP");
	        }
	        
	        if(tsRTP.isActivated()) {
	        	if(!toggleList.contains("RTP"))
		        toggleList.add("RTP");
	        }if(!tsRTP.isActivated()) {
		        toggleList.remove("RTP");
	        }
	        
	        if(tsIPv4.isActivated()) {
	        	if(!toggleList.contains("IPv4"))
	            toggleList.add("IPv4");
	        }if(!tsIPv4.isActivated()) {
		        toggleList.remove("IPv4");
	        }
	        
	        if(tsIPv6.isActivated()) {
	        	if(!toggleList.contains("IPv6"))
		        toggleList.add("IPv6");
	        }if(!tsIPv6.isActivated()) {
		        toggleList.remove("IPv6");
	        }
	        
	        if(tsFTP.isActivated()) {
	        	if(!toggleList.contains("FTP"))
		        toggleList.add("FTP");
	        }if(!tsFTP.isActivated()) {
		        toggleList.remove("FTP");
	        }
	        
	        if(tsDiameter.isActivated()) {
	        	if(!toggleList.contains("Diameter"))
		        toggleList.add("Diameter");
	        }if(!tsDiameter.isActivated()) {
		        toggleList.remove("Diameter");
	        }
	        
	        if(tsAll.isActivated()) {
	        	if(!toggleList.contains("All"))
		        toggleList.add("All");
	        }if(!tsAll.isActivated()) {
		        toggleList.remove("All");
	        }
	        

            searchPanel.setVisible(false);
            
		}
		if (e.getSource() == decode || e.getSource() == upload) {
			try {

				if(map.size()!=0) {
				jtreeScrollPane.setVisible(true);
	            download.setVisible(true);
				DefaultMutableTreeNode rootNode = null;
				DefaultMutableTreeNode ip = null;
				DefaultMutableTreeNode tcp = null;
				DefaultMutableTreeNode udp = null;
				DefaultMutableTreeNode sctp = null;
				DefaultMutableTreeNode http123 = null;
				DefaultMutableTreeNode sip = null;
				DefaultMutableTreeNode smtp = null;
				DefaultMutableTreeNode tls = null;
				DefaultMutableTreeNode dtls = null;
				DefaultMutableTreeNode option = null;
				DefaultMutableTreeNode diameter = null;
				DefaultMutableTreeNode icmp = null;
				DefaultMutableTreeNode ftp = null;
				DefaultMutableTreeNode icmp_ip = null;
				DefaultMutableTreeNode rtp = null;
				DefaultMutableTreeNode ipv4 = null;
				DefaultMutableTreeNode ipv6 = null;
				DefaultMutableTreeNode errorNode = null;


				rootNode = new DefaultMutableTreeNode("Decoded Packet");
				for (int i = 0; i < map.size(); i++) {
					ip = new DefaultMutableTreeNode("Packet " + (i + 1));
					ipv4 = new DefaultMutableTreeNode("Internet Protocol(IPv4)");
					ipv6 = new DefaultMutableTreeNode("Internet Protocol(IPv6)");

					tcp = new DefaultMutableTreeNode("Transmission Control Protocol(TCP)");
					udp = new DefaultMutableTreeNode("User Datagram Protocol(UDP)");
					sctp = new DefaultMutableTreeNode("Stream Control Transmission Protocol(SCTP)");
					option = new DefaultMutableTreeNode("Optional Parameters");
					http123 = new DefaultMutableTreeNode("HyperText Transfer Protocol(HTTP)");
					sip = new DefaultMutableTreeNode("Session Initiation Protocol(SIP)");
					smtp = new DefaultMutableTreeNode("Simple Mail Transfer Protocol(SMTP)");
					tls = new DefaultMutableTreeNode("Transport Layer Security(TLS)");
					dtls = new DefaultMutableTreeNode("Datagram Transport Layer Security(DTLS)");
					diameter = new DefaultMutableTreeNode("Diameter Protocol");
					icmp = new DefaultMutableTreeNode("Internet Control Message Protocol(ICMP)");
					ftp = new DefaultMutableTreeNode("File Transfer Protocol(FTP)");
					icmp_ip = new DefaultMutableTreeNode("Internet Protocol(IP)");
					rtp = new DefaultMutableTreeNode("Real-time Transport Protocol(RTP)");

					int nextProtocol = 0;
					Object applicationKey = null;
					Map<String, Object> map2 = (Map<String, Object>) map.get(i);
					
			        if(map2.containsKey("Error")) {
					String errorString = (String) map2.get("Error");
					List<String> list = Arrays.asList(errorString.split("-"));
					ip = new DefaultMutableTreeNode("Packet " + (i + 1) +" : "+list.get(0));
					}
					if(map2.get("SIP") != null) {
						List list = (List) map2.get("SIP");
						for (Object object : list) {

						if(object.toString().contains("Error")) {
							Map<String, Object> ftpLineMap = (Map<String, Object>) object;
							String errorString = (String) ftpLineMap.get("Error");
							List<String> list1 = Arrays.asList(errorString.split("-"));
                           ip = new DefaultMutableTreeNode("Packet " + (i + 1) +" : "+list1.get(0));
				}
						}
					}
					if(map2.get("Unsupported") != null) {
						ip = new DefaultMutableTreeNode("Packet " + (i + 1) +" : Unsupported Protocol");
					}
					if(map2.get("Diameter Protocol") != null) {
						List array = (List) map2.get("Diameter Protocol");
						for (Object object : array) {
							if(object.toString().contains("Error")) {
								Map<String, Object> diameterLineMap = (Map<String, Object>) object;
								if(diameterLineMap.keySet().size() < 2) {
								Map<String, Object> ftpLineMap = (Map<String, Object>) object;
								String errorString = (String) ftpLineMap.get("Error");
								List<String> list1 = Arrays.asList(errorString.split("-"));
	                           ip = new DefaultMutableTreeNode("Packet " + (i + 1) +" : "+list1.get(0));
					}
							}
					}
					}
					if(map2.get("TLS") != null) {
						List list = (List) map2.get("TLS");
						for (Object object : list) {
							if(object.toString().contains("Error")) {
								Map<String, Object> ftpLineMap = (Map<String, Object>) object;
								String errorString = (String) ftpLineMap.get("Error");
								List<String> list1 = Arrays.asList(errorString.split("-"));
	                           ip = new DefaultMutableTreeNode("Packet " + (i + 1) +" : "+list1.get(0));
					}
					}
					}
					if(map2.get("FTP") != null) {
						List list = (List) map2.get("FTP");
						for (Object object : list) {
							if(object.toString().contains("Error")) {
							Map<String, Object> ftpLineMap = (Map<String, Object>) object;
							String errorString = (String) ftpLineMap.get("Error");
							List<String> list1 = Arrays.asList(errorString.split("-"));
                           ip = new DefaultMutableTreeNode("Packet " + (i + 1) +" : "+list1.get(0));
					}
					}
					}
					if(map2.get("DTLS") != null) {
						List list = (List) map2.get("DTLS");
						for (Object object : list) {
							if(object.toString().contains("Error")) {
								Map<String, Object> ftpLineMap = (Map<String, Object>) object;
								String errorString = (String) ftpLineMap.get("Error");
								List<String> list1 = Arrays.asList(errorString.split("-"));
	                           ip = new DefaultMutableTreeNode("Packet " + (i + 1) +" : "+list1.get(0));
					}
					}
					}
					if(map2.get("SMTP") != null) {
						List list = (List) map2.get("SMTP");
						for (Object object : list) {
							if(object.toString().contains("Error")) {
								Map<String, Object> ftpLineMap = (Map<String, Object>) object;
								String errorString = (String) ftpLineMap.get("Error");
								List<String> list1 = Arrays.asList(errorString.split("-"));
	                           ip = new DefaultMutableTreeNode("Packet " + (i + 1) +" : "+list1.get(0));
					}
					}
					}

					if(map2.get("IP") != null) {
						List<Object> tcpList = (List<Object>) map2.get("IP");	
	 					   Map<String, Object> errorNodeTcp = (Map<String, Object>) tcpList.get(0);	
	     					for (String key : errorNodeTcp.keySet()) {
	     						if(errorNodeTcp.containsKey("Error")) {
	    							String errorString = (String) errorNodeTcp.get("Error");
	    							List<String> list1 = Arrays.asList(errorString.split("-"));
	                               ip = new DefaultMutableTreeNode("Packet " + (i + 1) +" : "+list1.get(0));
	     						}
	     						if(errorNodeTcp.get("Unsupported") != null) {
	     							ip = new DefaultMutableTreeNode("Packet " + (i + 1) +" : Partially Unsupported Protocol");
	     						}
	     						if(errorNodeTcp.get("IP") != null) {
	     							List list = (List) errorNodeTcp.get("IP");
	     							for (Object object : list) {
	     							if(object.toString().contains("Error"))
	     							ip = new DefaultMutableTreeNode("IP Packet " + (i + 1) +" : ICMP Other Protocols Malformed Packet");
	     						}
	     						}
	     						if(errorNodeTcp.get("SIP") != null) {
	     							List list = (List) errorNodeTcp.get("SIP");
	     							for (Object object : list) {

	     							if(object.toString().contains("Error")) {
	     								Map<String, Object> ftpLineMap = (Map<String, Object>) object;
	     								String errorString = (String) ftpLineMap.get("Error");
	     								List<String> list1 = Arrays.asList(errorString.split("-"));
	     	                           ip = new DefaultMutableTreeNode("Packet " + (i + 1) +" : "+list1.get(0));
	     					}
	     							}
	     						}
	     						if(errorNodeTcp.get("Diameter Protocol") != null) {
	     							List array = (List) errorNodeTcp.get("Diameter Protocol");
	     							for (Object object : array) {
	     								if(object.toString().contains("Error")) {
	     									Map<String, Object> diameterLineMap = (Map<String, Object>) object;
	     									if(diameterLineMap.keySet().size() < 2) {
	     									Map<String, Object> ftpLineMap = (Map<String, Object>) object;
	     									String errorString = (String) ftpLineMap.get("Error");
	     									List<String> list1 = Arrays.asList(errorString.split("-"));
	     		                           ip = new DefaultMutableTreeNode("Packet " + (i + 1) +" : "+list1.get(0));
	     						}
	     								}
	     						}
	     						}
	     						if(errorNodeTcp.get("TLS") != null) {
	     							List list = (List) errorNodeTcp.get("TLS");
	     							for (Object object : list) {
	     								if(object.toString().contains("Error")) {
	     									Map<String, Object> ftpLineMap = (Map<String, Object>) object;
	     									String errorString = (String) ftpLineMap.get("Error");
	     									List<String> list1 = Arrays.asList(errorString.split("-"));
	     		                           ip = new DefaultMutableTreeNode("Packet " + (i + 1) +" : "+list1.get(0));
	     						}
	     						}
	     						}
	     						if(errorNodeTcp.get("FTP") != null) {
	     							List list = (List) errorNodeTcp.get("FTP");
	     							for (Object object : list) {
	     								if(object.toString().contains("Error")) {
	     								Map<String, Object> ftpLineMap = (Map<String, Object>) object;
	     								String errorString = (String) ftpLineMap.get("Error");
	     								List<String> list1 = Arrays.asList(errorString.split("-"));
	     	                           ip = new DefaultMutableTreeNode("Packet " + (i + 1) +" : "+list1.get(0));
	     						}
	     						}
	     						}
	     						if(errorNodeTcp.get("DTLS") != null) {
	     							List list = (List) errorNodeTcp.get("DTLS");
	     							for (Object object : list) {
	     								if(object.toString().contains("Error")) {
	     									Map<String, Object> ftpLineMap = (Map<String, Object>) object;
	     									String errorString = (String) ftpLineMap.get("Error");
	     									List<String> list1 = Arrays.asList(errorString.split("-"));
	     		                           ip = new DefaultMutableTreeNode("Packet " + (i + 1) +" : "+list1.get(0));
	     						}
	     						}
	     						}
	     						if(errorNodeTcp.get("SMTP") != null) {
	     							List list = (List) errorNodeTcp.get("SMTP");
	     							for (Object object : list) {
	     								if(object.toString().contains("Error")) {
	     									Map<String, Object> ftpLineMap = (Map<String, Object>) object;
	     									String errorString = (String) ftpLineMap.get("Error");
	     									List<String> list1 = Arrays.asList(errorString.split("-"));
	     		                           ip = new DefaultMutableTreeNode("Packet " + (i + 1) +" : "+list1.get(0));
	     						}
	     						}
	     						}
	     						if(errorNodeTcp.get("RTP") != null) {
	     							LinkedHashMap<String, Object> rtpParsedMap = (LinkedHashMap<String, Object>) errorNodeTcp
	     									.get("RTP");
	     								if(rtpParsedMap.containsKey("Error")) {
	     								String errorString = (String) rtpParsedMap.get("Error");
	     								List<String> list1 = Arrays.asList(errorString.split("-"));
	     	                           ip = new DefaultMutableTreeNode("Packet " + (i + 1) +" : "+list1.get(0));
	     						}
	     						
	     						}
	     					}
					}
					if(map2.get("RTP") != null) {
						LinkedHashMap<String, Object> rtpParsedMap = (LinkedHashMap<String, Object>) map2
								.get("RTP");
							if(rtpParsedMap.containsKey("Error")) {
							String errorString = (String) rtpParsedMap.get("Error");
							List<String> list1 = Arrays.asList(errorString.split("-"));
                           ip = new DefaultMutableTreeNode("Packet " + (i + 1) +" : "+list1.get(0));
					}
					
					}
					
					for (String key1 : map2.keySet()) {
						if (key1.equals("SIP")) {
							rootNode.add(ip);
							ip.add(ProtocolValidation.getSIPObject(map2.get(key1)));
							break;
						}

						if (key1.equals("FTP")) {
							rootNode.add(ip);
							ip.add(ProtocolValidation.getFTPObject(map2.get(key1)));
							break;
						}
						
						if (key1.equals("Diameter")) {
							rootNode.add(ip);
							ip.add(ProtocolValidation.getDiameterObject(map2.get(key1)));
							break;
						}
						
						if (key1.equals("RTP")) {
							rootNode.add(ip);
							ip.add(ProtocolValidation.getRTPObject(map2.get(key1)));
							break;
						}
						
						if (key1.equals("HTTP")) {
							rootNode.add(ip);
							ip.add(ProtocolValidation.getHTTPObject(map2.get(key1)));
							break;
						}
						
						if (key1.equals("Error") || key1.equals("Unsupported")) {
							rootNode.add(ip);
							ip.add(new DefaultMutableTreeNode("Error" + ":" + map2.get("Error")));
                            break;
						}
						List<Object> tcpList = (List<Object>) map2.get(key1);	
 					   Map<Object, Object> errorNodeTcp = (Map<Object, Object>) tcpList.get(0);						
						if (key1.equals("IP")) {

						
     					for (Object key : errorNodeTcp.keySet()) {

	                    if (key.equals("version")) {
							applicationKey = key;

							c1 = 0;
							nextProtocol = 1;
						}
						if (key.equals("Version")) {
							c1 = 20;
							applicationKey = key;

							nextProtocol = 2;
						}
						if (key.equals("Error") || key.equals("Unsupported")) {
						     c1 = 30;
							applicationKey = key;

					     }
						

						if (key.equals("TCP Source Port")) {
							c1 = 1;
							applicationKey = key;

                            
						}
						if (key.equals("UDP Source Port")) {
							c1 = 4;
							applicationKey = key;

						}
						if (key.equals("SCTP Source Port")) {
							c1 = 3;
							applicationKey = key;

						}
						if (key.equals("Method")) {
							c1 = 2;
							applicationKey = key;

						}
						if (key.equals("SIP")) {
							c1 = 5;
							applicationKey = key;

						}
						if (key.equals("Optional Parameters")) {
							c1 = 6;
							applicationKey = key;

						}
						if (key.equals("SMTP")) {
							c1 = 7;
							applicationKey = key;

						}
						if (key.equals("TLS")) {
							c1 = 8;
							applicationKey = key;

						}
						if (key.equals("DTLS")) {
							c1 = 9;
							applicationKey = key;

						}
						if (key.equals("Diameter Protocol")) {
							c1 = 10;
							applicationKey = key;
						}
						if (key.equals("Type")) {
							c1 = 11;
							applicationKey = key;

						}
						if (key.equals("IP")) {
							c1 = 13;
							applicationKey = key;

						}
						if (key.equals("FTP")) {
							c1 = 12;
							applicationKey = key;
						}
						if (key.equals("RTP")) {
							c1 = 14;
							applicationKey = key;
						}
     					
     					
						rootNode.add(ip);

						// Add fields to IPv4 Packet node
						if (c1 == 0) {

							ip.add(ipv4);
							ipv4.add(new DefaultMutableTreeNode(key + ":" + errorNodeTcp.get(key)));
						
						}
						// Add fields to IPv6 Packet node
						if (c1 == 20) {

							if(errorNodeTcp.containsKey("version") && errorNodeTcp.containsKey("Version")) {
								ipv4.add(ipv6);
							}
							else {
								ip.add(ipv6);
							}
							ipv6.add(new DefaultMutableTreeNode(key + ":" + errorNodeTcp.get(key)));

							
						}
						if (c1 == 30) {
							if(errorNodeTcp.containsKey("Application Layer Protocol")) {
								String errorName = (String) errorNodeTcp.get("Application Layer Protocol");
								if(errorName.equalsIgnoreCase("Unsupported")) {
									errorNode = new DefaultMutableTreeNode("Unsupported Protocol");
									LinkedHashMap unsupported = (LinkedHashMap) errorNodeTcp.get("Unsupported");
								    errorNode.add(new DefaultMutableTreeNode("Error" + ":" + unsupported.get("Error")));
								}else {
								if(errorName.equalsIgnoreCase("HTTP")) {
								    errorNode = new DefaultMutableTreeNode("HyperText Transfer Protocol(HTTP)");
                                 
							    errorNode.add(new DefaultMutableTreeNode(applicationKey + ":" + errorNodeTcp.get(applicationKey)));
								}
								}
                                if(errorNodeTcp.containsValue("HTTP"))
                                	tcp.add(errorNode);
                                else if(errorNodeTcp.containsValue("Unsupported")) {
    								String errorTransportName = (String) errorNodeTcp.get("Transport Layer Protocol");
    								
    								if(errorTransportName.equalsIgnoreCase("TCP")) 
                                    	tcp.add(errorNode);
    								if(errorTransportName.equalsIgnoreCase("SCTP")) 
                                    	sctp.add(errorNode);
    								if(errorTransportName.equalsIgnoreCase("UDP")) 
                                    	udp.add(errorNode);
    								if(errorTransportName.equalsIgnoreCase("ICMP")) 
                                    	icmp.add(errorNode);

                                }
                                    break;
							
							}
							else if(errorNodeTcp.containsKey("Transport Layer Protocol")) {
							String errorNodeName = (String) errorNodeTcp.get("Transport Layer Protocol");
							if(errorNodeName.equalsIgnoreCase("Unsupported")) {
								errorNode = new DefaultMutableTreeNode("Unsupported Protocol");
								LinkedHashMap unsupported = (LinkedHashMap) errorNodeTcp.get("Unsupported");
							    errorNode.add(new DefaultMutableTreeNode("Error" + ":" + unsupported.get("Error")));
							}else {
								if(errorNodeName.equalsIgnoreCase("TCP"))
							      errorNode = new DefaultMutableTreeNode("Transmission Control Protocol(TCP)");
							    if(errorNodeName.equalsIgnoreCase("UDP"))
								  errorNode = new DefaultMutableTreeNode("User Datagram Protocol(UDP)");
							    if(errorNodeName.equalsIgnoreCase("SCTP"))
								  errorNode = new DefaultMutableTreeNode("Stream Control Transmission Protocol(SCTP)");
							    if(errorNodeName.equalsIgnoreCase("ICMP"))
								  errorNode = new DefaultMutableTreeNode("Internet Control Message Protocol(ICMP)");
							    
							    errorNode.add(new DefaultMutableTreeNode(applicationKey + ":" + errorNodeTcp.get(applicationKey)));
							}
							if(errorNodeTcp.containsKey("Version")) {
								ipv6.add(errorNode);
							}
							
							else {
								ipv4.add(errorNode);
							}
							break;
						} 
							else if(errorNodeTcp.containsKey("Internet Protocol")) {
								errorNode = new DefaultMutableTreeNode("Internet Protocol(IPv6)");
								errorNode.add(new DefaultMutableTreeNode(applicationKey + ":" + errorNodeTcp.get(applicationKey)));
								ipv4.add(errorNode);								
								
							} 
							else  if(errorNodeTcp.size()<2){
								ip.add(new DefaultMutableTreeNode(applicationKey + ":" + errorNodeTcp.get(applicationKey)));
							}
						}
						// Add fields to TCP Packet node
						if (c1 == 1) {

							if(nextProtocol == 1) {
								ipv4.add(tcp);
							}
							if(nextProtocol == 2) {
								ipv6.add(tcp);
							}
							tcp.add(new DefaultMutableTreeNode(key + ":" + errorNodeTcp.get(key)));
							

						}

						// Add fields to SMTP Packet node
						if (c1 == 7) {

							tcp.add(smtp);
							List array = (List) errorNodeTcp.get(key);
							for (Object object : array) {
								Map<String, Object> smtpLineMap = (Map<String, Object>) object;
								if (smtpLineMap.keySet().size() > 1) {
								DefaultMutableTreeNode commandNode = new DefaultMutableTreeNode(
										smtpLineMap.get("Line"));
								smtpLineMap.remove("Line");
									for (String smtpLineKey : smtpLineMap.keySet()) {
										DefaultMutableTreeNode child = new DefaultMutableTreeNode(
												smtpLineKey + ":" + smtpLineMap.get(smtpLineKey));
										commandNode.add(child);
									}
								
								smtp.add(commandNode);
								}else {
									smtp.add(new DefaultMutableTreeNode("Error" + ":" + smtpLineMap.get("Error")));

								}
							
							}
						}

						// Add fields to TLS Packet node
						if (c1 == 8) {

							if (applicationKey == "TLS") {
							tcp.add(tls);
							List array = (List) errorNodeTcp.get(key);
							for (Object object : array) {
								Map<String, Object> tlsLineMap = (Map<String, Object>) object;
								DefaultMutableTreeNode tlsRecordNode = null;
								if (tlsLineMap.keySet().size() > 1) {
								String handshakeType = null;
								
								if(tlsLineMap.get("Handshake Type")!=null) {
								handshakeType = "TLS Record Layer : "+ tlsLineMap.get("Handshake Type");
								}else {
									handshakeType = "TLS Record Layer : "+ tlsLineMap.get("Content Type");
								}
								if(handshakeType != null) 
								 tlsRecordNode = new DefaultMutableTreeNode(handshakeType);

									for (String tlsLineKey : tlsLineMap.keySet()) {
										DefaultMutableTreeNode recordChild = new DefaultMutableTreeNode(
												tlsLineKey + ":" + tlsLineMap.get(tlsLineKey));
										tlsRecordNode.add(recordChild);
										if (tlsLineKey.equalsIgnoreCase("Handshake Protocol")) {
											Map<String, Object> tlsLineMap1 = (Map<String, Object>) tlsLineMap
													.get(tlsLineKey);
											if (tlsLineMap1.keySet().size() > 1) {
												for (String tlsLineKey1 : tlsLineMap1.keySet()) {
													DefaultMutableTreeNode handshakeChild = new DefaultMutableTreeNode(
															tlsLineKey1 + ":" + tlsLineMap1.get(tlsLineKey1));
													recordChild.add(handshakeChild);
													if (tlsLineKey1.equalsIgnoreCase("Extension")) {
														List value = (List) tlsLineMap1.get(tlsLineKey1);
														for (Object object1 : value) {
															Map<String, Object> tlsLineMap2 = (Map<String, Object>) object1;
															String nodeName = "Extension: " + tlsLineMap2.get("Type");
															DefaultMutableTreeNode extensionNode = new DefaultMutableTreeNode(
																	nodeName);
															handshakeChild.add(extensionNode);
															if (tlsLineMap2.keySet().size() > 1) {
																for (String tlsLineKey2 : tlsLineMap2.keySet()) {
																	DefaultMutableTreeNode child = new DefaultMutableTreeNode(
																			tlsLineKey2 + ":"
																					+ tlsLineMap2.get(tlsLineKey2));
																	extensionNode.add(child);
																}
															}
														}
													}

												}
											}

										}
									}
								
								tls.add(tlsRecordNode);
								}
								else {
									tls.add(new DefaultMutableTreeNode("Error" +" : " + tlsLineMap.get("Error")));
								}
							

							}
						}
						}
						// Add fields to DTLS Packet node
						if (c1 == 9) {
							if (applicationKey == "DTLS") {
								udp.add(dtls);
								List array = (List) errorNodeTcp.get(applicationKey);
								for (Object object : array) {
									String handshakeType = null;
									DefaultMutableTreeNode recordNode = null;
									Map<String, Object> dtlsLineMap = (Map<String, Object>) object;	
									if(dtlsLineMap.keySet().size() > 1) {
									if(dtlsLineMap.get("Handshake")!=null) {
										Map<String, Object> dtlsLineMap3 = (Map<String, Object>) dtlsLineMap
												.get("Handshake");
										handshakeType =  (String) dtlsLineMap3.get("Handshake Type");
										}else {
											handshakeType =  (String) dtlsLineMap.get("DTLS Content Type");
										}
									if(handshakeType != null) 
									    recordNode = new DefaultMutableTreeNode("DTLS Record Layer : "+handshakeType);

									DefaultMutableTreeNode child = null;
										for (String dtlsLinekey : dtlsLineMap.keySet()) {
											child = new DefaultMutableTreeNode(
													dtlsLinekey + ":" + dtlsLineMap.get(dtlsLinekey));
											recordNode.add(child);
											if (dtlsLinekey.equalsIgnoreCase("Handshake")) {
												Map<String, Object> dtlsLineMap1 = (Map<String, Object>) dtlsLineMap
														.get(dtlsLinekey);
												
												if (dtlsLineMap1.keySet().size() > 1) {
													for (String dtlsLineKey1 : dtlsLineMap1.keySet()) {
														DefaultMutableTreeNode child1 = new DefaultMutableTreeNode(
																dtlsLineKey1 + ":" + dtlsLineMap1.get(dtlsLineKey1));
														child.add(child1);
														if (dtlsLineKey1.equalsIgnoreCase("Extension")) {
															List value = (List) dtlsLineMap1.get(dtlsLineKey1);
															for (Object object1 : value) {
																Map<String, Object> dtlsLineMap2 = (Map<String, Object>) object1;
																String nodeName = "Extension: "
																		+ dtlsLineMap2.get("Type");
																DefaultMutableTreeNode node1 = new DefaultMutableTreeNode(
																		nodeName);
																child1.add(node1);
																if (dtlsLineMap2.keySet().size() > 1) {
																	for (String dtlsLineKey2 : dtlsLineMap2.keySet()) {
																		DefaultMutableTreeNode child2 = new DefaultMutableTreeNode(
																				dtlsLineKey2 + ":" + dtlsLineMap2
																						.get(dtlsLineKey2));
																		node1.add(child2);
																	}
																}
															}
														}

													}
												

											}
										}
									}
								
									dtls.add(recordNode);
								}
									else {
										dtls.add(new DefaultMutableTreeNode("Error" +" : " + dtlsLineMap.get("Error")));
									}
							}
							
							}
						}
						// Add fields to HTTP Packet node
						if (c1 == 2) {
								tcp.add(http123);
								http123.add(new DefaultMutableTreeNode(key + ":" + errorNodeTcp.get(key)));
							
							
						}
						
						// Add fields to Diameter Protocol Packet node
						if (c1 == 10) {
							if (applicationKey == "Diameter Protocol") {

								tcp.add(ProtocolValidation.getDiameterObject(errorNodeTcp.get(key)));

							}
							
						}
						// Add fields to SIP Packet node
						if(c1==5)
						{
                           
							if (errorNodeTcp.containsValue("TCP")) {
								tcp.add(ProtocolValidation.getSIPObject(errorNodeTcp.get(key)));
							}
							
							if (errorNodeTcp.containsValue("UDP")) {
								udp.add(ProtocolValidation.getSIPObject(errorNodeTcp.get(key)));
								}

							
						}
								
						// Add fields to SCTP Packet node			
						if (c1 == 3) {

							if(nextProtocol == 1) {
								ip.add(ipv4);
								ipv4.add(sctp);
							}
							if(nextProtocol == 2) {
								ip.add(ipv6);
								ipv6.add(sctp);
							}					
							sctp.add(new DefaultMutableTreeNode(key + ":" + errorNodeTcp.get(key)));
						
						}
						// Add fields to SCTP Option Packet node
						if (c1 == 6) {
							sctp.add(option);
							List array = (List) errorNodeTcp.get(applicationKey);
							String nodeName = null;
							for (Object object : array) {
								Map<String, Object> optionMap = (Map<String, Object>) object;
								nodeName = "Parameter: " + optionMap.get("Parameter Type");
								DefaultMutableTreeNode parameterNode = new DefaultMutableTreeNode(nodeName);
								if (optionMap.keySet().size() > 0) {
									for (String optionKey : optionMap.keySet()) {
										DefaultMutableTreeNode child = new DefaultMutableTreeNode(
												optionKey + ":" + optionMap.get(optionKey));
										parameterNode.add(child);
									}
								}
								option.add(parameterNode);
							}
						}

						// Add fields to UDP Packet node
						if (c1 == 4) {

							if(nextProtocol == 1) {
								ip.add(ipv4);
								ipv4.add(udp);
							}
							if(nextProtocol == 2) {
								ip.add(ipv6);
								ipv6.add(udp);
							}
							udp.add(new DefaultMutableTreeNode(key + ":" + errorNodeTcp.get(key)));
						
						}
						// Add fields to ICMP Packet node
						if (c1 == 11) {

							if(nextProtocol == 1) {
								ipv4.add(icmp);
							}
							if(nextProtocol == 2) {
								ipv6.add(icmp);
							}
							icmp.add(new DefaultMutableTreeNode(key + ":" + errorNodeTcp.get(key)));

						
						}
						// Add fields to ICMP Other Protocols Packet node
						if (c1 == 13) {
							DefaultMutableTreeNode other = new DefaultMutableTreeNode("Other Protocols");
							DefaultMutableTreeNode rootOther = new DefaultMutableTreeNode("Other");

							icmp.add(other);
							List array = (List) errorNodeTcp.get(applicationKey);
							for (int l = 0; l < array.size(); l++) {
								Map<String, Object> mapOther = (Map<String, Object>) array.get(l);
                                DefaultMutableTreeNode otherIPv4 = new DefaultMutableTreeNode("IPv4");
                                DefaultMutableTreeNode otherIPv6 = new DefaultMutableTreeNode("IPv6");
                                DefaultMutableTreeNode otherTransport = null;

                                String nodeNameIpv4 = null;
                                String nodeNameIpv6 = null;

                                String nodeTransportName = null;

                                int next = 0;  
                                int countOther = 0;

          							 if(mapOther.containsKey("Transport Layer Protocol"))
           								nodeTransportName =  (String) mapOther.get("Transport Layer Protocol");
               						
          							
            					for (String keyOther : mapOther.keySet()) {
            						if(keyOther.equals("version")) {
            							next = 0;
            							countOther = 0;
            						}
            						if(keyOther.equals("Version")) {
            							next = 5;
            							countOther = 5;
            						
            						}
            						if(keyOther.equalsIgnoreCase("TCP Source Port")) {
               							otherTransport = new DefaultMutableTreeNode(nodeTransportName);
            							next = 1;
            							countOther = 1;
            						
            						}
            						if(keyOther.equalsIgnoreCase("UDP Source Port")) {
               							otherTransport = new DefaultMutableTreeNode(nodeTransportName);
            							next = 2;
            							countOther = 2;
            						
            						}
            						if(keyOther.equalsIgnoreCase("SCTP Source Port")) {
               							otherTransport = new DefaultMutableTreeNode(nodeTransportName);
            							next = 3;
            							countOther = 3;
            						
            						}
            						if(keyOther.equalsIgnoreCase("Type")) {
               							otherTransport = new DefaultMutableTreeNode(nodeTransportName);
            							next = 4;
            							countOther = 4;
            						
            						}

        							
            						if(countOther == 0) {
            							other.add(otherIPv4);
            							otherIPv4.add(new DefaultMutableTreeNode(keyOther + ":" + mapOther.get(keyOther)));
            						}
                                   if(countOther == 5) {
           							otherIPv6.add(new DefaultMutableTreeNode(keyOther + ":" + mapOther.get(keyOther)));

                                	   if(mapOther.containsKey("version") && mapOther.containsKey("Version")) {
           								otherIPv4.add(otherIPv6);
           							}
           							else {
                						other.add(otherIPv6);
           							}

            						}
                                 if(countOther == 1) {
                              	   if(mapOther.containsKey("version")) {
             							otherIPv4.add(otherTransport);
         							}
                            	   if(mapOther.containsKey("Version")) {
            							otherIPv6.add(otherTransport);
         							}
                                	 otherTransport.add(new DefaultMutableTreeNode(keyOther + ":" + mapOther.get(keyOther)));
            						}
                                 if(countOther == 2) {
                                	 if(mapOther.containsKey("version")) {
              							otherIPv4.add(otherTransport);
          							}
                             	   if(mapOther.containsKey("Version")) {
             							otherIPv6.add(otherTransport);
          							}
          							otherTransport.add(new DefaultMutableTreeNode(keyOther + ":" + mapOther.get(keyOther)));
         						}
                                 if(countOther == 3) {
                                	 if(mapOther.containsKey("version")) {
              							otherIPv4.add(otherTransport);
          							}
                             	   if(mapOther.containsKey("Version")) {
             							otherIPv6.add(otherTransport);
          							}
          							otherTransport.add(new DefaultMutableTreeNode(keyOther + ":" + mapOther.get(keyOther)));
         						}
                                 if(countOther == 4) {
                                	 if(mapOther.containsKey("version")) {
              							otherIPv4.add(otherTransport);
          							}
                             	   if(mapOther.containsKey("Version")) {
             							otherIPv6.add(otherTransport);
          							}
          							otherTransport.add(new DefaultMutableTreeNode(keyOther + ":" + mapOther.get(keyOther)));
         						}
                                if(keyOther.equalsIgnoreCase("Application Layer Protocol") || keyOther.equalsIgnoreCase("Security Layer Protocol") || keyOther.equalsIgnoreCase("Transport Layer Security"))
                                	break;
                               
            					}
            					

						}
						}




						
						// Add fields to FTP Packet node
						if (c1 == 12) {

							if(nextProtocol == 1) {
								ipv4.add(tcp);
							}
							if(nextProtocol == 2) {
								ipv6.add(tcp);
							}
							tcp.add(ProtocolValidation.getFTPObject(errorNodeTcp.get(key)));

						
						}
						// Add fields to RTP Packet node
						if (c1 == 14) {
							if(nextProtocol == 1) {
								ipv4.add(udp);
							}
							if(nextProtocol == 2) {
								ipv6.add(udp);
							}
							udp.add(ProtocolValidation.getRTPObject(errorNodeTcp.get(key)));

							
							}
     					}
						}
				}
				}
					
				clicked = clicked + 1;

				if (clicked <= 1) {
					jt = new JTree(rootNode);
					jt.setVisible(true);
					Icon leafIcon = new ImageIcon(getClass().getClassLoader().getResource("com/parser/leaf-image.png"));
					DefaultTreeCellRenderer render = (DefaultTreeCellRenderer) jt.getCellRenderer();
					render.setLeafIcon(leafIcon);
					jt.expandRow(0);
					jt.scrollRowToVisible(4);
					jtreePanel.add(jt);
					mainPanel.add(jtreeScrollPane);
					mainPanel.add(download);

					this.revalidate();
					this.repaint();
				} else {
					
					DefaultTreeModel model = (DefaultTreeModel) jt.getModel();
					DefaultMutableTreeNode root = (DefaultMutableTreeNode) model.getRoot();
					root.removeAllChildren();					
					jt.removeAll();
					mainPanel.remove(download);
					mainPanel.remove(jtreeScrollPane);
					jtreePanel.remove(jt);
					model.reload();
					model.setRoot(null);
					
					jt = new JTree(rootNode);
					jt.setVisible(true);
					Icon leafIcon = new ImageIcon(getClass().getClassLoader().getResource("com/parser/leaf-image.png"));
					DefaultTreeCellRenderer render = (DefaultTreeCellRenderer) jt.getCellRenderer();
					render.setLeafIcon(leafIcon);
					jt.expandRow(0);
					jt.scrollRowToVisible(4);
					jtreePanel.add(jt);
					mainPanel.add(jtreeScrollPane);
					mainPanel.add(download);

					this.revalidate();
					this.repaint();
					clicked = 1;
				}
				logger.info("Output result is parsed successfully!");
				
				}
				else {
					jtreeScrollPane.setVisible(false);
		            download.setVisible(false);
				}
			}catch (Exception e2) {
				e2.printStackTrace();
				logger.info("Output result is not parsed");

			}
		
}
		
		else if (e.getSource() == clear) {
			inputTextArea.setText("");
			jtreeScrollPane.setVisible(false);
            download.setVisible(false);
		}
	}

}