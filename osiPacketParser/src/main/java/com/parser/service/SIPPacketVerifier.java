package com.parser.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.parser.customError.CustomException;

       /**
        * @author AmantyaTech
        */
public class SIPPacketVerifier {
	/**
	 * Parses the hexdump of SIPPacket
	 * @request string
	 * @response LinkedHashMap<String, Object>
	 */
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SIPPacketVerifier.class);
	
	@SuppressWarnings("unused")
	public static LinkedHashMap<String, Object> verifySIPPacket(String hexdump)
			throws CustomException {
		LOGGER.info("sip input"+hexdump);
		LinkedHashMap<String, Object> list2 = new LinkedHashMap<String, Object>();

		LinkedHashMap<String, Object> outputMap = new LinkedHashMap<String, Object>();
		LinkedHashMap<String, Object> outputMap1 = new LinkedHashMap<String, Object>();

		List<Object> sipList = new ArrayList<>();

		try {

			List<Object> arList = new LinkedList<>();

			String requestMethodStr = new String(javax.xml.bind.DatatypeConverter.parseHexBinary(hexdump.substring(0)),
					"UTF-8");
			List<String> list = Arrays.asList(requestMethodStr.split("[\\r\\n]+"));
			for (int i = 0; i < list.size() - 1; i++) {
				list.removeAll(Arrays.asList("", null));
				if (list.get(0).split("\\s+")[0].equalsIgnoreCase("REGISTER")
						|| list.get(0).split("\\s+")[0].equalsIgnoreCase("INVITE")
						|| list.get(0).split("\\s+")[0].equalsIgnoreCase("ACK")
						|| list.get(0).split("\\s+")[0].equalsIgnoreCase("BYE")
						|| list.get(0).split("\\s+")[0].equalsIgnoreCase("CANCEL")
						|| list.get(0).split("\\s+")[0].equalsIgnoreCase("UPDATE")
						|| list.get(0).split("\\s+")[0].equalsIgnoreCase("REFER")
						|| list.get(0).split("\\s+")[0].equalsIgnoreCase("PRACK")
						|| list.get(0).split("\\s+")[0].equalsIgnoreCase("SUBSCRIBE")
						|| list.get(0).split("\\s+")[0].equalsIgnoreCase("NOTIFY")
						|| list.get(0).split("\\s+")[0].equalsIgnoreCase("PUBLISH")
						|| list.get(0).split("\\s+")[0].equalsIgnoreCase("MESSAGE")
						|| list.get(0).split("\\s+")[0].equalsIgnoreCase("INFO")
						|| list.get(0).split("\\s+")[0].equalsIgnoreCase("OPTIONS")) {
					 if(list.get(0).split("\\s+")[2].equalsIgnoreCase("SIP/2.0") 
							 || list.get(0).split("\\s+")[2].equalsIgnoreCase("SIPS/2.0")) {
					outputMap.put("Method", list.get(0).split("\\s+")[0]);
					outputMap.put("Request URI", list.get(0).split("\\s+")[1]);
					outputMap.put("Request Version", list.get(0).split("\\s+")[2]);
					 }else {
                         throw new CustomException("Invalid SIP Version", 1201);
					    }
				} else {
					if(list.get(0).split("\\s+")[0].equalsIgnoreCase("SIP/2.0") 
							|| list.get(0).split("\\s+")[0].equalsIgnoreCase("SIPS/2.0")) {
					outputMap.put("Method", list.get(0).split("\\s+")[0] + " " + list.get(0).split("\\s+")[1] + " "
							+ list.get(0).split("\\s+")[2]);
					outputMap.put("Status-Code", list.get(0).split("\\s+")[1]);
					}else {
                        throw new CustomException("Invalid SIP Version", 1201);

				    }
				}

				if (list.get(i + 1).contains(": ")) {
					arList.addAll(Arrays.asList(list.get(i + 1)));
					outputMap.put("Message Header", arList);
				}
				if (list.get(i + 1).split(": ")[0].equalsIgnoreCase("Content-Length")) {
					List<Object> li = new ArrayList<>();
					LinkedHashMap<String, Object> mapServerHello = new LinkedHashMap<String, Object>();

					String r = requestMethodStr.split("\n\r")[1];
					List<String> list1 = Arrays.asList(r.split("[\\r\\n]+"));
					for (int j = 1; j < list1.size(); j++) {

						if (list1.get(j).contains("v=")) {
							mapServerHello.put(" Session Description Protocol Version(v)", list1.get(j).split("=")[1]);
						}
						if (list1.get(j).contains("o=")) {
							mapServerHello.put("Owner/Creater or Session Id (o)", list1.get(j).split("=")[1]);
						}
						if (list1.get(j).contains("s=")) {
							mapServerHello.put("Session Name (s)", list1.get(j).split("=")[1]);
						}
						if (list1.get(j).contains("c=")) {
							mapServerHello.put("Connection Information (c)", list1.get(j).split("=")[1]);
						}

						if (list1.get(j).contains("t=")) {
							mapServerHello.put("Time Description/active time (t)", list1.get(j).split("=")[1]);
						}

						if (list1.get(j).contains("m=")) {
							mapServerHello.put("Media Description and name and address (m)",
									list1.get(j).split("=")[1]);
						}

						if (list1.get(j).contains("a=")) {
							mapServerHello.put("Media Attribute (a)", list1.get(j).split("=")[1]);
						}

						
					}
					if(mapServerHello.size() != 0)
						   li.add(mapServerHello);
					if (li.size() != 0)
						outputMap.put("Message Body", li);
					

				}

			}

		} catch (Exception e) {
			  outputMap.put("Error", "SIP Malformed Packet - "+e.getMessage());
			LOGGER.error("sip error"+e.getMessage());

		}
		
		if(!outputMap.isEmpty())
		sipList.add(outputMap);
		if(!sipList.isEmpty())
		list2.put("SIP", sipList);
		LOGGER.info("sip output "+sipList);
		return list2;
	}	

}