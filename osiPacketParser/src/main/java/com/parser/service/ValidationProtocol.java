package com.parser.service;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.parser.customError.CustomException;

public class ValidationProtocol {
	
	@SuppressWarnings({ "unchecked", "rawtypes", "unused" })
	public static List<Object> verifyProtocol(List<Object> toggleList,String hexdumpString) throws CustomException {
		final Logger logger = LoggerFactory.getLogger(ValidationProtocol.class);
		LinkedHashMap<String, Object> outputMap = null;
		LinkedHashMap<String, Object> errornode=null;
		List<Object> ipList = new ArrayList<>();
		java.util.List<Object> map = new ArrayList<Object>();
		if(toggleList.size()==0) {
			logger.error("ValidationProtocol not parsed..." + "Please select the valid protocols from settings tab and try again!");
          throw new CustomException("Please select the valid protocols from settings tab and try again!",400);		
		}
		map = IPPacketVerifier.verifyIPPacket(hexdumpString);
		for (int i = 0; i < map.size(); i++) {			
			outputMap=new LinkedHashMap<String, Object>();
			int nextProtocol = 0; int c1 =0;
			StringBuilder stringBuild = new StringBuilder();

			toggleList.forEach(stringBuild::append);
	         String activeSwitch = stringBuild.toString();
            
			Map<String, Object> map2 = (Map<String, Object>) map.get(i);
			for (String key : map2.keySet()) {

				if (key.equalsIgnoreCase("Error")) {
				     c1 = 30;
			     }
				if (key.equalsIgnoreCase("Method"))
					c1 = 2;
				if (key.equalsIgnoreCase("SIP")) {
					c1 = 5;
				}		
				if (key.equalsIgnoreCase("Diameter Protocol"))
					c1 = 10;
				if (key.equalsIgnoreCase("Type")) {
					c1 = 11;
				}
				if (key.equalsIgnoreCase("FTP"))
					c1 = 12;
				if (key.equalsIgnoreCase("RTP"))
					c1 = 14;
				
				if (c1 == 0) {
					outputMap.put(key, map2.get(key));
				}
				
				if (c1 == 2) {
					if (activeSwitch.contains("HTTP") || activeSwitch.contains("All")) {
						outputMap.put(key, map2.get(key));
					}
					else {
						outputMap.put("Error", "HTTP Not Decoded - " + "Protocol is Disabled for this Packet");
					}
				}
				
				if (c1 == 5) {
					if (activeSwitch.contains("SIP") || activeSwitch.contains("All")) {
						   outputMap.put(key, map2.get(key));
					}
					else {
							List<LinkedHashMap> errorlist = new LinkedList<LinkedHashMap>();							
							errornode = new LinkedHashMap<String, Object>();	
							errornode.put("Error", "SIP Not Decoded - " + "Protocol is Disabled for this Packet");
							errorlist.add(errornode);
							outputMap.put("SIP",errorlist );
						}
				}
			
				if (c1 == 10) {
					if (activeSwitch.contains("Diameter") || activeSwitch.contains("All")) {
						outputMap.put(key, map2.get(key));
					}else {
						List<LinkedHashMap> errorlist = new LinkedList<LinkedHashMap>();							
						 errornode = new LinkedHashMap<String, Object>();	
						 errornode.put("Error", "Diameter Not Decoded - " + "Protocol is Disabled for this Packet");
						 errorlist.add(errornode);
						 outputMap.put("Diameter Protocol",errorlist );
					}
				}
				
				if (c1 == 12) {
					if (activeSwitch.contains("FTP") || activeSwitch.contains("All")) {
						outputMap.put(key, map2.get(key));
					}else {
						List<LinkedHashMap> errorlist = new LinkedList<LinkedHashMap>();							
						 errornode = new LinkedHashMap<String, Object>();	
						 errornode.put("Error", "FTP Not Decoded - " + "Protocol is Disabled for this Packet");
						 errorlist.add(errornode);
						 outputMap.put("FTP",errorlist );
					}
				}
				if (c1 == 14) {
					if (activeSwitch.contains("RTP") || activeSwitch.contains("All")) {
						outputMap.put(key, map2.get(key));
					}else {
						List<LinkedHashMap> errorlist = new LinkedList<LinkedHashMap>();							
						 errornode = new LinkedHashMap<String, Object>();	
						 errornode.put("Error", "RTP Not Decoded - " + "Protocol is Disabled for this Packet");
						 outputMap.put("RTP",errornode );
					}
				}
				
				if (c1 == 30) {
					if(outputMap.size() > 1) {
						
					 if(!outputMap.containsKey("Application Layer Protocol")) {
					if((outputMap.containsValue("IPv6") && activeSwitch.contains("IPv6")) || (outputMap.containsValue("UDP") && activeSwitch.contains("UDP"))|| (outputMap.containsValue("TCP") && activeSwitch.contains("TCP"))||
					  (outputMap.containsValue("SCTP")&& activeSwitch.contains("SCTP")) || (outputMap.containsValue("ICMP") && activeSwitch.contains("ICMP")
					)) {

					outputMap.put(key, map2.get(key));
				    } 
					}
					 else if(outputMap.containsKey("Application Layer Protocol")){
				    	if((outputMap.containsValue("HTTP") && activeSwitch.contains("HTTP")))
				    	{
							outputMap.put(key, map2.get(key));
				    	}else if((outputMap.containsValue("HTTP"))){
							outputMap.put("Error", "HTTP Not Decoded - " + "Protocol is Disabled for this Packet");
				    	}
				    }
					
				}
				else if(map2.size() == 1) {
					if(activeSwitch.contains("IPv4") && map2.containsValue("IPPacket Error - IPPacket should not be less than 40 digits")) {
				         outputMap.put(key, map2.get(key));
					}
					else if(activeSwitch.contains("IPv6") && map2.containsValue("IPPacket Error - IPPacket should not be less than 80 digits")) {
				         outputMap.put(key, map2.get(key));
					}			

				}
		}
			}
			if(outputMap.size() == 0) {
				LinkedHashMap<String, Object> outputMapError = new LinkedHashMap<String, Object>();
				if(map2.get("Error") != null) {
				    outputMapError.put("Error", "Not Decoded - " + map2.get("Error"));
				  }
				else {
				   outputMapError.put("Error", "Not Decoded - " + "Protocol is Disabled for this Packet");
				}
			   ipList.add(outputMapError);
			}

			else {
			    ipList.add(outputMap);
			}
		}
		if(ipList.size()!=0) {
			if(ipList.size() == 1 && outputMap.size() == 0) {
				
				logger.error("ValidationProtocol not parsed data..." + "Selected Protocols not found in the Hexdump file");
		        throw new CustomException("Selected Protocols not found in the Hexdump file",400);		

			}else {
				logger.info("ValidationProtocol parsed data successfully.." + ipList);
				return ipList;
				
			}
		}
       else {
			logger.error("ValidationProtocol not parsed data..." + "Selected Protocols not found in the Hexdump file");
    	   throw new CustomException("Selected Protocols not found in the Hexdump file",400);		
       }
	}
}