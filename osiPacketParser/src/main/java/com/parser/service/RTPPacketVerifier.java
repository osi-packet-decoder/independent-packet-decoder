package com.parser.service;

import java.util.LinkedHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.parser.customError.CustomException;

/**
 * @author AmantyaTech
 */
public class RTPPacketVerifier {
	/**
	 * Parses the hexdump of RTPPacketVerifier
	 * 
	 * @request string
	 * @response LinkedHashMap<String, Object>
	 */
	private enum Type {

		Audio, Video;
	}

	private enum PayloadType {

		PCMU(0, Type.Audio.name() + "(ITU-T G.711 PCM μ-Law )"),
		Reserved_FS_1016_CELP(1, Type.Audio.name() + " (reserved, previously FS-1016 CELP )"),
		Reserved_G721_or_G726_32(2, Type.Audio.name() + " (reserved, previously ITU-T G.721 ADPCM )"),
		GSM(3, Type.Audio.name() + " (European GSM Full Rate )"), G723(4, Type.Audio.name() + " (ITU-T G.723.1)"),
		DIV4(5, Type.Audio.name() + " (IMA_ADPCM)"), PCMA(8, Type.Audio.name() + " (ITU-T G.711 PCM A-Law )"),
		LPC(7, Type.Audio.name() + " (Experimental Linear Predictive Coding)"),
		DVI4(6, Type.Audio.name() + " (IMA ADPCM audio 64 kbit/s)"),
		L16(10, Type.Audio.name() + " (Linear PCM 16-bit Stereo audio 1411.2 kbit/s)"),
		L16_1(10, Type.Audio.name() + " (Linear PCM 16-bit audio 705.6 kbit/s)"),
		QCELP(12, Type.Audio.name() + " (	QUALCOMM_CODE_EXCITED_LINEAR_Prediction)"),
		Comfort_Noise(13, Type.Audio.name() + " (CN)"), MPA(14, Type.Audio.name() + " (MPEG-1 or MPEG-2 audio only)"),
		G728(15, Type.Audio.name() + " (ITU-T G.728 audio 16 kbit/s)"),
		DVI4_1(16, Type.Audio.name() + " (IMA ADPCM audio 44.1 kbit/s)"),
		DVI4_2(17, Type.Audio.name() + " (IMA ADPCM audio 88.2 kbit/s)"),
		G729(18, Type.Audio.name() + " (ITU-T G.729 and G.729a audio 8 kbit/s)"),
		CELB(25, Type.Video.name() + " (Sun)"),
		JPEG(26, Type.Video.name() + " (JPEG)"),
		nv(28, Type.Video.name() + " (Xerox PARC's)"), H261(31, Type.Video.name() + " (ITU-T H.261 )"),
		MPV(32, Type.Video.name() + " (MPEG-1 and MPEG-2)"), MP2T(33, Type.Video.name() + " (MPEG-2 transport stream)"),
		H263(34, Type.Video.name() + " (H.263 video, first version)"),
		Dynamic(null, null);

		Integer code;
		String description;

		private PayloadType(Integer code, String description) {
			this.code = code;
			this.description = description;
		}

		public static PayloadType identify(Integer code) {
			for (PayloadType e : PayloadType.values()) {
				if (e.code == code) {
					return e;
				}
			}
			return PayloadType.Dynamic;
		}
	}

	public static LinkedHashMap<String, Object> parseRTPPacket(String hexdump) throws CustomException {
		final Logger logger = LoggerFactory.getLogger(RTPPacketVerifier.class);
		LinkedHashMap<String, Object> outputMap = new LinkedHashMap<String, Object>();
		LinkedHashMap<String, Object> outputMapError = new LinkedHashMap<String, Object>();

		try {
			String combinedInfo1Hex = hexdump.substring(0, 1);
			// TODO : parse 'Version', 'P', 'X' from combinedInfo1Hex
			int combinedInfo1Int = Integer.parseInt(combinedInfo1Hex, 16);
			String combinedInfo1Bin = Integer.toBinaryString(combinedInfo1Int);
			while (combinedInfo1Bin.length() < 4) {
				combinedInfo1Bin = "0" + combinedInfo1Bin;
			}

			String versionBin = combinedInfo1Bin.substring(0, 2);
			int versionDec = Integer.parseInt(versionBin, 2);		
			if (2 != versionDec) {
				throw new CustomException("expected Version 2 but found " + versionDec, 1201);
			}
			outputMap.put("Version", "Binary: " + versionBin + " , Decimal: " + versionDec);

			String pBin = combinedInfo1Bin.substring(2, 3);
			boolean pFlag = "1".equals(pBin);
			outputMap.put("Padding (P)", "Binary: " + pBin + " , Boolean: " + pFlag);

			String xBin = combinedInfo1Bin.substring(3, 4);
			boolean xFlag = "1".equals(xBin);
			outputMap.put("Extension(X)", "Binary: " + xBin + " , Boolean: " + xFlag);

			// ----------------------------------------------------------------------------

			String ccHex = hexdump.substring(1, 2);

			int ccDec = Integer.parseInt(ccHex, 16);
			String ccBin = Integer.toBinaryString(ccDec);

			while (ccBin.length() < 4) {
				ccBin = "0" + ccBin;
			}

			outputMap.put("Contributing Source Identifiers Count (CC)",
					"Hexadecimal: 0x" + ccHex + " , Binary: " + ccBin + " , Decimal: " + ccDec);

			// ----------------------------------------------------------------------------

			String combinedInfo2Hex = hexdump.substring(2, 3);
			String combinedInfo3Hex = hexdump.substring(3, 4);

			// TODO : parse 'M', 'PT' from combinedInfo2Hex
			int combinedInfo2Int = Integer.parseInt(combinedInfo2Hex, 16);
			String combinedInfo2Bin = Integer.toBinaryString(combinedInfo2Int);

			while (combinedInfo2Bin.length() < 4) {
				combinedInfo2Bin = "0" + combinedInfo2Bin;
			}

			int combinedInfo3Int = Integer.parseInt(combinedInfo3Hex, 16);
			String combinedInfo3Bin = Integer.toBinaryString(combinedInfo3Int);

			while (combinedInfo3Bin.length() < 4) {
				combinedInfo3Bin = "0" + combinedInfo3Bin;
			}

			String mBin = combinedInfo2Bin.substring(0, 1);
			boolean mFlag = "1".equals(mBin);
			outputMap.put("Marker (M)", "Binary: " + mBin + " , Boolean: " + mFlag);

			String ptBin = combinedInfo2Bin.substring(1, 4) + combinedInfo3Bin;
			int ptDec = Integer.parseInt(ptBin, 2);
			PayloadType payloadType = PayloadType.identify(ptDec);
			outputMap.put("Payload Type (PT)", "Binary: " + ptBin + " , Decimal: " + ptDec + " , Name: "+ payloadType.name() + " for " + payloadType.description);
			// ----------------------------------------------------------------------------
			String sequenceNumberHex = hexdump.substring(4, 8);
			int sequenceNumberDec = Integer.parseInt(sequenceNumberHex, 16);
			outputMap.put("Sequence Number","Hexadecimal: 0x" + sequenceNumberHex + " , Decimal: " + sequenceNumberDec);

			String timestampHex = hexdump.substring(8, 16);
			long timestampDec = Long.parseLong(timestampHex, 16);
			outputMap.put("Timestamp", "Hexadecimal: 0x" + timestampHex + " , Decimal: " + timestampDec);

			String ssrcIdentifierHex = hexdump.substring(16, 24);
			long ssrcIdentifierDec = Long.parseLong(ssrcIdentifierHex, 16);
			outputMap.put("Syncronization Source Identifier (SSRC Identifier)",	"Hexadecimal: 0x" + ssrcIdentifierHex + " , Decimal: " + ssrcIdentifierDec);

			String payload = hexdump.substring(24);
			outputMap.put("Payload", "Hexadecimal: 0x" + payload);
			logger.info("RTP Parser parsed data successfully.." + outputMap);
			return outputMap;

		} catch (Exception e) {
			logger.error("RTP Parser Error.. {}" , e.getMessage());
			outputMapError.put("Error", "RTP Malformed Packet - " + e.getMessage());
			return outputMapError;

		}

	}

}	
	

