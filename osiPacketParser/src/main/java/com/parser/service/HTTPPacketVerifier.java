package com.parser.service;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.xml.bind.DatatypeConverter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.parser.customError.CustomException;
import com.parser.utility.JsonUtil;

      /**
       * @author AmantyaTech
       */

public class HTTPPacketVerifier {

	private static final Logger logger = LoggerFactory.getLogger(HTTPPacketVerifier.class);

	static Set<String> headers = new HashSet<String>();

	static Set<String> methods = new HashSet<String>();

	private static JsonUtil<Object> mapJsonUtil = new JsonUtil<Object>();

	static {
		headers.add("A-IM");
		headers.add("Accept");
		headers.add("Accept-Charset");
		headers.add("Accept-Datetime");
		headers.add("Accept-Encoding");
		headers.add("Accept-Language");
		headers.add("Access-Control-Request-Method");
		headers.add("Authorization");
		headers.add("Cache-Control");
		headers.add("Connection");
		headers.add("Content-Encoding");
		headers.add("Content-Length");
		headers.add("Content-MD5");
		headers.add("Content-Type");
		headers.add("Cookie");
		headers.add("Date");
		headers.add("Expect");
		headers.add("Forwarded");
		headers.add("From");
		headers.add("Host");
		headers.add("HTTP2-Settings");
		headers.add("If-Modified-Since");
		headers.add("If-None-Match");
		headers.add("If-Range");
		headers.add("If-Unmodified-Since");
		headers.add("Max-Forwards");
		headers.add("Origin");
		headers.add("Proxy-Authorization");
		headers.add("Range");
		headers.add("TE");
		headers.add("Trailer");
		headers.add("Transfer-Encoding");
		headers.add("User-Agent");
		headers.add("Upgrade");
		headers.add("Via");
		headers.add("Warning");
		headers.add("Access-Control-Allow-Origin");
		headers.add("Access-Control-Allow-Credentials");
		headers.add("Access-Control-Expose-Headers");
		headers.add("Access-Control-Max-Age");
		headers.add("Access-Control-Allow-Methods");
		headers.add("Access-Control-Allow-Headers");
		headers.add("Accept-Patch");
		headers.add("Accept-Ranges");
		headers.add("Age");
		headers.add("Allow");
		headers.add("Alt-Svc");
		headers.add("Cache-Control");
		headers.add("Connection");
		headers.add("Content-Disposition");
		headers.add("Content-Encoding");
		headers.add("Content-Length");
		headers.add("Content-Language");
		headers.add("Content-Location");
		headers.add("Content-MD5");
		headers.add("Content-Range");
		headers.add("Content-Type");
		headers.add("Date");
		headers.add("Delta-Base");
		headers.add("ETag");
		headers.add("Expires");
		headers.add("IM");
		headers.add("Last-Modified");
		headers.add("Link");
		headers.add("Location");
		headers.add("P3P");
		headers.add("Pragma");
		headers.add("Proxy-Authenticate");
		headers.add("Public-Key-Pins");
		headers.add("Retry-After");
		headers.add("Server");
		headers.add("Set-Cookie");
		headers.add("Strict-Transport-Security");
		headers.add("Trailer");
		headers.add("Transfer-Encoding");
		headers.add("TK");
		headers.add("Upgrade");
		headers.add("Vary");
		headers.add("Via");
		headers.add("Warning");
		headers.add("WWW-Authenticate");
		headers.add("X-Frame-Options");
		
		methods.add("GET");
		methods.add("HEAD");
		methods.add("POST");
		methods.add("PUT");
		methods.add("DELETE");
		methods.add("CONNECT");
		methods.add("OPTIONS");
		methods.add("TRACE");
		methods.add("PATCH");
	}

	public static LinkedHashMap<String, Object> verifyHTTPPacket(String hexdump) throws CustomException {

		logger.info("-> verifyHTTPPacket : (hexdump) {}", hexdump);
		if (hexdump == null) {
			throw new CustomException("Hexdump missing, please provide the input and try again!", 400);
		}
		LinkedHashMap<String, Object> outputMap = new LinkedHashMap<String, Object>();
		try {
			String asciiText = new String(DatatypeConverter.parseHexBinary(hexdump), "UTF-8");
			String[] httpHeaderList = asciiText.split("[\\r\\n]+");
			logger.info("httpHeaderLineCount {}", httpHeaderList.length);
			Set<String> nonCoreHeaders = new LinkedHashSet<String>();
			String firstHeaderLine = httpHeaderList[0];
			logger.info("httpHeaderLine {} {}", 0, firstHeaderLine);

			String[] meta = firstHeaderLine.split("\\s+");
			String methodName = meta[0];

			if(firstHeaderLine.contains("HTTP/1.1")) {
				//HTTP Response
				if(methodName.contains("HTTP")) {
					outputMap.put("Method", firstHeaderLine);
					outputMap.put("Request Version", methodName);
					try {
					outputMap.put("Description", meta[2]+" "+meta[3]);

					}catch (Exception e) {
						// TODO: handle exception
						try {
							outputMap.put("Description", meta[2]);


						}catch (Exception e1) {
							// TODO: handle exception
							outputMap.put("Description", "Not Found");

						}

					}
					outputMap.put("Status Code", meta[1]);

				}
				//HTTP Request
				else {
					if (!methods.contains(methodName)) {
						throw new CustomException("invalid method name " + methodName, 1201);
					}
					outputMap.put("Method", firstHeaderLine);
					outputMap.put("Request Method", methodName);
					try {
					outputMap.put("Request URI", meta[1]);

					}catch (Exception e) {
						// TODO: handle exception
						outputMap.put("Request URI", "Not Found");
					}
					try {
						outputMap.put("Request Version", meta[2]);
					}catch (Exception e) {
						// TODO: handle exception
						outputMap.put("Request Version", "Not Found");

					}
				}
			
			
			StringBuilder body = new StringBuilder();
			int i = 0;
			for (i = 1; i < httpHeaderList.length; i++) {
				String element = httpHeaderList[i];
				logger.info("httpHeaderLine {} {}", i, element);
				if (element.contains(":")) {
					String[] parts = element.split(":");
					outputMap.put(parts[0], parts[1]);
					if (!headers.contains(parts[0])) {
						nonCoreHeaders.add(parts[0]);
					}
				} else {
					body.append(element.trim());
					i++;
					break;
				}
			}
			for (; i < httpHeaderList.length; i++) {
				String element = httpHeaderList[i];
				logger.info("httpHeaderLine {} {}", i, element);
				body.append(element.trim());
			}
			if (body.length() > 0) {
				String bodyString = body.toString();
				logger.info("body -> {}", bodyString);
				Object bodyObject = mapJsonUtil.convertJsonToObject(bodyString, Object.class);
				logger.info("bodyObject -> {}", bodyObject);
				//outputMap.put("BodyString", bodyString);
				if(bodyObject!= null)
				outputMap.put("Body", bodyObject);
			}
			if (nonCoreHeaders.size() > 0) {
				outputMap.put("Not Core Headers", nonCoreHeaders);
			}
			}
			else {
				outputMap.put("Error", "HTTP Malformed Packet - No Valid HTTP Packet");
			}
			
		} catch (Exception e) {
			//e.printStackTrace();
			outputMap.put("Error", "HTTP Malformed Packet - " +e.getMessage());
			logger.error("http exception : (message) {}", e.getMessage());
		}
		logger.info("<- verifyHTTPPacket : (outputMap) {}", outputMap);
		return outputMap;
	}

	

}