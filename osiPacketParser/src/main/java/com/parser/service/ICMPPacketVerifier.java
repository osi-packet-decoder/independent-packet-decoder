package com.parser.service;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.parser.customError.CustomException;

/**
 * @author AmantyaTech
 */


public class ICMPPacketVerifier {			
	/**
	 * Parses the hexdump of ICMPPacket
	 * @request string
	 * @response LinkedHashMap<String, Object>
	 */
		public static LinkedHashMap<String,Object> verifyICMPPacket(String hexdump, String fullString) throws CustomException {
			 final Logger logger = LoggerFactory.getLogger(ICMPPacketVerifier.class);

			LinkedHashMap<String, Object> outputMap = new LinkedHashMap<String, Object>();	
			LinkedHashMap<String, Object> outputMapError = new LinkedHashMap<String, Object>();	

			
			try {		
				isValidICMPPacket(hexdump);
				String  type = isValidType(hexdump.substring(0,2));
				 if(Integer.parseInt(type,16) == 0) {
						outputMap.put( "Type", "0(Echo Reply)");
						String  code= isValidCode(hexdump.substring(2,4));
						outputMap.put("Code",code);
						
						String checksum = isValidChecksum(hexdump.substring(4,8));
						outputMap.put("ICMP Checksum", "0x" +checksum);
						
						String identifier = isValidIdentifier(hexdump.substring(8,12));
						outputMap.put("ICMP Identifier(BE)",Integer.parseInt(identifier,16) + "(0x" +identifier+")");
						
						String part1 = hexdump.substring(8,10);
						String part2 = hexdump.substring(10,12);
						String identifier1 = part2+part1;
						outputMap.put("ICMP Identifier(LE)",Integer.parseInt(identifier1,16) + "(0x" +identifier1+")");
						
						String seq_no = isValidSequence(hexdump.substring(12,16));
						outputMap.put("Sequence Number(BE)",Integer.parseInt(seq_no,16) + "(0x" +seq_no+")");
					
						String part3 = hexdump.substring(12,14);
						String part4 = hexdump.substring(14,16);
						String seq = part4+part3;
						outputMap.put("Sequence Number(LE)",Integer.parseInt(seq,16) + "(0x" +seq+")");

						
						String data = hexdump.substring(16);
						outputMap.put("Data", data);
						
						outputMap.put("Length", data.length()/2);
				 }
				 if(Integer.parseInt(type,16) == 1) 
					    outputMap.put( "Type", "1(Unassigned)");		
				 if(Integer.parseInt(type,16) == 2) 
						outputMap.put( "Type", "2(Unassigned)");
				 if(Integer.parseInt(type,16) == 3) {
						outputMap.put( "Type", "3(Destination Unreachable)");
						String  code= isValidCode(hexdump.substring(2,4));
						 if(Integer.parseInt(code,16) == 0) 
								outputMap.put( "Code", "0(Net Unreachable)");
						 if(Integer.parseInt(code,16) == 1) 
								outputMap.put( "Code", "1(Host Unreachable)");
						 if(Integer.parseInt(code,16) == 2) 
								outputMap.put( "Code", "2(Protocol Unreachable)");
						 if(Integer.parseInt(code,16) == 3) 
								outputMap.put( "Code", "3(Port unreachable)");
						 if(Integer.parseInt(code,16) == 4) 
								outputMap.put( "Code", "4(Fragmentation Needed, and DF was set)");
						 if(Integer.parseInt(code,16) == 5) 
								outputMap.put( "Code", "5(Source route failed)");
						 if(Integer.parseInt(code,16) == 6) 
								outputMap.put( "Code", "6(Destination network unknown)");
						 if(Integer.parseInt(code,16) == 7) 
								outputMap.put( "Code", "7(Destination host unknown)");
						 if(Integer.parseInt(code,16) == 8) 
								outputMap.put( "Code", "8(Source host isolated)");
						 if(Integer.parseInt(code,16) == 9) 
								outputMap.put( "Code", "9(Communication with destination network is administratively prohibited)");
						 if(Integer.parseInt(code,16) == 10) 
								outputMap.put( "Code", "10(Communication with destination host is administratively prohibited)");
						 if(Integer.parseInt(code,16) == 11) 
								outputMap.put( "Code", "11(Destination network unreachable for type of service)");
						 if(Integer.parseInt(code,16) == 12) 
								outputMap.put( "Code", "12(Destination host unreachable for type of service)");
						 if(Integer.parseInt(code,16) == 13) 
								outputMap.put( "Code", "13(Communication administratively prohibited)");
						 if(Integer.parseInt(code,16) == 14) 
								outputMap.put( "Code", "14(Host precedence violation)");
						 if(Integer.parseInt(code,16) == 15) 
								outputMap.put( "Code", "15(Precedence cutoff in effect)");
						
						 String checksum = isValidChecksum(hexdump.substring(4,8));
							outputMap.put("ICMP Checksum", "0x" +checksum);
							String check = hexdump.substring(8);
							for (int i = 0 ; i != check.length() ; i++) {
							    char c = check.charAt(i);
								if(c =='4') {
							    String s1 = check.substring(check.indexOf("4")+0);
								s1.trim();
								List<Object> map = new ArrayList<Object>();
				        	map =  IPPacketVerifier.verifyIPPacket(s1);
				        	outputMap.put("IP", map);
				        	break;
								}
								if(c =='6') {
								    String s1 = check.substring(check.indexOf("6")+0);
									s1.trim();
									List<Object> map = new ArrayList<Object>();
					        	map =  IPPacketVerifier.verifyIPPacket(s1);
					        	outputMap.put("IP", map);
					        	break;
									}
							}
				 }
				 if(Integer.parseInt(type,16) == 4) 
						outputMap.put( "Type", "4(Source Quench (Deprecated))");
				 if(Integer.parseInt(type,16) == 5) {
						outputMap.put( "Type", "5(Redirect)");
						String  code= isValidCode(hexdump.substring(2,4));
						 if(Integer.parseInt(code,16) == 0) 
								outputMap.put( "Code", "0(Redirect datagram for the network (or subnet))");
						 if(Integer.parseInt(code,16) == 1) 
								outputMap.put( "Code", "1(Redirect datagram for the host)");
						 if(Integer.parseInt(code,16) == 2) 
								outputMap.put( "Code", "2(Redirect datagram for the type of service and network)");
						 if(Integer.parseInt(code,16) == 3) 
								outputMap.put( "Code", "3(Redirect datagram for the type of service and host)");
						 String checksum = isValidChecksum(hexdump.substring(4,8));
							outputMap.put("ICMP Checksum", "0x" +checksum);
						
							String check = hexdump.substring(8);
							for (int i = 0 ; i != check.length() ; i++) {
							    char c = check.charAt(i);
								if(c =='4') {
							    String s1 = check.substring(check.indexOf("4")+0);
								s1.trim();
								List<Object> map = new ArrayList<Object>();
				        	map =  IPPacketVerifier.verifyIPPacket(s1);
				        	outputMap.put("IP", map);
				        	break;
								}
								if(c =='6') {
								    String s1 = check.substring(check.indexOf("6")+0);
									s1.trim();
									List<Object> map = new ArrayList<Object>();
					        	map =  IPPacketVerifier.verifyIPPacket(s1);
					        	outputMap.put("IP", map);
					        	break;
									}
							}
				 }
				 if(Integer.parseInt(type,16) == 6) 
						outputMap.put( "Type", "6(Alternate Host Address (Deprecated))");
				 if(Integer.parseInt(type,16) == 7) 
						outputMap.put( "Type", "7(Unassigned)");
				 if(Integer.parseInt(type,16) == 8) {
						outputMap.put( "Type", "8(Echo Request)");
						String  code= isValidCode(hexdump.substring(2,4));
						outputMap.put("Code",code);
						
						String checksum = isValidChecksum(hexdump.substring(4,8));
						outputMap.put("ICMP Checksum", "0x" +checksum);
						
						String identifier = isValidIdentifier(hexdump.substring(8,12));
						outputMap.put("ICMP Identifier(BE)",Integer.parseInt(identifier,16) + "(0x" +identifier+")");
						
						String part1 = hexdump.substring(8,10);
						String part2 = hexdump.substring(10,12);
						String identifier1 = part2+part1;
						outputMap.put("ICMP Identifier(LE)",Integer.parseInt(identifier1,16) + "(0x" +identifier1+")");
						
						String seq_no = isValidSequence(hexdump.substring(12,16));
						outputMap.put("Sequence Number(BE)",Integer.parseInt(seq_no,16) + "(0x" +seq_no+")");
					
						String part3 = hexdump.substring(12,14);
						String part4 = hexdump.substring(14,16);
						String seq = part4+part3;
						outputMap.put("Sequence Number(LE)",Integer.parseInt(seq,16) + "(0x" +seq+")");

						
						String data = hexdump.substring(16);
						outputMap.put("Data", data);
						
						outputMap.put("Length", data.length()/2);
				 }
				 if(Integer.parseInt(type,16) == 9) {
						outputMap.put( "Type", "9(Router Advertisement)");
						String  code= isValidCode(hexdump.substring(2,4));
						if(Integer.parseInt(code,16) == 0) 
							outputMap.put( "Code", "0(Normal router advertisement)");
						if(Integer.parseInt(code,16) == 16) 
							outputMap.put( "Code", "16(Does not route common traffic)");
				 }
				 if(Integer.parseInt(type,16) == 10) 
						outputMap.put( "Type", "10(Router Solicitation)");
				 if(Integer.parseInt(type,16) == 11) {
						outputMap.put( "Type", "11(Time Exceeded)");
						String  code= isValidCode(hexdump.substring(2,4));
						if(Integer.parseInt(code,16) == 0) 
							outputMap.put( "Code", "0(Time to live exceeded in transit)");
						if(Integer.parseInt(code,16) == 1) 
							outputMap.put( "Code", "1(Fragment reassembly time exceeded)");
						String checksum = isValidChecksum(hexdump.substring(4,8));
						outputMap.put("ICMP Checksum", "0x" +checksum);
					
						String check = hexdump.substring(8);
						for (int i = 0 ; i != check.length() ; i++) {
						    char c = check.charAt(i);
							if(c =='4') {
						    String s1 = check.substring(check.indexOf("4")+0);
							s1.trim();
							List<Object> map = new ArrayList<Object>();
			        	map =  IPPacketVerifier.verifyIPPacket(s1);
			        	outputMap.put("IP", map);
			        	break;
							}
							if(c =='6') {
							    String s1 = check.substring(check.indexOf("6")+0);
								s1.trim();
								List<Object> map = new ArrayList<Object>();
				        	map =  IPPacketVerifier.verifyIPPacket(s1);
				        	outputMap.put("IP", map);
				        	break;
								}
						}
				 }
				 if(Integer.parseInt(type,16) == 12) {
						outputMap.put( "Type", "12(Parameter Problem)");
						String  code= isValidCode(hexdump.substring(2,4));
						if(Integer.parseInt(code,16) == 0) 
							outputMap.put( "Code", "0(Pointer indicates the error)");
						if(Integer.parseInt(code,16) == 1) 
							outputMap.put( "Code", "1(Missing a required option)");
						if(Integer.parseInt(code,16) == 2) 
							outputMap.put( "Code", "2(Bad length)");
						
				 }
				 if(Integer.parseInt(type,16) == 13) 
						outputMap.put( "Type", "13(Timestamp)");
				 if(Integer.parseInt(type,16) == 14) 
						outputMap.put( "Type", "14(Timestamp Reply)"); 
				 if(Integer.parseInt(type,16) == 15) 
							outputMap.put( "Type", "15(Information Request (Deprecated))");
				 if(Integer.parseInt(type,16) == 16) 
						outputMap.put( "Type", "16(Information Reply (Deprecated))");
				 if(Integer.parseInt(type,16) == 17) 
						outputMap.put( "Type", "17(Address Mask Request (Deprecated))");
				 if(Integer.parseInt(type,16) == 18) 
						outputMap.put( "Type", "18(Address Mask Reply (Deprecated))");
				 if(Integer.parseInt(type,16) == 19) 
						outputMap.put( "Type", "19(Reserved (for Security))");
				 if(Integer.parseInt(type,16) > 19 && Integer.parseInt(type,16)< 30) 
						outputMap.put( "Type", "(Reserved (for Robustness Experiment))");
				 if(Integer.parseInt(type,16) == 30) 
						outputMap.put( "Type", "30(Traceroute (Deprecated))");
				 if(Integer.parseInt(type,16) == 31) 
						outputMap.put( "Type", "31(Datagram Conversion Error (Deprecated))");
				 if(Integer.parseInt(type,16) == 32) 
						outputMap.put( "Type", "32(Mobile Host Redirect (Deprecated))");
				 if(Integer.parseInt(type,16) == 33) 
						outputMap.put( "Type", "33(IPv6 Where-Are-You (Deprecated))");
				 if(Integer.parseInt(type,16) == 34) 
						outputMap.put( "Type", "34(IPv6 I-Am-Here (Deprecated))");
				 if(Integer.parseInt(type,16) == 35) 
						outputMap.put( "Type", "35(Mobile Registration Request (Deprecated))");
				 if(Integer.parseInt(type,16) == 36) 
						outputMap.put( "Type", "36(Mobile Registration Reply (Deprecated))");
				 if(Integer.parseInt(type,16) == 37) 
						outputMap.put( "Type", "37(Domain Name Request (Deprecated))");
				 if(Integer.parseInt(type,16) == 38) 
						outputMap.put( "Type", "38(Domain Name Reply (Deprecated))");
				 if(Integer.parseInt(type,16) == 39) 
						outputMap.put( "Type", "39(SKIP (Deprecated))");
				 if(Integer.parseInt(type,16) == 40) {
						outputMap.put( "Type", "40(Photuris)");
						String  code= isValidCode(hexdump.substring(2,4));
						if(Integer.parseInt(code,16) == 0) 
							outputMap.put( "Code", "0(Bad SPI)");
						if(Integer.parseInt(code,16) == 1) 
							outputMap.put( "Code", "1(Authentication Failed)");
						if(Integer.parseInt(code,16) == 2) 
							outputMap.put( "Code", "2(Decompression failed)");
						if(Integer.parseInt(code,16) == 3) 
							outputMap.put( "Code", "3(Decryption failed)");
						if(Integer.parseInt(code,16) == 4) 
							outputMap.put( "Code", "4(Need Authentication)");
						if(Integer.parseInt(code,16) == 5) 
							outputMap.put( "Code", "5(Need Authorization)");
						
				 }
				 if(Integer.parseInt(type,16) == 41) 
						outputMap.put( "Type", "41(ICMP messages utilized by experimental mobility protocols such as Seamoby	)");
				 if(Integer.parseInt(type,16) == 42) 
						outputMap.put( "Type", "42(Extended Echo Request)");
				 if(Integer.parseInt(type,16) == 43) 
						outputMap.put( "Type", "43(Extended Echo Reply)");
				 if(Integer.parseInt(type,16) > 43 && Integer.parseInt(type,16) < 253) {
						outputMap.put( "Type", Integer.parseInt(type,16)+"(Unassigned)");
						String  code= isValidCode(hexdump.substring(2,4));
						outputMap.put("Code",code);
						
						String checksum = isValidChecksum(hexdump.substring(4,8));
						outputMap.put("ICMP Checksum", "0x" +checksum);
						
						String identifier = isValidIdentifier(hexdump.substring(8,12));
						outputMap.put("ICMP Identifier(BE)",Integer.parseInt(identifier,16) + "(0x" +identifier+")");
						
						String part1 = hexdump.substring(8,10);
						String part2 = hexdump.substring(10,12);
						String identifier1 = part2+part1;
						outputMap.put("ICMP Identifier(LE)",Integer.parseInt(identifier1,16) + "(0x" +identifier1+")");
						
						String seq_no = isValidSequence(hexdump.substring(12,16));
						outputMap.put("Sequence Number(BE)",Integer.parseInt(seq_no,16) + "(0x" +seq_no+")");
					
						String part3 = hexdump.substring(12,14);
						String part4 = hexdump.substring(14,16);
						String seq = part4+part3;
						outputMap.put("Sequence Number(LE)",Integer.parseInt(seq,16) + "(0x" +seq+")");

						
						String data = hexdump.substring(16);
						outputMap.put("Data", data);
						
						outputMap.put("Length", data.length()/2);
				 }
				 if(Integer.parseInt(type,16) == 253) 
						outputMap.put( "Type", "253(RFC3692-style Experiment 1)");
				 if(Integer.parseInt(type,16) == 254) 
						outputMap.put( "Type", "254(RFC3692-style Experiment 2)");
				 if(Integer.parseInt(type,16) == 255) 
						outputMap.put( "Type", "255(Reserved)");
				 
				 
					logger.info("ICMP Parser parsed data successfully.."+outputMap);

					return outputMap;	

				
			} catch (Exception e) {				
				outputMapError.put("Error", "ICMP Malformed Packet - " +e.getMessage());
				logger.error("ICMP Parser Error.."+e.getMessage());
				return outputMapError;
			}			
		}	
					
		private static String isValidSequence(String seq_no) throws CustomException {
			if(seq_no.length()!=4)
				throw new CustomException("Sequence Number not valid...."+ seq_no,1201);
			return seq_no ;			
		}
		
		private static String isValidIdentifier(String identifier) throws CustomException {
			if(identifier.length()!=4)
				throw new CustomException("Identifier not valid...."+ identifier,1201);
			return identifier ;			
		}

		
		private static String isValidChecksum(String checksum) throws CustomException {
			if(checksum.length()!=4)
				throw new CustomException("Checksum not valid...."+ checksum,1201);
			return checksum ;			
		}


		private static String isValidCode(String code) throws CustomException {
			if(code.length() != 2) {			 
				throw new CustomException("ICMP Code is not valid "+code,1201);
				}
			else
			return  Integer.parseInt(code,16)+"";		
		}


		private static String isValidType(String type) throws CustomException {
			if(type.length() != 2) {			 
				throw new CustomException("Message Type is not valid "+type,1201);
				}
			else
			return  type;	
		}


	

		private static String isValidICMPPacket(String hexdump) throws CustomException {
			if( hexdump.length() < 40)
				throw new CustomException("Invalid ICMP Packet "+hexdump.substring(0),400);
			
			else return hexdump;
		}		
		


}