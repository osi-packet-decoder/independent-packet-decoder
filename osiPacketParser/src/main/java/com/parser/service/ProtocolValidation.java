package com.parser.service;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import javax.swing.tree.DefaultMutableTreeNode;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.parser.commonFunction.CommonFunction;
import com.parser.commonFunction.CommonFunction.ErrorResponseMessage;
import com.parser.commonFunction.CommonFunction.SuccessResponseMessage;
import com.parser.customError.CustomException;

public class ProtocolValidation {
	/**
	 * 
	 * @author AmantyaTech
	 * 
	 *      Used for adding nodes to a map object 	
	 * @param i 
	 * @throws CustomException 
	 *		   
	 */	
	private static final Logger LOGGER = LoggerFactory.getLogger(CommonFunction.class);
	
	public static DefaultMutableTreeNode getSIPObject(Object sipObject) throws CustomException {
		 Map<Object, Object> rootObject =new LinkedHashMap<>();
		 DefaultMutableTreeNode	sip = new DefaultMutableTreeNode("Session Initiation Protocol(SIP)");

		List array = (List) sipObject;
		for (Object object : array) {
			Map<String, Object> sipLineMap = (Map<String, Object>) object;
			if (sipLineMap.keySet().size() > 0) {
				for (String sipLineKey : sipLineMap.keySet()) {
					DefaultMutableTreeNode child = new DefaultMutableTreeNode(sipLineKey + ":" + sipLineMap.get(sipLineKey));
					sip.add(child);
					if(sipLineKey.equalsIgnoreCase("Message Header")) {
						List array1 = (List) sipLineMap.get(sipLineKey);
						for (Object object1 : array1) {
								String value = (String) object1;

						String[] keyValuePairs = value.split(", ");              
						Map<String,String> map = new HashMap<>();               

						for(String pair : keyValuePairs)                        
						{
							if(pair.contains(": ")) {
							DefaultMutableTreeNode child1 = new DefaultMutableTreeNode(pair);
                               child.add(child1);
						}
						}
						}
					}
					if(sipLineKey.equalsIgnoreCase("Message Body")) {
						DefaultMutableTreeNode node = new DefaultMutableTreeNode("Session Description Protocol(SDP)");
						child.add(node);
						List array1 = (List) sipLineMap.get(sipLineKey);
						for (Object object1 : array1) { 
							Map<String, Object> sipLineMap1 = (Map<String, Object>) object1;
							if (sipLineMap1.keySet().size() > 0) {
								for (String sipLineKey1 : sipLineMap1.keySet()) {
									DefaultMutableTreeNode child2 = new DefaultMutableTreeNode(sipLineKey1 + ":" + sipLineMap1.get(sipLineKey1));
									node.add(child2);
								}
							}
						}

							
					}
				}
			}
		}
		return sip;

	}
	
	public static DefaultMutableTreeNode getFTPObject(Object ftpObject) throws CustomException {
		 Map<Object, Object> rootObject =new LinkedHashMap<>();
		 DefaultMutableTreeNode ftp = new DefaultMutableTreeNode("File Transfer Protocol(FTP)");

		 List array = (List) ftpObject;
			for (Object object : array) {
				Map<String, Object> ftpLineMap = (Map<String, Object>) object;
				if (ftpLineMap.keySet().size() > 1) {
					DefaultMutableTreeNode node = new DefaultMutableTreeNode(ftpLineMap.get("Line"));
					for (String ftpLineKey : ftpLineMap.keySet()) {
						DefaultMutableTreeNode child = new DefaultMutableTreeNode(
								ftpLineKey + ":" + ftpLineMap.get(ftpLineKey));
						node.add(child);
					}
				
				ftp.add(node);
			}
					else {
						ftp.add(new DefaultMutableTreeNode("Error" + ":" + ftpLineMap.get("Error")));

					}
			}
		return ftp;

	}
	
	public static DefaultMutableTreeNode getRTPObject(Object rtpObject) throws CustomException {
		 Map<Object, Object> rootObject =new LinkedHashMap<>();
		 DefaultMutableTreeNode	rtp = new DefaultMutableTreeNode("Real-time Transport Protocol(RTP)");


				LinkedHashMap<String, Object> rtpParsedMap = (LinkedHashMap<String, Object>) rtpObject;
				for (String rtpAttribute : rtpParsedMap.keySet()) {
					rtp.add(new DefaultMutableTreeNode(
							rtpAttribute + ":" + rtpParsedMap.get(rtpAttribute)));
				}
					
	return rtp;

	}
	
	public static DefaultMutableTreeNode getDiameterObject(Object diameterObject) throws CustomException {
		 Map<Object, Object> rootObject =new LinkedHashMap<>();
		 DefaultMutableTreeNode	diameter = new DefaultMutableTreeNode("Diameter Protocol");


		 List array = (List) diameterObject;
		 for (Object object : array) {
				Map<String, Object> diameterLineMap = (Map<String, Object>) object;
				DefaultMutableTreeNode node = null;
				if(diameterLineMap.keySet().size() > 1) {
				String treeNode = "Diameter Layer: " + diameterLineMap.get("Diameter Version");
				node = new DefaultMutableTreeNode(treeNode);
				DefaultMutableTreeNode child = null;
					for (String diameterLineKey : diameterLineMap.keySet()) {
						child = new DefaultMutableTreeNode(
								diameterLineKey + ":" + diameterLineMap.get(diameterLineKey));
						node.add(child);

						if (diameterLineKey.equalsIgnoreCase("AVPs")) {
							List value = (List) diameterLineMap.get(diameterLineKey);
							for (Object object1 : value) {
								Map<String, Object> diameterLineMap1 = (Map<String, Object>) object1;
								String nodeName = "AVPs: " + diameterLineMap1.get("AVP code");
								;
								DefaultMutableTreeNode node1 = new DefaultMutableTreeNode(nodeName);
								child.add(node1);
								if (diameterLineMap1.keySet().size() > 1) {
									for (String diameterLineKey1 : diameterLineMap1.keySet()) {
										DefaultMutableTreeNode child2 = new DefaultMutableTreeNode(
												diameterLineKey1 + ":"
														+ diameterLineMap1.get(diameterLineKey1));
										node1.add(child2);
										if (diameterLineKey1.equalsIgnoreCase("AVP flags (Parsed)")) {
											Map<String, Object> diameterLineMap2 = (Map<String, Object>) diameterLineMap1
													.get(diameterLineKey1);
											if (diameterLineMap2.keySet().size() > 0) {
												for (String diameterLineKey2 : diameterLineMap2.keySet()) {
													DefaultMutableTreeNode child1 = new DefaultMutableTreeNode(
															diameterLineKey2 + ":" + diameterLineMap2
																	.get(diameterLineKey2));
													child2.add(child1);
												}
											}
										}

									}
								}
							}
						}
					}
				
				diameter.add(node);
				}
			
			else {
				diameter.add(new DefaultMutableTreeNode("Error" +" : " + diameterLineMap.get("Error")));
			}
		}
	
		
		return diameter;

	}
	
	public static DefaultMutableTreeNode getHTTPObject(Object httpObject) throws CustomException {
		 Map<Object, Object> rootObject =new LinkedHashMap<>();
		 DefaultMutableTreeNode http = new DefaultMutableTreeNode("HyperText Transfer Protocol(HTTP)");


			LinkedHashMap<String, Object> rtpParsedMap = (LinkedHashMap<String, Object>) httpObject;
			for (String rtpAttribute : rtpParsedMap.keySet()) {
				http.add(new DefaultMutableTreeNode(
						rtpAttribute + ":" + rtpParsedMap.get(rtpAttribute)));
			}
		return http;

	}
}