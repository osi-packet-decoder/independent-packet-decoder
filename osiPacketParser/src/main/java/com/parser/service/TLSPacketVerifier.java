package com.parser.service;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.codec.DecoderException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.parser.customError.CustomException;

        /**
         * @author AmantyaTech
         */

public class TLSPacketVerifier {

	/**
	 * Parses the hexdump of TLSPacket
	 * @request string
	 * @response LinkedHashMap<String, Object>
	 */
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TLSPacketVerifier.class);
	
	public static LinkedHashMap<String, Object> verifyTLSPacket(String hexdump) {
		LOGGER.info("TLS input "+hexdump);
		LinkedHashMap<String, Object> tlsObj = new LinkedHashMap<String, Object>();
		List<Object> tlsList = new ArrayList<>();
		int TLSLength = 0;
		try {
			int from = TLSLength * 2;
			int to = (TLSLength * 2) + 2;
			String tlsstr = hexdump.substring(from, to);
			if (tlsstr.equals("14") || tlsstr.equals("15") || tlsstr.equals("16") || tlsstr.equals("17")
					|| tlsstr.equals("18")) {
				try {
					LinkedHashMap<String, Object> outputMap = new LinkedHashMap<String, Object>();
					int tlsIndex = 0;
					String TLSContent = isValidContentType(hexdump.substring(tlsIndex, tlsIndex + 2));
					outputMap.put("Content Type", TLSContent);
					String TLSVer = isValidTLSVersion(hexdump.substring(tlsIndex + 2, tlsIndex + 2 + 4));
					if (TLSVer != null) {
						try {
							TLSLength = isValidLength(hexdump.substring(tlsIndex + 6, tlsIndex + 10));
							outputMap.put("Version", TLSVer);
							outputMap.put("Length", TLSLength + "");
							if (TLSContent.equalsIgnoreCase("Handshake(22)")) {
								String handshakeType = isValidHandshakeType(
										hexdump.substring(tlsIndex + 10, tlsIndex + 12));
								outputMap.put("Handshake Type", handshakeType);
								if (handshakeType.equals("Client Hello(1)")) {
									List<Object> list = new ArrayList<>();
									LinkedHashMap<String, Object> mapClientHello = new LinkedHashMap<String, Object>();
									String handshakeTypeLength = isValidHandshakeTypeLength(
											hexdump.substring(tlsIndex + 12, tlsIndex + 18));
									mapClientHello.put("Length", handshakeTypeLength);
									String handshakeTypeVersion = isValidHandshakeTypeVersion(
											hexdump.substring(tlsIndex + 18, tlsIndex + 22));
									mapClientHello.put("Version", handshakeTypeVersion);
									String gmtUnixTime = isValidgmtUnixTime(
											hexdump.substring(tlsIndex + 22, tlsIndex + 30));
									mapClientHello.put("GMT Unix Time", gmtUnixTime);
									mapClientHello.put("Random Bytes:",
											hexdump.substring(tlsIndex + 30, tlsIndex + 86));
									int sessionIdLength = isValidSessionIdLength(
											hexdump.substring(tlsIndex + 86, tlsIndex + 88));
									mapClientHello.put("Session ID Length", sessionIdLength + "");
									mapClientHello.put("Session ID",
											hexdump.substring(tlsIndex + 88, tlsIndex + 88 + (sessionIdLength * 2)));
									int cipherSuitsLength = isValidSessionIdLength(
											hexdump.substring(tlsIndex + 88 + (sessionIdLength * 2),
													((tlsIndex + 88 + (sessionIdLength * 2)) + 4)));
									mapClientHello.put("Cipher Suites Length", cipherSuitsLength + "");
									int cipherSuits = ((tlsIndex + 88 + (sessionIdLength * 2)) + 4);
									int compressionMethodLength = isValidCompressionLength(
											hexdump.substring(cipherSuits + (cipherSuitsLength * 2),
													((cipherSuits + (cipherSuitsLength * 2)) + 2)));
									mapClientHello.put("Compression Methods Length", compressionMethodLength + "");

									int cipherRange = (cipherSuits + (cipherSuitsLength * 2)) + 2;
									List compressionMethodList = isValidCompressionmethodList(hexdump
											.substring(cipherRange, cipherRange + (compressionMethodLength * 2)));
									mapClientHello.put("Compression Methods", compressionMethodList);

									int extensionLength = isValidExtensionLength(
											hexdump.substring(cipherRange + (compressionMethodLength * 2),
													(cipherRange + (compressionMethodLength * 2)) + 4));
									mapClientHello.put("Extensions Length", extensionLength + "");

									String hex = hexdump.substring(cipherRange + 6,
											(cipherRange + 6) + (extensionLength * 2));
									String hex2 = hex;
									int dataLength1 = 0;

									List<Object> l1 = new LinkedList<>();
									for (int i = 0; i < hex2.length();) {
										LinkedHashMap<String, String> mapServerHello1 = new LinkedHashMap<String, String>();
										try {
											String extensionType1 = isValidExtensionType(hex2.substring(i, i + 4));
											mapServerHello1.put("Type", extensionType1);
											dataLength1 = isValidDataLength(hex2.substring(i + 4, i + 4 + 4));
											mapServerHello1.put("Data length", dataLength1 + "");
											String data1 = (hex2.substring(i + 8, (i + 8) + dataLength1 * 2));
											if (dataLength1 == 0)
												mapServerHello1.put("Data", "0 bytes");
											else
												mapServerHello1.put("Data", dataLength1 + " bytes");
											l1.add(mapServerHello1);
											hex2 = hex2.substring((i + 8) + dataLength1 * 2);

										} catch (Exception e) {
										}
									}

									mapClientHello.put("Extension", l1);

									list.add(mapClientHello);
									if (list.size() != 0)
										outputMap.put("Handshake Protocol", list.get(0));
								} else if (handshakeType.equals("Hello Request(0)")) {
									String handshakeTypeLength = isValidHandshakeTypeLength(
											hexdump.substring(tlsIndex + 12, tlsIndex + 18));
									outputMap.put("Handshake Type Length", handshakeTypeLength);
								} else if (handshakeType.equals("Server Hello(2)")) {
									List<Object> list = new ArrayList<>();
									LinkedHashMap<String, Object> mapServerHello = new LinkedHashMap<String, Object>();
									String handshakeTypeLength = isValidHandshakeTypeLength(
											hexdump.substring(tlsIndex + 12, tlsIndex + 18));
									mapServerHello.put("Length", handshakeTypeLength);
									String handshakeTypeVersion = isValidHandshakeTypeVersion(
											hexdump.substring(tlsIndex + 18, tlsIndex + 22));
									mapServerHello.put("Version", handshakeTypeVersion);
									String gmtUnixTime = isValidgmtUnixTime(
											hexdump.substring(tlsIndex + 22, tlsIndex + 30));
									mapServerHello.put("GMT Unix Time", gmtUnixTime);
									mapServerHello.put("Random Bytes:",
											hexdump.substring(tlsIndex + 30, tlsIndex + 86));
									int sessionIdLength = isValidSessionIdLength(
											hexdump.substring(tlsIndex + 86, tlsIndex + 88));
									mapServerHello.put("Session ID Length", sessionIdLength + "");
									if (sessionIdLength != 0)
										mapServerHello.put("Session ID", hexdump.substring(tlsIndex + 88,
												tlsIndex + 88 + (sessionIdLength * 2)));
									String cipherSuits = isValidCipherSuits(
											hexdump.substring(tlsIndex + 88 + (sessionIdLength * 2),
													((tlsIndex + 88 + (sessionIdLength * 2)) + 4)));
									mapServerHello.put("Cipher Suite", cipherSuits);
									int cipherRange = ((tlsIndex + 88 + (sessionIdLength * 2)) + 4);
									String compressionMethod = isValidCompressionmethod(
											hexdump.substring(cipherRange, cipherRange + 2));
									mapServerHello.put("Compression Method", compressionMethod);
									int extensionLength = isValidExtensionLength(
											hexdump.substring(cipherRange + 2, (cipherRange + 2) + 4));
									mapServerHello.put("Extensions Length", extensionLength + "");
									String hex = hexdump.substring(cipherRange + 6,
											(cipherRange + 6) + (extensionLength * 2));
									String hex2 = hex;
									int dataLength1 = 0;
									List<Object> l1 = new LinkedList<>();
									for (int i = 0; i < hex2.length();) {
										LinkedHashMap<String, String> mapServerHello1 = new LinkedHashMap<String, String>();
										try {
											String extensionType1 = isValidExtensionType(hex2.substring(i, i + 4));
											mapServerHello1.put("Type", extensionType1);
											dataLength1 = isValidDataLength(hex2.substring(i + 4, i + 4 + 4));
											mapServerHello1.put("Data length", dataLength1 + "");
											String data1 = (hex2.substring(i + 8, (i + 8) + dataLength1 * 2));
											if (dataLength1 == 0)
												mapServerHello1.put("Data", "0 bytes");
											else
												mapServerHello1.put("Data", data1);
											l1.add(mapServerHello1);
											hex2 = hex2.substring((i + 8) + dataLength1 * 2);

										} catch (Exception e) {
										}
									}
									mapServerHello.put("Extension", l1);

									list.add(mapServerHello);
									if (list.size() != 0)
										outputMap.put("Handshake Protocol", list.get(0));
								} else if (handshakeType.equals("New Session Ticket(4)")) {
									List<Object> list = new ArrayList<>();
									LinkedHashMap<String, Object> mapServerHello = new LinkedHashMap<String, Object>();
									String handshakeTypeLength = isValidHandshakeTypeLength(
											hexdump.substring(tlsIndex + 12, tlsIndex + 18));
									mapServerHello.put("Length", handshakeTypeLength);
									int sessionTicketLifetimeHint = isValidSessionTicketLifetimeHint(
											hexdump.substring(tlsIndex + 18, tlsIndex + 26));
									mapServerHello.put("Session Ticket Lifetime Hint", sessionTicketLifetimeHint + "");
									int sessionTicketLength = isValidSessionTicketLength(
											hexdump.substring(tlsIndex + 26, tlsIndex + 30));
									mapServerHello.put("Session Ticket Length", sessionTicketLength + "");
									mapServerHello.put("Session Ticket", hexdump.substring(tlsIndex + 30,
											tlsIndex + 30 + (sessionTicketLength * 2)));
									list.add(mapServerHello);
									if (list.size() != 0)
										outputMap.put("Handshake Protocol", list.get(0));

								} else if (handshakeType.equals("Encrypted Extensions(TLS 1.3 Only)(8)")) {
									List<Object> list = new ArrayList<>();
									LinkedHashMap<String, Object> mapEncrypted = new LinkedHashMap<String, Object>();
									String handshakeTypeLength = isValidHandshakeTypeLength(
											hexdump.substring(tlsIndex + 12, tlsIndex + 18));
									mapEncrypted.put("Length", handshakeTypeLength);
									int extensionsLength = isValidExtensionsLength(
											hexdump.substring(tlsIndex + 18, tlsIndex + 22));
									mapEncrypted.put("Extensions Length", extensionsLength + "");
									list.add(mapEncrypted);
									if (list.size() != 0)
										outputMap.put("Handshake Protocol", list.get(0));
								} else if (handshakeType.equals("Certificate(11)")) {
									List<Object> list = new ArrayList<>();
									LinkedHashMap<String, Object> mapCertificate = new LinkedHashMap<String, Object>();
									String handshakeTypeLength = isValidHandshakeTypeLength(
											hexdump.substring(tlsIndex + 12, tlsIndex + 18));
									mapCertificate.put("Length", handshakeTypeLength);
									int certificatesLength = isValidCertificatesLength(
											hexdump.substring(tlsIndex + 18, tlsIndex + 24));
									mapCertificate.put("Certificates Length", certificatesLength + "");
									int certificateLength = isValidCertificateLength(
											hexdump.substring(tlsIndex + 24, tlsIndex + 30));
									mapCertificate.put("Certificate Length", certificateLength + "");
									list.add(mapCertificate);
									if (list.size() != 0)
										outputMap.put("Handshake Protocol", list.get(0));
								} else if (handshakeType.equals("Server Hello Done(14)")) {
									List<Object> list = new ArrayList<>();
									LinkedHashMap<String, Object> mapCertificate = new LinkedHashMap<String, Object>();
									String handshakeTypeLength = isValidHandshakeTypeLength(
											hexdump.substring(tlsIndex + 12, tlsIndex + 18));
									mapCertificate.put("Length", handshakeTypeLength);
									list.add(mapCertificate);
									if (list.size() != 0)
										outputMap.put("Handshake Protocol", list.get(0));
								} else if (handshakeType.equals("Client Key Exchange(16)")) {
									List<Object> list = new ArrayList<>();
									LinkedHashMap<String, Object> mapCertificate = new LinkedHashMap<String, Object>();
									String handshakeTypeLength = isValidHandshakeTypeLength(
											hexdump.substring(tlsIndex + 12, tlsIndex + 18));
									mapCertificate.put("Length", handshakeTypeLength);
									list.add(mapCertificate);
									if (list.size() != 0)
										outputMap.put("Handshake Protocol", list.get(0));
								} else if (handshakeType.equals("Certificate verify(15)")) {
									List<Object> list = new ArrayList<>();
									LinkedHashMap<String, Object> mapCertificate = new LinkedHashMap<String, Object>();
									String handshakeTypeLength = isValidHandshakeTypeLength(
											hexdump.substring(tlsIndex + 12, tlsIndex + 18));
									mapCertificate.put("Length", handshakeTypeLength);
									list.add(mapCertificate);
									if (list.size() != 0)
										outputMap.put("Handshake Protocol", list.get(0));
								}
							} else if (TLSContent.equalsIgnoreCase("Application Data(23)")) {
								outputMap.put("Encrypted Application Data",
										hexdump.substring(tlsIndex + 10, tlsIndex + 10 + (TLSLength * 2)));
							} else if (TLSContent.equalsIgnoreCase("Heartbeat(24)")) {
								List<Object> list = new ArrayList<>();
								LinkedHashMap<String, Object> heartbeatMsg = new LinkedHashMap<String, Object>();
								String heartBeatType = isValidHeartBeatType(
										hexdump.substring(tlsIndex + 10, tlsIndex + 10 + 2));
								heartbeatMsg.put("Type", heartBeatType);
								int payloadLength = isValidPayloadLength(
										hexdump.substring(tlsIndex + 12, tlsIndex + 16));
								heartbeatMsg.put("Payload Length", payloadLength + "");
								heartbeatMsg.put("Payload ", payloadLength + " bytes");
								heartbeatMsg.put("Padding and HMAC ", (TLSLength - (3 + payloadLength)) + " bytes");
								list.add(heartbeatMsg);
								if (list.size() != 0)
									outputMap.put("Heartbeat Message", list.get(0));
							} else if (TLSContent.equalsIgnoreCase("Alert(21)")) {
								List<Object> list = new ArrayList<>();
								LinkedHashMap<String, Object> heartbeatMsg = new LinkedHashMap<String, Object>();
								String alertLevel = isValidLevel(hexdump.substring(tlsIndex + 10, tlsIndex + 10 + 2));
								heartbeatMsg.put("Level", alertLevel);
								String alertDescription = isValidAlertDescription(
										hexdump.substring(tlsIndex + 12, tlsIndex + 14));
								heartbeatMsg.put("Description", alertDescription);
								list.add(heartbeatMsg);
								if (list.size() != 0)
									outputMap.put("Heartbeat Message", list.get(0));
							}
						} catch (Exception e) {
                            outputMap.put("Error", "TLS Malformed Packet - " +e.getMessage());
                            LOGGER.error("error "+e.getMessage());
						}
					}
					hexdump = hexdump.substring(((TLSLength * 2) + 10));
					tlsList.add(outputMap);
				} catch (Exception e) {
					LinkedHashMap<String, Object> outputMapError = new LinkedHashMap<String, Object>();
					outputMapError.put("Error", "TLS Malformed Packet - " +e.getMessage());
					tlsList = new ArrayList<>();
					tlsList.add(outputMapError);
					hexdump = "";
					LOGGER.error("error tls "+e.getMessage());
				}
				tlsObj.put("TLS", tlsList);
			}
		} catch (IndexOutOfBoundsException e) {
		}
		return tlsObj;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static List isValidCompressionmethodList(String compressionmethodHex) throws CustomException {
		if (compressionmethodHex == null) {
			throw new CustomException("Compression method string cannot be null",1201);
		} else {
			int i;
			List<Object> compressionList = new LinkedList<>();
			for (i = 0; i < compressionmethodHex.length();) {
				try {
					LinkedHashMap<String, Object> compressionobj = new LinkedHashMap<>();
					int compressionMethod = 0;
					compressionMethod = Integer.parseInt(compressionmethodHex.substring(i, i + 2), 16);
					if (compressionMethod == 0) {
						compressionobj.put("Compression Method", "null" + "(" + compressionMethod + ")");
					} else if (compressionMethod == 1) {
						compressionobj.put("Compression Method", "DEFLATE" + "(" + compressionMethod + ")");
					} else if ((compressionMethod >= 2 && compressionMethod <= 63)
							|| (compressionMethod >= 65 && compressionMethod <= 223)) {
						compressionobj.put("Compression Method", "Unassigned" + "(" + compressionMethod + ")");
					} else if (compressionMethod == 64) {
						compressionobj.put("Compression Method", "LZS" + "(" + compressionMethod + ")");
					} else if (compressionMethod >= 224 && compressionMethod <= 255) {
						compressionobj.put("Compression Method",
								"Reserved for Private Use" + "(" + compressionMethod + ")");
					} else {
						throw new CustomException("Error in compression method string",1201);
					}
					compressionList.add(compressionobj);
					compressionmethodHex = compressionmethodHex.substring(i + 2);
				} catch (Exception e) {
				}
			}
			if (compressionList.size() != 0)
				return compressionList;
			else
				throw new CustomException("Exception in compression method string",1201);
		}

	}

	private static int isValidDataLength(String dataLengthHex) throws CustomException {
		if (dataLengthHex == null) {
			throw new CustomException("Data length string cannot be null",1201);
		} else {
			int compressionMethod = 0;
			compressionMethod = Integer.parseInt(dataLengthHex, 16);
			return compressionMethod;
		}
	}

	private static String isValidExtensionType(String extensionTypeHex) throws CustomException {
		if (extensionTypeHex == null) {
			throw new CustomException("Extension type string cannot be null",1201);
		} else {
			int compressionMethod = 0;
			compressionMethod = Integer.parseInt(extensionTypeHex, 16);
			if (compressionMethod == 0) {
				return "server_name" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 1) {
				return "max_fragment_length" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 2) {
				return "client_certificate_url" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 3) {
				return "trusted_ca_keys" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 4) {
				return "truncated_hmac" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 5) {
				return "status_request" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 6) {
				return "user_mapping" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 7) {
				return "client_authz" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 8) {
				return "server_authz" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 9) {
				return "cert_type" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 10) {
				return "supported_groups (renamed from \"elliptic_curves\")" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 11) {
				return "ec_point_formats" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 12) {
				return "srp" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 13) {
				return "signature_algorithms" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 14) {
				return "use_srtp" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 15) {
				return "heartbeat" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 16) {
				return "application_layer_protocol_negotiation" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 17) {
				return "status_request_v2" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 18) {
				return "signed_certificate_timestamp" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 19) {
				return "client_certificate_type" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 20) {
				return "server_certificate_type" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 21) {
				return "padding" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 22) {
				return "encrypt_then_mac" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 23) {
				return "extended_master_secret" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 24) {
				return "token_binding" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 25) {
				return "cached_info" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 26) {
				return "tls_lts" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 27) {
				return "compress_certificate" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 28) {
				return "record_size_limit" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 29) {
				return "pwd_protect" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 30) {
				return "pwd_clear" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 31) {
				return "password_salt" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 32) {
				return "ticket_pinning" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 33) {
				return "tls_cert_with_extern_psk" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 34) {
				return "delegated_credentials" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 35) {
				return "session_ticket (renamed from \"SessionTicket TLS\")" + "(" + compressionMethod + ")";
			} else if (compressionMethod >= 36 && compressionMethod <= 40) {
				return "Unassigned" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 41) {
				return "pre_shared_key" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 42) {
				return "early_data" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 43) {
				return "supported_versions" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 44) {
				return "cookie" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 45) {
				return "psk_key_exchange_modes" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 46) {
				return "Unassigned" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 47) {
				return "certificate_authorities" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 48) {
				return "oid_filters" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 49) {
				return "post_handshake_auth" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 50) {
				return "signature_algorithms_cert" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 51) {
				return "key_share" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 52) {
				return "transparency_info" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 53) {
				return "connection_id (TEMPORARY - registered 2019-07-02, expires 2020-07-02)" + "(" + compressionMethod
						+ ")";
			} else if (compressionMethod == 54) {
				return "Unassigned" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 55) {
				return "external_id_hash" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 56) {
				return "external_session_id" + "(" + compressionMethod + ")";
			} else if ((compressionMethod >= 57 && compressionMethod <= 2569)
					|| (compressionMethod >= 2571 && compressionMethod <= 6681)
					|| (compressionMethod >= 6683 && compressionMethod <= 10793)
					|| (compressionMethod >= 10795 && compressionMethod <= 14905)
					|| (compressionMethod >= 14907 && compressionMethod <= 19017)
					|| (compressionMethod >= 19019 && compressionMethod <= 23129)
					|| (compressionMethod >= 23131 && compressionMethod <= 27241)
					|| (compressionMethod >= 27243 && compressionMethod <= 31353)
					|| (compressionMethod >= 31355 && compressionMethod <= 35465)
					|| (compressionMethod >= 35467 && compressionMethod <= 39577)
					|| (compressionMethod >= 39579 && compressionMethod <= 43689)
					|| (compressionMethod >= 43691 && compressionMethod <= 47801)
					|| (compressionMethod >= 47803 && compressionMethod <= 51913)
					|| (compressionMethod >= 51915 && compressionMethod <= 56025)
					|| (compressionMethod >= 56027 && compressionMethod <= 60137)
					|| (compressionMethod >= 60139 && compressionMethod <= 64249)
					|| (compressionMethod >= 64251 && compressionMethod <= 65279)) {
				return "Unassigned" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 2570 || compressionMethod == 6682 || compressionMethod == 10794
					|| compressionMethod == 14906 || compressionMethod == 19018 || compressionMethod == 23130
					|| compressionMethod == 31354 || compressionMethod == 35466 || compressionMethod == 39578
					|| compressionMethod == 43690 || compressionMethod == 47802 || compressionMethod == 51914
					|| compressionMethod == 56026 || compressionMethod == 60138 || compressionMethod == 64250) {
				return "Reserved" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 65280) {
				return "Reserved for Private Use" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 65281) {
				return "renegotiation_info" + "(" + compressionMethod + ")";
			} else if ((compressionMethod >= 65282 && compressionMethod >= 65535)) {
				return "Reserved for Private Use" + "(" + compressionMethod + ")";
			} else {
				throw new CustomException("Error in Extension type",1201);
			}
		}
	}

	private static String isValidAlertDescription(String substring) throws CustomException {
		int isValidAlertDescription = Integer.parseInt(substring, 16);
		if (isValidAlertDescription == 0) {
			return "Close Notify" + "(" + isValidAlertDescription + ")";
		} else if (isValidAlertDescription == 10) {
			return "Unexpected message" + "(" + isValidAlertDescription + ")";
		} else if (isValidAlertDescription == 20) {
			return "Bad record MAC" + "(" + isValidAlertDescription + ")";
		} else if (isValidAlertDescription == 21) {
			return "Decryption failed" + "(" + isValidAlertDescription + ")";
		} else if (isValidAlertDescription == 22) {
			return "Record overflow" + "(" + isValidAlertDescription + ")";
		} else if (isValidAlertDescription == 30) {
			return "Decompression failure" + "(" + isValidAlertDescription + ")";
		} else if (isValidAlertDescription == 40) {
			return "Handshake failure" + "(" + isValidAlertDescription + ")";
		} else if (isValidAlertDescription == 41) {
			return "No certificate" + "(" + isValidAlertDescription + ")";
		} else if (isValidAlertDescription == 42) {
			return "Bad certificate" + "(" + isValidAlertDescription + ")";
		} else if (isValidAlertDescription == 43) {
			return "Unsupported certificate" + "(" + isValidAlertDescription + ")";
		} else if (isValidAlertDescription == 44) {
			return "Certificate revoked" + "(" + isValidAlertDescription + ")";
		} else if (isValidAlertDescription == 45) {
			return "Certificate expired" + "(" + isValidAlertDescription + ")";
		} else if (isValidAlertDescription == 46) {
			return "Certificate unknown" + "(" + isValidAlertDescription + ")";
		} else if (isValidAlertDescription == 47) {
			return "Illegal parameter" + "(" + isValidAlertDescription + ")";
		} else if (isValidAlertDescription == 48) {
			return "Unknown CA(Certificate authority)" + "(" + isValidAlertDescription + ")";
		} else if (isValidAlertDescription == 49) {
			return "Access denied" + "(" + isValidAlertDescription + ")";
		} else if (isValidAlertDescription == 50) {
			return "Decode error" + "(" + isValidAlertDescription + ")";
		} else if (isValidAlertDescription == 51) {
			return "Decrypt error" + "(" + isValidAlertDescription + ")";
		} else if (isValidAlertDescription == 60) {
			return "Expert restriction" + "(" + isValidAlertDescription + ")";
		} else if (isValidAlertDescription == 70) {
			return "Protocol version" + "(" + isValidAlertDescription + ")";
		} else if (isValidAlertDescription == 71) {
			return "Insufficient security" + "(" + isValidAlertDescription + ")";
		} else if (isValidAlertDescription == 80) {
			return "Internal error" + "(" + isValidAlertDescription + ")";
		} else if (isValidAlertDescription == 86) {
			return "Inappropriate Fallback" + "(" + isValidAlertDescription + ")";
		} else if (isValidAlertDescription == 90) {
			return "User cancelled" + "(" + isValidAlertDescription + ")";
		} else if (isValidAlertDescription == 100) {
			return "No renegotiation" + "(" + isValidAlertDescription + ")";
		} else if (isValidAlertDescription == 110) {
			return "Unsupported extension" + "(" + isValidAlertDescription + ")";
		} else if (isValidAlertDescription == 111) {
			return "Certificate unobtainable" + "(" + isValidAlertDescription + ")";
		} else if (isValidAlertDescription == 112) {
			return "Unrecognized name" + "(" + isValidAlertDescription + ")";
		} else if (isValidAlertDescription == 113) {
			return "Bad certificate status response" + "(" + isValidAlertDescription + ")";
		} else if (isValidAlertDescription == 114) {
			return "Bad certificate hash value" + "(" + isValidAlertDescription + ")";
		} else if (isValidAlertDescription == 115) {
			return "Unknown PSK identity(used in TLS-PSK and TLS-SRP)" + "(" + isValidAlertDescription + ")";
		} else if (isValidAlertDescription == 120) {
			return "No application protocol" + "(" + isValidAlertDescription + ")";
		} else {
			throw new CustomException("Malformed packet" + isValidAlertDescription,1201);
		}
	}

	private static String isValidLevel(String substring) throws CustomException {
		int isValidLevel = Integer.parseInt(substring, 16);
		if (isValidLevel == 1) {
			return "Warning" + "(" + isValidLevel + ")";
		} else if (isValidLevel == 2) {
			return "Fatal" + "(" + isValidLevel + ")";
		} else {
			throw new CustomException("Malformed packet " + isValidLevel,1201);
		}
	}

	private static int isValidPayloadLength(String substring) {
		int payloadLength = Integer.parseInt(substring, 16);
		return payloadLength;
	}

	private static String isValidHeartBeatType(String substring) throws CustomException {
		int isValidHeartBeatType = Integer.parseInt(substring, 16);
		if (isValidHeartBeatType == 1) {
			return "Request" + "(" + isValidHeartBeatType + ")";
		} else if (isValidHeartBeatType == 2) {
			return "Response" + "(" + isValidHeartBeatType + ")";
		} else {
			throw new CustomException("Malformed packet " + substring,1201);
		}
	}

	private static int isValidCertificateLength(String substring) {
		int certificateLength = Integer.parseInt(substring, 16);
		return certificateLength;
	}

	private static int isValidCertificatesLength(String substring) {
		int certificatesLength = Integer.parseInt(substring, 16);
		return certificatesLength;
	}

	private static int isValidExtensionsLength(String substring) {
		int sessionTicketLength = Integer.parseInt(substring, 16);
		return sessionTicketLength;
	}

	private static int isValidSessionTicketLength(String substring) {
		int sessionTicketLength = Integer.parseInt(substring, 16);
		return sessionTicketLength;
	}

	private static int isValidSessionTicketLifetimeHint(String sessiontkthex) {
		int compressionMethod = Integer.parseInt(sessiontkthex, 16);
		return compressionMethod;
	}

	private static int isValidExtensionLength(String ExtensionLengthHex) throws CustomException {
		int extensionLength = 0;
		extensionLength = Integer.parseInt(ExtensionLengthHex, 16);
		return extensionLength;
	}

	private static String isValidCompressionmethod(String compressionMethodHex) throws CustomException {
		if (compressionMethodHex == null) {
			throw new CustomException("Compression method string cannot be null",1201);
		} else {
			int compressionMethod = 0;
			compressionMethod = Integer.parseInt(compressionMethodHex, 16);
			if (compressionMethod == 0) {
				return "null" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 1) {
				return "DEFLATE" + "(" + compressionMethod + ")";
			} else if ((compressionMethod >= 2 && compressionMethod <= 63)
					|| (compressionMethod >= 65 && compressionMethod <= 223)) {
				return "Unassigned" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 64) {
				return "LZS" + "(" + compressionMethod + ")";
			} else if (compressionMethod >= 224 && compressionMethod <= 255) {
				return "Reserved for Private Use" + "(" + compressionMethod + ")";
			} else {
				throw new CustomException("Exception in compression method string",1201);
			}
		}
	}

	private static String isValidCipherSuits(String cipherHex) throws CustomException {
		String cipherSuit = "";
		cipherSuit = "0x" + cipherHex;
		return cipherSuit;
	}

	private static int isValidCompressionLength(String compressionMethodHex) throws CustomException {
		int compressionMethodLength = 0;
		compressionMethodLength = Integer.parseInt(compressionMethodHex, 16);
		return compressionMethodLength;
	}

	private static int isValidSessionIdLength(String sessionIdHex) throws CustomException {
		int sessionIDLength = 0;
		sessionIDLength = Integer.parseInt(sessionIdHex, 16);
		return sessionIDLength;
	}

	private static String isValidgmtUnixTime(String gmtString) throws CustomException {
		if (gmtString != null) {
			long gmtLong = 0;
			gmtLong = Integer.parseInt(gmtString, 16);
			Date date = new Date();
			date.setTime((long) gmtLong * 1000);
			return date + "";
		} else
			throw new CustomException("Malformed packet",1201);
	}

	private static String isValidHandshakeTypeVersion(String versionHex) throws CustomException {
		String version = null;
		if ((versionHex.substring(3, 4).equals("0")))
			version = "SSL 3.0" + "(" + "0x" + versionHex + ")";
		else if ((versionHex.substring(3, 4).equals("1")))
			version = "TLS 1.0" + "(" + "0x" + versionHex + ")";
		else if ((versionHex.substring(3, 4).equals("2")))
			version = "TLS 1.1" + "(" + "0x" + versionHex + ")";
		else if ((versionHex.substring(3, 4).equals("3")))
			version = "TLS 1.2" + "(" + "0x" + versionHex + ")";
		else
			version = "Unknown" + "(" + "0x" + versionHex + ")";
		return version;
	}

	private static String isValidHandshakeTypeLength(String lengthHex) throws CustomException {
		String lengthString = null;
		lengthString = Integer.parseInt(lengthHex, 16) + "";
		return lengthString;
	}

	private static String isValidHandshakeType(String handShakeHex) throws CustomException {
		String handShakeString = null, handshakeType = null;
		handShakeString = Integer.parseInt(handShakeHex, 16) + "";
		if (handShakeString.equals("0")) {
			handshakeType = "Hello Request" + "(" + handShakeString + ")";
			return handshakeType;
		} else if (handShakeString.equals("1")) {
			handshakeType = "Client Hello" + "(" + handShakeString + ")";
			return handshakeType;
		} else if (handShakeString.equals("2")) {
			handshakeType = "Server Hello" + "(" + handShakeString + ")";
			return handshakeType;
		} else if (handShakeString.equals("4")) {
			handshakeType = "New Session Ticket" + "(" + handShakeString + ")";
			return handshakeType;
		} else if (handShakeString.equals("8")) {
			handshakeType = "Encrypted Extensions(TLS 1.3 Only)" + "(" + handShakeString + ")";
			return handshakeType;
		} else if (handShakeString.equals("11")) {
			handshakeType = "Certificate" + "(" + handShakeString + ")";
			return handshakeType;
		} else if (handShakeString.equals("8")) {
			handshakeType = "Server Key Exchange" + "(" + handShakeString + ")";
			return handshakeType;
		} else if (handShakeString.equals("13")) {
			handshakeType = "Certificate request" + "(" + handShakeString + ")";
			return handshakeType;
		} else if (handShakeString.equals("14")) {
			handshakeType = "Server Hello Done" + "(" + handShakeString + ")";
			return handshakeType;
		} else if (handShakeString.equals("15")) {
			handshakeType = "Certificate verify" + "(" + handShakeString + ")";
			return handshakeType;
		} else if (handShakeString.equals("16")) {
			handshakeType = "Client Key Exchange" + "(" + handShakeString + ")";
			return handshakeType;
		} else if (handShakeString.equals("20")) {
			handshakeType = "Finished" + "(" + handShakeString + ")";
			return handshakeType;
		} else {
			throw new CustomException("Malformed packet",1201);
		}
	}

	private static int isValidLength(String lengthHex) throws CustomException {
		if (lengthHex.length() != 4)
			throw new CustomException("Bit length is not valid " + lengthHex + " it must be 4 digits",1201);
		else {
			int lengthString = 0;
			lengthString = Integer.parseInt(lengthHex, 16);
			return lengthString;
		}
	}

	private static String isValidTLSVersion(String versionHex) throws CustomException {
		String version = null;
		if (versionHex.length() != 4) {
			throw new CustomException("Version length must be 4 digits " + versionHex,1201);
		} else if (!((versionHex.substring(1, 2).equals("3")) && (versionHex.substring(0, 1).equals("0"))
				&& ((versionHex.substring(2, 3).equals("0")) || (versionHex.substring(2, 3).equals("1"))
						|| (versionHex.substring(2, 3).equals("2")) || (versionHex.substring(2, 3).equals("3"))))) {
			throw new CustomException("Version not valid " + versionHex,1201);
		} else {
			if ((versionHex.substring(3, 4).equals("0"))) {
				version = "SSL 3.0" + "(" + "0x" + versionHex + ")";
				return version;
			} else if ((versionHex.substring(3, 4).equals("1"))) {
				version = "TLS 1.0" + "(" + "0x" + versionHex + ")";
				return version;
			} else if ((versionHex.substring(3, 4).equals("2"))) {
				version = "TLS 1.1" + "(" + "0x" + versionHex + ")";
				return version;
			} else if ((versionHex.substring(3, 4).equals("3"))) {
				version = "TLS 1.2" + "(" + "0x" + versionHex + ")";
				return version;
			}

			else
				return version;

		}
	}

	private static String isValidContentType(String contentTypeHex)
			throws CustomException, DecoderException, UnsupportedEncodingException {
		String contentTypeString = null, contentType = null;
		if (contentTypeHex.length() != 2)
			throw new CustomException("Bit length is not valid " + contentTypeHex + " it must be 2 digits",1201);
		else {
			contentTypeString = Integer.parseInt(contentTypeHex, 16) + "";
			if (contentTypeString.equals("20")) {
				contentType = "Change Cipher Spec" + "(" + contentTypeString + ")";
				return contentType;
			} else if (contentTypeString.equals("21")) {
				contentType = "Alert" + "(" + contentTypeString + ")";
				return contentType;
			} else if (contentTypeString.equals("22")) {
				contentType = "Handshake" + "(" + contentTypeString + ")";
				return contentType;
			} else if (contentTypeString.equals("23")) {
				contentType = "Application Data" + "(" + contentTypeString + ")";
				return contentType;
			} else if (contentTypeString.equals("24")) {
				contentType = "Heartbeat" + "(" + contentTypeString + ")";
				return contentType;
			} else
				throw new CustomException("Unknown Handshake content type ",1201);
		}
	}

}