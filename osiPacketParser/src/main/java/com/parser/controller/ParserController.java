package com.parser.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.parser.commonFunction.CommonFunction;
import com.parser.customError.CustomErrorType;
import com.parser.customError.CustomException;
import com.parser.service.ParserService;

@RestController
@RequestMapping(path = "/v1")

/**
 * @author AmantyaTech
 *
 */
public class ParserController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ParserController.class);

	/**
	 * @param ipObj
	 * @return
	 * @throws CustomException 
	 */

	@SuppressWarnings("unchecked")
	@PostMapping(value = "/possiblePacket", produces = MediaType.APPLICATION_JSON_VALUE)
	@CrossOrigin(origins = "*")
	public @ResponseBody ResponseEntity<?> getPossiblePackets(@RequestParam("hexdump") String hexdump,@RequestParam("toView") String toView) throws CustomException {
try {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		List<Object> toViewList = new ArrayList<>();
		if (toView.length() != 0)
			toViewList = Arrays.asList(toView.split(","));
		List<Object> ipPacket= ParserService.getPossiblePackets(toViewList,hexdump);
		List<Object> list = new ArrayList<>();
		ipPacketNodeCreation(list, gson, ipPacket);
		
		if (ipPacket.isEmpty()) {
			return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<Object>(list, HttpStatus.OK);
		}
	}catch (CustomException e) {
		String getAttribute = CommonFunction.getErrorResponse(e.getMessage(), e.getCode());
		LOGGER.error("error response {}", getAttribute, e);
		if (e.getCode() == 422)
			return new ResponseEntity<>(getAttribute, HttpStatus.UNPROCESSABLE_ENTITY);
		else if (e.getCode() == 400)
			return new ResponseEntity<>(getAttribute, HttpStatus.BAD_REQUEST);
		else
			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
	}
	
	}
	
	@PostMapping(value = "/multiplePossiblePacket", produces = MediaType.APPLICATION_JSON_VALUE)
	@CrossOrigin(origins = "*")
	public @ResponseBody ResponseEntity<?>fileUploadforMultiple(@RequestParam("file") MultipartFile file,
			@RequestParam("toView") String toView) throws IOException {
		
		List<Object> toViewList = new ArrayList<>();
		if (toView.length() != 0)
			toViewList = Arrays.asList(toView.split(","));
			LOGGER.info("User file input " + file);
		try {
			if (file.isEmpty())
				return new ResponseEntity<>(new CustomErrorType("File is Empty"), HttpStatus.BAD_REQUEST);
			else {
				List<Object> list = new ArrayList<>();
				String fileString = CommonFunction.getFileString(file);
				Gson gson = new GsonBuilder().setPrettyPrinting().create();
				List<Object> ipPacket = ParserService.getPossiblePackets(toViewList, fileString);
				ipPacketNodeCreation(list, gson, ipPacket);
				return new ResponseEntity<>(list, HttpStatus.OK);
			}

		} catch (CustomException e) {
			String getAttribute = CommonFunction.getErrorResponse(e.getMessage(), e.getCode());
			LOGGER.error("controller error " + getAttribute);
			if (e.getCode() == 422)
				return new ResponseEntity<>(getAttribute, HttpStatus.UNPROCESSABLE_ENTITY);
			else if (e.getCode() == 400)
				return new ResponseEntity<>(getAttribute, HttpStatus.BAD_REQUEST);
			else
				return ResponseEntity.status(1201).body(getAttribute);
		}
	}
			

	@SuppressWarnings({ "unchecked" })
	private void ipPacketNodeCreation(List<Object> list, Gson gson, List<Object> ipPacket) throws CustomException {
		try {
			LOGGER.info("node creation method input " + ipPacket);
			Map<Object, Object> ipObject = null;
			for (int i = 0; i < ipPacket.size(); i++) {
				ipObject = CommonFunction.getNodeObject(ipPacket.get(i), i);
				if (ipObject != null)
					list.add(ipObject);
			}

			LOGGER.info("node creation output " + list);
		} catch (Exception e) {
			LOGGER.error("error in node creation output " + e.getMessage());
		}
	}

}