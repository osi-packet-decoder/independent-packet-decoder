package com.parser.model;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;

//@JsonInclude(JsonInclude.Include.NON_ABSENT)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class TCPPacket {
	private String sourcePort;
	private String destinationPort;
	private String sequenceNumber;
	private String acknowledgementNumber;
	private String headerlength;
	private String windowSize;
	private String checksum;
	private String urgentPointer;	
	private String protocol;
	private String payload;
	private String flagsBin;
	private String error;
	
	private Map<String,String> httpPacket;

	
		
	public Map<String, String> getHttpPacket() {
		return httpPacket;
	}
	public void setHttpPacket(Map<String, String> httpPacket) {
		this.httpPacket = httpPacket;
	}
	public String getSourcePort() {
		return sourcePort;
	}
	public void setSourcePort(String sourcePort) {
		this.sourcePort = sourcePort;
	}
	public String getDestinationPort() {
		return destinationPort;
	}
	public void setDestinationPort(String destinationPort) {
		this.destinationPort = destinationPort;
	}
	public String getSequenceNumber() {
		return sequenceNumber;
	}
	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}
	public String getAcknowledgementNumber() {
		return acknowledgementNumber;
	}
	public void setAcknowledgementNumber(String acknowledgementNumber) {
		this.acknowledgementNumber = acknowledgementNumber;
	}
	public String getHeaderlength() {
		return headerlength;
	}
	public void setHeaderlength(String headerlength) {
		this.headerlength = headerlength;
	}
	public String getWindowSize() {
		return windowSize;
	}
	public void setWindowSize(String windowSize) {
		this.windowSize = windowSize;
	}
	public String getChecksum() {
		return checksum;
	}
	public void setChecksum(String checksum) {
		this.checksum = checksum;
	}
	public String getUrgentPointer() {
		return urgentPointer;
	}
	public void setUrgentPointer(String urgentPointer) {
		this.urgentPointer = urgentPointer;
	}
	
	public String getProtocol() {
		return protocol;
	}
	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}
	
	public String getPayload() {
		return payload;
	}
	public void setPayload(String payload) {
		this.payload = payload;
	}
	
	public String getFlagsBin() {
		return flagsBin;
	}
	public void setFlagsBin(String flagsBin) {
		this.flagsBin = flagsBin;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	
	
	
}
