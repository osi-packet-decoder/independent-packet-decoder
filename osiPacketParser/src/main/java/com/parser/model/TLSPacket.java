package com.parser.model;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class TLSPacket {

	private String contentType;
	private String legacyVersion;
	private String lenght;
	private String protocolMessages;
	
	
	
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public String getLegacyVersion() {
		return legacyVersion;
	}
	public void setLegacyVersion(String legacyVersion) {
		this.legacyVersion = legacyVersion;
	}
	public String getLenght() {
		return lenght;
	}
	public void setLenght(String lenght) {
		this.lenght = lenght;
	}
	public String getProtocolMessages() {
		return protocolMessages;
	}
	public void setProtocolMessages(String protocolMessages) {
		this.protocolMessages = protocolMessages;
	}
	
	
}
