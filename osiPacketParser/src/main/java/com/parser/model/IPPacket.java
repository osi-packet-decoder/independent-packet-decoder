package com.parser.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_ABSENT)
//@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class IPPacket {
	private String hexDump;
	private List<Object> toView;
	private String version;
	private String headerLength;
	private String sourceIPAddress;
	private String destinationIPAddress;
	private String checkSum;
	private String totalLength;
	private String protocol;
	private String payload;	
	private String fragmentOffset;
	private String identification;
	private String timetolive;
	private String flags;
	private String fragFlags;
	private String Precedence;	
	private String fileString;
	private String error;
	
	private String sourceIPV6Address;
	private String destinationIPV6Address;
	private String trafficClass;
	private String flowLabel;
	private String payloadLength;
	private String nextHeader;
	private String transPortLayerProtocol;
	private String HopLimit;
	
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getHeaderLength() {
		return headerLength;
	}
	public void setHeaderLength(String headerLength) {
		this.headerLength = headerLength;
	}
	public String getSourceIPAddress() {
		return sourceIPAddress;
	}
	public void setSourceIPAddress(String sourceIPAddress) {
		this.sourceIPAddress = sourceIPAddress;
	}
	public String getDestinationIPAddress() {
		return destinationIPAddress;
	}
	public void setDestinationIPAddress(String destinationIPAddress) {
		this.destinationIPAddress = destinationIPAddress;
	}
	public String getCheckSum() {
		return checkSum;
	}
	public void setCheckSum(String checkSum) {
		this.checkSum = checkSum;
	}
	public String getTotalLength() {
		return totalLength;
	}
	public void setTotalLength(String totalLength) {
		this.totalLength = totalLength;
	}
	public String getProtocol() {
		return protocol;
	}
	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}
	public String getPayload() {
		return payload;
	}
	public void setPayload(String payload) {
		this.payload = payload;
	}

	
	public String getFlags() {
		return flags;
	}
	public void setFlags(String flags) {
		this.flags = flags;
	}
	public String getFragmentOffset() {
		return fragmentOffset;
	}
	public void setFragmentOffset(String fragmentOffset) {
		this.fragmentOffset = fragmentOffset;
	}
	public String getIdentification() {
		return identification;
	}
	public void setIdentification(String identification) {
		this.identification = identification;
	}
	public String getTimetolive() {
		return timetolive;
	}
	public void setTimetolive(String timetolive) {
		this.timetolive = timetolive;
	}
	

	public String getHexDump() {
		return hexDump;
	}
	public void setHexDump(String hexDump) {
		this.hexDump = hexDump;
	}
	public String getPrecedence() {
		return Precedence;
	}
	public void setPrecedence(String precedence) {
		Precedence = precedence;
	}
		public String getFragFlags() {
		return fragFlags;
	}
	public void setFragFlags(String fragFlags) {
		this.fragFlags = fragFlags;
	}
	public String getFileString() {
		return fileString;
	}
	public void setFileString(String fileString) {
		this.fileString = fileString;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public String getSourceIPV6Address() {
		return sourceIPV6Address;
	}
	public void setSourceIPV6Address(String sourceIPV6Address) {
		this.sourceIPV6Address = sourceIPV6Address;
	}
	public String getDestinationIPV6Address() {
		return destinationIPV6Address;
	}
	public void setDestinationIPV6Address(String destinationIPV6Address) {
		this.destinationIPV6Address = destinationIPV6Address;
	}
	public String getTrafficClass() {
		return trafficClass;
	}
	public void setTrafficClass(String trafficClass) {
		this.trafficClass = trafficClass;
	}
	public String getFlowLabel() {
		return flowLabel;
	}
	public void setFlowLabel(String flowLabel) {
		this.flowLabel = flowLabel;
	}
	public String getPayloadLength() {
		return payloadLength;
	}
	public void setPayloadLength(String payloadLength) {
		this.payloadLength = payloadLength;
	}
	public String getNextHeader() {
		return nextHeader;
	}
	public void setNextHeader(String nextHeader) {
		this.nextHeader = nextHeader;
	}
	public String getTransPortLayerProtocol() {
		return transPortLayerProtocol;
	}
	public void setTransPortLayerProtocol(String transPortLayerProtocol) {
		this.transPortLayerProtocol = transPortLayerProtocol;
	}
	public String getHopLimit() {
		return HopLimit;
	}
	public void setHopLimit(String hopLimit) {
		HopLimit = hopLimit;
	}
	public List<Object> getToView() {
		return toView;
	}
	public void setToView(List<Object> toView) {
		this.toView = toView;
	}
	

	
		
	
}
