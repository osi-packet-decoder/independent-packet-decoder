import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  constructor() {   
    
   }

  ngOnInit() {
   // this.protocolDiv=true;

    for( var i=0;i < this.options.length; i++) {
      if(this.options[i].checked)
         this.selected.push(this.options[i].name);
    }      
    var noDupe = Array.from(new Set(this.selected))
    localStorage.setItem("SelectedProtocol",noDupe.toString());  
  }
   count=0; 
  noDupe=[];
  selected = []
  messages = [];
  protocolDiv=false;
  toggleIPList = [];
  checkedvalArray: any = [];
  // check if the item are selected
  checked(item,name){
    //console.log("item...."+item);
    if(this.selected.indexOf(item) != -1){
      return true;
    }
  }
   closeNav() {
    document.getElementById("sidebar-wrapper").style.width = "0";
    //   this.protocolDiv=false;
    //  localStorage.removeItem("SelectedProtocol")
    //   for( var i=0;i < this.options.length; i++) {
    //     this.options[i].checked=false;
    //   }
  }
  selectedItem=false;
  CheckAllOptions() {
    document.getElementById("sidebar-wrapper").style.width = "250px";
    this.protocolDiv=true;    
    this.selectedItem=true;
    this.selected=[];
   // console.log("ccccccccccccccccc",this.options.every(val => val.checked))
    for( var i=0;i < this.options.length; i++) {
      //console.log("aaaa...",this.options[i].checked,this.options[i].name)
       // this.options[i].checked = true;
       if( this.options[i].checked)
        this.selected.push(this.options[i].name);    
    }   
 }  

 
    options=[ 
               { name: 'All', value: 'All', checked: true },              
               //{ name: 'IP', value: 'IP', checked: true },
              { name: 'IPv4', value: 'IPv4', checked: true },
              { name: 'IPv6', value: 'IPv6', checked: true },
              // { name: 'TCP', value: 'TCP', checked: true },
              // { name: 'UDP', value: 'UDP', checked: true },
              // { name: 'SCTP', value: 'SCTP', checked: true },
              // { name: 'ICMP', value: 'ICMP', checked: true },
              // { name: 'SMTP', value: 'SMTP', checked: true },
              { name: 'HTTP', value: 'HTTP', checked: true },
              { name: 'FTP', value: 'FTP', checked: true },
              { name: 'SIP', value: 'SIP', checked: true },
              { name: 'RTP', value: 'RTP', checked: true },
              // { name: 'TLS', value: 'TLS', checked: true },
              // { name: 'DTLS', value: 'DTLS', checked: true },
              { name: 'Diameter', value: 'Diameter', checked: true }
            ]
                
    // when checkbox change, add/remove the item from the array
    onChange(checked, item){

      if(checked){
        //console.log("if..", this.toggleIPList)
        if(item == "IPv4"){
          const index = this.toggleIPList.indexOf("IPv4");
          //console.log("index.."+index)
          if (index > -1) {
            this.toggleIPList.splice(index, 1);
          }
      }

      if(item == "IPv6"){
        const index2 = this.toggleIPList.indexOf("IPv6");
        //console.log("index2.."+index2)
        if (index2 > -1) {
          this.toggleIPList.splice(index2, 1);
        }
      }
       // console.log("toggleIPList..", this.toggleIPList)
    
        this.toggleIPList=[];this.selected=[]
        for( var i=0;i < this.options.length; i++) {
         // console.log("aaaa...",this.options[i].checked,this.options[i].name)
          
          if(item=="All"){
            this.options[i].checked = true;   
          }  

          if(this.options[i].checked)
          this.selected.push(this.options[i].name)
          var noDupee = Array.from(new Set(this.selected))         
         if(noDupee.length==7){
          if(this.options[0].name=="All"){
              this.options[0].checked = true;   
            }  
         }
          
             
        //    if(item=="TCP"||item=="UDP"||item=="SCTP"||item=="ICMP"){
        //     if(this.options[i].name=="IPv6" || this.options[i].name=="IPv4"){
        //       this.options[i].checked = true;
        //    }
        //   }

        //    if(item=="TLS"||item=="HTTP"||item=="SMTP"||item=="FTP"||item =="Diameter"){
        //     if(this.options[i].name=="IPv6" || this.options[i].name=="IPv4"){
        //       this.options[i].checked = true;
        //    }
        //     if(this.options[i].name=="TCP"){
        //       this.options[i].checked = true;
        //    }
        //   }

        //    if(item=="RTP"||item=="DTLS"){
        //     if(this.options[i].name=="IPv6" || this.options[i].name=="IPv4"){
        //       this.options[i].checked = true;
        //    }
        //     if(this.options[i].name=="UDP"){
        //       this.options[i].checked = true;
        //   }
        // }
      //    if(item=="SIP"){
      //     if(this.options[i].name=="IPv6" || this.options[i].name=="IPv4"){
      //       this.options[i].checked = true;
      //    }
      //     if(this.options[i].name=="UDP"||this.options[i].name=="TCP"){
      //       this.options[i].checked = true;
      //   }
      // }
      }
                
      }else{

       // console.log("else..",item)
        this.toggleIPList.push(item);
         this.noDupe = Array.from(new Set(this.toggleIPList))  
       // console.log(".toggleIPList ",this.noDupe)
        var ipv4="",ipv6="",tcp="",udp="",ip="";
        for(var i=0;i<=this.toggleIPList.length;i++){
          if(this.toggleIPList[i]=="IP"){
            ip="false";
          }
          if(this.toggleIPList[i]=="IPv4"){
            ipv4="false";
          }
          if(this.toggleIPList[i]=="IPv6"){
            ipv6="false";
          }
          if(this.toggleIPList[i]=="TCP"){
            tcp="false";
          }
          if(this.toggleIPList[i]=="UDP"){
            udp="false";
          }
        }        
        if(this.toggleIPList.length !=0){
          for( var i=0;i < this.options.length; i++) {      
            if(item=="All") 
              this.options[i].checked = false;       
              if(this.options[i].name=="All"){
                this.options[i].checked = false;
              }
            }
        }
        for( var i=0;i < this.options.length; i++) {
          if(ipv4=="false" && ipv6 =="false"){
              this.options[i].checked = false;              
          }

        if(tcp=="false" && udp =="false"){
          if(this.options[i].name=="SIP"){
            this.options[i].checked = false;
          }           
        }
         if(tcp=="false"){
            //console.log("tcp true")
            if(this.options[i].name=="TLS"||this.options[i].name=="HTTP"||this.options[i].name=="FTP"||this.options[i].name=="SMTP"||this.options[i].name=="Diameter"){
              this.options[i].checked = false;
            }
          }

           if(udp=="false"){
           // console.log("udp true")
            if(this.options[i].name=="RTP"||this.options[i].name=="DTLS")
              this.options[i].checked = false;
          }

          if(ip=="false"){
               this.options[i].checked = false;
           }

        }
      }

    }

    cancel(){
     // debugger
      document.getElementById("sidebar-wrapper").style.width = "0";
      this.protocolDiv=false;
     //localStorage.removeItem("SelectedProtocol")
      // for( var i=0;i < this.options.length; i++) {
      //   this.options[i].checked=false;
      // }

     for( var i=0;i < this.options.length; i++) {
        //  if( this.options[i].checked)
        //   this.selected.push(this.options[i].name);    

        for( var j=0;j < this.selected.length; j++) {
          if(this.options[i].name==this.selected[j]){
            //console.log(this.options[i].name,this.selected[j])
            this.options[i].checked = true;
          }

        }//selected
      }  

    
     }
 
    save(){   
      document.getElementById("sidebar-wrapper").style.width = "0";
      this.protocolDiv=false;
      this.selected=[];
      for( var i=0;i < this.options.length; i++) {
       // console.log("bbbbbbbbbb...",this.options[i].checked,this.options[i].name)
        if(this.options[i].checked)
        this.selected.push(this.options[i].name);
      }
      
     // console.log("submit selected..", this.selected) 
      var noDupe = Array.from(new Set(this.selected))
      //console.log("noDupe ",noDupe);  
      localStorage.setItem("SelectedProtocol",noDupe.toString());
      
    }

}
