package com.parser.commonFunction;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.parser.customError.CustomException;


public class CommonFunction {
	/**
	 * 
	 * @author AmantyaTech
	 * 
	 *      Used for adding nodes to a map object 	
	 * @param i 
	 * @throws CustomException 
	 *		   
	 */	
	private static final Logger LOGGER = LoggerFactory.getLogger(CommonFunction.class);
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static Map getNodeObject(Object object, int i) throws CustomException {
		
		LOGGER.info("Common function start "+object);
		
		 List<Object> li=new ArrayList<>();	
		 Map<Object, Object> rootObject =new LinkedHashMap<>();
		 Map<Object, Object> ipObject = new LinkedHashMap<>();
		 Map<Object, Object> tcpObject = new LinkedHashMap<>();
		 Map<Object, Object> udpObject = new LinkedHashMap<>();
		 Map<Object, Object> httpObject = new LinkedHashMap<>();
		 Map<Object, Object> sctpObject = new LinkedHashMap<>();
		 Map<Object, Object> icmpObject = new LinkedHashMap<>();
		 Map<Object, Object> ip6Object = new LinkedHashMap<>();
		 Map<Object, Object> ip4Object = new LinkedHashMap<>();
		 Map<Object, Object> errorObject = new LinkedHashMap<>();
		 List<String> errorlist = new ArrayList<>();
		 
	try {
		
		 int transportLayerProtocol =0,c1=0,error=0,applicationerror=0;		             
            Map<String, Object> map = (LinkedHashMap<String, Object>) object;
           // int i=0;
            
			for (String key1 : map.keySet()) {
				System.out.println("map  outer key "+key1);
				try {
				 if (key1.equalsIgnoreCase("Error")) {
					  String errorString = (String) map.get(key1);
					  LOGGER.info("Common function checking for error message "+errorString);
					     errorlist = Arrays.asList(errorString.split("-"));
					     errorObject.put(key1,map.get(key1));
				 }
				 
			if(key1.equalsIgnoreCase("IP")||key1.equalsIgnoreCase("IPv4")||key1.equalsIgnoreCase("IPv6"))	 {
				List<Object> IPList = (List<Object>) map.get(key1);							
				Map<String, Object> map2 = (Map<String, Object>) IPList.get(0);	
				
				for (String key : map2.keySet()) {
				 Map<Object, Object> errorMessage = new LinkedHashMap<>();				
				
				 if (key.equals("Hexdump")) {
					   c1 = 0 ;
				   }
				 
				 
				   if (key.equalsIgnoreCase("Error")) {
						
					  String errorString = (String) map2.get(key);
					  LOGGER.info("Common function checking for error message "+errorString);
					     errorlist = Arrays.asList(errorString.split("-"));
					   c1 = 10;
					   try {
							  if(map2.get("Transport Layer Protocol").equals("SCTP")) {
								   transportLayerProtocol=3;
								   error = 1;
								}
							  else if(map2.get("Transport Layer Protocol").equals("UDP")) {
								  
								   transportLayerProtocol=4;
								   error = 2;
							   }
							   else if(map2.get("Transport Layer Protocol").equals("TCP")) {
								   transportLayerProtocol=1;
								   error = 3;
							   }
							   else if(map2.get("Transport Layer Protocol").equals("ICMP")) {
								   transportLayerProtocol=9;
								   error = 4;
							   }
							  
							   
							  if(map2.get("Application Layer Protocol").equals("HTTP")) {
								  applicationerror=1;
							   }
							   					    
							 }catch (Exception e) {
								   
							}
				   }
				   
				   else if(key.equalsIgnoreCase("SMTP")||key.equalsIgnoreCase("TLS")|| key.equalsIgnoreCase("DTLS") || key.equalsIgnoreCase("FTP") ||key.equalsIgnoreCase("SIP")|| key.equalsIgnoreCase("Diameter Protocol")) {
					   Map<Object, Object> errorNodeTcp =null;     
						for (Object keytcp : map2.keySet()) {					
							if(keytcp.equals("SMTP") || keytcp.equals("TLS")|| keytcp.equals("DTLS") || keytcp.equals("FTP")|| keytcp.equals("SIP")|| keytcp.equals("Diameter Protocol")) {						
								List<Object> tcpList = (List<Object>) map2.get(keytcp);							
								errorNodeTcp = (Map<Object, Object>) tcpList.get(0);						
								if(errorNodeTcp.get("Error") !=null) {
									String errorString = (String) errorNodeTcp.get("Error");                   
									errorlist = Arrays.asList(errorString.split("-"));
								}
					   
							}
					   
						}
				   }
				   
				   else if(key.equalsIgnoreCase("RTP")||key.equalsIgnoreCase("Unsupported")) {	
					   Map<Object, Object> mapObj =  (Map<Object, Object>) map2.get(key);
						if(mapObj.get("Error") !=null) {
							String errorString = (String) mapObj.get("Error");    
							 errorlist = Arrays.asList(errorString.split("-"));
							//rootObject.put("IP Packet " + (i + 1)+":"+errorlist.get(0), ipObject);
						}
					}
				   
				    if(key.equalsIgnoreCase("Internet Protocol")) {	
						if(map2.get("Error") !=null) {							
							 error = 5;							
						}
					}
				 
				   if (key.equalsIgnoreCase("TCP Source Port")) {
					   	c1 = 1;
			    		transportLayerProtocol=1;
			    	}
				   if (key.equalsIgnoreCase("Method")){
						c1=2;
			    	}			    	
			    	if (key.equalsIgnoreCase("SCTP Source Port")){
						c1=3;
						transportLayerProtocol=3;
					}
			    	if (key.equalsIgnoreCase("UDP Source Port")) {
			    		c1 = 4;
			    		transportLayerProtocol=4;
			    	}
			    	if (key.equalsIgnoreCase("Type")) {
			    		c1 = 5;
			    		transportLayerProtocol=5;
					}
			    	if (key.equalsIgnoreCase("IP")) {
			    		c1 = 6;
					}
			    	if (key.equals("HexDump")) {
			    		c1 = 7;
					}
			    	
			    	if(c1==10)
						errorObject.put(key,map2.get(key));
			    	
			    	if(c1==0) {
						ip4Object.put(key,map2.get(key));
			    	}					
					
					if(c1==1||error==3){
						if(applicationerror==1) {
							if(!key.equals("Error"))
								tcpObject.put(key,map2.get(key));
						}
						else
						tcpObject.put(key,map2.get(key));									
					}

					if(c1==2||applicationerror==1){
						httpObject.put(key,map2.get(key));
					}
					
					if(c1==3||error==1){
						sctpObject.put(key,map2.get(key));
						//System.out.println("sctpObject..."+sctpObject);
					}
					if(c1==4||error==2) {
						udpObject.put(key,map2.get(key));
					}
					
					if(c1==5||error==4) {
						icmpObject.put(key,map2.get(key));
					}
					if(c1==7) {
						ip6Object.put(key,map2.get(key));
					}
					
					if(error==5) {
						if(!key.equals("Internet Protocol"))
						ip6Object.put(key,map2.get(key));
					}
					
					if(c1==6) {						
						List array = (List) map2.get("IP");					
						
				//********ICMP NODE creation...
					if(array !=null) {
						
						int transportLayerProtocolIC =0,ic=0;
						
						 Map<Object, Object> ipObjectIC = new LinkedHashMap<>();
						 Map<Object, Object> tcpObjectIC = new LinkedHashMap<>();
						 Map<Object, Object> udpObjectIC = new LinkedHashMap<>();
						 Map<Object, Object> httpObjectIC = new LinkedHashMap<>();
						 Map<Object, Object> sctpObjectIC = new LinkedHashMap<>();
						 Map<Object, Object> icmpObjectProtocol = new LinkedHashMap<>();
						 Map<Object, Object> icmpObjectIC = new LinkedHashMap<>();
						 
						for (Object objectIC : array) {							
							li = new ArrayList<Object>();														
							Map<String, Object> icmpLineMap = (Map<String, Object>) objectIC;							
						for (String icmpLineKey : icmpLineMap.keySet()) {																						
							 if (icmpLineKey.equalsIgnoreCase("Hexdump")) {
								 ic = 0 ;
								 transportLayerProtocolIC=0;
								 
							   }
							
							 if (icmpLineKey.equalsIgnoreCase("TCP Source Port")) {
								 	ic = 1;
								   	transportLayerProtocolIC=1;
						    	}
							   if (icmpLineKey.equalsIgnoreCase("Method")){
								   ic=2;
						    	}			    	
						    	if (icmpLineKey.equalsIgnoreCase("SCTP Source Port")){
						    		ic=3;
						    		transportLayerProtocolIC=3;
								}
						    	if (icmpLineKey.equalsIgnoreCase("UDP Source Port")) {
						    		ic = 4;
						    		transportLayerProtocolIC=4;
						    	}
						    	if (icmpLineKey.equalsIgnoreCase("Type")) {
						    		ic = 5;
						    		transportLayerProtocolIC=5;
								}
						    								 
							 if(ic==0) {
								 ipObjectIC.put(icmpLineKey,icmpLineMap.get(icmpLineKey));
								 icmpObjectIC.put("IP Packet", ipObjectIC);	
							 }
							 if(ic==1){	
								 if(icmpLineKey !="Application Layer Protocol")
								 tcpObjectIC.put(icmpLineKey,icmpLineMap.get(icmpLineKey));					
							 } 
								if(ic==2){
									httpObjectIC.put(icmpLineKey,icmpLineMap.get(icmpLineKey));
								}
								
								if(ic==3){
									sctpObjectIC.put(icmpLineKey,icmpLineMap.get(icmpLineKey));
								}
								if(ic==4) {
									udpObjectIC.put(icmpLineKey,icmpLineMap.get(icmpLineKey));
								}
								if(ic==5) {
									icmpObjectProtocol.put(icmpLineKey,icmpLineMap.get(icmpLineKey));
								}							
						  }						
						if(transportLayerProtocolIC==1)
				  		{
							ipObjectIC.put("TCP", tcpObjectIC);	
				  		}else if(transportLayerProtocolIC==3)
				  		{
				  			ipObjectIC.put("SCTP", sctpObjectIC);	
				  		}
				  		
				  		else if(transportLayerProtocolIC==4)
				  		{
				  			ipObjectIC.put("UDP", udpObjectIC);
				  		}else if(transportLayerProtocolIC==5)
				  		{
				  			ipObjectIC.put("ICMP", icmpObjectProtocol);	
				  		}
						li.add(icmpObjectIC);	
						}
				    }					
						
				 }
					
			}
				
			}
				
			
			
				}catch (Exception e) {
					// TODO: handle exception
				}
				

				System.out.println("ip  "+ip4Object+" tcp "+tcpObject+" http "+httpObject);
				
			}
			
			
			
//			if(!errorObject.isEmpty())
//				ipObject.putAll(errorObject);
			
			if(!ip6Object.isEmpty() && ip4Object.isEmpty()) {				
				ipObject.put("IPv6", ip6Object);				
				if(transportLayerProtocol==1||error==3)
		  		{
					ip6Object.put("TCP", tcpObject);	
					if(c1==2 ||applicationerror==1) 
		  				tcpObject.put("HTTP", httpObject);
		  		}
		  		if(transportLayerProtocol==3||error==1)
		  		{
		  			ip6Object.put("SCTP", sctpObject);	
		  			if(c1==2||applicationerror==1) 
		  				sctpObject.put("HTTP", httpObject);
		  		}
		  		if(transportLayerProtocol==5||error==4)
		  		{
		  			ip6Object.put("ICMP", icmpObject);
		  			if(li.size()!=0)
		  			icmpObject.put("Other Protocol", li);	  			  			
		  		}
		  		else if(transportLayerProtocol==4||error==2)
		  		{
		  			ip6Object.put("UDP", udpObject);
		  			if(c1==2||applicationerror==1) 
		  				udpObject.put("HTTP", httpObject);
		  		}				
			}
			else if(ip6Object.isEmpty() && !ip4Object.isEmpty()) {
				ipObject.put("IPv4", ip4Object);
				
				
			if(transportLayerProtocol==1||error==3)
	  		{				
				ip4Object.put("TCP", tcpObject);	
	  			if(c1==2||applicationerror==1) 
	  				tcpObject.put("HTTP", httpObject);
	  		}
	  		if(transportLayerProtocol==3||error==1)
	  		{
	  			ip4Object.put("SCTP", sctpObject);	
	  			if(c1==2||applicationerror==1) 
	  				sctpObject.put("HTTP", httpObject);
	  		}
	  		if(transportLayerProtocol==5||error==4)
	  		{
	  			ip4Object.put("ICMP", icmpObject);
	  			if(li.size()!=0)
	  			icmpObject.put("Other Protocol", li);	  			  			
	  		}
	  		else if(transportLayerProtocol==4||error==2)
	  		{
	  			ip4Object.put("UDP", udpObject);
	  			if(c1==2||applicationerror==1) 
	  				udpObject.put("HTTP", httpObject);
	  		}
		}	
					
			
			else if(!ip6Object.isEmpty() && !ip4Object.isEmpty() ) {
				
				if(ip4Object.size()==1 ){
					ipObject.put("Error", ip4Object);
					ip4Object.put("IPv6", ip6Object);	
				}else {
								
				ipObject.put("IPv4", ip4Object);
				ip4Object.put("IPv6", ip6Object);				
				}
		  		if(transportLayerProtocol==1||error==3)
		  		{
					ip6Object.put("TCP", tcpObject);	
		  			if(c1==2||applicationerror==1) 
		  				tcpObject.put("HTTP", httpObject);
		  		}
		  		if(transportLayerProtocol==3||error==1)
		  		{
		  			ip6Object.put("SCTP", sctpObject);	
		  			if(c1==2||applicationerror==1) 
		  				sctpObject.put("HTTP", httpObject);
		  		}
		  		if(transportLayerProtocol==5||error==4)
		  		{
		  			ip6Object.put("ICMP", icmpObject);
		  			if(li.size()!=0)
		  			icmpObject.put("Other Protocol", li);	  			  			
		  		}
		  		else if(transportLayerProtocol==4||error==2)
		  		{
		  			ip6Object.put("UDP", udpObject);
		  			if(c1==2||applicationerror==1) 
		  				udpObject.put("HTTP", httpObject);
		  		}	  		
		  		
			}
			else if(errorObject!=null)
				ipObject.putAll(errorObject);
			
		if(errorlist.size()!=0) {
			rootObject.put("IP Packet " + (i + 1)+":"+errorlist.get(0), ipObject);
		}else {
			if(ipObject.size() >0)
			rootObject.put("IP Packet " + (i + 1), ipObject);
			
			else
				rootObject.put("IP Packet " + (i + 1), object);
		}
		LOGGER.info("output "+ipObject);
	}catch (Exception e) {
		//throw new CustomException("Exception occured while creating a node");		
		  LOGGER.error("Error in common function "+e);
	}
	//return ipObject;	 	
	return rootObject;
	}
	

	public static String getFileString(MultipartFile file) throws CustomException {
		/**			  
		 *         Get multipart file data;			 		  
		 */		
		 LOGGER.info("input file "+file);
		try {
			String fileString="";
	        byte[] bytes = file.getBytes();
	        Path path = Paths.get( file.getOriginalFilename());
	       // Files.write(path, bytes);
	        fileString = new String(bytes);	 	        
	        Scanner scanner = new Scanner(fileString);	
	       	scanner.useDelimiter("#");
	    	StringBuilder fileBuilder = new StringBuilder();
	    	String filehexdump="";
		  while(scanner.hasNext()) {
			  	  fileString = scanner.nextLine();			  	  
			  	// System.out.println(fileString);			  	  
		        if(!fileString.contains("#")) {
		        	fileBuilder.append(fileString+"\n");
		        	filehexdump = fileBuilder.toString();
		      }
		  }	
		   // Close the scanner 
	        scanner.close(); 
	        LOGGER.info("output "+filehexdump);
	        return filehexdump;   
		}catch (Exception e) {
			LOGGER.error("error while reading the file "+e);
			throw new CustomException("Invalid file content,please try again!",422);
		}
			
	}
	
	
	public enum SuccessResponseMessage {		
		SUCCESS("OK","Success",200);		
		 int code;
		 String description;
		 String type;

		private SuccessResponseMessage( String type, String description,  int code) {
			this.type = type;
			this.description = description;
			this.code = code;
		}		
	}
	
	public static String getAttribute( List<Object> list) {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		LinkedHashMap<String,Object> responseAttribute = new LinkedHashMap<String, Object>();
		LinkedHashMap<String,Object> message = new LinkedHashMap<String, Object>();		 		 
		for (SuccessResponseMessage e : SuccessResponseMessage.values()) {
				message.put("code", e.code);
				message.put("type", e.type);
				message.put("description", e.description);
				responseAttribute.put("message", message);
				responseAttribute.put("response", list);	
				String json = gson.toJson(responseAttribute, LinkedHashMap.class);
				return json;
		}		
		return null;		
	}
	
	public enum ErrorResponseMessage {		
		BAD_REQUEST("type","BAD_REQUEST",400),
		BAD_ATTRIBUTE("type","BAD_ATTRIBUTE",1201),
		UNPROCESSABLE_ENTITY("type","UNPROCESSABLE_ENTITY",422);
		 int code;
		 String description;
		 String type;

		private ErrorResponseMessage(String type, String description,  int code) {
			this.type = type;
			this.description = description;
			this.code = code;
		}		
	}
	public static String getErrorResponse(String errorMessage, int code) {
		String json=null;
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		LinkedHashMap<String,Object> responseAttribute = new LinkedHashMap<String, Object>();
		LinkedHashMap<String,Object> message = new LinkedHashMap<String, Object>();		 		 
		for (ErrorResponseMessage e : ErrorResponseMessage.values()) {
			if(code==e.code) {
				responseAttribute.put("code", e.code);
				responseAttribute.put("type", e.description);
				responseAttribute.put("message", errorMessage);
				json = gson.toJson(responseAttribute, LinkedHashMap.class);
			}
		}		
		return json;	
	}

	
	

}