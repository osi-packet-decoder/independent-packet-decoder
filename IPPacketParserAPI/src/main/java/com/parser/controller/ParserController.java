package com.parser.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.parser.PacketParserApplication;
import com.parser.commonFunction.CommonFunction;
import com.parser.customError.CustomErrorType;
import com.parser.customError.CustomException;
import com.parser.model.IPPacket;
import com.parser.model.Protocol;
import com.parser.service.IPPacketVerifier;
import com.parser.service.ParserService;
import com.parser.service.ValidationProtocol;

@RestController
@RequestMapping(path = "/v1")

/**
 * @author AmantyaTech
 *
 */
public class ParserController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ParserController.class);

	/**
	 * @param ipObj
	 * @return
	 * @throws CustomException 
	 */

	@PostMapping(value = "/possiblePacket", produces = MediaType.APPLICATION_JSON_VALUE)
	@CrossOrigin(origins = "*")
	public @ResponseBody ResponseEntity<?> getPossiblePackets(@RequestBody String hexdump) throws CustomException {
try {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
//		Map<Protocol, Object> possibilities = ParserService.getPossiblePackets(hexdump);
		List<Object> ipPacket= ParserService.getPossiblePackets(hexdump);
		System.out.println("possibilities  "+ipPacket);
		
		List<Object> list = new ArrayList<>();
		ipPacketNodeCreation(list, gson, ipPacket);
		//String getAttribute = CommonFunction.getAttribute(list);
		
		if (ipPacket.isEmpty()) {
			return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity(list, HttpStatus.OK);
		}
}catch (CustomException e) {
	String getAttribute = CommonFunction.getErrorResponse(e.getMessage(), e.getCode());
	LOGGER.error("error response {}", getAttribute, e);
	if (e.getCode() == 422)
		return new ResponseEntity<>(getAttribute, HttpStatus.UNPROCESSABLE_ENTITY);
	else if (e.getCode() == 400)
		return new ResponseEntity<>(getAttribute, HttpStatus.BAD_REQUEST);
	else
		return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
}
	
	}
	
	@PostMapping(value = "/multiplePossiblePacket", produces = MediaType.APPLICATION_JSON_VALUE)
	@CrossOrigin(origins = "*")
	public @ResponseBody ResponseEntity<?>fileUploadforMultiple(@RequestParam("file") MultipartFile file) throws IOException {

//		List<Object> possibilitiesList = ParserService.getPossiblePackets(file);
//		if (null == possibilitiesList || possibilitiesList.isEmpty()) {
//			return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
//		} else {
//			return new ResponseEntity<List<Object>>(possibilitiesList, HttpStatus.OK);
//		}
		

		LOGGER.info("User file input " + file);
		try {
			if (file.isEmpty())
				return new ResponseEntity<>(new CustomErrorType("File is Empty"), HttpStatus.BAD_REQUEST);
			else {
				List<Object> list = new ArrayList<>();
				String fileString = CommonFunction.getFileString(file);
				System.out.println("fileString   "+fileString);
				Gson gson = new GsonBuilder().setPrettyPrinting().create();
				// List<Object> ipPacket = IPPacketVerifier.verifyIPPacket(fileString);

				List<Object> ipPacket = ParserService.getPossiblePackets( fileString);
				ipPacketNodeCreation(list, gson, ipPacket);
//				String getAttribute = CommonFunction.getAttribute(list);
//				LOGGER.info("controller output " + getAttribute);
				return new ResponseEntity<>(list, HttpStatus.OK);
			}

		} catch (CustomException e) {
			String getAttribute = CommonFunction.getErrorResponse(e.getMessage(), e.getCode());
			LOGGER.error("controller error " + getAttribute);
			if (e.getCode() == 422)
				return new ResponseEntity<>(getAttribute, HttpStatus.UNPROCESSABLE_ENTITY);
			else if (e.getCode() == 400)
				return new ResponseEntity<>(getAttribute, HttpStatus.BAD_REQUEST);
			else
				return ResponseEntity.status(1201).body(getAttribute);
		}
		
		
		
		
	}
			
	
	@PostMapping(value = "/ipPacketParser", produces = MediaType.APPLICATION_JSON_VALUE)
	@CrossOrigin(origins = "*")
	public @ResponseBody ResponseEntity<Object> getTcpData(@RequestBody IPPacket ipObj) throws CustomException {
		try {
			LOGGER.info("User hexdump input {}", ipObj);
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			String hexDump = ipObj.getHexDump();
			if (hexDump == null || hexDump.equals(""))
				return new ResponseEntity<>(
						new CustomErrorType("No hexdump found, Please give the hexdump and try again!"),
						HttpStatus.BAD_REQUEST);
			else {
				// Validator.validateEntry(ipObj.getToView());
				// List<Object> ipPacket = IPPacketVerifier.verifyIPPacket(hexDump);
				System.out.println("ipObj.getToView()...." + ipObj.getToView());
				List<Object> ipPacket = ValidationProtocol.verifyProtocol(ipObj.getToView(), hexDump);
				List<Object> list = new ArrayList<>();
				ipPacketNodeCreation(list, gson, ipPacket);
				String getAttribute = CommonFunction.getAttribute(list);
				LOGGER.info("controller output {}", getAttribute);
				return new ResponseEntity<>(getAttribute, HttpStatus.OK);
			}
		} catch (CustomException e) {
			String getAttribute = CommonFunction.getErrorResponse(e.getMessage(), e.getCode());
			LOGGER.error("error response {}", getAttribute, e);
			if (e.getCode() == 422)
				return new ResponseEntity<>(getAttribute, HttpStatus.UNPROCESSABLE_ENTITY);
			else if (e.getCode() == 400)
				return new ResponseEntity<>(getAttribute, HttpStatus.BAD_REQUEST);
			else
				return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
		}
	}

	/**
	 * @param file
	 * @return
	 * @throws IOException
	 */
	@PostMapping(value = "/uploadBulkIpPackets", produces = MediaType.APPLICATION_JSON_VALUE)
	@CrossOrigin(origins = "*")
	public @ResponseBody ResponseEntity<Object> fileUpload(@RequestParam("file") MultipartFile file,
			@RequestParam("toView") String toView) throws IOException {
		List<Object> toViewList = new ArrayList<>();

		if (toView.length() != 0)
			toViewList = Arrays.asList(toView.split(","));

		LOGGER.info("User file input " + toView);
		try {
			if (file.isEmpty())
				return new ResponseEntity<>(new CustomErrorType("File is Empty"), HttpStatus.BAD_REQUEST);
			else {
				List<Object> list = new ArrayList<>();
				String fileString = CommonFunction.getFileString(file);
				Gson gson = new GsonBuilder().setPrettyPrinting().create();
				// List<Object> ipPacket = IPPacketVerifier.verifyIPPacket(fileString);

				List<Object> ipPacket = ValidationProtocol.verifyProtocol(toViewList, fileString);
				ipPacketNodeCreation(list, gson, ipPacket);
				String getAttribute = CommonFunction.getAttribute(list);
				LOGGER.info("controller output " + getAttribute);
				return new ResponseEntity<>(getAttribute, HttpStatus.OK);
			}

		} catch (CustomException e) {
			String getAttribute = CommonFunction.getErrorResponse(e.getMessage(), e.getCode());
			LOGGER.error("controller error " + getAttribute);
			if (e.getCode() == 422)
				return new ResponseEntity<>(getAttribute, HttpStatus.UNPROCESSABLE_ENTITY);
			else if (e.getCode() == 400)
				return new ResponseEntity<>(getAttribute, HttpStatus.BAD_REQUEST);
			else
				return ResponseEntity.status(1201).body(getAttribute);
		}
	}

	@SuppressWarnings({ "unchecked" })
	private void ipPacketNodeCreation(List<Object> list, Gson gson, List<Object> ipPacket) throws CustomException {
		try {
			LOGGER.info("node creation method input " + ipPacket);

			// list =new ArrayList<>();
			Map<Object, Object> ipObject = null;
			for (int i = 0; i < ipPacket.size(); i++) {
				System.out.println("iii   "+ipPacket.get(i));
				
//				 Map<String, Object> map = (HashMap<String, Object>) ipPacket.get(i);
//		          for (String key1 : map.keySet()) {
//		        	  if(key1.equalsIgnoreCase("IP")||key1.equalsIgnoreCase("IPv4")||key1.equalsIgnoreCase("IPv6")) {
//		        	  List<Object> IPList = (List<Object>) map.get(key1);	
//		        	  System.out.println("IPList.get(i)   "+IPList);
//		        	  ipObject = CommonFunction.getNodeObject(IPList.get(i), i);
//		        	  }else {
//		        		  ipObject = CommonFunction.getNodeObject(ipPacket.get(i), i);
//		        	  }
//					}
				
				ipObject = CommonFunction.getNodeObject(ipPacket.get(i), i);
				if (ipObject != null)
					list.add(ipObject);
			}

			LOGGER.info("node creation output " + list);
		} catch (Exception e) {
			LOGGER.error("error in node creation output " + e.getMessage());
			// e.printStackTrace();
			// new CustomException("Exception occured while parsing the packet");
		}
	}

}