package com.parser.customError;

@SuppressWarnings("serial")

public class CustomException extends Exception {

	private String message;
	private int code;

	public CustomException(String message, int code) {
		this.message = message;
		this.code = code;
	}

	public int getCode() {
		return code;
	}

	@Override
	public String getMessage() {
		return message;
	}

}
