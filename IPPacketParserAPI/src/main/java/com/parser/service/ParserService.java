package com.parser.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.parser.customError.CustomException;
import com.parser.utility.JsonUtil;

public class ParserService {

	private static final Logger logger = LoggerFactory.getLogger(ParserService.class);

	private static JsonUtil<Map<?, ?>> mapJsonUtil = new JsonUtil<Map<?, ?>>();
	private static JsonUtil<List<?>> listJsonUtil = new JsonUtil<List<?>>();

	private static boolean isValid(String json) {

		return !json.contains("Error");
	}

	private static boolean isValid(Map<String, Object> outputMap) {

		logger.debug("isValid -> (outputMap) {}", outputMap);
		String json = mapJsonUtil.convertObjectToJson(outputMap);
		logger.debug("isValid -> (json) {}", json);
		return isValid(json);
	}

	private static boolean isValid(List<?> outputList) {

		logger.debug("isValid -> (outputList) {}", outputList);
		String json = listJsonUtil.convertObjectToJson(outputList);
		logger.debug("isValid -> (json) {}", json);
		return isValid(json);
	}

	//public static Map<Protocol, Object> getPossiblePackets(String hexdump) {

	public static List<Object> getPossiblePackets(List<Object> toggleList,String hexdumpString) throws CustomException {
		if(toggleList.size()==0) {
			//logger.error("ValidationProtocol not parsed..." + "Please select the valid protocols from settings tab and try again!");
          throw new CustomException("Please select the valid protocols from settings tab and try again!",400);		
		}
        System.out.println("toggleListSer"+toggleList);
		StringBuilder stringBuild = new StringBuilder();

		toggleList.forEach(stringBuild::append);
		//String st = b.toString();           
         String activeSwitch = stringBuild.toString();
		if (hexdumpString != null) {
            System.out.println("hexdumpString"+hexdumpString);
            
			String hexdump = null, errormsg = null;
			int counter = 0, errorCode = 0;
			LinkedHashMap<String, Object> rootOutputMap = null;
			LinkedHashMap<String, Object> outputMap = null;
			boolean flag = false;
			List<Object> ipList = null;
			List<Object> ipErrorList = null;
			String currentProtocol = null;

			String regx = "[\\|\t\n\r]+";
			List<String> list = Arrays.asList(hexdumpString.split(regx));
			// ******** Remove empty index from list
			list = list.stream().filter(s -> !s.isEmpty()).collect(Collectors.toList());
			ipList = new ArrayList<>();
            System.out.println("list"+list);
			for (int i = 0; i < list.size(); i++) {
				hexdump = list.get(i).replaceAll("\\s", "");
				outputMap = new LinkedHashMap<String, Object>();
				rootOutputMap = new LinkedHashMap<String, Object>();
				String hexStart = "<<", hexEnd = ">>";
				try {

					if ((hexdump.matches("[0-9A-Fa-f<>]+"))) {
						System.out.println("iffffffffffffffff............");
						counter++;
						if (((hexdump.startsWith(hexStart)) && (hexdump.endsWith(hexEnd)))) {
							hexdump = hexdump.replace(hexEnd, "");
							hexdump = hexdump.replace(hexStart, "");
						} else {
							hexdump = hexdump;
						}

						boolean isV4 = false, isV6 = false;
						String IPVer = null;
						if (hexdump.length() > 0) {
							IPVer = hexdump.substring(0, 1);
							if (IPVer.equals("4")) {
								isV4 = true;
							} else if (IPVer.equals("6")) {
								isV6 = true;
							}
						}

						outputMap = new LinkedHashMap<>();
						LinkedHashMap<String, Object> httpOutput =new LinkedHashMap<>();
						try {
							currentProtocol = "HTTP";
							 httpOutput = HTTPPacketVerifier.verifyHTTPPacket(hexdump);
					    	System.out.println("httpOutput  "+httpOutput);
							if (isValid(httpOutput)) {
								if((activeSwitch.contains("HTTP") || activeSwitch.contains("All"))) {
								outputMap.put(currentProtocol, httpOutput);
							}else {
								outputMap.put("Error", "HTTP Not Decoded - " + "Protocol is Disabled for this Packet");	

							}
					    	}
						} catch (Exception e) {
							//logger.error("exception in {}", currentProtocol, e);
						}
						
					//if(httpOutput.containsKey("Error"))	{
						
						try {
							   currentProtocol = "IP";
	                            List<Object> ipOutput = IPPacketVerifier.verifyIPPacket(hexdump);
	                            System.out.println("ipOutput  "+ipOutput);
	                            if (ipOutput.size() >= 1) {
	                                if(ipOutput.get(0).toString().contains("version")) {
	                                if (activeSwitch.contains("IPv4") || activeSwitch.contains("All")) {
	                                    //outputMap.put(currentProtocol, ipOutput);
	                                    
	                                    
	                                    
	                                    
	                                    
	                                    if(!activeSwitch.contains("SIP") || !activeSwitch.contains("HTTP")|| !activeSwitch.contains("Diameter")|| !activeSwitch.contains("FTP")|| !activeSwitch.contains("RTP")) {
			                            	//try {
	                                    	List<Object> ipPacket = ValidationProtocol.verifyProtocol( toggleList, hexdump);
			                            	 System.out.println("ipPacket!!!!!!!  "+ipPacket);
			 	                            outputMap.put(currentProtocol, ipPacket);
//			                            	}catch (Exception e) {
//											e.printStackTrace();
//											}
			                            }
			                            else {
			                            	outputMap.put(currentProtocol, ipOutput);
			                            }
	                                    
	                                    
	                                    
	                                    
	                                    
	                                    
	                                    
	                                }else {
	                                    outputMap.put("Error", "IPv4 Not Decoded - " + "Protocol is Disabled for this Packet");   
	                                }
	                                }
	                                System.out.println("IP"+outputMap);
	                                if(ipOutput.get(0).toString().contains("Version")) {
	                                if (activeSwitch.contains("IPv6") || activeSwitch.contains("All")) {
	                                   // outputMap.put(currentProtocol, ipOutput);
	                                	
	                                	if(!activeSwitch.contains("SIP") || !activeSwitch.contains("HTTP")|| !activeSwitch.contains("Diameter")|| !activeSwitch.contains("FTP")|| !activeSwitch.contains("RTP")) {
			                            	List<Object> ipPacket = ValidationProtocol.verifyProtocol( toggleList, hexdump);
			                            	 System.out.println("ipPacket!!!!!!!  "+ipPacket);
			 	                            outputMap.put(currentProtocol, ipPacket);
			                            }
			                            else {
			                            	outputMap.put(currentProtocol, ipOutput);
			                            }
	                                    
	                                }else {
	                                    outputMap.put("Error", "IPv6 Not Decoded - " + "Protocol is Disabled for this Packet");   
	                                }
	                                }
	                                }
							
							
						} catch (Exception e) {
							//logger.error("exception in {}", currentProtocol, e);
							e.printStackTrace();
						}


						

						try {
							currentProtocol = "RTP";
							LinkedHashMap<String, Object> rtpOutput = RTPPacketVerifier.parseRTPPacket(hexdump);
							if (isValid(rtpOutput)) {
								if (activeSwitch.contains("RTP") || activeSwitch.contains("All")) {
								outputMap.put(currentProtocol, rtpOutput);
							}else {
								outputMap.put("Error", "RTP Not Decoded - " + "Protocol is Disabled for this Packet");	
							}
							}
						} catch (Exception e) {
							//logger.error("exception in {}", currentProtocol, e);
						}

					
				
						
						try {
							currentProtocol = "TLS";
							 LinkedHashMap<String, Object> tlsOutput = TLSPacketVerifier.verifyTLSPacket(hexdump);
							if (isValid(tlsOutput)) {
								if(tlsOutput.size() !=0)
									outputMap.put(currentProtocol, tlsOutput);
							}
						} catch (Exception e) {
							//logger.error("exception in {}", currentProtocol, e);
						}
					
						
						try {
							currentProtocol = "FTP";
							List<LinkedHashMap> ftpOutput = FTPPacketVerifier.verifyFTPPacket(hexdump);
							if (isValid(ftpOutput)) {
								if (activeSwitch.contains("FTP") || activeSwitch.contains("All")) {
									System.out.println("output FTP"+outputMap);
								outputMap.put(currentProtocol,ftpOutput);
							}else {
								outputMap.put("Error", "FTP Not Decoded - " + "Protocol is Disabled for this Packet");	
							}
							}
						} catch (Exception e) {
							//logger.error("exception in {}", currentProtocol, e);
						}
						
						try {
							currentProtocol = "Diameter";
							List<LinkedHashMap<String, Object>> daimeterOutput = DiameterProtocol.verifyDiameterProtocol(hexdump);
							System.out.println("daimeterOutput.contains(\"Error\") "+daimeterOutput);
							if(daimeterOutput.size() !=0 && !daimeterOutput.get(0).containsKey("Error")) {
								if (activeSwitch.contains("Diameter") || activeSwitch.contains("All")) {
								if(!daimeterOutput.get(0).containsKey("Error"))
									outputMap.put(currentProtocol,daimeterOutput);
							}else {
								outputMap.put("Error", "Diameter Not Decoded - " + "Protocol is Disabled for this Packet");	
							}
							}
							
						} catch (Exception e) {
							////logger.error("exception in {}", currentProtocol, e);
						}
						
						try {
							currentProtocol = "SIP";
							LinkedHashMap<String, Object> sipOutput = SIPPacketVerifier.verifySIPPacket(hexdump);
							System.out.println("siiiiiiiiiiipppppppppp>>>>.  "+sipOutput);
							if (isValid(sipOutput) && sipOutput.size()>0) {
								if (activeSwitch.contains("SIP") || activeSwitch.contains("All")) {
//								System.out.println("sipsize"+sipOutput.size());
//								if(sipOutput.size() !=0)
									outputMap.putAll(sipOutput);
							}else  {
								outputMap.put("Error", "SIP Not Decoded - " + "Protocol is Disabled for this Packet");	
							}
							}
						} catch (Exception e) {
							//logger.error("exception in {}", currentProtocol, e);
						}
					//}
						
						if(outputMap.isEmpty()) {
							if(list.size()>1)
								outputMap.put("Error", "Malformed Packet - No valid hexdump found, Please try again!! '" + hexdump);
                            else
                                outputMap.put("Error", "Malformed Packet - No valid hexdump found, Please try again!! " );
						}
						//rootOutputMap.put("Packet " + (i + 1), outputMap);
						ipList.add(outputMap);
						flag = true;
					} else {
						
						throw new CustomException("Malformed Packet - Hexdump format is not valid '" + hexdump + "' ",400);
					}

					System.out.println("counter >>>>>>>>>>>>  "+counter);
				} catch (CustomException e) {
					
					errormsg = e.getMessage();
					errorCode = e.getCode();
					System.out.println("counter >>>>>>>>>>>>  "+counter+e.getMessage());
					if (counter == 0)
						throw new CustomException("No valid hexdump found, Please try again!", 400);
//					else
//						return ipList;
					// System.out.println(errorCode+"....flag...."+flag);
					// e.printStackTrace();
					// outputMap.put("Hexdump", hexdump);
//					outputMap.put("Error", e.getMessage());
//					ipList.add(outputMap);
				}
			}

			 System.out.println("ipList..."+ipList.size()+ipList.toString());

			if (ipList != null) {
				if (ipList.size() == 1) {
					@SuppressWarnings("unchecked")
					Map<Object, Object> ipObject = (Map<Object, Object>) ipList.get(0);
					if (ipObject.size() == 1) {
						if (ipObject.containsKey("Error"))
							throw new CustomException(ipObject.get("Error").toString(), errorCode);
//						else
//							throw new CustomException(ipObject.get("Error").toString(), 1201);
					} else
						return ipList;
				} else if (ipList.size() > 1) {
					ipErrorList = new ArrayList<>();
					 System.out.println("........ip...."+ipErrorList.size());
					for (int j = 0; j < ipList.size(); j++) {
						@SuppressWarnings("unchecked")
						LinkedHashMap<String, Object> rootOut = (LinkedHashMap<String, Object>) ipList.get(j);
						System.out.println("rootOut...."+rootOut);
//						if (rootOut.get("Hexdump") == null && rootOut.get("HexDump") == null)
//							ipErrorList.add(ipList.get(j));
						
						if (rootOut.get("Error") != null ) {
							 System.out.println("........error...>>>>....");
							ipErrorList.add(ipList.get(j));
						}
					}
					System.out.println("ipList"+ipList);
					System.out.println(ipErrorList.size()+"....listsize"+ipList.size());
					if (ipList.size() == ipErrorList.size())
						throw new CustomException("No valid hexdump found, Please try again!!", 400);

					else
						return ipList;
				}

//		else if(flag == false)	
//			throw new CustomException("No valid hexdump found, Please try again!!",400);

				if (counter == 0)
					throw new CustomException("No valid hexdump found, Please try again!", 400);
				else
					return ipList;
			} else
				throw new CustomException("Data could not be processed,please try again!", 400);

//		if(counter==0)
//			throw new CustomException("No valid hexdump found, Please try again!");
//		else {
//			return ipList;
//		}

		} else
			throw new CustomException("No hexdump found, Please give the hexdump and try again!", 400);


		
//		if(hexdump == null)
//		   throw new CustomException("No hexdump found, Please give the hexdump and try again!", 400);
//
//		 List<Object> finalResult= new ArrayList<>();
//         Map<String, Object> possibilities = new LinkedHashMap<String, Object>();
//
//		String currentProtocol = null;
//		
//		if(hexdump.length()>0) {
////			if ((hexdump.matches("[0-9A-Fa-f<>]+"))) {
//
//		try {
//			currentProtocol = "IP";
//			List<Object> ipOutput = IPPacketVerifier.verifyIPPacket(hexdump);
//			System.out.println("ipOutput"+ipOutput);
//			if (ipOutput.size() >= 1) {
//				possibilities.put(currentProtocol, ipOutput);
//				System.out.println("IP"+possibilities);
//				//LinkedHashMap<String, Object> finalProtocol = IndependentVerifier.getIndependentPacket(possibilities);
//			}
//		} catch (Exception e) {
//			//logger.error("exception in {}", currentProtocol, e);
//		}
//
////		try {
////			currentProtocol = Protocol.TCP;
////			LinkedHashMap<String, Object> tcpOutput = TCPPacketVerifier.verifyTCPPacket(hexdump);
////			if (isValid(tcpOutput)) {
////				possibilities.put(currentProtocol, tcpOutput);
////			}
////		} catch (Exception e) {
////			//logger.error("exception in {}", currentProtocol, e);
////		}
////
////		try {
////			currentProtocol = Protocol.UDP;
////			LinkedHashMap<String, Object> udpOutput = UDPPacketVerifier.verifyUDPPacket(hexdump);
////			if (isValid(udpOutput)) {
////				possibilities.put(currentProtocol, udpOutput);
////				LinkedHashMap<String, Object> finalProtocol = IndependentVerifier.getIndependentPacket(udpOutput);
////			}
////		} catch (Exception e) {
////			//logger.error("exception in {}", currentProtocol, e);
////		}
//
//		try {
//			currentProtocol = "HTTP";
//			LinkedHashMap<String, Object> httpOutput = HTTPPacketVerifier.verifyHTTPPacket(hexdump);
//			if (isValid(httpOutput)) {
//				possibilities.put(currentProtocol, httpOutput);
//			}
//		} catch (Exception e) {
//			//logger.error("exception in {}", currentProtocol, e);
//		}
//
//		try {
//			currentProtocol = "RTP";
//			LinkedHashMap<String, Object> rtpOutput = RTPPacketVerifier.parseRTPPacket(hexdump);
//			if (isValid(rtpOutput)) {
//				possibilities.put(currentProtocol, rtpOutput);
//			}
//		} catch (Exception e) {
//			//logger.error("exception in {}", currentProtocol, e);
//		}
//
//	try {
//			currentProtocol = "SIP";
//			LinkedHashMap<String, Object> sipOutput = SIPPacketVerifier.verifySIPPacket(hexdump);
//			if (isValid(sipOutput)) {
//				System.out.println("sipsize"+sipOutput.size());
//				if(sipOutput.size() !=0)
//				possibilities.putAll(sipOutput);
//			}
//		} catch (Exception e) {
//			//logger.error("exception in {}", currentProtocol, e);
//		}
//		
////		try {
////			currentProtocol = Protocol.SMTP;
////			List<LinkedHashMap> smtpOutput = SMTPPacketVerifier.verifySMTPPacket(hexdump);
////			if (isValid(smtpOutput)) {
////				if(smtpOutput.size() !=0)
////				possibilities.put(currentProtocol, smtpOutput);
////			}
////		} catch (Exception e) {
////			//logger.error("exception in {}", currentProtocol, e);
////		}
//		
////		try {
////			currentProtocol = Protocol.DTLS;
////			List<LinkedHashMap<String, Object>> dtlsOutput = DTLSPacketVerifier.parse(hexdump);
////			if (isValid(dtlsOutput)) {
////				possibilities.put(currentProtocol, dtlsOutput);
////			}
////		} catch (Exception e) {
////			//logger.error("exception in {}", currentProtocol, e);
////		}
//		
//		try {
//			currentProtocol = "TLS";
//			 LinkedHashMap<String, Object> tlsOutput = TLSPacketVerifier.verifyTLSPacket(hexdump);
//			if (isValid(tlsOutput)) {
//				if(tlsOutput.size() !=0)
//				possibilities.put(currentProtocol, tlsOutput);
//			}
//		} catch (Exception e) {
//			//logger.error("exception in {}", currentProtocol, e);
//		}
////		
////		try {
////			currentProtocol = Protocol.SCTP;
////			 LinkedHashMap<String, Object> sctpOutput = SCTPPacketVerifier.verifySCTPPacket(hexdump);
////			if (isValid(sctpOutput)) {
////				possibilities.put(currentProtocol,sctpOutput);
////			}
////		} catch (Exception e) {
////			//logger.error("exception in {}", currentProtocol, e);
////		}
////		
////		try {
////			currentProtocol = Protocol.ICMP;
////			LinkedHashMap<String,Object> icmpOutput = ICMPPacketVerifier.verifyICMPPacket( hexdump,hexdump );
////			if (isValid(icmpOutput)) {
////				possibilities.put(currentProtocol,icmpOutput);
////			}
////		} catch (Exception e) {
////			//logger.error("exception in {}", currentProtocol, e);
////		}
//		
//		try {
//			currentProtocol = "FTP";
//			List<LinkedHashMap> ftpOutput = FTPPacketVerifier.verifyFTPPacket(hexdump);
//			if (isValid(ftpOutput)) {
//				possibilities.put(currentProtocol,ftpOutput);
//			}
//		} catch (Exception e) {
//			//logger.error("exception in {}", currentProtocol, e);
//		}
//		
//		try {
//			currentProtocol = "Diameter";
//			List<LinkedHashMap<String, Object>> daimeterOutput = DiameterProtocol.verifyDiameterProtocol(hexdump);
//			System.out.println("daimeterOutput.contains(\"Error\") "+daimeterOutput.get(0).containsKey("Error"));
//			if(daimeterOutput.size() !=0) {
//				if(!daimeterOutput.get(0).containsKey("Error"))
//				possibilities.put(currentProtocol,daimeterOutput);
//			}
//			
//		} catch (Exception e) {
//			//logger.error("exception in {}", currentProtocol, e);
//		}
		
//		System.out.println("outputMap"+outputMap);
//		  if(outputMap.isEmpty())
//			throw new CustomException("Malformed packet- Please give the valid hexdump and try again!", 400);
//
//			 // possibilities.put("Error", "Malformed packet- Please give the valid hexdump and try again!");
//			}else {
//				throw new CustomException("No valid hexdump- Please give the valid hexdump and try again!", 400);
//
//			}
////		}else {
////			possibilities.put("Error", "No hexdump found, Please give the hexdump and try again!");
////		}		
//		
//		finalResult.add(possibilities);
//		System.out.println("possibilities"+possibilities);
//		return finalResult;
	}
	}
	
		
	
	
