package com.parser.service;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.parser.customError.CustomException;

      /**
       * @author AmantyaTech
       */

public class DTLSPacketVerifier {
	private static final Logger LOGGER = LoggerFactory.getLogger(DTLSPacketVerifier.class);
	/**
	 * Parses the hexdump of DTLSPacket
	 * @request string
	 * @response LinkedHashMap<String, Object>
	 */
	private List<LinkedHashMap<String, Object>> list = new ArrayList<LinkedHashMap<String, Object>>();

	private List<LinkedHashMap<String, Object>> getInnerResults() {
		return list;
	}

	private List<Object> extensions(String hex2) throws CustomException {

		int dataLength1 = 0;

		List<Object> extensionList = new LinkedList<>();
		for (int i = 0; hex2.length() >= 8;) {

			LinkedHashMap<String, String> mapServerHello1 = new LinkedHashMap<String, String>();

			try {

				dataLength1 = isValidDataLength(hex2.substring(i + 4, i + 4 + 4));
				mapServerHello1.put("Length", dataLength1 + "");

				String extensionType1 = isValidExtensionType(hex2.substring(i, i + 4));
				mapServerHello1.put("Type", extensionType1);

				if (dataLength1 != 0) {
					String data1 = hex2.substring(i + 8, i + 8 + (dataLength1 * 2));
					mapServerHello1.put("Data", data1);

					hex2 = hex2.substring(i + 8 + (dataLength1 * 2));
				} else {
					hex2 = hex2.substring(i + 8);
				}

				extensionList.add(mapServerHello1);

			} catch (Exception e) {
				throw new CustomException(e.getMessage(),400);
			}
		}

		return extensionList;

	}

	private String handshake(String hexdump, Map originalOutputMap) throws CustomException {
		Map outputMap = new LinkedHashMap();
		String handshakeType = isValidHandshakeType(hexdump.substring(0, 2));
		outputMap.put("Handshake Type", handshakeType);
		int type = Integer.parseInt(hexdump.substring(0, 2), 16);
		String handshakeTypeLength = isValidHandshakeTypeLength(hexdump.substring(2, 8));
		outputMap.put("Handshake Type Length", handshakeTypeLength);
		String handshakeTypeMessage = isValidHandshakeTypeMessage(hexdump.substring(8, 12));
		outputMap.put("Handshake Type Message", handshakeTypeMessage);
		String handshakeTypeFragment = isValidHandshakeTypeFragement(hexdump.substring(12, 18));
		outputMap.put("Handshake Type Fragement offset", handshakeTypeFragment);
		int handshakeTypeFragmentLength = isValidHandshakeTypeFragementLength(hexdump.substring(18, 24));
		outputMap.put("Handshake Type Fragement Length", handshakeTypeFragmentLength);
		int nextPacketFromIndex = 0;
		if (type == 1) {
			String handshakeTypeVersion = isValidHandshakeTypeVersion(hexdump.substring(24, 28));
			outputMap.put("Version", handshakeTypeVersion);
			String gmtUnixTime = isValidgmtUnixTime(hexdump.substring(28, 36));
			outputMap.put("GMT Unix Time", gmtUnixTime);
			outputMap.put("Random Bytes:", hexdump.substring(36, 92));
			int sessionIDLength = isvalidSessionIDLength(hexdump.substring(92, 94));
			outputMap.put("SessionID", sessionIDLength);
			int cookieLength = isvalidCookieLength(hexdump.substring(94, 96));
			outputMap.put("CookieLength", cookieLength);
			int cipherSuitesLength = isvalidCipherSuitesLength(hexdump.subSequence(96, 100));
			outputMap.put("Cipher Suites Length", cipherSuitesLength);
			List<Object> cipherList = new LinkedList<>();
			for (int i = 0; i < cipherSuitesLength / 2; i++) {
				int beginIndex = 100 + (i * 4);
				String cipher = hexdump.substring(beginIndex, beginIndex + 4);
				cipher = cipher(cipher) + "(0x" + cipher + ")";
				cipherList.add(cipher);
			}
			outputMap.put("Cipher Suites", cipherList);
			int compressionMethodsLength = isValidCompressionMethodsLength(hexdump.substring(276, 278));
			outputMap.put("Compression Methods Length", compressionMethodsLength);
			int extensionLength = isVAlidExtensionLength(hexdump.substring(280, 284));
			outputMap.put("Extension Length", extensionLength);

			/******************************************************************************************************/

			int cipherSuits = ((92 + (sessionIDLength * 2)) + 4);
			int cipherRange = (cipherSuits + (cipherSuitesLength * 2)) + 2;
			String hex2 = hexdump.substring(cipherRange + 10, (cipherRange + 10) + (extensionLength * 2));

			/******************************************************************************************************/

			List<Object> extensionList = extensions(hex2);

			outputMap.put("Extension", extensionList);

			/******************************************************************************************************/

		} else if (type == 2) {
			String handshakeTypeVersion = isValidHandshakeTypeVersion(hexdump.substring(24, 28));
			outputMap.put("Version", handshakeTypeVersion);
			String gmtUnixTime = isValidgmtUnixTime(hexdump.substring(28, 36));
			outputMap.put("GMT Unix Time", gmtUnixTime);
			outputMap.put("Random Bytes:", hexdump.substring(36, 92));
			int sessionIDLength = isValidSessionIdLength1(hexdump.substring(92, 94));
			outputMap.put("SessionID", sessionIDLength);
			String cipher = cipher(hexdump.substring(94, 98));
			outputMap.put("Cipher Suite", cipher + " (0x" + hexdump.substring(94, 98) + ")");
			int compressionMethodsLength = isValidCompressionMethodsLength(hexdump.substring(98, 100));
			outputMap.put("Compression Methods Length", compressionMethodsLength);
			int extensionLength = isVAlidExtensionLength(hexdump.substring(100, 104));
			outputMap.put("Extensions Length", extensionLength);

			int cipherSuits = ((92 + (sessionIDLength * 2)) + 4);
			int cipherRange = (cipherSuits + (1 * 2));
			String hex2 = hexdump.substring(cipherRange + 6, (cipherRange + 6) + (extensionLength * 2));

			List<Object> extensionList = extensions(hex2);

			outputMap.put("Extension", extensionList);
		} else if (type == 3) {
			String handshakeTypeVersion = isValidHandshakeTypeVersion(hexdump.substring(24, 28));
			outputMap.put("Version", handshakeTypeVersion);
			int cookieLength = isvalidCookieLength(hexdump.substring(28, 96));
			outputMap.put("CookieLength", cookieLength);
		} else if (type == 14) {

		} else if (type == 11) {
			int certificatesLength = isValidCertificatesLength(hexdump.substring(24, 30));
			outputMap.put("Certificates Length", certificatesLength);
			int certificateLength = isValidCertificateLength(hexdump.substring(30, 36));
			outputMap.put("Certificate Length", certificateLength);
		} else if (type == 4) {
			int sessionTicketLifetimeHint = isValidSessionTicketLifetimeHint(hexdump.substring(24, 32));
			outputMap.put("Session Ticket Lifetime Hint", sessionTicketLifetimeHint + "");
			int sessionTicketLength = isValidSessionTicketLength(hexdump.substring(32, 36));
			outputMap.put("Session Ticket Length", sessionTicketLength + "");
			outputMap.put("Session Ticket", hexdump.substring(36));
			// return hexdump.substring( sessionTicketLength +36 );
		} else if (type == 16) {

		} else if (type == 0) {

		} else if (type == 15) {

		} else {
			nextPacketFromIndex = hexdump.length();
			// can throw exception also
			// throw new CustomException("Did not match any known implemented handshake type
			// : decimal value ( " + type + " ) , hexadecimal value ( 0x" +
			// hexdump.substring(0, 2) + " )");
		}
		if (0 == nextPacketFromIndex) {
			nextPacketFromIndex = handshakeTypeFragmentLength + 24 + Integer.parseInt(handshakeTypeLength);
		}
		originalOutputMap.put("Handshake", outputMap);
		return hexdump.substring(nextPacketFromIndex);
	}

	private int isValidDataLength(String substring) {
		int DataLength = Integer.parseInt(substring, 16);
		return DataLength;

	}

	private static int isVAlidExtensionLength(String substring) {
		int extensionLength = Integer.parseInt(substring, 16);
		return extensionLength;
	}

	private static int isValidCompressionMethodsLength(String substring) {
		int compressionMethodsLength = Integer.parseInt(substring, 16);
		return compressionMethodsLength;
	}

	private int isvalidSessionIDLength(String substring) {
		int SessionIDLength = Integer.parseInt(substring, 16);
		return SessionIDLength;
	}

	private int isvalidCipherSuitesLength(CharSequence charSequence) {
		int cipherSuitesLength = Integer.parseInt((String) charSequence, 16);
		return cipherSuitesLength;
	}

	private int isvalidCookieLength(String substring) {
		int cookieLength = Integer.parseInt(substring, 16);
		return cookieLength;
	}

	private String isValidCertificate(String CertificateHex) {
		String CertificateString = null;
		CertificateString = Long.parseLong(CertificateHex, 16) + "";
		return CertificateString;

	}

	private String cipher(String hexdump, String length, Map outputMap) throws CustomException {
		return hexdump.substring(Integer.parseInt(length) * 2);

	}

	private int isvalidchangeCipherSecMessge(String changeCipherSecMessageHex) {
		int changeCipherSecMessageLength = 0;
		changeCipherSecMessageLength = Integer.parseInt(changeCipherSecMessageHex, 16);
		return changeCipherSecMessageLength;
	}

	private String application(String hexdump, String length, Map outputMap)
			throws CustomException, UnsupportedEncodingException {
		outputMap.put("Encrypted Application Data", hexdump.substring(0, 2));
		return hexdump.substring(Integer.parseInt(length) * 2);
	}

	private String heartbeat(String hexdump, String length, Map outputMap)
			throws CustomException, UnsupportedEncodingException {
		String type = isValidType(hexdump.substring(0, 2));
		outputMap.put("Type", type);
		int payloadLength = isValidPayloadLength(hexdump.substring(2, 4));
		outputMap.put("Payload Length", payloadLength + "");
		return hexdump.substring(Integer.parseInt(length) * 2);

	}

	private String isValidType(String typeHex) {
		String TypeString = null;
		TypeString = Integer.parseInt(typeHex, 16) + "";
		return TypeString;
	}

	private String alert(String hexdump, String length, Map outputMap)
			throws CustomException, UnsupportedEncodingException {
		String level = isValidLevel(hexdump.substring(0, 2));
		outputMap.put("Level", level);
		String description = isValidDescription(hexdump.substring(2, 4));
		outputMap.put("Description", description);
		return hexdump.substring(Integer.parseInt(length) * 2);

	}

	private String isValidDescription(String descriptionHex) throws CustomException {
		if (descriptionHex.length() != 2)
			throw new CustomException("Protocol Version not valid" + descriptionHex,1201);
		return Integer.parseInt(descriptionHex, 16) + "";
	}

	private static String isValidLevel(String substring) throws CustomException {
		int isValidLevel = Integer.parseInt(substring, 16);
		if (isValidLevel == 1) {
			return "Warning" + "(" + isValidLevel + ")";
		} else if (isValidLevel == 2) {
			return "Fatal" + "(" + isValidLevel + ")";
		} else {
			throw new CustomException("Malformed packet " + isValidLevel,1201);
		}
	}

	private String isValidEncryptedAlert(String EncryptedAlertHex) {
		String encryptedAlertString = null;
		encryptedAlertString = Integer.parseInt(EncryptedAlertHex, 16) + "";
		return encryptedAlertString;
	}

	private String isValidAplicationDataProtocol(String ApplicationDataProtocolHex) {
		String applicationDataProtocolString = null;
		applicationDataProtocolString = Integer.parseInt(ApplicationDataProtocolHex, 16) + "";
		return applicationDataProtocolString;

	}

	private LinkedHashMap<String, Object> verifyDTLSPacket(String hexdump) throws CustomException {
		LOGGER.info("-> verifyDTLSPacket : (hexdump) {}", hexdump);
		LinkedHashMap<String, Object> outputMap = new LinkedHashMap();
		LinkedHashMap<String, Object> outputMapError = new LinkedHashMap<String, Object>();	

		try {
			// String dtlsPacket = isValidDTLSPacket(hexdump);
			String contentTYpe = isValidContentTYpe(hexdump.substring(0, 2));
			outputMap.put("DTLS Content Type", contentTYpe);
			String protocolVersion = isValidProtocolVersion(hexdump.substring(2, 6));
			outputMap.put("DTLS Protocol Version:", "0x" + Integer.toHexString(Integer.parseInt(protocolVersion)));
			String epoch = isVaildEpoch(hexdump.substring(6, 10));
			outputMap.put("DTLS Epoch", epoch);
			String sequenceNumber = isValidSequenceNumber(hexdump.substring(10, 22));
			outputMap.put("DTLS Sequence Number", sequenceNumber);
			String length = isValidLength(hexdump.substring(22, 26));
			outputMap.put("DTLS Length", length);
			String fragement = hexdump.substring(26);
			outputMap.put("Random", fragement);
			int type = Integer.parseInt(hexdump.substring(0, 2), 16);
			String nextpacket;
			switch (type) {
			case 20:
				nextpacket = cipher(fragement, length, outputMap);
				if (null != nextpacket && !nextpacket.trim().isEmpty())
					list.add(verifyDTLSPacket(nextpacket));
				break;
			case 21:
				nextpacket = alert(fragement, length, outputMap);
				if (null != nextpacket && !nextpacket.trim().isEmpty())
					list.add(verifyDTLSPacket(nextpacket));
				break;
			case 22:
				nextpacket = handshake(fragement, outputMap);
				if (null != nextpacket && !nextpacket.trim().isEmpty())
					list.add(verifyDTLSPacket(nextpacket));
				break;
			case 23:
				nextpacket = application(fragement, length, outputMap);
				if (null != nextpacket && !nextpacket.trim().isEmpty())
					list.add(verifyDTLSPacket(nextpacket));
				break;
			case 24:
				nextpacket = heartbeat(fragement, length, outputMap);
				if (null != nextpacket && !nextpacket.trim().isEmpty())
					list.add(verifyDTLSPacket(nextpacket));
				break;
			default:
				break;
			}
		} catch (Exception e) {
			//e.printStackTrace();
			outputMapError.put("Error", "DTLS Protocol Malformed Packet - " + e.getMessage());
			LOGGER.error("dtls exception -> (message) {}", e.getMessage(), e);
			//return outputMapError;
			throw new CustomException(e.getMessage(),400);
		}
		LOGGER.info("<- verifyDTLSPacket : (outputMap) {}", outputMap);
		return outputMap;
	}

	private int isValidCertificateLength(String substring) {
		int certificateLength = Integer.parseInt(substring, 16);
		return certificateLength;
	}

	private int isValidCertificatesLength(String substring) {
		int certificatesLength = Integer.parseInt(substring, 16);
		return certificatesLength;
	}

	private int isValidExtensionsLength(String substring) {
		int sessionTicketLength = Integer.parseInt(substring, 16);
		return sessionTicketLength;
	}

	private int isValidSessionTicketLength(String substring) {
		int sessionTicketLength = Integer.parseInt(substring, 16);
		return sessionTicketLength;
	}

	private int isValidSessionTicketLifetimeHint(String sessiontkthex) {
		int compressionMethod = Integer.parseInt(sessiontkthex, 16);
		return compressionMethod;
	}

	private int isValidExtensionLength(String ExtensionLengthHex) throws CustomException {
		int extensionLength = 0;
		extensionLength = Integer.parseInt(ExtensionLengthHex, 16);
//		if(extensionLength>9)
//			throw new CustomException("Vector length "+extensionLength +"is too large, truncating it to 9");
//		else
		return extensionLength;
	}

	private String isValidCompressionmethod(String compressionMethodHex) throws CustomException {
		if (compressionMethodHex == null) {
			throw new CustomException("Compression method string cannot be null",1201);
		} else {
			int compressionMethod = 0;
			compressionMethod = Integer.parseInt(compressionMethodHex, 16);
			if (compressionMethod == 0) {
				return "null" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 1) {
				return "DEFLATE" + "(" + compressionMethod + ")";
			} else if ((compressionMethod >= 2 && compressionMethod <= 63)
					|| (compressionMethod >= 65 && compressionMethod <= 223)) {
				return "Unassigned" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 64) {
				return "LZS" + "(" + compressionMethod + ")";
			}
//			else if( compressionMethod>= 65 && compressionMethod<= 223) {
//				return "Unassigned"+"("+compressionMethod+")";
//			}
			else if (compressionMethod >= 224 && compressionMethod <= 255) {
				return "Reserved for Private Use" + "(" + compressionMethod + ")";
			} else {
				throw new CustomException("Exception in compression method string",1201);
			}

		}
	}

	private String isValidCipherSuits(String cipherHex) throws CustomException {
		String cipherSuit = "";
		cipherSuit = "0x" + cipherHex;
		return cipherSuit;
	}

	private int isValidCompressionLength(String compressionMethodHex) throws CustomException {
		int compressionMethodLength = 0;
		compressionMethodLength = Integer.parseInt(compressionMethodHex, 16);
		return compressionMethodLength;
	}

	private int isValidSessionIdLength1(String sessionIdHex) throws CustomException {
		int sessionIDLength = 0;
		sessionIDLength = Integer.parseInt(sessionIdHex, 16);
		return sessionIDLength;
	}

	private String isValidgmtUnixTime(String gmtString) throws CustomException {
		if (gmtString != null) {
			long gmtLong = 0;
			gmtLong = Long.parseLong(gmtString, 16);
			Date date = new Date();
			date.setTime((long) gmtLong * 1000);
			return date + "";
		} else
			throw new CustomException("Malformed packet",400);
		
	}

	private String isValidHandshakeTypeVersion(String versionHex) throws CustomException {
		String version = null;

		if ((versionHex.substring(3, 4).equals("0")))
			version = "DTLS 1.1" + "(" + "0x" + versionHex + ")";
		else if ((versionHex.substring(3, 4).equals("2")))
			version = "DTLS 1.2" + "(" + "0x" + versionHex + ")";
		else
			version = "Unknown" + "(" + "0x" + versionHex + ")";
		return version;
	}

	private int isValidPayloadLength(String substring) {
		int payloadLength = Integer.parseInt(substring, 16);
		return payloadLength;
	}

	private String isValidHeartBeatType(String substring) throws CustomException {
		int isValidHeartBeatType = Integer.parseInt(substring, 16);
		if (isValidHeartBeatType == 1) {
			return "Request" + "(" + isValidHeartBeatType + ")";
		} else if (isValidHeartBeatType == 2) {
			return "Response" + "(" + isValidHeartBeatType + ")";
		} else {
			throw new CustomException("Malformed packet " + substring,1201);
		}
	}

	private String isValidHandshakeTypeLength(String lengthHex) throws CustomException {
		String lengthString = null;
		lengthString = Integer.parseInt(lengthHex, 16) + "";
		return lengthString;
	}

	private String isValidChangeCipherSecProtocol(String CipherSecProtocol) {
		String CipherSecProtocolString = null;
		CipherSecProtocolString = Integer.parseInt(CipherSecProtocol, 16) + "";
		return CipherSecProtocolString;
	}

	private int isValidCipherLength(String CipherLengthHex) {
		int cipherLength = Integer.parseInt(CipherLengthHex, 16);
		return cipherLength;
	}

	private String isValidCipherSequenceNumber(String CipherSequenceHex) {
		String CipherSequenceString = null;
		CipherSequenceString = Integer.parseInt(CipherSequenceHex, 16) + "";
		return CipherSequenceString;
	}

	private String isValidCipherEpoch(String CipherEpochHex) {
		String CipherEpochString = null;
		CipherEpochString = Integer.parseInt(CipherEpochHex, 16) + "";
		return CipherEpochString;

	}

	private String isValidCipherVersion(String CipherVersionHex) {
		String CipherVersionString = null;
		CipherVersionString = Integer.parseInt(CipherVersionHex, 16) + "";
		return CipherVersionString;
	}

	private int isValidHandshakeTypeFragementLength(String FragmentLengthHex) {
		int sessionIDLength = 0;
		sessionIDLength = Integer.parseInt(FragmentLengthHex, 16);
		return sessionIDLength;
	}

	private String isValidHandshakeTypeFragement(String FragmentHex) {
		String fragementString = null;
		fragementString = Integer.parseInt(FragmentHex, 16) + "";
		return fragementString;
	}

	private String isValidHandshakeTypeLength1(String lengthHex) {
		String lengthString = null;
		lengthString = Integer.parseInt(lengthHex, 16) + "";
		return lengthString;
	}

	private String isValidHandshakeTypeMessage(String messageHex) {
		String messageString = null;
		messageString = Integer.parseInt(messageHex, 16) + "";
		return messageString;
	}

	private String isValidLength(String dtlsLength) throws CustomException {
		if (dtlsLength.length() != 4)
			throw new CustomException("DTLS length not valid" + dtlsLength,1201);
		return Integer.parseInt(dtlsLength, 16) + "";
	}

	private String isValidSequenceNumber(String sequenceNumber) throws CustomException {
		if (sequenceNumber.length() != 12)
			throw new CustomException("Sequence Number not valid" + sequenceNumber,1201);
		return Long.parseLong(sequenceNumber, 16) + "";

	}

	private String isVaildEpoch(String epoch) throws CustomException {
		if (epoch.length() != 4)
			throw new CustomException("Epoch not valid" + epoch,1201);
		return Integer.parseInt(epoch, 16) + "";
	}

	private String isValidProtocolVersion(String protocolVersion) throws CustomException {
		if (protocolVersion.length() != 4)
			throw new CustomException("Protocol Version not valid" + protocolVersion,1201);
		return Integer.parseInt(protocolVersion, 16) + "";

	}

	private static String isValidExtensionType(String extensionTypeHex) throws CustomException {
		if (extensionTypeHex == null) {
			throw new CustomException("Extension type string cannot be null",1201);
		} else {
			int compressionMethod = 0;
			compressionMethod = Integer.parseInt(extensionTypeHex, 16);
			if (compressionMethod == 0) {
				return "server_name" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 1) {
				return "max_fragment_length" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 2) {
				return "client_certificate_url" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 3) {
				return "trusted_ca_keys" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 4) {
				return "truncated_hmac" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 5) {
				return "status_request" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 6) {
				return "user_mapping" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 7) {
				return "client_authz" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 8) {
				return "server_authz" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 9) {
				return "cert_type" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 10) {
				return "supported_groups (renamed from \"elliptic_curves\")" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 11) {
				return "ec_point_formats" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 12) {
				return "srp" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 13) {
				return "signature_algorithms" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 14) {
				return "use_srtp" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 15) {
				return "heartbeat" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 16) {
				return "application_layer_protocol_negotiation" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 17) {
				return "status_request_v2" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 18) {
				return "signed_certificate_timestamp" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 19) {
				return "client_certificate_type" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 20) {
				return "server_certificate_type" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 21) {
				return "padding" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 22) {
				return "encrypt_then_mac" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 23) {
				return "extended_master_secret" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 24) {
				return "token_binding" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 25) {
				return "cached_info" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 26) {
				return "tls_lts" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 27) {
				return "compress_certificate" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 28) {
				return "record_size_limit" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 29) {
				return "pwd_protect" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 30) {
				return "pwd_clear" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 31) {
				return "password_salt" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 32) {
				return "ticket_pinning" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 33) {
				return "tls_cert_with_extern_psk" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 34) {
				return "delegated_credentials" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 35) {
				return "session_ticket (renamed from \"SessionTicket TLS\")" + "(" + compressionMethod + ")";
			} else if (compressionMethod >= 36 && compressionMethod <= 40) {
				return "Unassigned" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 41) {
				return "pre_shared_key" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 42) {
				return "early_data" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 43) {
				return "supported_versions" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 44) {
				return "cookie" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 45) {
				return "psk_key_exchange_modes" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 46) {
				return "Unassigned" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 47) {
				return "certificate_authorities" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 48) {
				return "oid_filters" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 49) {
				return "post_handshake_auth" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 50) {
				return "signature_algorithms_cert" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 51) {
				return "key_share" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 52) {
				return "transparency_info" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 53) {
				return "connection_id (TEMPORARY - registered 2019-07-02, expires 2020-07-02)" + "(" + compressionMethod
						+ ")";
			} else if (compressionMethod == 54) {
				return "Unassigned" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 55) {
				return "external_id_hash" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 56) {
				return "external_session_id" + "(" + compressionMethod + ")";
			} else if ((compressionMethod >= 57 && compressionMethod <= 2569)
					|| (compressionMethod >= 2571 && compressionMethod <= 6681)
					|| (compressionMethod >= 6683 && compressionMethod <= 10793)
					|| (compressionMethod >= 10795 && compressionMethod <= 14905)
					|| (compressionMethod >= 14907 && compressionMethod <= 19017)
					|| (compressionMethod >= 19019 && compressionMethod <= 23129)
					|| (compressionMethod >= 23131 && compressionMethod <= 27241)
					|| (compressionMethod >= 27243 && compressionMethod <= 31353)
					|| (compressionMethod >= 31355 && compressionMethod <= 35465)
					|| (compressionMethod >= 35467 && compressionMethod <= 39577)
					|| (compressionMethod >= 39579 && compressionMethod <= 43689)
					|| (compressionMethod >= 43691 && compressionMethod <= 47801)
					|| (compressionMethod >= 47803 && compressionMethod <= 51913)
					|| (compressionMethod >= 51915 && compressionMethod <= 56025)
					|| (compressionMethod >= 56027 && compressionMethod <= 60137)
					|| (compressionMethod >= 60139 && compressionMethod <= 64249)
					|| (compressionMethod >= 64251 && compressionMethod <= 65279)) {
				return "Unassigned" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 2570 || compressionMethod == 6682 || compressionMethod == 10794
					|| compressionMethod == 14906 || compressionMethod == 19018 || compressionMethod == 23130
					|| compressionMethod == 31354 || compressionMethod == 35466 || compressionMethod == 39578
					|| compressionMethod == 43690 || compressionMethod == 47802 || compressionMethod == 51914
					|| compressionMethod == 56026 || compressionMethod == 60138 || compressionMethod == 64250) {
				return "Reserved" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 65280) {
				return "Reserved for Private Use" + "(" + compressionMethod + ")";
			} else if (compressionMethod == 65281) {
				return "renegotiation_info" + "(" + compressionMethod + ")";
			} else if ((compressionMethod >= 65282 && compressionMethod >= 65535)) {
				return "Reserved for Private Use" + "(" + compressionMethod + ")";
			} else {
				throw new CustomException("Error in Extension type",1201);
			}
		}
	}

	private String isValidHandshakeType(String handShakeHex) throws CustomException {
		String handShakeString = null, handshakeType = null;
		handShakeString = Integer.parseInt(handShakeHex, 16) + "";
		if (handShakeString.equals("0")) {
			handshakeType = "Hello Request" + "(" + handShakeString + ")";
			return handshakeType;
		} else if (handShakeString.equals("1")) {
			handshakeType = "Client Hello" + "(" + handShakeString + ")";
			return handshakeType;
		} else if (handShakeString.equals("2")) {
			handshakeType = "Server Hello" + "(" + handShakeString + ")";
			return handshakeType;
		} else if (handShakeString.equals("4")) {
			handshakeType = "New Session Ticket" + "(" + handShakeString + ")";
			return handshakeType;
		} else if (handShakeString.equals("8")) {
			handshakeType = "Encrypted Extensions(TLS 1.3 Only)" + "(" + handShakeString + ")";
			return handshakeType;
		} else if (handShakeString.equals("11")) {
			handshakeType = "Certificate" + "(" + handShakeString + ")";
			return handshakeType;
		} else if (handShakeString.equals("8")) {
			handshakeType = "Server Key Exchange" + "(" + handShakeString + ")";
			return handshakeType;
		} else if (handShakeString.equals("13")) {
			handshakeType = "Certificate request" + "(" + handShakeString + ")";
			return handshakeType;
		} else if (handShakeString.equals("14")) {
			handshakeType = "Server Hello Done" + "(" + handShakeString + ")";
			return handshakeType;
		} else if (handShakeString.equals("15")) {
			handshakeType = "Certificate verify" + "(" + handShakeString + ")";
			return handshakeType;
		} else if (handShakeString.equals("16")) {
			handshakeType = "Client Key Exchange" + "(" + handShakeString + ")";
			return handshakeType;
		} else if (handShakeString.equals("20")) {
			handshakeType = "Finished" + "(" + handShakeString + ")";
			return handshakeType;
		} else {
		}
		return handShakeString;
	}

	private String isValidAlertDescription(String substring) throws CustomException {
		int isValidAlertDescription = Integer.parseInt(substring, 16);
		if (isValidAlertDescription == 0) {
			return "Close Notify" + "(" + isValidAlertDescription + ")";
		} else if (isValidAlertDescription == 10) {
			return "Unexpected message" + "(" + isValidAlertDescription + ")";
		} else if (isValidAlertDescription == 20) {
			return "Bad record MAC" + "(" + isValidAlertDescription + ")";
		} else if (isValidAlertDescription == 21) {
			return "Decryption failed" + "(" + isValidAlertDescription + ")";
		} else if (isValidAlertDescription == 22) {
			return "Record overflow" + "(" + isValidAlertDescription + ")";
		} else if (isValidAlertDescription == 30) {
			return "Decompression failure" + "(" + isValidAlertDescription + ")";
		} else if (isValidAlertDescription == 40) {
			return "Handshake failure" + "(" + isValidAlertDescription + ")";
		} else if (isValidAlertDescription == 41) {
			return "No certificate" + "(" + isValidAlertDescription + ")";
		} else if (isValidAlertDescription == 42) {
			return "Bad certificate" + "(" + isValidAlertDescription + ")";
		} else if (isValidAlertDescription == 43) {
			return "Unsupported certificate" + "(" + isValidAlertDescription + ")";
		} else if (isValidAlertDescription == 44) {
			return "Certificate revoked" + "(" + isValidAlertDescription + ")";
		} else if (isValidAlertDescription == 45) {
			return "Certificate expired" + "(" + isValidAlertDescription + ")";
		} else if (isValidAlertDescription == 46) {
			return "Certificate unknown" + "(" + isValidAlertDescription + ")";
		} else if (isValidAlertDescription == 47) {
			return "Illegal parameter" + "(" + isValidAlertDescription + ")";
		} else if (isValidAlertDescription == 48) {
			return "Unknown CA(Certificate authority)" + "(" + isValidAlertDescription + ")";
		} else if (isValidAlertDescription == 49) {
			return "Access denied" + "(" + isValidAlertDescription + ")";
		} else if (isValidAlertDescription == 50) {
			return "Decode error" + "(" + isValidAlertDescription + ")";
		} else if (isValidAlertDescription == 51) {
			return "Decrypt error" + "(" + isValidAlertDescription + ")";
		} else if (isValidAlertDescription == 60) {
			return "Expert restriction" + "(" + isValidAlertDescription + ")";
		} else if (isValidAlertDescription == 70) {
			return "Protocol version" + "(" + isValidAlertDescription + ")";
		} else if (isValidAlertDescription == 71) {
			return "Insufficient security" + "(" + isValidAlertDescription + ")";
		} else if (isValidAlertDescription == 80) {
			return "Internal error" + "(" + isValidAlertDescription + ")";
		} else if (isValidAlertDescription == 86) {
			return "Inappropriate Fallback" + "(" + isValidAlertDescription + ")";
		} else if (isValidAlertDescription == 90) {
			return "User cancelled" + "(" + isValidAlertDescription + ")";
		} else if (isValidAlertDescription == 100) {
			return "No renegotiation" + "(" + isValidAlertDescription + ")";
		} else if (isValidAlertDescription == 110) {
			return "Unsupported extension" + "(" + isValidAlertDescription + ")";
		} else if (isValidAlertDescription == 111) {
			return "Certificate unobtainable" + "(" + isValidAlertDescription + ")";
		} else if (isValidAlertDescription == 112) {
			return "Unrecognized name" + "(" + isValidAlertDescription + ")";
		} else if (isValidAlertDescription == 113) {
			return "Bad certificate status response" + "(" + isValidAlertDescription + ")";
		} else if (isValidAlertDescription == 114) {
			return "Bad certificate hash value" + "(" + isValidAlertDescription + ")";
		} else if (isValidAlertDescription == 115) {
			return "Unknown PSK identity(used in TLS-PSK and TLS-SRP)" + "(" + isValidAlertDescription + ")";
		} else if (isValidAlertDescription == 120) {
			return "No application protocol" + "(" + isValidAlertDescription + ")";
		} else {
			throw new CustomException("Malformed packet" + isValidAlertDescription,1201);
		}
	}

	String cipher(String cipher) throws CustomException {
		// String cipher = (cipherList);
		if (cipher.equalsIgnoreCase("c014")) {
			return "cipher suite:TLS_RSA_ECDSA_WITH_AES_128_CBC_SHA256";
		} else if (cipher.equalsIgnoreCase("c00a")) {
			return "cipher suite:TLS_ECDH_ECDSA_WITH_AES_128_CBC_SHA256";
		} else if (cipher.equalsIgnoreCase("c022")) {
			return "cipher suite:TLS_SRP_SHA_DSS_WITH_AES_256_CBC_SHA";
		} else if (cipher.equalsIgnoreCase("c021")) {
			return "cipher suite:TLS_SRP_SHA_RSA_WITH_AES_256_CBC_SHA";
		} else if (cipher.equalsIgnoreCase("0038")) {
			return "cipher suite:TLS_DHE_DSS_WITH_AES_256_CBC_SHA256";
		} else if (cipher.equalsIgnoreCase("0039")) {
			return "cipher suite:TLS_DHE_RSA_WITH_AES_256_CBC_SHA256";
		} else if (cipher.equalsIgnoreCase("0087")) {
			return "cipher suite:TLS_DHE_DSS_WITH_CAMELLIA_256_CBC_SHA";
		} else if (cipher.equalsIgnoreCase("0088")) {
			return "cipher suite:TLS_DHE_RSA_WITH_CAMELLIA_256_CBC_SHA";
		} else if (cipher.equalsIgnoreCase("c00f")) {
			return "cipher suite:TLS_ECDH_RSA_WITH_AES_128_CBC_SHA256";
		} else if (cipher.equalsIgnoreCase("c005")) {
			return "cipher suite:TLS_ECDH_ECDSA_WITH_AES_128_CBC_SHA256";
		} else if (cipher.equalsIgnoreCase("0035")) {
			return "cipher suite:TLS_RSA_WITH_AES_256_CBC_SHA";
		} else if (cipher.equalsIgnoreCase("0084")) {
			return "cipher suite:TLS_RSA_WITH_CAMELLIA_256_CBC_SHA";
		} else if (cipher.equalsIgnoreCase("c008")) {
			return "cipher suite:TLS_ECDHE_ECDSA_WITH_3DES_EDE_CBC_SHA";
		} else if (cipher.equalsIgnoreCase("c012")) {
			return "cipher suite:TLS_ECDHE_RSA_WITH_3DES_EDE_CBC_SHA";
		} else if (cipher.equalsIgnoreCase("c01b")) {
			return "cipher suite:TLS_SRP_SHA_RSA_WITH_3DES_EDE_CBC_SHA";
		} else if (cipher.equalsIgnoreCase("c01c")) {
			return "cipher suite:TLS_SRP_SHA_DSS_WITH_3DES_EDE_CBC_SHA";
		} else if (cipher.equalsIgnoreCase("0013")) {
			return "cipher suite:TLS_DHE_DSS_WITH_3DES_EDE_CBC_SHA";
		} else if (cipher.equalsIgnoreCase("0016")) {
			return "cipher suite:TLS_DHE_RSA_WITH_3DES_EDE_CBC_SHA";
		} else if (cipher.equalsIgnoreCase("c00d")) {
			return "cipher suite:TLS_ECDH_RSA_WITH_3DES_EDE_CBC_SHA";
		} else if (cipher.equalsIgnoreCase("c003")) {
			return "cipher suite:TLS_ECDH_ECDSA_WITH_3DES_EDE_CBC_SHA";
		} else if (cipher.equalsIgnoreCase("000a")) {
			return "cipher suite:TLS_RSA_WITH_3DES_EDE_CBC_SHA";
		} else if (cipher.equalsIgnoreCase("c013")) {
			return "cipher suite:TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA";
		} else if (cipher.equalsIgnoreCase("c009")) {
			return "cipher suite:TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA";
		} else if (cipher.equalsIgnoreCase("c01f")) {
			return "cipher suite:TLS_SRP_SHA_DSS_WITH_AES_128_CBC_SHA";
		} else if (cipher.equalsIgnoreCase("c01e")) {
			return "cipher suite:TLS_SRP_SHA_RSA_WITH_AES_128_CBC_SHA";
		} else if (cipher.equalsIgnoreCase("0033")) {
			return "cipher suite:TLS_DHE_RSA_WITH_AES_128_CBC_SHA ";
		} else if (cipher.equalsIgnoreCase("0032")) {
			return "cipher suite:TLS_DHE_DSS_WITH_AES_128_CBC_SHA";
		} else if (cipher.equalsIgnoreCase("0099")) {
			return "cipher suite:TLS_DHE_DSS_WITH_SEED_CBC_SHA";
		} else if (cipher.equalsIgnoreCase("009a")) {
			return "cipher suite:TLS_DHE_RSA_WITH_SEED_CBC_SHA";
		} else if (cipher.equalsIgnoreCase("0044")) {
			return "cipher suite:TLS_DHE_DSS_WITH_CAMELLIA_128_CBC_SHA";
		} else if (cipher.equalsIgnoreCase("0045")) {
			return "cipher suite:TLS_DHE_RSA_WITH_CAMELLIA_128_CBC_SHA";
		} else if (cipher.equalsIgnoreCase("c00e")) {
			return "cipher suite:TLS_ECDH_RSA_WITH_AES_128_CBC_SHA";
		} else if (cipher.equalsIgnoreCase("c004")) {
			return "cipher suite:TLS_ECDH_ECDSA_WITH_AES_128_CBC_SHA";
		} else if (cipher.equalsIgnoreCase("002f")) {
			return "cipher suite:TLS_RSA_WITH_AES_128_CBC_SHA";
		} else if (cipher.equalsIgnoreCase("0096")) {
			return "cipher suite:TLS_RSA_WITH_SEED_CBC_SHA";
		} else if (cipher.equalsIgnoreCase("0041")) {
			return "cipher suite:TLS_RSA_WITH_CAMELLIA_128_CBC_SHA";
		} else if (cipher.equalsIgnoreCase("0015")) {
			return "cipher suite:TLS_DHE_RSA_WITH_DES_CBC_SHA";
		} else if (cipher.equalsIgnoreCase("0012")) {
			return "cipher suite:TLS_DHE_DSS_WITH_DES_CBC_SHA";
		} else if (cipher.equalsIgnoreCase("0009")) {
			return "cipher suite:TLS_RSA_WITH_DES_CBC_SHA";
		} else if (cipher.equalsIgnoreCase("0014")) {
			return "cipher suite:TLS_DHE_RSA_WITH_EXPORT_WITH_DES40_CBC_SHA";
		} else if (cipher.equalsIgnoreCase("0011")) {
			return "cipher suite:TLS_DHE_DSS_WITH_EXPORT_WITH_DES40_CBC_SHA";
		} else if (cipher.equalsIgnoreCase("0008")) {
			return "cipher suite:TLS_RSA_EXPORT_WITH_DES40_CBC_SHA";
		} else if (cipher.equalsIgnoreCase("0006")) {
			return "cipher suite:TLS_RSA_EXPORT_WITH_RC2_CBC_40_MD5";
		} else if (cipher.equalsIgnoreCase("00ff")) {
			return "cipher suite:TLS_EMPTY_RENEGOTIATION_INFO_SCVS";
		} else if (cipher.equalsIgnoreCase("0007")) {
			return "TLS_RSA_WITH_IDEA_CBC_SHA	";
		} else if (cipher.equalsIgnoreCase("0009")) {
			return "TLS_RSA_WITH_DES_CBC_SHA	";
		} else if (cipher.equalsIgnoreCase("000b")) {
			return "TLS_DH_DSS_EXPORT_WITH_DES40_CBC_SHA";
		} else if (cipher.equalsIgnoreCase("000c")) {
			return "TLS_DH_DSS_WITH_DES_CBC_SHA	";
		} else if (cipher.equalsIgnoreCase("000d")) {
			return "TLS_DH_DSS_WITH_3DES_EDE_CBC_SHA";
		} else if (cipher.equalsIgnoreCase("000e")) {
			return "TLS_DH_RSA_EXPORT_WITH_DES40_CBC_SHA";
		} else if (cipher.equalsIgnoreCase("000f")) {
			return "TLS_DH_RSA_WITH_DES_CBC_SHA";
		} else if (cipher.equalsIgnoreCase("0010")) {
			return "TLS_DH_RSA_WITH_3DES_EDE_CBC_SHA";
		} else if (cipher.equalsIgnoreCase("c03c")) {
			return "TLS_RSA_WITH_ARIA_128_CBC_SHA256";
		} else if (cipher.equalsIgnoreCase("c03d")) {
			return "TLS_RSA_WITH_ARIA_256_CBC_SHA384";
		} else if (cipher.equalsIgnoreCase("co3e")) {
			return "TLS_DH_DSS_WITH_ARIA_128_CBC_SHA256";
		} else if (cipher.equalsIgnoreCase("c03f")) {
			return "TLS_DH_DSS_WITH_ARIA_256_CBC_SHA384";
		} else if (cipher.equalsIgnoreCase("c040")) {
			return "TLS_DH_RSA_WITH_ARIA_128_CBC_SHA256";
		} else if (cipher.equalsIgnoreCase("c041")) {
			return "TLS_DH_RSA_WITH_ARIA_256_CBC_SHA384";
		} else if (cipher.equalsIgnoreCase("c042")) {
			return "TLS_DHE_DSS_WITH_ARIA_128_CBC_SHA256";
		} else if (cipher.equalsIgnoreCase("c043")) {
			return "TLS_DHE_DSS_WITH_ARIA_256_CBC_SHA384";
		} else if (cipher.equalsIgnoreCase("c044")) {
			return "TLS_DHE_RSA_WITH_ARIA_128_CBC_SHA256";
		} else if (cipher.equalsIgnoreCase("c045")) {
			return "TLS_DHE_RSA_WITH_ARIA_256_CBC_SHA384";
		} else if (cipher.equalsIgnoreCase("c046")) {
			return "TLS_DH_anon_WITH_ARIA_128_CBC_SHA256";
		} else if (cipher.equalsIgnoreCase("c047")) {
			return "TLS_DH_anon_WITH_ARIA_256_CBC_SHA384";
		} else if (cipher.equalsIgnoreCase("c048")) {
			return "TLS_ECDHE_ECDSA_WITH_ARIA_128_CBC_SHA256";
		} else if (cipher.equalsIgnoreCase("c049")) {
			return "TLS_ECDHE_ECDSA_WITH_ARIA_256_CBC_SHA384";
		} else if (cipher.equalsIgnoreCase("c04a")) {
			return "TLS_ECDH_ECDSA_WITH_ARIA_128_CBC_SHA256";
		} else if (cipher.equalsIgnoreCase("c04b")) {
			return "TLS_ECDH_ECDSA_WITH_ARIA_256_CBC_SHA384";
		} else if (cipher.equalsIgnoreCase("c04c")) {
			return "TLS_ECDHE_RSA_WITH_ARIA_128_CBC_SHA256";
		} else if (cipher.equalsIgnoreCase("c04d")) {
			return "TLS_ECDHE_RSA_WITH_ARIA_256_CBC_SHA384";
		} else if (cipher.equalsIgnoreCase("c04e")) {
			return "TLS_ECDH_RSA_WITH_ARIA_128_CBC_SHA256";
		} else if (cipher.equalsIgnoreCase("c04f")) {
			return "TLS_ECDH_RSA_WITH_ARIA_256_CBC_SHA384";
		} else if (cipher.equalsIgnoreCase("c050")) {
			return "TLS_RSA_WITH_ARIA_128_GCM_SHA256";
		} else if (cipher.equalsIgnoreCase("c051")) {
			return "TLS_RSA_WITH_ARIA_256_GCM_SHA384";
		} else if (cipher.equalsIgnoreCase("c052")) {
			return "TLS_DHE_RSA_WITH_ARIA_128_GCM_SHA256";
		} else if (cipher.equalsIgnoreCase("c053")) {
			return "TLS_DHE_RSA_WITH_ARIA_256_GCM_SHA384";
		} else if (cipher.equalsIgnoreCase("c054")) {
			return "TLS_DH_RSA_WITH_ARIA_128_GCM_SHA256";
		} else if (cipher.equalsIgnoreCase("c055")) {
			return "TLS_DH_RSA_WITH_ARIA_256_GCM_SHA384";
		} else if (cipher.equalsIgnoreCase("c056")) {
			return "TLS_DHE_DSS_WITH_ARIA_128_GCM_SHA256";
		} else if (cipher.equalsIgnoreCase("c057")) {
			return "TLS_DHE_DSS_WITH_ARIA_256_GCM_SHA384";
		} else if (cipher.equalsIgnoreCase("c058")) {
			return "TLS_DH_DSS_WITH_ARIA_128_GCM_SHA256";
		} else if (cipher.equalsIgnoreCase("c059")) {
			return "TLS_DH_DSS_WITH_ARIA_256_GCM_SHA384";
		} else if (cipher.equalsIgnoreCase("c05a")) {
			return "TLS_DH_anon_WITH_ARIA_128_GCM_SHA256";
		} else if (cipher.equalsIgnoreCase("c05b")) {
			return "TLS_DH_anon_WITH_ARIA_256_GCM_SHA384";
		} else if (cipher.equalsIgnoreCase("c05c")) {
			return "TLS_ECDHE_ECDSA_WITH_ARIA_128_GCM_SHA256";
		} else if (cipher.equalsIgnoreCase("c05d")) {
			return "TLS_ECDHE_ECDSA_WITH_ARIA_256_GCM_SHA384";
		} else if (cipher.equalsIgnoreCase("c05e")) {
			return "TLS_ECDH_ECDSA_WITH_ARIA_128_GCM_SHA256";
		} else if (cipher.equalsIgnoreCase("c05f")) {
			return "TLS_ECDH_ECDSA_WITH_ARIA_256_GCM_SHA384";
		} else if (cipher.equalsIgnoreCase("c060")) {
			return "TLS_ECDHE_RSA_WITH_ARIA_128_GCM_SHA256";
		} else if (cipher.equalsIgnoreCase("c061")) {
			return "TLS_ECDHE_RSA_WITH_ARIA_256_GCM_SHA384";
		} else if (cipher.equalsIgnoreCase("c062")) {
			return "TLS_ECDH_RSA_WITH_ARIA_128_GCM_SHA256";
		} else if (cipher.equalsIgnoreCase("c063")) {
			return "TLS_ECDH_RSA_WITH_ARIA_256_GCM_SHA384";
		} else if (cipher.equalsIgnoreCase("c064")) {
			return "TLS_PSK_WITH_ARIA_128_CBC_SHA256";
		} else if (cipher.equalsIgnoreCase("c065")) {
			return "TLS_PSK_WITH_ARIA_256_CBC_SHA384";
		} else if (cipher.equalsIgnoreCase("c066")) {
			return "TLS_DHE_PSK_WITH_ARIA_128_CBC_SHA256";
		} else if (cipher.equalsIgnoreCase("c067")) {
			return "TLS_DHE_PSK_WITH_ARIA_256_CBC_SHA384";
		} else if (cipher.equalsIgnoreCase("c068")) {
			return "TLS_RSA_PSK_WITH_ARIA_128_CBC_SHA256";
		} else if (cipher.equalsIgnoreCase("c069")) {
			return "TLS_RSA_PSK_WITH_ARIA_256_CBC_SHA384";
		} else if (cipher.equalsIgnoreCase("c06a")) {
			return "TLS_PSK_WITH_ARIA_128_GCM_SHA256";
		} else if (cipher.equalsIgnoreCase("c06b")) {
			return "TLS_PSK_WITH_ARIA_256_GCM_SHA384";
		} else if (cipher.equalsIgnoreCase("c06c")) {
			return "TLS_DHE_PSK_WITH_ARIA_128_GCM_SHA256";
		} else if (cipher.equalsIgnoreCase("c06d")) {
			return "TLS_DHE_PSK_WITH_ARIA_256_GCM_SHA384";
		} else if (cipher.equalsIgnoreCase("c06e")) {
			return "TLS_RSA_PSK_WITH_ARIA_128_GCM_SHA256";
		} else if (cipher.equalsIgnoreCase("c06f")) {
			return "TLS_RSA_PSK_WITH_ARIA_256_GCM_SHA384";
		} else if (cipher.equalsIgnoreCase("c070")) {
			return "TLS_ECDHE_PSK_WITH_ARIA_128_CBC_SHA256";
		} else if (cipher.equalsIgnoreCase("c071")) {
			return "TLS_ECDHE_PSK_WITH_ARIA_256_CBC_SHA384";
		} else if (cipher.equalsIgnoreCase("c072")) {
			return "TLS_ECDHE_ECDSA_WITH_CAMELLIA_128_CBC_SHA256";
		} else if (cipher.equalsIgnoreCase("c073")) {
			return "TLS_ECDHE_ECDSA_WITH_CAMELLIA_256_CBC_SHA384";
		} else if (cipher.equalsIgnoreCase("c074")) {
			return "TLS_ECDH_ECDSA_WITH_CAMELLIA_128_CBC_SHA256";
		} else if (cipher.equalsIgnoreCase("c075")) {
			return "TLS_ECDH_ECDSA_WITH_CAMELLIA_256_CBC_SHA384";
		} else if (cipher.equalsIgnoreCase("c076")) {
			return "TLS_ECDHE_RSA_WITH_CAMELLIA_128_CBC_SHA256";
		} else if (cipher.equalsIgnoreCase("c077")) {
			return "TLS_ECDHE_RSA_WITH_CAMELLIA_256_CBC_SHA384";
		} else if (cipher.equalsIgnoreCase("c078")) {
			return "TLS_ECDH_RSA_WITH_CAMELLIA_128_CBC_SHA256";
		} else if (cipher.equalsIgnoreCase("c079")) {
			return "TLS_ECDH_RSA_WITH_CAMELLIA_256_CBC_SHA384";
		} else if (cipher.equalsIgnoreCase("c07a")) {
			return "TLS_RSA_WITH_CAMELLIA_128_GCM_SHA256";
		} else if (cipher.equalsIgnoreCase("c07b")) {
			return "TLS_RSA_WITH_CAMELLIA_256_GCM_SHA384";
		} else if (cipher.equalsIgnoreCase("c07c")) {
			return "TLS_DHE_RSA_WITH_CAMELLIA_128_GCM_SHA256";
		} else if (cipher.equalsIgnoreCase("c07d")) {
			return "TLS_DHE_RSA_WITH_CAMELLIA_256_GCM_SHA384";
		} else if (cipher.equalsIgnoreCase("c07e")) {
			return "TLS_DH_RSA_WITH_CAMELLIA_128_GCM_SHA256";
		} else if (cipher.equalsIgnoreCase("c07f")) {
			return "TLS_DH_RSA_WITH_CAMELLIA_256_GCM_SHA384";
		} else if (cipher.equalsIgnoreCase("c080")) {
			return "TLS_DHE_DSS_WITH_CAMELLIA_128_GCM_SHA256";
		} else if (cipher.equalsIgnoreCase("c081")) {
			return "TLS_DHE_DSS_WITH_CAMELLIA_256_GCM_SHA384";
		} else if (cipher.equalsIgnoreCase("c082")) {
			return "TLS_DH_DSS_WITH_CAMELLIA_128_GCM_SHA256";
		} else if (cipher.equalsIgnoreCase("c083")) {
			return "TLS_DH_DSS_WITH_CAMELLIA_256_GCM_SHA384";
		} else if (cipher.equalsIgnoreCase("c084")) {
			return "TLS_DH_anon_WITH_CAMELLIA_128_GCM_SHA256";
		} else if (cipher.equalsIgnoreCase("c085")) {
			return "TLS_DH_anon_WITH_CAMELLIA_256_GCM_SHA384";
		} else if (cipher.equalsIgnoreCase("c086")) {
			return "TLS_ECDHE_ECDSA_WITH_CAMELLIA_128_GCM_SHA256";
		} else if (cipher.equalsIgnoreCase("c087")) {
			return "TLS_ECDHE_ECDSA_WITH_CAMELLIA_256_GCM_SHA384";
		} else if (cipher.equalsIgnoreCase("c088")) {
			return "TLS_ECDH_ECDSA_WITH_CAMELLIA_128_GCM_SHA256";
		} else if (cipher.equalsIgnoreCase("c089")) {
			return "TLS_ECDH_ECDSA_WITH_CAMELLIA_256_GCM_SHA384";
		} else if (cipher.equalsIgnoreCase("c08a")) {
			return "TLS_ECDHE_RSA_WITH_CAMELLIA_128_GCM_SHA256";
		} else if (cipher.equalsIgnoreCase("c08b")) {
			return "TLS_ECDHE_RSA_WITH_CAMELLIA_256_GCM_SHA384";
		} else if (cipher.equalsIgnoreCase("c08c")) {
			return "TLS_ECDH_RSA_WITH_CAMELLIA_128_GCM_SHA256";
		} else if (cipher.equalsIgnoreCase("c08d")) {
			return "TLS_ECDH_RSA_WITH_CAMELLIA_256_GCM_SHA384";
		} else if (cipher.equalsIgnoreCase("c08e")) {
			return "TLS_PSK_WITH_CAMELLIA_128_GCM_SHA256";
		} else if (cipher.equalsIgnoreCase("c08f")) {
			return "TLS_PSK_WITH_CAMELLIA_256_GCM_SHA384";
		} else if (cipher.equalsIgnoreCase("c090")) {
			return "TLS_DHE_PSK_WITH_CAMELLIA_128_GCM_SHA256";
		} else if (cipher.equalsIgnoreCase("c091")) {
			return "TLS_DHE_PSK_WITH_CAMELLIA_256_GCM_SHA384";
		} else if (cipher.equalsIgnoreCase("c092")) {
			return "TLS_RSA_PSK_WITH_CAMELLIA_128_GCM_SHA256";
		} else if (cipher.equalsIgnoreCase("c093")) {
			return "TLS_RSA_PSK_WITH_CAMELLIA_256_GCM_SHA384";
		} else if (cipher.equalsIgnoreCase("c094")) {
			return "TLS_PSK_WITH_CAMELLIA_128_CBC_SHA256";
		} else if (cipher.equalsIgnoreCase("c095")) {
			return "TLS_PSK_WITH_CAMELLIA_256_CBC_SHA384";
		} else if (cipher.equalsIgnoreCase("c096")) {
			return "TLS_DHE_PSK_WITH_CAMELLIA_128_CBC_SHA256";
		} else if (cipher.equalsIgnoreCase("c097")) {
			return "TLS_DHE_PSK_WITH_CAMELLIA_256_CBC_SHA384";
		} else if (cipher.equalsIgnoreCase("c098")) {
			return "TLS_RSA_PSK_WITH_CAMELLIA_128_CBC_SHA256";
		} else if (cipher.equalsIgnoreCase("c099")) {
			return "TLS_RSA_PSK_WITH_CAMELLIA_256_CBC_SHA384";
		} else if (cipher.equalsIgnoreCase("c09a")) {
			return "TLS_ECDHE_PSK_WITH_CAMELLIA_128_CBC_SHA256";
		} else if (cipher.equalsIgnoreCase("c09b")) {
			return "TLS_ECDHE_PSK_WITH_CAMELLIA_256_CBC_SHA384";
		} else if (cipher.equalsIgnoreCase("c09c")) {
			return "TLS_RSA_WITH_AES_128_CCM";
		} else if (cipher.equalsIgnoreCase("c09d")) {
			return "TLS_RSA_WITH_AES_256_CCM";
		} else if (cipher.equalsIgnoreCase("c09e")) {
			return "TLS_DHE_RSA_WITH_AES_128_CCM";
		} else if (cipher.equalsIgnoreCase("c09f")) {
			return "TLS_DHE_RSA_WITH_AES_256_CCM";
		} else if (cipher.equalsIgnoreCase("c0a0")) {
			return "TLS_RSA_WITH_AES_128_CCM_8";
		} else if (cipher.equalsIgnoreCase("c0a1")) {
			return "TLS_RSA_WITH_AES_256_CCM_8";
		} else if (cipher.equalsIgnoreCase("c0a2")) {
			return "TLS_DHE_RSA_WITH_AES_128_CCM_8";
		} else if (cipher.equalsIgnoreCase("c0a4")) {
			return "TLS_PSK_WITH_AES_128_CCM";
		} else if (cipher.equalsIgnoreCase("c0a5")) {
			return "TLS_PSK_WITH_AES_256_CCM";
		} else if (cipher.equalsIgnoreCase("c0a6")) {
			return "TLS_DHE_PSK_WITH_AES_128_CCM";
		} else if (cipher.equalsIgnoreCase("c0a7")) {
			return "TLS_DHE_PSK_WITH_AES_256_CCM";
		} else if (cipher.equalsIgnoreCase("c0a8")) {
			return "TLS_PSK_WITH_AES_128_CCM_8";
		} else if (cipher.equalsIgnoreCase("c0a9")) {
			return "TLS_PSK_WITH_AES_256_CCM_8";
		} else if (cipher.equalsIgnoreCase("c0aa")) {
			return "TLS_PSK_DHE_WITH_AES_128_CCM_8";
		} else if (cipher.equalsIgnoreCase("c0ab")) {
			return "TLS_PSK_DHE_WITH_AES_256_CCM_8";
		} else if (cipher.equalsIgnoreCase("c0ac")) {
			return "TLS_ECDHE_ECDSA_WITH_AES_128_CCM";
		} else if (cipher.equalsIgnoreCase("c0ad")) {
			return "TLS_ECDHE_ECDSA_WITH_AES_256_CCM";
		} else if (cipher.equalsIgnoreCase("c0ae")) {
			return "TLS_ECDHE_ECDSA_WITH_AES_128_CCM_8";
		} else if (cipher.equalsIgnoreCase("c0af")) {
			return "TLS_ECDHE_ECDSA_WITH_AES_256_CCM_8";
		} else if (cipher.equalsIgnoreCase("c0b0")) {
			return "TLS_ECCPWD_WITH_AES_128_GCM_SHA256";
		} else if (cipher.equalsIgnoreCase("c0b1")) {
			return "TLS_ECCPWD_WITH_AES_256_GCM_SHA384";
		} else if (cipher.equalsIgnoreCase("c0b2")) {
			return "TLS_ECCPWD_WITH_AES_128_CCM_SHA256";
		} else if (cipher.equalsIgnoreCase("c0b3")) {
			return "TLS_ECCPWD_WITH_AES_256_CCM_SHA384";
		} else if (cipher.equalsIgnoreCase("c0b4")) {
			return "TLS_SHA256_SHA256";
		} else if (cipher.equalsIgnoreCase("c0b5")) {
			return "TLS_SHA384_SHA384";
		} else if (cipher.equalsIgnoreCase("c001")) {
			return "TLS_ECDH_ECDSA_WITH_NULL_SHA";
		} else if (cipher.equalsIgnoreCase("0001")) {
			return "TLS_RSA_WITH_NULL_MD5	";
		} else if (cipher.equalsIgnoreCase("0002")) {
			return "TLS_RSA_WITH_NULL_SHA	";
		} else if (cipher.equalsIgnoreCase("0007")) {
			return "TLS_RSA_WITH_IDEA_CBC_SHA	";
		} else if (cipher.equalsIgnoreCase("0008")) {
			return "TLS_RSA_EXPORT_WITH_DES40_CBC_SHA	";
		} else if (cipher.equalsIgnoreCase("0009")) {
			return "TLS_RSA_WITH_DES_CBC_SHA	";
		} else if (cipher.equalsIgnoreCase("001e")) {
			return "TLS_KRB5_WITH_DES_CBC_SHA	";
		} else if (cipher.equalsIgnoreCase("001f")) {
			return "TLS_KRB5_WITH_3DES_EDE_CBC_SHA	";
		} else if (cipher.equalsIgnoreCase("0020")) {
			return "TLS_KRB5_WITH_RC4_128_SHA	";
		} else if (cipher.equalsIgnoreCase("0021")) {
			return "TLS_KRB5_WITH_IDEA_CBC_SHA";
		} else if (cipher.equalsIgnoreCase("0022")) {
			return "TLS_KRB5_WITH_DES_CBC_MD5";
		} else if (cipher.equalsIgnoreCase("0023")) {
			return "TLS_KRB5_WITH_3DES_EDE_CBC_MD5";
		} else if (cipher.equalsIgnoreCase("0024")) {
			return "TLS_KRB5_WITH_RC4_128_MD5";
		} else if (cipher.equalsIgnoreCase("0025")) {
			return "TLS_KRB5_WITH_IDEA_CBC_MD5";
		} else if (cipher.equalsIgnoreCase("0026")) {
			return "TLS_KRB5_EXPORT_WITH_DES_CBC_40_SHA";
		} else if (cipher.equalsIgnoreCase("0027")) {
			return "TLS_KRB5_EXPORT_WITH_RC2_CBC_40_SHA";
		} else if (cipher.equalsIgnoreCase("0028")) {
			return "TLS_KRB5_EXPORT_WITH_RC4_40_SHA";
		} else if (cipher.equalsIgnoreCase("0029")) {
			return "TLS_KRB5_EXPORT_WITH_DES_CBC_40_MD5";
		} else if (cipher.equalsIgnoreCase("002a")) {
			return "TLS_KRB5_EXPORT_WITH_RC2_CBC_40_MD5";
		} else if (cipher.equalsIgnoreCase("002b")) {
			return "TLS_KRB5_EXPORT_WITH_RC4_40_MD5";
		} else if (cipher.equalsIgnoreCase("002c")) {
			return "TLS_PSK_WITH_NULL_SHA";
		} else if (cipher.equalsIgnoreCase("002d")) {
			return "TLS_DHE_PSK_WITH_NULL_SHA";
		} else if (cipher.equalsIgnoreCase("002e")) {
			return "TLS_RSA_PSK_WITH_NULL_SHA";
		} else if (cipher.equalsIgnoreCase("002f")) {
			return "TLS_RSA_WITH_AES_128_CBC_SHA";
		} else if (cipher.equalsIgnoreCase("002f")) {
			return "TLS_RSA_WITH_AES_128_CBC_SHA";
		} else if (cipher.equalsIgnoreCase("0030")) {
			return "TLS_DH_DSS_WITH_AES_128_CBC_SHA";
		} else if (cipher.equalsIgnoreCase("0031")) {
			return "TLS_DH_RSA_WITH_AES_128_CBC_SHA";
		} else if (cipher.equalsIgnoreCase("0031")) {
			return "TLS_DH_anon_WITH_AES_128_CBC_SHA";
		} else {
			return "Unknown" + cipher;
		}
	}

	private String isValidContentTYpe(String contentTypeHex) throws CustomException, UnsupportedEncodingException {
		String contentTypeString = null, contentType = null;
		if (contentTypeHex.length() != 2)
			throw new CustomException("Bit length is not valid " + contentTypeHex + " it must be 2 digits",1201);

		contentTypeString = Integer.parseInt(contentTypeHex, 16) + "";
		if (contentTypeString.equals("20"))
			contentType = "Change Cipher Spec" + "(" + contentTypeString + ")";

		if (contentTypeString.equals("21"))
			contentType = "Alert" + "(" + contentTypeString + ")";

		if (contentTypeString.equals("22")) {
			contentType = "Handshake" + "(" + contentTypeString + ")";
		}
		if (contentTypeString.equals("23"))
			contentType = "Application Data" + "(" + contentTypeString + ")";

		if (contentTypeString.equals("24"))
			contentType = "Heartbeat" + "(" + contentTypeString + ")";

		return contentType;
	}

	private String isValidDTLSPacket(String hexdump) throws UnsupportedEncodingException {

		String text = new String(javax.xml.bind.DatatypeConverter.parseHexBinary(hexdump), "UTF-16");
				return text;
	}

	public static List<LinkedHashMap<String, Object>> parse(String hexdump) throws CustomException {
		DTLSPacketVerifier dtlsPacketVerifier1 = new DTLSPacketVerifier();

		List<LinkedHashMap<String, Object>> remainingResults = dtlsPacketVerifier1.getInnerResults();

		LinkedHashMap<String, Object> outerResult = dtlsPacketVerifier1.verifyDTLSPacket(hexdump);

		List<LinkedHashMap<String, Object>> allResults = new ArrayList<LinkedHashMap<String, Object>>();

		allResults.add(outerResult);
		for (int i = remainingResults.size() - 1; i >= 0; i--) {
			LinkedHashMap<String, Object> innerResult = remainingResults.get(i);
			allResults.add(innerResult);
		}

		for (LinkedHashMap<String, Object> result : allResults) {
			
		}

		return allResults;
	}
	
	
	
}