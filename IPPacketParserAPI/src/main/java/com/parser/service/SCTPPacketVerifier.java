package com.parser.service;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.parser.customError.CustomException;

/**
 * @author AmantyaTech
 */

public class SCTPPacketVerifier {
	/**
	 * Parses the hexdump of SCTPPacket
	 * 
	 * @request string
	 * @response LinkedHashMap<String, Object>
	 */

	public static LinkedHashMap<String, Object> verifySCTPPacket(String hexdump) throws CustomException {
		 final Logger logger = LoggerFactory.getLogger(SCTPPacketVerifier.class);

		LinkedHashMap<String, Object> outputMap = new LinkedHashMap<String, Object>();
		LinkedHashMap<String, Object> outputMapError = new LinkedHashMap<String, Object>();

		try {
			isValidSCTPPacket(hexdump);

			String sourcePort = isValidSourcePort(hexdump.substring(0, 4));
			outputMap.put("SCTP Source Port", sourcePort);

			String destinationPort = isValidDestinationPort(hexdump.substring(4, 8));
			outputMap.put("SCTP Destination Port", destinationPort);

			String verificationTag = isVaildVerificationTag(hexdump.substring(8, 16));
			outputMap.put("SCTP Verification Tag", "0x" + verificationTag);

			String checksum = isValidChecksum(hexdump.substring(16, 24));
			outputMap.put("SCTP Checksum", "0x" + checksum);

			String chunkFlags = isValidFlags(hexdump.substring(26, 28));
			outputMap.put("SCTP Chunk Flag", "0x" + chunkFlags);

			String chunkLength = isValidChunkLength(hexdump.substring(28, 32));
			outputMap.put("SCTP Chunk Length", chunkLength);

			String payloadData = hexdump.substring(28, 32);

			String chunkType = isValidChunkType(hexdump.substring(24, 26));
			if (Integer.parseInt(chunkType, 16) == 0) {
				outputMap.put("SCTP Chunk Type", "DATA");
				String cumulative = isValidCumulative(hexdump.substring(32, 40));
				outputMap.put("Cumulative TSN Ack", cumulative);
				outputMap.put("SCTP Payload: ", ((Integer.parseInt(payloadData, 16) - 16)) + " Bytes");
				String streamIdentifier = hexdump.substring(40, 44);
				outputMap.put("Stream Identifier", "0x" + streamIdentifier);

				String streamSequence = hexdump.substring(44, 48);
				outputMap.put("Stream Sequence Number", "" + Integer.parseInt(streamSequence, 16));
				String payloadIdentifier = hexdump.substring(48, 56);
				if (Integer.parseInt(payloadIdentifier, 16) == 0)
					outputMap.put("Payload Protocol Identifier",
							"(Reserved by SCTP)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 1)
					outputMap.put("Payload Protocol Identifier", "(IUA)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 2)
					outputMap.put("Payload Protocol Identifier", "(M2UA)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 3)
					outputMap.put("Payload Protocol Identifier", "(M3UA)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 4)
					outputMap.put("Payload Protocol Identifier", "(SUA)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 5)
					outputMap.put("Payload Protocol Identifier", "(M2PA)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 6)
					outputMap.put("Payload Protocol Identifier", "(V5UA)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 7)
					outputMap.put("Payload Protocol Identifier", "(H.248)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 8)
					outputMap.put("Payload Protocol Identifier",
							"(BICC/Q.2150.3)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 9)
					outputMap.put("Payload Protocol Identifier", "(TALI)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 10)
					outputMap.put("Payload Protocol Identifier", "(DUA)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 11)
					outputMap.put("Payload Protocol Identifier", "(ASAP)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 12)
					outputMap.put("Payload Protocol Identifier", "(ENRP)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 13)
					outputMap.put("Payload Protocol Identifier", "(H.323)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 14)
					outputMap.put("Payload Protocol Identifier",
							"(Q.IPC/Q.2150.3)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 15)
					outputMap.put("Payload Protocol Identifier", "(SIMCO)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 16)
					outputMap.put("Payload Protocol Identifier",
							"(DDP Segment Chunk)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 17)
					outputMap.put("Payload Protocol Identifier",
							"(DDP Stream Session Control)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 18)
					outputMap.put("Payload Protocol Identifier", "(S1AP)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 19)
					outputMap.put("Payload Protocol Identifier", "(RUA)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 20)
					outputMap.put("Payload Protocol Identifier", "(HNBAP)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 21)
					outputMap.put("Payload Protocol Identifier",
							"(ForCES-HP)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 22)
					outputMap.put("Payload Protocol Identifier",
							"(ForCES-MP)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 23)
					outputMap.put("Payload Protocol Identifier",
							"(ForCES-LP)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 24)
					outputMap.put("Payload Protocol Identifier", "(SBc-AP)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 25)
					outputMap.put("Payload Protocol Identifier", "(NBAP)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 26)
					outputMap.put("Payload Protocol Identifier",
							"(Unassigned)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 27)
					outputMap.put("Payload Protocol Identifier", "(X2AP)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 28)
					outputMap.put("Payload Protocol Identifier",
							"(IRCP - Inter Router Capability Protocol)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 29)
					outputMap.put("Payload Protocol Identifier", "(LCS-AP)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 30)
					outputMap.put("Payload Protocol Identifier", "(MPICH2)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 31)
					outputMap.put("Payload Protocol Identifier",
							"Service Area Broadcast Protocol (SABP)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 32)
					outputMap.put("Payload Protocol Identifier",
							"(Fractal Generator Protocol)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 33)
					outputMap.put("Payload Protocol Identifier",
							"Ping Pong Protocol(PPP)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 34)
					outputMap.put("Payload Protocol Identifier",
							"CalcApp Protocol (CALCAPP)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 35)
					outputMap.put("Payload Protocol Identifier",
							"Scripting Service Protocol (SSP)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 36)
					outputMap.put("Payload Protocol Identifier",
							"(NPMP-CONTROL)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 37)
					outputMap.put("Payload Protocol Identifier",
							"(NPMP-DATA)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 38)
					outputMap.put("Payload Protocol Identifier", "(ECHO)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 39)
					outputMap.put("Payload Protocol Identifier", "(DISCARD)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 40)
					outputMap.put("Payload Protocol Identifier", "(DAYTIME)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 41)
					outputMap.put("Payload Protocol Identifier", "(CHARGEN)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 42)
					outputMap.put("Payload Protocol Identifier",
							"(3GPP RNA)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 43)
					outputMap.put("Payload Protocol Identifier",
							"(3GPP M2AP)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 44)
					outputMap.put("Payload Protocol Identifier",
							"(3GPP M3AP)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 45)
					outputMap.put("Payload Protocol Identifier", "(SSH)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 46)
					outputMap.put("Payload Protocol Identifier",
							"(Diameter in a SCTP DATA chunk)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 47)
					outputMap.put("Payload Protocol Identifier",
							"(Diameter in a DTLS/SCTP DATA chunk)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 48)
					outputMap.put("Payload Protocol Identifier",
							"(R14P. BER Encoded ASN.1 over SCTP)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 49)
					outputMap.put("Payload Protocol Identifier",
							"(Unassigned)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 50)
					outputMap.put("Payload Protocol Identifier",
							"(WebRTC DCEP)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 51)
					outputMap.put("Payload Protocol Identifier",
							"(WebRTC String)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 52)
					outputMap.put("Payload Protocol Identifier",
							"(WebRTC Binary Partial)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 53)
					outputMap.put("Payload Protocol Identifier",
							"(WebRTC Binary)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 54)
					outputMap.put("Payload Protocol Identifier",
							"(WebRTC String Partial)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 55)
					outputMap.put("Payload Protocol Identifier",
							"(3GPP PUA)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 56)
					outputMap.put("Payload Protocol Identifier",
							"(WebRTC String Empty)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 57)
					outputMap.put("Payload Protocol Identifier",
							"(WebRTC Binary Empty)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 58)
					outputMap.put("Payload Protocol Identifier",
							"(3GPP XwAP)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 59)
					outputMap.put("Payload Protocol Identifier",
							"(3GPP Xw-Control Plane)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 60)
					outputMap.put("Payload Protocol Identifier",
							"(3GPP NG Application Protocol)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 61)
					outputMap.put("Payload Protocol Identifier",
							"(3GPP Xn Application Protocol)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 62)
					outputMap.put("Payload Protocol Identifier",
							"(3GPP F1 Application Protocol)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 63) {
					outputMap.put("Payload Protocol Identifier", "(HTTP)" + Integer.parseInt(payloadIdentifier, 16));
					if(hexdump.length()>56) {
						LinkedHashMap<String, Object> map3 = new LinkedHashMap();
			        	map3 =  HTTPPacketVerifier.verifyHTTPPacket(hexdump.substring(56));
			        	outputMap.putAll(map3);
					}
			 
				}
				if (Integer.parseInt(payloadIdentifier, 16) == 64)
					outputMap.put("Payload Protocol Identifier",
							"(3GPP E1 Application Protocol)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 65)
					outputMap.put("Payload Protocol Identifier",
							"(ELE2 Lawful Interception)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 66)
					outputMap.put("Payload Protocol Identifier",
							"(3GPP NGAP over DTLS over SCTP)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 67)
					outputMap.put("Payload Protocol Identifier",
							"(3GPP XnAP over DTLS over SCTP)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 68)
					outputMap.put("Payload Protocol Identifier",
							"(3GPP F1AP over DTLS over SCTP)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 69)
					outputMap.put("Payload Protocol Identifier",
							"(3GPP E1AP over DTLS over SCTP)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 70)
					outputMap.put("Payload Protocol Identifier", "(E2-CP)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 71)
					outputMap.put("Payload Protocol Identifier", "(E2-UP)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) == 72)
					outputMap.put("Payload Protocol Identifier", "(E2-DU)" + Integer.parseInt(payloadIdentifier, 16));
				if (Integer.parseInt(payloadIdentifier, 16) > 72)
					outputMap.put("Payload Protocol Identifier",
							"(Unassigned/Unknown)" + Integer.parseInt(payloadIdentifier, 16));
			}
			if (Integer.parseInt(chunkType, 16) == 1) {
				outputMap.put("SCTP Chunk Type", "INIT");
				List<Object> list = new ArrayList<>();
				LinkedHashMap<String, String> map2 = new LinkedHashMap<String, String>();

				String initiateTag = hexdump.substring(32, 40);
				outputMap.put("Initiate Tag", "0x" + initiateTag);
				String windowCredit = hexdump.substring(40, 48);
				outputMap.put("Window Credit", "" + Integer.parseInt(windowCredit, 16));
				String outboundStream = hexdump.substring(48, 52);
				outputMap.put("Number of outbound streams", "" + Integer.parseInt(outboundStream, 16));
				String inboundStream = hexdump.substring(52, 56);
				outputMap.put("Number of inbound streams", "" + Integer.parseInt(inboundStream, 16));
				String initialTSN = hexdump.substring(56, 64);
				outputMap.put("Initial TSN", "" + Integer.parseInt(initialTSN, 16));

				String parameterInput = hexdump.substring(64);
				int index = 0;
				while (index < parameterInput.length()) {
					String param = parameterInput.substring(index, Math.min(index + 4, parameterInput.length()));

					map2 = new LinkedHashMap<>();

					if (Integer.parseInt(param, 16) == 1) {
						map2.put("Parameter Type", "Heartbeat Info");
						index += 4;
						String param1 = parameterInput.substring(index, Math.min(index + 4, parameterInput.length()));
						map2.put("Parameter Length", "" + Integer.parseInt(param1, 16));
						index += 4;

					}
					if (Integer.parseInt(param, 16) < 5 && Integer.parseInt(param, 16) > 1) {
						map2.put("Parameter Type", "Unassigned1");
						index += 4;
						String param1 = parameterInput.substring(index, Math.min(index + 4, parameterInput.length()));
						map2.put("Parameter Length", "" + Integer.parseInt(param1, 16));
						index += 4;

					}
					if (Integer.parseInt(param, 16) == 5) {

						map2.put("Parameter Type", "IPv4 Address (0x0005)");
						index += 4;
						String param1 = parameterInput.substring(index, Math.min(index + 4, parameterInput.length()));
						map2.put("Parameter Length", "" + Integer.parseInt(param1, 16));
						String param2 = parameterInput.substring(index + 4,
								Math.min(index + 12, parameterInput.length()));

						String o1 = Integer.parseInt(param2.substring(0, 2), 16) + ".";
						String o2 = Integer.parseInt(param2.substring(2, 4), 16) + ".";
						String o3 = Integer.parseInt(param2.substring(4, 6), 16) + ".";
						String o4 = Integer.parseInt(param2.substring(6, 8), 16) + "";
						String output = o1 + o2 + o3 + o4;
						map2.put("IPv4 Address:", output);
						index += 12;

					}
					if (Integer.parseInt(param, 16) == 6) {
						map2.put("Parameter Type", "IPv6 Address	");
						index += 4;
						String param1 = parameterInput.substring(index, Math.min(index + 8, parameterInput.length()));
						outputMap.put("Parameter Length", "" + Integer.parseInt(param1, 16));
						String param2 = parameterInput.substring(index + 8,
								Math.min(index + 40, parameterInput.length()));

						String output = param2.substring(0, 4) + ":" + param2.substring(4, 8) + ":"
								+ param2.substring(8, 12) + ":" + param2.substring(12, 16) + ":"
								+ param2.substring(16, 20) + ":" + param2.substring(20, 24) + ":"
								+ param2.substring(24, 28) + ":" + param2.substring(28, 32);
						map2.put("IPv6 Address:", output);
						index += 40;

					}
					if (Integer.parseInt(param, 16) == 7) {
						map2.put("Parameter Type", "State Cookie");
						index += 4;
						String param1 = parameterInput.substring(index, Math.min(index + 4, parameterInput.length()));
						map2.put("Parameter Length", "" + Integer.parseInt(param1, 16));
						index += 4;

					}
					if (Integer.parseInt(param, 16) == 8) {
						map2.put("Parameter Type", "Unrecognized Parameters");
						index += 4;
						String param1 = parameterInput.substring(index, Math.min(index + 4, parameterInput.length()));
						map2.put("Parameter Length", "" + Integer.parseInt(param1, 16));
						index += 4;

					}
					if (Integer.parseInt(param, 16) == 9) {
						map2.put("Parameter Type", "Cookie Preservative");
						index += 4;
						String param1 = parameterInput.substring(index, Math.min(index + 4, parameterInput.length()));
						map2.put("Parameter Length", "" + Integer.parseInt(param1, 16));
						String param2 = parameterInput.substring(index + 4,
								Math.min(index + 36, parameterInput.length()));
						map2.put("Suggested Cookie Life-Span Increment", "" + Integer.parseInt(param2, 16)); // 32 bit
						index += 40;

					}
					if (Integer.parseInt(param, 16) == 10) {
						map2.put("Parameter Type", "Unassigned2");
						index += 4;
						String param1 = parameterInput.substring(index, Math.min(index + 4, parameterInput.length()));
						map2.put("Parameter Length", "" + Integer.parseInt(param1, 16));
						index += 4;

					}
					if (Integer.parseInt(param, 16) == 11) {
						map2.put("Parameter Type", "Host Name Address");
						index += 4;
						String param1 = parameterInput.substring(index, Math.min(index + 4, parameterInput.length()));
						map2.put("Parameter Length", "" + Integer.parseInt(param1, 16));
						index += 4;

					}
					if (Integer.parseInt(param, 16) == 12) {
						map2.put("Parameter Type", "Supported Address Types");
						index += 4;
						String param1 = parameterInput.substring(index, Math.min(index + 4, parameterInput.length()));
						map2.put("Parameter Length", "" + Integer.parseInt(param1, 16));
						String param2 = parameterInput.substring(index + 4,
								Math.min(index + 8, parameterInput.length()));
						if (Integer.parseInt(param2, 16) == 5)
							map2.put("Supported Address Types", "IPv4 Address" + Integer.parseInt(param2, 16));// 16 bit
						if (Integer.parseInt(param2, 16) == 6)
							map2.put("Supported Address Types", "IPv6 Address" + Integer.parseInt(param2, 16));// 16 bit
						else
							map2.put("Supported Address Types", "" + Integer.parseInt(param2, 16));// 16 bit
						String param3 = parameterInput.substring(index + 8,
								Math.min(index + 12, parameterInput.length()));
						map2.put("Parameter Padding", "" + param3);
						index += 12;

					}
					if (Integer.parseInt(param, 16) == 13) {
						map2.put("Parameter Type", "Outgoing SSN Reset Request Parameter");
						index += 4;
						String param1 = parameterInput.substring(index, Math.min(index + 4, parameterInput.length()));
						map2.put("Parameter Length", "" + Integer.parseInt(param1, 16));
						index += 4;

					}
					if (Integer.parseInt(param, 16) == 14) {
						map2.put("Parameter Type", "Incoming SSN Reset Request Parameter");
						index += 4;
						String param1 = parameterInput.substring(index, Math.min(index + 4, parameterInput.length()));
						map2.put("Parameter Length", "" + Integer.parseInt(param1, 16));
						index += 4;

					}
					if (Integer.parseInt(param, 16) == 15) {
						map2.put("Parameter Type", "SSN/TSN Reset Request Parameter");
						index += 4;
						String param1 = parameterInput.substring(index, Math.min(index + 4, parameterInput.length()));
						map2.put("Parameter Length", "" + Integer.parseInt(param1, 16));
						index += 4;

					}
					if (Integer.parseInt(param, 16) == 16) {
						map2.put("Parameter Type", "Re-configuration Response Parameter	");
						index += 4;
						String param1 = parameterInput.substring(index, Math.min(index + 4, parameterInput.length()));
						map2.put("Parameter Length", "" + Integer.parseInt(param1, 16));
						index += 4;

					}
					if (Integer.parseInt(param, 16) == 17) {
						map2.put("Parameter Type", "Add Outgoing Streams Request Parameter	");
						index += 4;
						String param1 = parameterInput.substring(index, Math.min(index + 4, parameterInput.length()));
						map2.put("Parameter Length", "" + Integer.parseInt(param1, 16));
						index += 4;

					}
					if (Integer.parseInt(param, 16) == 18) {
						map2.put("Parameter Type", "Add Incoming Streams Request Parameter");
						index += 4;
						String param1 = parameterInput.substring(index, Math.min(index + 4, parameterInput.length()));
						map2.put("Parameter Length", "" + Integer.parseInt(param1, 16));
						index += 4;

					}
					if (Integer.parseInt(param, 16) > 18 && Integer.parseInt(param, 16) < 32768) {
						map2.put(" Parameter Type", "Unassigned3");
						index += 4;
						String param1 = parameterInput.substring(index, Math.min(index + 4, parameterInput.length()));
						map2.put("Parameter Length", "" + Integer.parseInt(param1, 16));
						index += 4;

					}
					if (Integer.parseInt(param, 16) == 32768) {
						map2.put("Parameter Type", "Reserved for ECN Capable (0x8000)");
						index += 4;
						String param1 = parameterInput.substring(index, Math.min(index + 4, parameterInput.length()));
						map2.put("Parameter Length", "" + Integer.parseInt(param1, 16));
						index += 4;

					}
					if (Integer.parseInt(param, 16) == 32770) {
						map2.put("Parameter Type", "Random (0x8002)");
						index += 4;
						String param1 = parameterInput.substring(index, Math.min(index + 4, parameterInput.length()));
						map2.put("Parameter Length", "" + Integer.parseInt(param1, 16));
						index += 4;

					}
					if (Integer.parseInt(param, 16) == 32771) {
						map2.put("Parameter Type", "Chunk List (0x8003)");
						index += 4;
						String param1 = parameterInput.substring(index, Math.min(index + 4, parameterInput.length()));
						map2.put("Parameter Length", "" + Integer.parseInt(param1, 16));
						index += 4;

					}
					if (Integer.parseInt(param, 16) == 32772) {
						map2.put("Parameter Type", "Requested HMAC Algorithm Parameter (0x8004)");
						index += 4;
						String param1 = parameterInput.substring(index, Math.min(index + 4, parameterInput.length()));
						map2.put("Parameter Length", "" + Integer.parseInt(param1, 16));
						index += 4;

					}
					if (Integer.parseInt(param, 16) == 32773) {
						map2.put("Parameter Type", "Padding (0x8005)");
						index += 4;
						String param1 = parameterInput.substring(index, Math.min(index + 4, parameterInput.length()));
						map2.put("Parameter Length", "" + Integer.parseInt(param1, 16));
						index += 4;

					}
					if (Integer.parseInt(param, 16) == 32776) {
						map2.put("Parameter Type", "Supported Extensions (0x8008)");
						index += 4;
						String param1 = parameterInput.substring(index, Math.min(index + 4, parameterInput.length()));
						map2.put("Parameter Length", "" + Integer.parseInt(param1, 16));
						index += 4;

					}
					if (Integer.parseInt(param, 16) > 32776 && Integer.parseInt(param, 16) < 49152) {
						map2.put("Parameter Type", "Unassigned4");
						index += 4;
						String param1 = parameterInput.substring(index, Math.min(index + 4, parameterInput.length()));
						map2.put("Parameter Length", "" + Integer.parseInt(param1, 16));
						index += 4;

					}
					if (Integer.parseInt(param, 16) == 49152) {
						map2.put("Parameter Type", "Forward TSN supported (0xC000)");
						index += 4;
						String param1 = parameterInput.substring(index, Math.min(index + 4, parameterInput.length()));
						map2.put("Parameter Length", "" + Integer.parseInt(param1, 16));
						index += 4;

					}
					if (Integer.parseInt(param, 16) == 49153) {
						map2.put("Parameter Type", "Add IP Address (0xC001)	");
						index += 4;
						String param1 = parameterInput.substring(index, Math.min(index + 4, parameterInput.length()));
						map2.put("Parameter Length", "" + Integer.parseInt(param1, 16));
						index += 4;

					}
					if (Integer.parseInt(param, 16) == 49154) {
						map2.put("Parameter Type", "Delete IP Address (0xC002)");
						index += 4;
						String param1 = parameterInput.substring(index, Math.min(index + 4, parameterInput.length()));
						map2.put("Parameter Length", "" + Integer.parseInt(param1, 16));
						index += 4;

					}
					if (Integer.parseInt(param, 16) == 49155) {
						map2.put("Parameter Type", "Error Cause Indication (0xC003)");
						index += 4;
						String param1 = parameterInput.substring(index, Math.min(index + 4, parameterInput.length()));
						map2.put("Parameter Length", "" + Integer.parseInt(param1, 16));
						index += 4;

					}
					if (Integer.parseInt(param, 16) == 49156) {
						map2.put("Parameter Type", "Set Primary Address (0xC004)	");
						index += 4;
						String param1 = parameterInput.substring(index, Math.min(index + 4, parameterInput.length()));
						map2.put("Parameter Length", "" + Integer.parseInt(param1, 16));
						index += 4;

					}
					if (Integer.parseInt(param, 16) == 49157) {
						map2.put("Parameter Type", "Success Indication (0xC005)");
						index += 4;
						String param1 = parameterInput.substring(index, Math.min(index + 4, parameterInput.length()));
						map2.put("Parameter Length", "" + Integer.parseInt(param1, 16));
						index += 4;

					}
					if (Integer.parseInt(param, 16) == 49158) {
						map2.put("Parameter Type", "Adaptation Layer Indication (0xC006)");
						index += 4;
						String param1 = parameterInput.substring(index, Math.min(index + 4, parameterInput.length()));
						map2.put("Parameter Length", "" + Integer.parseInt(param1, 16));
						index += 4;
					}
					list.add(map2);
					outputMap.put("Optional Parameters", list);
				}
			}
			if (Integer.parseInt(chunkType, 16) == 2) {
				outputMap.put("SCTP Chunk Type", "INIT ACK");
				String initiateTag = hexdump.substring(32, 40);
				outputMap.put("Initiate Tag", "0x" + initiateTag);
				String windowCredit = hexdump.substring(40, 48);
				outputMap.put("Window Credit", "" + Integer.parseInt(windowCredit, 16));
				String outboundStream = hexdump.substring(48, 52);
				outputMap.put("Number of outbound streams", "" + Integer.parseInt(outboundStream, 16));
				String inboundStream = hexdump.substring(52, 56);
				outputMap.put("Number of inbound streams", "" + Integer.parseInt(inboundStream, 16));
				String initialTSN = hexdump.substring(56, 64);
				outputMap.put("Initial TSN", "" + Integer.parseInt(initialTSN, 16));
			}
			if (Integer.parseInt(chunkType, 16) == 3) {
				outputMap.put("SCTP Chunk Type", "SACK");
				String cumulative = hexdump.substring(32, 40);
				outputMap.put("Cumulative TSN Ack", Long.parseLong(cumulative, 16));
				String windowCredit = hexdump.substring(40, 48);
				outputMap.put("Window Credit", "" + Integer.parseInt(windowCredit, 16));
				String gap = hexdump.substring(48, 52);
				outputMap.put("Number of gap acknowledgement blocks", "" + Integer.parseInt(gap, 16));
				String duplicateTSN = hexdump.substring(52, 56);
				outputMap.put("Number of duplicated TSNs", "" + Integer.parseInt(duplicateTSN, 16));

			}
			if (Integer.parseInt(chunkType, 16) == 4) {
				outputMap.put("SCTP Chunk Type", "HEARTBEAT");
				String param3 = hexdump.substring(32, 36);
				outputMap.put("Parameter Type", "0x" + param3);
				String param2 = hexdump.substring(36, 40);
				outputMap.put("Parameter Length", "" + Integer.parseInt(param2, 16));
				String param1 = hexdump.substring(40);
				outputMap.put("Heartbeat Information", "" + param1);
			}
			if (Integer.parseInt(chunkType, 16) == 5) {
				outputMap.put("SCTP Chunk Type", "HEARTBEAT ACK");
				String param3 = hexdump.substring(32, 36);
				outputMap.put("Parameter Type", "0x" + param3);
				String param2 = hexdump.substring(36, 40);
				outputMap.put("Parameter Length", "" + Integer.parseInt(param2, 16));
				String param1 = hexdump.substring(40);
				outputMap.put("Heartbeat Information", "" + param1);
			}
			if (Integer.parseInt(chunkType, 16) == 6) {
				outputMap.put("SCTP Chunk Type", "ABORT");
				String causeCode = hexdump.substring(32, 36);
				if (Integer.parseInt(causeCode, 16) == 1)
					outputMap.put("Cause Code", "Invalid Stream Identifier(1)");
				if (Integer.parseInt(causeCode, 16) == 2)
					outputMap.put("Cause Code", "Missing Mandatory Parameter(2)");
				if (Integer.parseInt(causeCode, 16) == 3)
					outputMap.put("Cause Code", "Stale Cookie Error(3)");
				if (Integer.parseInt(causeCode, 16) == 4)
					outputMap.put("Cause Code", "Out of Resource(4)");
				if (Integer.parseInt(causeCode, 16) == 5)
					outputMap.put("Cause Code", "Unresolvable Address(5)");
				if (Integer.parseInt(causeCode, 16) == 6)
					outputMap.put("Cause Code", "Unrecognized Chunk Type(6)");
				if (Integer.parseInt(causeCode, 16) == 7)
					outputMap.put("Cause Code", "Invalid Mandatory Parameter(7)");
				if (Integer.parseInt(causeCode, 16) == 8)
					outputMap.put("Cause Code", "Unrecognized Parameters(8)");
				if (Integer.parseInt(causeCode, 16) == 9)
					outputMap.put("Cause Code", "No User Data(9)");
				if (Integer.parseInt(causeCode, 16) == 10)
					outputMap.put("Cause Code", "Cookie Received While Shutting Down(10)");
				if (Integer.parseInt(causeCode, 16) == 11)
					outputMap.put("Cause Code", "Restart of an Association with New Addresses(11)");
				if (Integer.parseInt(causeCode, 16) == 12)
					outputMap.put("Cause Code", "User Initiated Abort(12)");
				if (Integer.parseInt(causeCode, 16) == 13)
					outputMap.put("Cause Code", "Protocol Violation(13)");
				String Cause_Length = hexdump.substring(36, 40);
				outputMap.put("Cause Length", "" + Integer.parseInt(Cause_Length, 16));

			}
			if (Integer.parseInt(chunkType, 16) == 7) {
				outputMap.put("SCTP Chunk Type", "SHUTDOWN");
				String cumulative = isValidCumulative(hexdump.substring(32, 40));
				outputMap.put("Cumulative TSN Ack", cumulative);
			}
			if (Integer.parseInt(chunkType, 16) == 8)
				outputMap.put("SCTP Chunk Type", "SHUTDOWN ACK");
			if (Integer.parseInt(chunkType, 16) == 9) {
				outputMap.put("SCTP Chunk Type", "ERROR");
				String causeCode = hexdump.substring(26, 30);
				if (Integer.parseInt(causeCode, 16) == 1)
					outputMap.put("Cause Code", "Invalid Stream Identifier(1)");
				if (Integer.parseInt(causeCode, 16) == 2)
					outputMap.put("Cause Code", "Missing Mandatory Parameter(2)");
				if (Integer.parseInt(causeCode, 16) == 3)
					outputMap.put("Cause Code", "Stale Cookie Error(3)");
				if (Integer.parseInt(causeCode, 16) == 4)
					outputMap.put("Cause Code", "Out of Resource(4)");
				if (Integer.parseInt(causeCode, 16) == 5)
					outputMap.put("Cause Code", "Unresolvable Address(5)");
				if (Integer.parseInt(causeCode, 16) == 6)
					outputMap.put("Cause Code", "Unrecognized Chunk Type(6)");
				if (Integer.parseInt(causeCode, 16) == 7)
					outputMap.put("Cause Code", "Invalid Mandatory Parameter(7)");
				if (Integer.parseInt(causeCode, 16) == 8)
					outputMap.put("Cause Code", "Unrecognized Parameters(8)");
				if (Integer.parseInt(causeCode, 16) == 9)
					outputMap.put("Cause Code", "No User Data(9)");
				if (Integer.parseInt(causeCode, 16) == 10)
					outputMap.put("Cause Code", "Cookie Received While Shutting Down(10)");
				if (Integer.parseInt(causeCode, 16) == 11)
					outputMap.put("Cause Code", "Restart of an Association with New Addresses(11)");
				if (Integer.parseInt(causeCode, 16) == 12)
					outputMap.put("Cause Code", "User Initiated Abort(12)");
				if (Integer.parseInt(causeCode, 16) == 13)
					outputMap.put("Cause Code", "Protocol Violation(13)");
				String Cause_Length = hexdump.substring(30, 34);
				outputMap.put("Cause Length", "" + Integer.parseInt(Cause_Length, 16));
				String causeInfo = hexdump.substring(34);
				//@SuppressWarnings("restriction")
				byte[] bytes = DatatypeConverter.parseHexBinary(causeInfo);
				outputMap.put("Cause-Specific Information", new String(bytes, "UTF-8"));

			}
			if (Integer.parseInt(chunkType, 16) == 10) {
				outputMap.put("SCTP Chunk Type", "COOKIE ECHO");
				String cookie = hexdump.substring(26);
				//@SuppressWarnings("restriction")
				byte[] bytes = DatatypeConverter.parseHexBinary(cookie);
				outputMap.put("Cookie", new String(bytes, "UTF-8"));

			}
			if (Integer.parseInt(chunkType, 16) == 11)
				outputMap.put("SCTP Chunk Type", "COOKIE ACK");
			if (Integer.parseInt(chunkType, 16) == 12)
				outputMap.put("SCTP Chunk Type", "ECNE");
			if (Integer.parseInt(chunkType, 16) == 13)
				outputMap.put("SCTP Chunk Type", "CWR");
			if (Integer.parseInt(chunkType, 16) == 14)
				outputMap.put("SCTP Chunk Type", "SHUTDOWN COMPLETE");
			if (Integer.parseInt(chunkType, 16) == 15)
				outputMap.put("SCTP Chunk Type", "AUTH");
			if (Integer.parseInt(chunkType, 16) > 15)
				outputMap.put("SCTP Chunk Type", "Reserved by IETF");

			
			logger.info("SCTP Parser parsed data successfully.."+outputMap);

			return outputMap;

		} catch (Exception e) {
			logger.error("SCTP Parser Error.."+e.getMessage());
			outputMapError.put("Error", "SCTP Malformed Packet - " +e.getMessage());
			return outputMapError;

		}
	}

	private static String isValidCumulative(String cumulative) throws CustomException {
		if (cumulative.length() != 8)
			throw new CustomException("Cumulative not valid...." + cumulative,1201);
		return Integer.parseInt(cumulative, 16) + "";
	}

	private static String isValidChunkLength(String chunkLength) throws CustomException {
		if (chunkLength.length() != 4)
			throw new CustomException("Chunk Length not valid...." + chunkLength,1201);
		return Integer.parseInt(chunkLength, 16) + "";
	}

	private static String isValidChunkType(String chunkType) throws CustomException {
		if (chunkType.length() != 2) {
			throw new CustomException("Chunk Type is not valid " + chunkType,1201);
		} else
			return Integer.parseInt(chunkType, 16) + "";
	}

	private static String isVaildVerificationTag(String verificationTag) throws CustomException {
		if (verificationTag.length() != 8)
			throw new CustomException("Verification Tag not valid...." + verificationTag,1201);
		return verificationTag;
	}

	private static String isValidChecksum(String checksum) throws CustomException {
		if (checksum.length() != 8)
			throw new CustomException("Checksum not valid...." + checksum,1201);
		return checksum;
	}

	private static String isValidFlags(String chunkFlags) throws CustomException {

		if (chunkFlags.length() != 2)
			throw new CustomException("Chunk Flags not valid...." + chunkFlags,1201);
		return chunkFlags;
	}

	private static String isValidDestinationPort(String destinationPort) throws CustomException {
		if (destinationPort.length() != 4)
			throw new CustomException("Destination Port not valid...." + destinationPort,1201);
		else {
			return Integer.parseInt(destinationPort, 16) + "";
		}
	}

	private static String isValidSourcePort(String sourcePort) throws CustomException {
		if (sourcePort.length() != 4)
			throw new CustomException("Source Port not valid...." + sourcePort,1201);
		else {
			return Integer.parseInt(sourcePort, 16) + "";
		}
	}

	private static String isValidSCTPPacket(String hexdump) throws CustomException {
		if (hexdump.length() < 40)
			throw new CustomException("Invalid SCTP Packet " + hexdump.substring(0),400);

		else
			return hexdump;

	}

}
