package com.parser.service;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.parser.customError.CustomException;
/**
 * @author AmantyaTech
 */
public class DiameterProtocol {
	private static final Logger LOGGER = LoggerFactory.getLogger(DiameterProtocol.class);
	
	/**
	 * Parses the hexdump of DiameterProtocol
	 * @request string
	 * @response LinkedHashMap<String, Object>
	 */

	private enum Flag {
		Set(true), Unset(false);

		boolean value;

		private Flag(boolean value) {
			this.value = value;
		}
	}

	private enum DataCategory {
		Basic, Derived,
	}

	private interface DataType {
		String getName();

		DataCategory getCategory();
	}

	private enum BasicDataType implements DataType {
		OctetString, Integer32, Integer64, Unsigned32, Unsigned64, Float32, Float64, Grouped;

		@Override
		public String getName() {
			return name();
		}

		@Override
		public DataCategory getCategory() {
			return DataCategory.Basic;
		}
	}

	private enum DerivedDataType implements DataType {
		Address, Time, UTF8String, DiameterIdentity, DiameterURI, Enumerated, IPFilterRule;

		@Override
		public String getName() {
			return name();
		}

		@Override
		public DataCategory getCategory() {
			return DataCategory.Derived;
		}
	}

	/**
	 * 
	 *
	 * 
	 *         {@link} https://tools.ietf.org/html/rfc6733#section-4.5
	 *
	 */
	private enum Attribute {

		ACCT_INTERIM_INTERVAL("Acct-Interim-Interval", 85, BasicDataType.Unsigned32, Flag.Set, Flag.Unset),
		ACCOUNTING_REALTIME_REQUIRED("Accounting-Realtime-Required", 483, DerivedDataType.Enumerated, Flag.Set,
				Flag.Unset),
		ACCT_MULTI_SESSION_ID("Acct-Multi-Session-Id", 50, DerivedDataType.UTF8String, Flag.Set, Flag.Unset),
		ACCOUNTING_RECORD_NUMBER("Accounting-Record-Number", 485, BasicDataType.Unsigned32, Flag.Set, Flag.Unset),
		ACCOUNTING_RECORD_TYPE("Accounting-Record-Type", 480, DerivedDataType.Enumerated, Flag.Set, Flag.Unset),
		ACCT_SESSION_ID("Acct-Session-Id", 44, BasicDataType.OctetString, Flag.Set, Flag.Unset),
		ACCOUNTING_SUB_SESSION_ID("Accounting-Sub-Session-Id", 287, BasicDataType.Unsigned64, Flag.Set, Flag.Unset),
		ACCT_APPLICATION_ID("Acct-Application-Id", 259, BasicDataType.Unsigned32, Flag.Set, Flag.Unset),
		AUTH_APPLICATION_ID("Auth-Application-Id", 258, BasicDataType.Unsigned32, Flag.Set, Flag.Unset),
		AUTH_REQUEST_TYPE("Auth-Request-Type", 274, DerivedDataType.Enumerated, Flag.Set, Flag.Unset),
		AUTHORIZATION_LIFETIME("Authorization-Lifetime", 291, BasicDataType.Unsigned32, Flag.Set, Flag.Unset),
		AUTH_GRACE_PERIOD("Auth-Grace-Period", 276, BasicDataType.Unsigned32, Flag.Set, Flag.Unset),
		AUTH_SESSION_STATE("Auth-Session-State", 277, DerivedDataType.Enumerated, Flag.Set, Flag.Unset),
		RE_AUTH_REQUEST_TYPE("Re-Auth-Request-Type", 285, DerivedDataType.Enumerated, Flag.Set, Flag.Unset),
		CLASS("Class", 25, BasicDataType.OctetString, Flag.Set, Flag.Unset),
		DESTINATION_HOST("Destination-Host", 293, DerivedDataType.DiameterIdentity, Flag.Set, Flag.Unset),
		DESTINATION_REALM("Destination-Realm", 283, DerivedDataType.DiameterIdentity, Flag.Set, Flag.Unset),
		DISCONNECT_CAUSE("Disconnect-Cause" , 273 ,DerivedDataType.Enumerated, Flag.Set, Flag.Unset),
		E2E_SEQUENCE("E2E-Sequence",300,BasicDataType.Grouped,null,null),
		ERROR_MESSAGE(" Error-Message", 281, DerivedDataType.UTF8String, Flag.Unset, Flag.Unset),
		ERROR_REPORTING_HOST("Error-Reporting- Host", 294, DerivedDataType.DiameterIdentity, Flag.Unset, Flag.Unset), 
		EVENT_TIMESTAMP(" Event-Timestamp ",55,DerivedDataType.Time,Flag.Set,Flag.Unset),
		EXPERIMENTAL_RESULT(" Experimental-Result",297,BasicDataType.Grouped,Flag.Set,Flag.Unset),
		EXPERIMENTAL_RESULT_CODE("Experimental-Result-Code " , 298 ,BasicDataType.Unsigned32, Flag.Set,Flag.Unset),
	    FAILED_AVP("Failed-AVP", 279,BasicDataType.Grouped, Flag.Set,Flag.Unset),   
	    FIRMWARE_REVISION("Firmware-Revision" , 267,BasicDataType.Unsigned32 ,Flag.Unset,Flag.Unset),
		HOST_IP_ADDRESS("Host-IP-Address", 257, DerivedDataType.Address,Flag.Set,Flag.Unset ),
		INBAND_SECURITY_Id("Inband-Security-Id",299 ,BasicDataType.Unsigned32, Flag.Set,Flag.Unset ),
		MULTI_ROUND_TIME_OUT("Multi-Round-Time-Out",272 , BasicDataType.Unsigned32 ,Flag.Set,Flag.Unset),
		ORIGIN_HOST( "Origin-Host", 264  ,DerivedDataType.DiameterIdentity ,Flag.Set,Flag.Unset),
		ORIGIN_REALM( " Origin-Realm" , 296 ,DerivedDataType.DiameterIdentity,Flag.Set,Flag.Unset  ),
		ORIGIN_STATE_ID("Origin-State-Id" ,278, BasicDataType.Unsigned32,Flag.Set,Flag.Unset  ),
		PRODUCT_NAME("Product-Name",269 ,DerivedDataType.UTF8String ,Flag.Unset,Flag.Unset),
		PROXY_HOST("Proxy-Host ", 280 ,DerivedDataType.DiameterIdentity ,Flag.Set,Flag.Unset),
		PROXY_INFO("Proxy-Info", 284 ,BasicDataType.Grouped,Flag.Set,Flag.Unset),
		PROXY_STATE("Proxy-State",33,BasicDataType.OctetString,Flag.Set,Flag.Unset),
		REDIRECT_HOST("Redirect-Host",292 , DerivedDataType.DiameterURI ,Flag.Set,Flag.Unset),
		REDIRECT_HOST_USAGE("Redirect-Host- Usage",261,DerivedDataType.Enumerated, Flag.Set, Flag.Unset),
		REDIRECT_MAX_CACHE_TIME("Redirect-Max- Cache-Time",262, BasicDataType.Unsigned32,Flag.Set,Flag.Unset  ),
		RESULT_CODE(" Result-Code ",268, BasicDataType.Unsigned32,Flag.Set,Flag.Unset  ), 
		ROUTE_RECORD("Route-Record",282, DerivedDataType.DiameterIdentity,Flag.Set,Flag.Unset  ),
		SESSION_ID("Session-Id", 263,DerivedDataType.UTF8String ,Flag.Set,Flag.Unset  ),
		SESSION_TIMEOUT(" Session-Timeout", 27,BasicDataType.Unsigned32,Flag.Set,Flag.Unset  ), 
		SESSION_BINDING("Session-Binding",  270, BasicDataType.Unsigned32,Flag.Set,Flag.Unset  ),    
		SESSION_SERVER_FAILOVER("Session-Server- Failover",271 , DerivedDataType.Enumerated, Flag.Set, Flag.Unset),
		SUPPORTED_VENDOR_ID("Supported- Vendor-Id ",265,BasicDataType.Unsigned32,Flag.Set,Flag.Unset  ),  
		TERMINATION_CAUSE(" Termination- Cause ",295,DerivedDataType.Enumerated, Flag.Set, Flag.Unset),  
		USER_NAME(" User-Name",1 ,DerivedDataType.UTF8String ,Flag.Set,Flag.Unset  ),
		VENDOR_ID("Vendor-Id",266 ,BasicDataType.Unsigned32,Flag.Set,Flag.Unset  ),   
		VENDOR_SPECIFIC_APPLICATION_ID("Vendor-Specific- Application-Id ", 260 ,  BasicDataType.Grouped, Flag.Set,Flag.Unset); 
		
		
		private Attribute(String attributeName, int code, DataType dataType, Flag mandatory, Flag vendor) {
			this.attributeName = attributeName;
			this.code = code;
			this.dataType = dataType;
			this.mandatory = mandatory;
			this.vendor = vendor;
		}

		String attributeName;
		int code;
		DataType dataType;
		Flag mandatory;
		Flag vendor;

		public static Attribute getAttribute(int code) {
			for (Attribute e : Attribute.values()) {
				if (e.code == code) {
					return e;
				}
			}
			return null;
		}

	}

	private static	Map<String, Object> data(String dataHex, int avpCode) {

		Attribute attribute = Attribute.getAttribute(avpCode);

		Map<String, Object> response = new LinkedHashMap<String, Object>();

		if (null != attribute) {
			response.put("attribute", attribute);
			response.put("attribute name", attribute.attributeName);
			response.put("attribute data type category", attribute.dataType.getCategory());
			response.put("attribute data type name", attribute.dataType.getName());

			switch (attribute.dataType.getCategory()) {

			case Basic:
				switch (attribute.dataType.getName()) {
				case "OctetString":
					break;
				case "Integer32":
					response.put("value", Integer.parseInt(dataHex, 16));
					break;
				case "Integer64":
					response.put("value", Long.parseLong(dataHex, 16));
					break;
				case "Unsigned32":
					response.put("value", Integer.parseUnsignedInt(dataHex, 16));
					break;
				case "Unsigned64":
					response.put("value", Long.parseUnsignedLong(dataHex, 16));
					break;
				case "Float32":
					response.put("value", Float.valueOf(dataHex));
					break;
				case "Float64":
					response.put("value", Double.valueOf(dataHex));
					break;
				case "Grouped":
					Object innerAVPs = null;
					try {
						innerAVPs = AVPsParser(dataHex);
					} catch (Exception e) {
						response.put("Diameter Protocol Malformed Packet Error", e.getMessage());
					}
					response.put("Group AVPs", innerAVPs);
					break;
				}
				break;

			case Derived:
				switch (attribute.dataType.getName()) {
				case "Address":
					break;
				case "Time":
					break;
				case "UTF8String":
					break;
				case "DiameterIdentity":
					break;
				case "DiameterURI":
					break;
				case "Enumerated":
					break;
				case "IPFilterRule":
					break;
				}
				break;

			}

		}

		return response;
	}
	
	private enum CommandCode {
		AAR ( 265,true,"AA-Request"),
		AAA (265,false,"AA-Answer"),
		ASR (274, true, "Abort-Session-Request"),
		ASA (274, false, "Abort-Session-Answer"),
		ACR (271, true, "Accounting-Request"),
		ACA (271, false, "Accounting-Answer"),
		CER (257, true, "Capabilities-Exchange-Request"),
		CEA (257, false, "Capabilities-Exchange-Answer"),
		DWR (280, true, "Device-Watchdog-Request"),
		DWA (280, false, "Device-Watchdog-Answer"),
		DPR (282, true, "Disconnect-Peer-Request"),
		DPA (282, false, "Disconnect-Peer-Answer"),
		DER (268, true ,"Diameter-EAP-Request"),
		DEA (268,false,"Diameter-EAP-Answer"),
		RAR (258, true, "Re-Auth-Request"),
		RAA (258, false, "Re-Auth-Answer"),
		STR (275, true, "Session-Termination-Request"),
		STA (275, false, "Session-Termination-Answer"),
        CCR (272 ,true  ,"Credit-Control-Request"),
		CCA	(272 ,false, "Credit-Control-Answer"),
		UAR_SIP (283 , true ,"User-Authorization-Request"),
		UAA_SIP (283,false, "User-Authorization-Answer"),
		SAR_SIP (284 , true,"Server-Assignment-Request"),
		SAA_SIP (284,false,"Server-Assignment-Answer"),
		LIR_SIP (285, true, "Location-Info-Request"),
		LIA_SIP (285, false, "Location-Info-Answer"),
		MAR_SIP (286,true, "Multimedia-Auth-Request"),
		MAA_SIP (286,false, "Multimedia-Auth-Answer"),
		RTR_SIP (287,true, "Registration-Termination-Request"),
		RTA_SIP (287,false, "Registration-Termination-Answer"),
		PPR_SIP (288 ,true ,"Push-Profile-Request"),
		PPA_SIP (288,false,"Push-Profile-Answer"),
		UAR_3GPP(300 ,true ,"User-Authorization-Request "),
		UAA_3GPP(300 ,false,"User-Authorization-Answer "),
	    SAR_3GPP(301 ,true ,"Server-Assignment-Request"),
	    SAA_3GPP(301 ,false ,"Server-Assignment-Answer"),
	    LIR_3GPP(302,true  ,"Location-Info-Request"),
	    LIA_3GPP(302,false  ,"Location-Info-Answer"),
	    MAR_3GPP(303,true,"Multimedia-Auth-Request"),
	    MAA_3GPP(303,false,"Multimedia-Auth-Answer"),
	    RTR_3GPP(304,true ,"Registration-Termination-Request"),
	    RTA_3GPP(304,false,"Registration-Termination-Answer"),
	    PPR_3GPP(305,true," Push-Profile-Request"),
	    PPA_3GPP(305,false," Push-Profile-Answer"),
	    UDR(306,true, "User-Data-Request"),
	    UDA(306,false, "User-Data-Answer"),
	    PUR(307,true ,"Profile-Update-Request"),
	    PUA(307,false ,"Profile-Update-Answer"),
	    SNR(308 ,true," Subscribe-Notifications-Request"),
	    SNA(308 ,false," Subscribe-Notifications-Answer"),  
	    PNR(309 ,true ,"Push-Notification-Request"),
	    PNA(309 ,false ,"Push-Notification-Answer"),   
	    BIR(310 ,true ," Bootstrapping-Info-Request"),
	    BIA(310 ,false ," Bootstrapping-Info-Answer"),
	    MPR(311,true , " Message-Process-Request"),
	    MPA(311,false, " Message-Process-Answer"),
	    ULR(316,true ," Update-Location-Request"),
	    ULA(316,false ," Update-Location-Answer"),
	    CLR(317, true ,"Cancel-Location-Request"),
	    CLA(317, false ,"Cancel-Location-Answer"), 
	    AIR(318,true ,"Authentication-Information-Request"),
	    AIA(318,false ,"Authentication-Information-Answer"),
	    IDR(319,true,"Insert-Subscriber-Data-Request"),
	    IDA(319,false,"Insert-Subscriber-Data-Answer"),
	    DSR(320,true,"Delete-Subscriber-Data-Request"),
	    DSA(320,false,"Delete-Subscriber-Data-Answer"),
	    PER(321,true ,"Purge-UE-Request"),
	    PEA(321,false ,"Purge-UE-Answer"),
	    NR(323 ,true,"Notify-Request"),
	    NA(323 ,false,"Notify-Answer"),
	    PLR(8388620,true," Provide-Location-Request"),
	    PLA(8388620,false," Provide-Location-Answer"),
	    RIR(8388622, true, "Routing-Info-Request"),
	    RIA(8388622, false, "Routing-Info-Answer"),
	    AMR(260,true ,"AA-Mobile-Node-Request"),
	    AMA(260,false,"AA-Mobile-Node-Answer"),
	    HAR(262,true, "Home-Agent-MIP-Request"),
	    HAA(262,false, "Home-Agent-MIP-Answer"),
	    CIR(8388718,true,"Configuration-Information-Request"),
	    CIA(8388718,false,"Configuration-Information-Answer"),
	    RIR_1(8388719,true,"Reporting-Information-Request"),
	    RIA_1(8388719,false,"Reporting-Information-Answer"),
	    NIR(8388726,true,"NIDD-Information-Request"),
	    NIA(8388726,false,"NIDD-Information-Answer");
	    		
		int code;
		boolean request;
		String command;
		
		private CommandCode(int code, boolean request, String command) {
			this.code = code;
			this.request = request;
			this.command = command;
		}
		
		public static CommandCode identify(int code, boolean request) {
			for (CommandCode e : CommandCode.values()) {
				if (e.code == code && e.request == request) {
					return e;
				}
			}
			return null;
		}
		
	}
	
	private static List<LinkedHashMap<String, Object>> AVPsParser(String hexdump) throws CustomException {

		List<LinkedHashMap<String, Object>> outputList = new ArrayList<LinkedHashMap<String, Object>>();
		
		while (hexdump.length() > 0) {

			LinkedHashMap<String, Object> outputMap = new LinkedHashMap<String, Object>();
			String AVPcodeHex = hexdump.substring(0, 8);
			int AVPcodeDec = Integer.parseInt(AVPcodeHex, 16);
			outputMap.put("AVP code", "Decimal: " + AVPcodeDec + " , Hexadecimal: 0x" + AVPcodeHex);

			String AVPflagsHex = hexdump.substring(8, 10);
			int flagsAVPDec = Integer.parseInt(AVPflagsHex, 16);
			String AVPflagsBin = Integer.toBinaryString(flagsAVPDec);
			while (AVPflagsBin.length() < 8) {
				AVPflagsBin = "0" + AVPflagsBin;
			}
			outputMap.put("AVP flag",
					"Binary: " + AVPflagsBin + " , Decimal: " + flagsAVPDec + " , Hexadecimal: 0x" + AVPflagsHex);

			LinkedHashMap<String, Object> flagMap = new LinkedHashMap<String, Object>();
			boolean vendor = "1".equals(AVPflagsBin.substring(0, 1));
			flagMap.put("Vendor", vendor);
			boolean must = "1".equals(AVPflagsBin.substring(1, 2));
			flagMap.put("Must", must);
			boolean futureMechanism = "1".equals(AVPflagsBin.substring(2, 3));
			flagMap.put("Future Mechanism", futureMechanism);
			flagMap.put("Reserved", AVPflagsBin.substring(3));
			outputMap.put("AVP flags (Parsed)", flagMap);

			String AVPlengthHex = hexdump.substring(10, 16);
			int lengthAVPInBytes = Integer.parseInt(AVPlengthHex, 16);
			String lengthString = "Decimal: " + lengthAVPInBytes + " , Hexadecimal: 0x" + AVPlengthHex;

			int processedTill = 16;

			if (vendor) {
				String vendorId = hexdump.substring(16, 24);
				outputMap.put("vendorId",
						"Decimal: " + Integer.parseInt(vendorId, 16) + " , Hexadecimal: 0x" + vendorId);

				processedTill = 24;
			}

			// get just higher number or same number perfectly divisible by 4
			int cycleOf4Bytes = lengthAVPInBytes / 4;
			if (4 * cycleOf4Bytes < lengthAVPInBytes) {
				++cycleOf4Bytes;
			}

			int actualLengthInBytes = cycleOf4Bytes * 4;
			int avpHexDumpLength = actualLengthInBytes * 2;

			lengthString += " , " + "Original Passed : " + lengthAVPInBytes + " Bytes";
			lengthString += " , " + "Number of Hex Char Octets : " + cycleOf4Bytes;
			lengthString += " , " + "Actual length in bytes : " + actualLengthInBytes;
			lengthString += " , " + "AVP HexDump Length : " + avpHexDumpLength;

			outputMap.put("Avps length", lengthString);

			String AVPHexdump = hexdump.substring(0, avpHexDumpLength);
			outputMap.put("AVP Hexdump", AVPHexdump);

			String data = AVPHexdump.substring(processedTill);
			outputMap.put("Data Hexdump", data);

			// write data parsing logic here
			Map<String, Object> dataObj = data(data, AVPcodeDec);
			
			if(dataObj.keySet().size()>0)				
			outputMap.put("Data", dataObj);
			else
				outputMap.put("Data", "Unknown");	

			try {
				hexdump = hexdump.substring(avpHexDumpLength);

			} catch (Exception e) {
				hexdump = "";
			}

			outputList.add(outputMap);

		}

		return outputList;
	}

	public static List<LinkedHashMap<String, Object>> verifyDiameterProtocol(String hexdump)
			throws CustomException {

		LOGGER.info("-> verifyDiameterProtocol : (hexdump) {}", hexdump);
		List<LinkedHashMap<String, Object>> outputList = new ArrayList<LinkedHashMap<String, Object>>();

		while (hexdump.length() > 0) {

			LinkedHashMap<String, Object> outputMap = new LinkedHashMap();
			LinkedHashMap<String, Object> outputMapError = new LinkedHashMap<String, Object>();

			try {
			
				String versionHex = hexdump.substring(0, 2);
				validateVersion(versionHex);
				outputMap.put("Diameter Version", "0x" + versionHex);

				String lengthHex = hexdump.substring(2, 8);
				validateLength(lengthHex);
				int lengthDec = Integer.parseInt(lengthHex, 16);
				outputMap.put("Diameter Length", lengthDec);

				String diameterFlagsHex = hexdump.substring(8, 10);
				int diameterFlagsDec = Integer.parseInt(diameterFlagsHex, 16);
				String diameterFlagsBin = Integer.toBinaryString(diameterFlagsDec);
				while (diameterFlagsBin.length() < 8) {
					diameterFlagsBin = "0" + diameterFlagsBin;
				}
				outputMap.put("Diameter flag",
						"Binary: " + diameterFlagsBin + " , Decimal: " + diameterFlagsDec + " , Hexadecimal: 0x" + diameterFlagsHex);

				LinkedHashMap<String, Object> flagMap = new LinkedHashMap<String, Object>();
				boolean request = "1".equals(diameterFlagsBin.substring(0, 1));
				flagMap.put("Request", request);
				boolean proxyable = "1".equals(diameterFlagsBin.substring(1, 2));
				flagMap.put("Proxyable", proxyable);
				boolean error = "1".equals(diameterFlagsBin.substring(2, 3));
				 flagMap.put("E", error);
				boolean potentiallyRetransmittedMessage = "1".equals(diameterFlagsBin.substring(3, 4));
				flagMap.put("Potentially Retransmitted Message", potentiallyRetransmittedMessage);
				flagMap.put("Reserved", diameterFlagsBin.substring(4));
				outputMap.put("AVP flags (Parsed)", flagMap);
				
				String commandCodeHex = hexdump.substring(10, 16);
				isValidCommandCode(commandCodeHex);
				int commandCodeDec = Integer.parseInt(commandCodeHex, 16);
				CommandCode cc	=CommandCode.identify(commandCodeDec, request);
				String commandCodeStr = cc.name()+" " + cc.command  + " (Hexadecimal: 0x" + commandCodeHex + " , Decimal: " + commandCodeDec + ")";
				outputMap.put("Diameter Command Code", commandCodeStr);
				
				String applicationId = hexdump.substring(16, 24);
				validateApplicationId(applicationId);
				outputMap.put("Diameter Application Id", applicationId);

				String hop_by_hopIdentifier = isValidHopByHop(hexdump.substring(24, 32));
				outputMap.put("Hop-By-Hop Identifier", hop_by_hopIdentifier);

				String end_to_endIdentifier = isValidEndT0EndIdentifier(hexdump.substring(32, 40));
				outputMap.put("End-To-End Identifier", end_to_endIdentifier);
				
				String AVPsHexDump = hexdump.substring(40, 2 * lengthDec);
				outputMap.put("AVPsHexDump", AVPsHexDump);

				List<LinkedHashMap<String, Object>> AVPs = AVPsParser(AVPsHexDump);
				outputMap.put("AVPs", AVPs);

				hexdump = hexdump.substring(2 * lengthDec);
				outputMap.put("nextFragement", hexdump);

				outputMap.put("nextFragementLength", hexdump.length());
				
				outputList.add(outputMap);

			} catch (Exception e) {
				outputMapError.put("Error", "Diameter Protocol Malformed Packet - " +e.getMessage());
				outputList = new ArrayList<LinkedHashMap<String, Object>>();
				outputList.add(outputMapError);
				hexdump = "";
				LOGGER.error("diameter exception -> (message) {}", e.getMessage(), e);
			}

		}
		LOGGER.info("<- verifyDiameterProtocol : (outputList) {}", outputList);
		return outputList;
	}

	private static void isValidCommandCode(String commandCode) throws CustomException {
		if (commandCode.length() != 6)
			throw new CustomException("Command code not valid...." + commandCode,1201);
	}

	private static void validateApplicationId(String applicationId) throws CustomException {
		if (applicationId.length() != 8)
			throw new CustomException("Application Id not valid...." + applicationId,1201);
	}

	private static String isValidEndT0EndIdentifier(String endtoend) throws CustomException {
		if (endtoend.length() != 8)
			throw new CustomException("End to End not valid...." + endtoend,1201);
		return "0x" + endtoend;
	}

	private static String isValidHopByHop(String HbyH) throws CustomException {
		if (HbyH.length() != 8)
			throw new CustomException("HOP BY HOP not valid...." + HbyH,1201);
		return "0x" + HbyH;
	}

	private static void validateLength(String DiameterLength) throws CustomException {
		if (DiameterLength.length() != 6)
			throw new CustomException("Diameter Length not valid...." + DiameterLength,1201);
	}

	private static String validateVersion(String version) throws CustomException {
		System.out.println("version  "+version);
		if (version.length() != 2)
			throw new CustomException("Version not valid...." + version,1201);
		else{
			if(version.equals("01"))
				return version;
			else throw new CustomException("Version not valid...." + version,1201);
		}
	}

	private static String isValidDiameterProtocol(String hexdump) throws CustomException {
		if (hexdump.length() < 40)
			throw new CustomException("Invalid  Diameter Protocol " + hexdump.substring(0),400);

		else
			return hexdump;
	}
	
	

	
	
}