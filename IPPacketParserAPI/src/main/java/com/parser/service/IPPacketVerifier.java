package com.parser.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.parser.customError.CustomException;

public class IPPacketVerifier {
	/**
	 * Parses the hexdump of IPPacket
	 * 
	 * @request string
	 * @response List<Object>
	 * @author AmantyaTech
	 */
	@SuppressWarnings("unused")
	public static List<Object> verifyIPPacket(String hexdumpString) throws CustomException {

		if (hexdumpString != null) {

			String hexdump = null, errormsg = null;
			int counter = 0, errorCode = 0;
			LinkedHashMap<String, Object> rootOutputMap = null;
			LinkedHashMap<String, Object> outputMap = null;
			boolean flag = false;
			List<Object> ipList = null;
			List<Object> ipErrorList = null;
			String regx = "[\\|\t\n\r]+";
			List<String> list = Arrays.asList(hexdumpString.split(regx));
			// ******** Remove empty index from list
			list = list.stream().filter(s -> !s.isEmpty()).collect(Collectors.toList());
			ipList = new ArrayList<>();

			for (int i = 0; i < list.size(); i++) {
				hexdump = list.get(i).replaceAll("\\s", "");
				outputMap = new LinkedHashMap<String, Object>();
				rootOutputMap = new LinkedHashMap<String, Object>();
				String hexStart = "<<", hexEnd = ">>";
				try {

					if ((hexdump.matches("[0-9A-Fa-f<>]+"))) {
						counter++;
						if (((hexdump.startsWith(hexStart)) && (hexdump.endsWith(hexEnd)))) {
							hexdump = hexdump.replace(hexEnd, "");
							hexdump = hexdump.replace(hexStart, "");
						} else {
							hexdump = hexdump;
						}

						boolean isV4 = false, isV6 = false;
						String IPVer = null;
						if (hexdump.length() > 0) {
							IPVer = hexdump.substring(0, 1);
							if (IPVer.equals("4")) {
								isV4 = true;
							} else if (IPVer.equals("6")) {
								isV6 = true;
							}
						}

						outputMap = new LinkedHashMap<>();
						String ipPacket = isValidIPPacket(hexdump, IPVer);
						String version = isValidVersion(hexdump.substring(0, 1));
						// IPv4 Header ..

						if (isV4) {

							Map<String, Object> parsedipv4 = getParsedIPv4(hexdump, version);
							outputMap.putAll(parsedipv4);
						}
						// IPV6 Header...
						if (isV6) {
							Map<String, Object> parsedipv6 = getParsedIPv6(hexdump);
							outputMap.putAll(parsedipv6);
						}
						// counter++;
						rootOutputMap.put("IP Packet " + (i + 1), outputMap);
						ipList.add(outputMap);
						flag = true;
						// System.out.println("flag...."+flag);
					} else {
						throw new CustomException("IPPacket Error - Hexdump format is not valid '" + hexdump + "' ",
								400);
					}

				} catch (CustomException e) {
					errormsg = e.getMessage();
					errorCode = e.getCode();

					// System.out.println(errorCode+"....flag...."+flag);
					// e.printStackTrace();
					// outputMap.put("Hexdump", hexdump);
					outputMap.put("Error", e.getMessage());
					ipList.add(outputMap);
				}
			}

			// System.out.println("ipList..."+ipList.size());

			if (ipList != null) {
				if (ipList.size() == 1) {
					@SuppressWarnings("unchecked")
					Map<Object, Object> ipObject = (Map<Object, Object>) ipList.get(0);
					if (ipObject.size() == 1) {
						// System.out.println(errormsg+"...errorCode........."+errorCode);
						if (errorCode != 0)
							throw new CustomException(ipObject.get("Error").toString(), errorCode);
						else
							throw new CustomException(ipObject.get("Error").toString(), 1201);
					} else
						return ipList;
				} else if (ipList.size() > 1) {
					ipErrorList = new ArrayList<>();
					// System.out.println("........ip...."+ipErrorList.size());
					for (int j = 0; j < ipList.size(); j++) {
						@SuppressWarnings("unchecked")
						LinkedHashMap<String, Object> rootOut = (LinkedHashMap<String, Object>) ipList.get(j);
						if (rootOut.get("Hexdump") == null && rootOut.get("HexDump") == null)
							ipErrorList.add(ipList.get(j));
					}
					if (ipList.size() == ipErrorList.size())
						throw new CustomException("No valid hexdump found, Please try again!!", 400);

					else
						return ipList;
				}

//		else if(flag == false)	
//			throw new CustomException("No valid hexdump found, Please try again!!",400);

				if (counter == 0)
					throw new CustomException("No valid hexdump found, Please try again!", 400);
				else
					return ipList;
			} else
				throw new CustomException("Data could not be processed,please try again!", 400);

//		if(counter==0)
//			throw new CustomException("No valid hexdump found, Please try again!");
//		else {
//			return ipList;
//		}

		} else
			throw new CustomException("No hexdump found, Please give the hexdump and try again!", 400);

	}

	private static Map<String, Object> getParsedIPv4(String hexdump, String version) {
		final Logger logger = LoggerFactory.getLogger(IPPacketVerifier.class);

		Map<String, Object> outputMapv4 = new LinkedHashMap<>();
		LinkedHashMap<String, Object> outputMapError = new LinkedHashMap<String, Object>();
		try {
			//outputMapv4.put("Hexdump", hexdump);
			// version = isValidVersion(hexdump.substring(0, 1));
			outputMapv4.put("version", version);

			String headerLengthHex = hexdump.substring(1, 2);
			int headerLengthDec = Integer.parseInt(headerLengthHex, 16);
			int headerLengthBytes = 4 * headerLengthDec;
			if (headerLengthBytes < 20 || headerLengthBytes > 60)
				throw new CustomException("IPPacket Error - Header Length not valid - " + headerLengthBytes
						+ " bytes , range should be 20 - 60 bytes", 1201);
			outputMapv4.put("Header Length", "Hexadecimal: " + headerLengthHex + " ; Decimal: " + headerLengthDec
					+ " ; Bytes: " + headerLengthBytes);

			String sourcetype = isValidPrecedence(hexdump.substring(2, 4));
			while (sourcetype.length() < 8)
				sourcetype = "0" + sourcetype;

			Integer pre = Integer.parseInt(sourcetype.substring(0, 3), 2);
			outputMapv4.put("Precedence", pre.toString());

			if (sourcetype.charAt(3) == '1')
				outputMapv4.put("Precedence : Minimize Delay Requested", "true");
			if (sourcetype.charAt(4) == '1')
				outputMapv4.put("Precedence : Maximize Throughput Requested", "true");
			if (sourcetype.charAt(5) == '1')
				outputMapv4.put("Precedence : Maximize Reliability Requested", "true");
			if (sourcetype.charAt(6) == '1')
				outputMapv4.put("Precedence :Minimize Cost Requested", "true");

			String totalLength = Integer.parseInt(version) == 4 ? isValidTotalLength(hexdump.substring(4, 8))
					: isValidTotalLength(hexdump.substring(8, 12));
			outputMapv4.put("Total Length", totalLength + " Bytes");

			String id = isValidIdentification(hexdump.substring(8, 12));
			outputMapv4.put("Identification", id);

			String fragment = isValidfragmentmentLength(hexdump.substring(12, 16));
			String fragmentBinary = Integer.toBinaryString(Integer.parseInt(hexdump.substring(12, 16), 16));
			while (fragmentBinary.length() < 16)
				fragmentBinary = "0" + fragmentBinary;
			String fragmentFlags = fragmentBinary.substring(0, 3);

			if (fragment.charAt(1) == '1')
				outputMapv4.put("Do not fragment Packet", "true");
			else
				outputMapv4.put("Can be fragmented", "true");
			if (fragment.charAt(2) == '1')
				outputMapv4.put("More fragments pending", "true");
			else
				outputMapv4.put("No more fragments pending", "true");

			if (fragment.charAt(1) != '1' && fragment.charAt(2) != '1')
				outputMapv4.put("Fragmentation Offset ", (Integer.parseInt(hexdump.substring(12, 16), 16)) + "");
			else
				outputMapv4.put("Fragmentation Offset", "0");

			String TimeToLive = isValidTimeToLive(hexdump.substring(16, 18));
			outputMapv4.put("Time to live: ", TimeToLive);

			String checksum1 = isValidChecksumLength(hexdump.substring(20, 24));
			outputMapv4.put("Header Checksum: ", "0x" + checksum1);

			String ipPayload = ((Integer.parseInt(totalLength)) - headerLengthBytes) + " Bytes";
			outputMapv4.put("IP Payload", ipPayload);

			String sourceIp = isValidIPv4Address(hexdump.substring(24, 32));
			outputMapv4.put("Source IP Address", sourceIp);

			String destinationIp = isValidIPv4Address(hexdump.substring(32, 40));
			outputMapv4.put("Destination IP", destinationIp);

			String protocol = isValidProtocolLength(hexdump.substring(18, 20));
			if (Integer.parseInt(protocol) == 1) {
				outputMapv4.put("Transport Layer Protocol", "ICMP(Unsupported");
//				if (hexdump.length() > 40) {
//					LinkedHashMap<String, Object> map3 = new LinkedHashMap<String, Object>();
//					map3 = ICMPPacketVerifier.verifyICMPPacket(hexdump.substring(40), hexdump);
//					outputMapv4.putAll(map3);
//				}
			} else if (Integer.parseInt(protocol) == 58) {
				outputMapv4.put("Transport Layer Protocol", "ICMPv6(Unsupported)");
			}

			else if (Integer.parseInt(protocol) == 41) {
				outputMapv4.put("Internet Protocol", "IPv6");
				if (hexdump.length() > 40) {
					Map<String, Object> map3 = new LinkedHashMap<String, Object>();
					map3 = getParsedIPv6(hexdump.substring(40));
					// outputMapv4.put("IPv6",map3);
					outputMapv4.putAll(map3);
				}
			}

			else if (Integer.parseInt(protocol) == 2)
				outputMapv4.put("Transport Layer Protocol", "IGMP(Unsupported)");

			else if (Integer.parseInt(protocol) == 89)
				outputMapv4.put("Transport Layer Protocol", "OSPF(Unsupported)");

			else if (Integer.parseInt(protocol) == 17) {
				outputMapv4.put("Transport Layer Protocol", "UDP(Unsupported)");
				if (hexdump.length() > 40) {
					LinkedHashMap<String, Object> map3 = new LinkedHashMap<String, Object>();
					map3 = UDPPacketVerifier.verifyUDPPacket(hexdump.substring(40));
					outputMapv4.putAll(map3);
				}
			}

			else if (Integer.parseInt(protocol) == 6) {
				outputMapv4.put("Transport Layer Protocol", "TCP(Unsupported)");
				if (hexdump.length() > 40) {
					LinkedHashMap<String, Object> map2 = new LinkedHashMap<String, Object>();
					map2 = TCPPacketVerifier.verifyTCPPacket(hexdump.substring(40));
					// outputMapv4.put("TCP",map2);
					outputMapv4.putAll(map2);
				}
			}

			else if (Integer.parseInt(protocol) == 132) {
				outputMapv4.put("Transport Layer Protocol", "SCTP(Unsupported)");
//				if (hexdump.length() > 40) {
//					LinkedHashMap<String, Object> map3 = new LinkedHashMap<String, Object>();
//					map3 = SCTPPacketVerifier.verifySCTPPacket(hexdump.substring(40));
//					outputMapv4.putAll(map3);
//				}
			} else {
				if (hexdump.length() > 40) {
					LinkedHashMap<String, Object> map3 = new LinkedHashMap<String, Object>();
					outputMapv4.put("Transport Layer Protocol", "Unsupported");

					try {
						
						throw new CustomException("Unsupported Protocol-Packet does not contained supported protocol",
								400);
					} catch (Exception e) {
						map3.put("Error", e.getMessage());
						outputMapv4.put("Unsupported", map3);
					}
				}
			}
			logger.info("IPv4 Parser parsed data successfully.." + outputMapv4);

			return outputMapv4;
		} catch (Exception e) {
			// e.printStackTrace();
			outputMapError.put("Error", "IPv4 Malformed Packet - " + e.getMessage());
			logger.error("IPv4 Parser Error.." + e.getMessage());
			return outputMapError;
		}

	}

	private static Map<String, Object> getParsedIPv6(String hexdump) {
		final Logger logger = LoggerFactory.getLogger(IPPacketVerifier.class);

		Map<String, Object> outputMapv6 = new LinkedHashMap<>();
		LinkedHashMap<String, Object> outputMapError = new LinkedHashMap<String, Object>();
		try {
			//outputMapv6.put("HexDump", hexdump);
			String version = isValidVersion(hexdump.substring(0, 1));
			outputMapv6.put("Version", version);

			String trafficClass = isValidTrafficClass(hexdump.substring(1, 3));
			outputMapv6.put("Traffic Class", "0x" + trafficClass);

			String flowLabel = isValidFlowLabel(hexdump.substring(3, 8));
			outputMapv6.put("Flow Label", "0x" + flowLabel);

			String payloadLength = isValidPayloadLength(hexdump.substring(8, 12));
			outputMapv6.put("Payload Length", payloadLength);

			String hopLimit = isValidHopLimit(hexdump.substring(14, 16));
			outputMapv6.put("HopLimit", hopLimit);

			String sourceIp = isValidSourceIPv6Address(hexdump.substring(16, 48));
			outputMapv6.put("Source IP Address", sourceIp);

			String destinationIp = isValidSourceIPv6Address(hexdump.substring(48, 80));
			outputMapv6.put("Destination IP", destinationIp);

			String nextHeader = isValidNextHeader(hexdump.substring(12, 14));
			outputMapv6.put("Next Header", nextHeader);
			if (Integer.parseInt(nextHeader) == 1) {
				outputMapv6.put("Transport Layer Protocol", "ICMP(Unsupported)");
//				if (hexdump.length() > 80) {
//					LinkedHashMap<String, Object> map3 = new LinkedHashMap<String, Object>();
//					map3 = ICMPPacketVerifier.verifyICMPPacket(hexdump.substring(80), hexdump);
//					outputMapv6.putAll(map3);
//				}
			} else if (Integer.parseInt(nextHeader) == 58) {
				outputMapv6.put("Transport Layer Protocol", "ICMPv6(Unsupported)");
			}

			else if (Integer.parseInt(nextHeader) == 2)
				outputMapv6.put("Transport Layer Protocol", "IGMP(Unsupported)");

			else if (Integer.parseInt(nextHeader) == 89)
				outputMapv6.put("Transport Layer Protocol", "OSPF(Unsupported)");

			else if (Integer.parseInt(nextHeader) == 17) {
				outputMapv6.put("Transport Layer Protocol", "UDP(Unsupported)");
				if (hexdump.length() > 80) {
					LinkedHashMap<String, Object> map3 = new LinkedHashMap<String, Object>();
					map3 = UDPPacketVerifier.verifyUDPPacket(hexdump.substring(80));
					outputMapv6.putAll(map3);
				}
			} else if (Integer.parseInt(nextHeader) == 6) {
				outputMapv6.put("Transport Layer Protocol", "TCP(Unsupported)");
				if (hexdump.length() > 80) {
					LinkedHashMap<String, Object> map2 = new LinkedHashMap<String, Object>();
					map2 = TCPPacketVerifier.verifyTCPPacket(hexdump.substring(80));
					outputMapv6.putAll(map2);
				}
			} else if (Integer.parseInt(nextHeader) == 132) {
				outputMapv6.put("Transport Layer Protocol", "SCTP(Unsupported)");
//				if (hexdump.length() > 80) {
//					LinkedHashMap<String, Object> map3 = new LinkedHashMap<String, Object>();
//					map3 = SCTPPacketVerifier.verifySCTPPacket(hexdump.substring(80));
//					outputMapv6.putAll(map3);
//				}
			}
			
			else {
				if (hexdump.length() > 80) {
					LinkedHashMap<String, Object> map3 = new LinkedHashMap<String, Object>();
					outputMapv6.put("Transport Layer Protocol", "Unsupported");

					try {
						throw new CustomException("Unsupported Protocol-Packet does not contained supported protocol",
								400);
					} catch (Exception e) {
						map3.put("Error", e.getMessage());
						outputMapv6.put("Unsupported", map3);
					}
				}
			}
			
			logger.info("IPv6 Parser parsed data successfully.." + outputMapv6);

			return outputMapv6;
		} catch (Exception e) {
			// e.printStackTrace();
			outputMapError.put("Error", "IPv6 Malformed Packet - " + e.getMessage());
			logger.error("IPv6 Parser Error.." + e.getMessage());
			return outputMapError;
		}

	}

	private static String isValidHopLimit(String hexHopLimit) throws CustomException {
		if (hexHopLimit.length() != 2) {
			throw new CustomException("IPPacket Error - Hop Limit - " + hexHopLimit + " is not valid ", 1201);
		} else
			return Integer.parseInt(hexHopLimit, 16) + "";
	}

	private static String isValidNextHeader(String hexNextHeader) throws CustomException {
		if (hexNextHeader.length() != 2) {
			throw new CustomException("IPPacket Error - Next header length - " + hexNextHeader + " is not valid ",
					1201);
		} else
			return Integer.parseInt(hexNextHeader, 16) + "";
	}

	private static String isValidPayloadLength(String hexPayloadLength) {
		return Integer.parseInt(hexPayloadLength, 16) + "";
	}

	private static String isValidFlowLabel(String hexFlowLabel) throws CustomException {
		if (hexFlowLabel.length() != 5) {
			throw new CustomException("IPPacket Error - Flow label - " + hexFlowLabel + " is not valid ", 1201);
		} else
			return (hexFlowLabel);
	}

	private static String isValidTrafficClass(String hexTrafficClass) {
		return (hexTrafficClass);
	}

	private static String isValidProtocolLength(String hexProtocol) throws CustomException {

		if (hexProtocol.length() != 2) {
			throw new CustomException("IPPacket Error - Protocol length - " + hexProtocol + " is not valid ", 1201);
		} else
			return Integer.parseInt(hexProtocol, 16) + "";
	}

	private static String isValidChecksumLength(String hexCheckSum) throws CustomException {
		if (hexCheckSum.length() != 4)
			throw new CustomException("IPPacket Error - CheckSum length - " + hexCheckSum + " is not valid ", 1201);
		else {
			return Integer.parseInt(hexCheckSum, 16) + "";
		}
	}

	private static String isValidTimeToLive(String hexTimeToLive) throws CustomException {
		if (hexTimeToLive.length() != 2)
			throw new CustomException("IPPacket Error - TimeToLive length - " + hexTimeToLive + " is not valid ", 1201);
		else
			return Integer.parseInt(hexTimeToLive, 16) + " Hops";
	}

	private static String isValidfragmentmentLength(String hexfragmentment) throws CustomException {
		if (hexfragmentment.length() != 4)
			throw new CustomException(
					"IPPacket Error - Fragmentmentation length - " + hexfragmentment + " is not valid ", 1201);
		else {
			String fragmentBinary = Integer.toBinaryString(Integer.parseInt(hexfragmentment, 16));
			while (fragmentBinary.length() < 16)
				fragmentBinary = "0" + fragmentBinary;
			String fragmentFlags = fragmentBinary.substring(0, 3);
			return fragmentFlags;
		}
	}

	private static String isValidIdentification(String hexidentification) throws CustomException {
		if (hexidentification.length() != 4)
			throw new CustomException(
					"IPPacket Error - Identification length - " + hexidentification + " is not valid ", 1201);
		else
			return " " + Integer.parseInt(hexidentification, 16);
	}

	private static String isValidTotalLength(String totalLength) throws CustomException {
		if (totalLength.length() != 4)
			throw new CustomException(
					"IPPacket Error - Total Length - " + totalLength + " is not valid , valid length is 20 bytes",
					1201);
		else
			return Integer.parseInt(totalLength, 16) + "";
	}

	private static String isValidPrecedence(String precedence) throws CustomException {
		if (precedence.length() > 2)
			throw new CustomException("IPPacket Error - Invalid precedence Length - " + precedence, 1201);
		else {
			String s_t_bin = Integer.toBinaryString(Integer.parseInt(precedence, 16));
			return s_t_bin;
		}
	}

	private static String isValidIPv4Address(String ipAddress) throws CustomException {
		if (ipAddress.length() != 8)
			throw new CustomException("IPPacket Error - IP Address length -" + ipAddress
					+ "  is invalid , it should not be greater than 8", 1201);
		else {
			String ipBinary = Long.toBinaryString(Long.parseLong(ipAddress, 16));
			while (ipBinary.length() < 32)
				ipBinary = "0" + ipBinary;
			String sip = Integer.parseInt(ipBinary.substring(0, 8), 2) + "."
					+ Integer.parseInt(ipBinary.substring(8, 16), 2) + "."
					+ Integer.parseInt(ipBinary.substring(16, 24), 2) + "."
					+ Integer.parseInt(ipBinary.substring(24, 32), 2);
			return sip;
		}
	}

	private static String isValidSourceIPv6Address(String ipAddress) throws CustomException {

		if (ipAddress.length() != 32)
			throw new CustomException("IPPacket Error - IP Address length - " + ipAddress
					+ "  is invalid , it should not be greater than 8", 1201);
		else {
			String output = ipAddress.substring(0, 4) + ":" + ipAddress.substring(4, 8) + ":"
					+ ipAddress.substring(8, 12) + ":" + ipAddress.substring(12, 16) + ":" + ipAddress.substring(16, 20)
					+ ":" + ipAddress.substring(20, 24) + ":" + ipAddress.substring(24, 28) + ":"
					+ ipAddress.substring(28, 32);
			return (output);
		}
	}

	private static String isValidVersion(String hexVersion) throws CustomException {
		int versionInInteger = Integer.parseInt(hexVersion, 16);
		if (versionInInteger != 4 && versionInInteger != 6)
			throw new CustomException(
					"IPPacket Error - IP version - " + hexVersion + " is invalid , expected IP Version is 4 or 6",
					1201);
		else
			return new String(versionInInteger + "");
	}

	private static String isValidIPPacket(String hexdump, String iPVer) throws CustomException {
		if (hexdump.length() < 40) {
			throw new CustomException("IPPacket Error - IPPacket should not be less than 40 digits", 400);
		}
		if (iPVer.equals("4") && hexdump.length() < 40)
			throw new CustomException("IPPacket Error - IPPacket should not be less than 40 digits", 400);
		if (iPVer.equals("6") && hexdump.length() < 80)
			throw new CustomException("IPPacket Error - IPPacket should not be less than 80 digits", 400);
		else
			return hexdump;
	}

}
