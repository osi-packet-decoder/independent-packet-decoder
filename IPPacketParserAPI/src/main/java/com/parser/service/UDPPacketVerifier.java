package com.parser.service;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.parser.customError.CustomException;

       /**
        * @author AmantyaTech
        */

public class UDPPacketVerifier {
	/**
	 * Parses the Hexdump of UDPPacket
	 * @request string
	 * @response LinkedHashMap<String, Object>
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(UDPPacketVerifier.class);
	
	public static LinkedHashMap<String, Object> verifyUDPPacket(String hexdump)
			throws CustomException {
		
		LOGGER.info("UDP input "+hexdump);

		LinkedHashMap<String, Object> outputMap = new LinkedHashMap();
		LinkedHashMap<String, Object> outputMapError = new LinkedHashMap();

		try {
			String udpPacket = isValidUDPPacket(hexdump);
			String sourcePort = isValidSourcePort(hexdump.substring(0, 4));
			//outputMap.put("UDP Source Port", sourcePort);
			String destinationPort = isValidDestinationPort(hexdump.substring(4, 8));
			//outputMap.put("Destination Port", destinationPort);
			String totalLength = isValidTotalLength(hexdump.substring(8, 12));
			//outputMap.put("Packet Total Length ", totalLength + " Bytes");
			String checksum = isValidChecksum(hexdump.substring(12, 16));
			//outputMap.put("Checksum", "0x" + checksum);
			int payloadCharcterCount = hexdump.length()-16;
			//outputMap.put("Payload Size",payloadCharcterCount/2 + " Bytes");
			
			// UDP over SIP
			if (Integer.parseInt(sourcePort) == 5060 || Integer.parseInt(destinationPort) == 5060
					|| Integer.parseInt(sourcePort) == 5061 || Integer.parseInt(destinationPort) == 5061) {

				outputMap.put("Application Layer Protocol", "SIP");
				if (hexdump.length() > 16) {
					LinkedHashMap<String, Object> map3 = new LinkedHashMap();
					LOGGER.info("calling sip "+hexdump.substring(16));
					map3 = SIPPacketVerifier.verifySIPPacket(hexdump.substring(16));
					outputMap.putAll(map3);
				}
			}
			//UDP over DTLS
			else if ((Integer.parseInt(sourcePort) == 6000 && Integer.parseInt(destinationPort) == 7000)
					|| (Integer.parseInt(sourcePort) == 7000 && Integer.parseInt(destinationPort) == 6000)) {

//				outputMap.put("Security Layer Protocol", "DTLS");
//				if (hexdump.length() > 16) {
//					List<LinkedHashMap<String, Object>> dtlslist = new ArrayList<LinkedHashMap<String, Object>>();
//					try {
//						LOGGER.info("calling dtls "+hexdump.substring(16));
//						dtlslist = DTLSPacketVerifier.parse(hexdump.substring(16));
//					} catch (Exception e) {
//						LinkedHashMap<String, Object> errorMap = new LinkedHashMap<String, Object>();
//						errorMap.put("Error","DTLS Malformed Packet - " + e.getMessage());
//						dtlslist.add(errorMap);
//						LOGGER.error("calling RTP "+e.getMessage());
//					}
//					outputMap.put("DTLS", dtlslist);
//				}
			}
			
			 //UDP over RTP
			else if ((Integer.parseInt(sourcePort) >= 1024 && Integer.parseInt(sourcePort) <= 65536)
					|| (Integer.parseInt(destinationPort) >= 1024 && Integer.parseInt(destinationPort) <= 65536)) {
				
				outputMap.put("Application Layer Protocol", "RTP");
				if (hexdump.length() > 16) {
					LinkedHashMap<String, Object> map4 = new LinkedHashMap();
					LOGGER.info("calling RTP "+hexdump.substring(16));
					map4 = RTPPacketVerifier.parseRTPPacket(hexdump.substring(16));
					outputMap.put("RTP", map4);
				}
			}else {	
				if(hexdump.length() > 16) {
					LinkedHashMap<String, Object> map3 = new LinkedHashMap<String, Object>();
					outputMap.put("Application Layer Protocol", "Unsupported");

				try {					
						throw new CustomException("Unsupported Protocol-Packet does not contained supported protocol" ,400);
					}catch (Exception e) {
						map3.put("Error", e.getMessage());
						outputMap.put("Unsupported",map3);
				}
			}
		}
			LOGGER.info("UDP input "+outputMap);
			return outputMap;

		} catch (Exception e) {
			outputMapError.put("Error", "UDP Malformed Packet - " +e.getMessage());
			LOGGER.error("UDP input "+e.getMessage());
			return outputMapError;

		}
	}

	private static String isValidChecksum(String checksum) throws CustomException {
		if (checksum.length() != 4)
			throw new CustomException("Checksum not valid...." + checksum,1201);
		return checksum;
	}

	private static String isValidTotalLength(String totalLength) throws CustomException {
		if (totalLength.length() != 4)
			throw new CustomException("Total Length not valid...." + totalLength,1201);
		return Integer.parseInt(totalLength, 16) + "";
	}

	private static String isValidDestinationPort(String destinationPort) throws CustomException {
		if (destinationPort.length() != 4)
			throw new CustomException("Destination Port not valid...." + destinationPort,1201);
		return Integer.parseInt(destinationPort, 16) + "";
	}

	private static String isValidSourcePort(String sourcePort) throws CustomException {
		if (sourcePort.length() != 4)
			throw new CustomException("Source Port not valid...." + sourcePort,1201);
		return Integer.parseInt(sourcePort, 16) + "";
	}

	private static String isValidUDPPacket(String hexdump) throws CustomException {
		if (hexdump.length() > 0 && hexdump.length() < 16) {
			throw new CustomException("Invalid UDP Packet " + hexdump.substring(0),400);
		} else
			return hexdump;
	}
}
