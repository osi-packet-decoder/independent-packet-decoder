import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent, HttpRequest } from '@angular/common/http';
import { map } from "rxjs/operators";
import { Observable } from 'rxjs/Observable';

@Injectable({
  providedIn: 'root'
})
export class UserService {
//decoderUrl = "http://203.122.22.90:8087/";
decoderUrl = "http://localhost:8044/";
//decoderUrl = "http://107.178.209.64:8088/";
  baseUrl: any;

  constructor( private http : HttpClient) { }

  decoder(hexDump:any):any {    
    var abc=[];
    var str = localStorage.getItem("SelectedProtocol");
    console.log("str..",str.length)
    if(str.length ==0)
     abc = [];
    else
    abc = str.split(",");
    var hexDump:any={
      hexDump:hexDump.hexDump,
      toView:abc,
    }    
    const data = new FormData();
    data.append('hexdump',hexDump.hexDump);
    data.append('toView',abc.toString());

    return this.http.post(this.decoderUrl +'v1/'+'possiblePacket',data);
  }

  fileUpload(fileString:any) {
    const data = new FormData();
    var abc=[];
    var str = localStorage.getItem("SelectedProtocol");
   // console.log("str..",str.length)
    if(str.length ==0)
     abc = [];
    else
    abc = str.split(",");
    //console.log("abc..."+abc.length,abc)
   
    data.append('file',fileString);
    data.append('toView',abc.toString());
    return this.http.post(this.decoderUrl + 'v1/'+'multiplePossiblePacket',data);
    }

  // decoder(hexDump:any):any {    
  //   var abc=[];
  //   var str = localStorage.getItem("SelectedProtocol");
  //   console.log("str..",str.length)
  //   if(str.length ==0)
  //    abc = [];
  //   else
  //   abc = str.split(",");
  //   console.log("abc..."+abc.length,abc)
  //   var hexDump:any={
  //     hexDump:hexDump.hexDump,
  //     toView:abc,
  //   }    
  //   return this.http.post(this.decoderUrl +'v1/'+'ipPacketParser',hexDump);
  // }  
  // fileUpload(fileString:any) {
  //   return this.http.post(this.decoderUrl + 'uploadBulkIpPackets',fileString);
  // }
  // fileUpload(fileString:any) {
  //   const data = new FormData();
  //   var abc=[];
  //   var str = localStorage.getItem("SelectedProtocol");
  //  // console.log("str..",str.length)
  //   if(str.length ==0)
  //    abc = [];
  //   else
  //   abc = str.split(",");
  //   //console.log("abc..."+abc.length,abc)
   
  //   data.append('file',fileString);
  //   data.append('toView',abc.toString());
  //   return this.http.post(this.decoderUrl + 'v1/'+'uploadBulkIpPackets',data);
  //   }
}
