import { Component, OnInit ,ViewChild,ElementRef} from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { UserService } from '../_service/user.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import Swal from 'sweetalert2';
import { HttpErrorResponse } from '@angular/common/http';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers: [DatePipe]
})
export class HomeComponent implements OnInit {
  @ViewChild('userFile',{ static: true }) userFile: ElementRef;
  fileToUpload: File = null;
  inputForm: FormGroup;
  Data;
  uploadFileData: any;
  decodeData: any;
  outputBox = false;
  result: any = [];
  keys: any;
  nestedData: any;
  tcpDataKey: any;
  ipPacketData: any;
  TCPData: any;
  tcpDownloadData: any;
  HttpData;
  HttpDataKey;
  packetName: any;
  packetData:any;
  tcp = false;
  udp = false;
  http = false;
  singlePacket = false;
  multiplePacket = false;
  multipleFile;
  fileLength:any;
  downloadData:any;
  checkData = [];
  muliplePacketIp = [];
  hexString=[];
  hexDump: string;
  hexString2=[];  
  today:any;
  sctp: boolean;
  sctpDataKey: string[];
  SCTPData: {};
  uploadFileData1: File;
  fileMessage: boolean;
  fileName: string;

  constructor(private userService: UserService,private datePipe:DatePipe) {
    this.today = this.datePipe.transform( new Date(),'yyyy-MM-dd  h:mm:ss');
  }

  ngOnInit() {
    this.inputForm = new FormGroup({
      hexDump: new FormControl()
    })
  }

  ///****** Decode hexcode from text file */
 handleFileInput(files: FileList) {
   //debugger
   this.outputBox = false;      
   this.inputForm.reset();
   this.uploadFileData1 = files[0];  
   var reader = new FileReader();
    // var file1 = this.uploadFileData1;
    this.fileName = this.uploadFileData1.name;
    var allowedExtensions = ["txt"];
    var fileExtension = this.fileName.split('.').pop();
    
    if(fileExtension == "txt" && this.uploadFileData1.size >0){
      this.fileMessage = true; 

    //******* Display file content in text box**** */ 
    // this.inputForm.setValue({
    //     //hexDump: atob(this.Data)
    //     hexDump:photoName+" uploaded!"          
    //   })
       //Swal.fire('Success', 'Your file is uploaded successfully!', 'success');
      if (files && files[0]) {
        reader.onload = (event: any) => {         
          this.uploadFileData = event.target.result;  
          this.Data = this.uploadFileData.split(",").pop();
          var uploadData = {};
          var key = "file";
          uploadData[key] =  atob(this.Data);
          this.userService.fileUpload( this.uploadFileData1).subscribe((res) => {
            let data:any= res;
          if (res == null) {
              Swal.fire('Oops...', 'No input, Please give the input and try again!', 'error');              
          } 
          else if ( data.errorMessage) {
              Swal.fire('Oops...', 'No valid hexdump found, Please try again!', 'error');
              this.fileMessage = false;
          }             
          else {
            //****Added to get body from response. Remove the line if json does not contain body in response   
            res = data; 
           // res = data.response;     
              this.outputBox = true;
              this.tcpDownloadData=res;
              this.packetData = res;
              this.multipleFile = res;
              this.downloadData = res;
              this.fileLength=this.multipleFile.length   
              if(this.fileLength) {
                Swal.fire('Congratulations', 'Your result is here!', 'success');
                this.fileMessage = false;
                for( var i=0;i < this.fileLength; i++) { 
                  this.singlePacket = false;
                  this.multiplePacket = true;
                }
              }
              else {
                this.addToBox(res);
                this.multiplePacket = false;
                this.singlePacket = true;
              }
            }
          }, (error: HttpErrorResponse) => {

            if(error.status==0)
              Swal.fire('Oops...', 'Server error!', 'error');  
            else if(error.error.code == undefined && error.error.errorMessage != undefined)
              Swal.fire('Oops...', error.error.errorMessage, 'error');
            else if(error.error.code == undefined)
              Swal.fire('Oops...', error.error, 'error');
            else if(error.error.code != undefined)
              Swal.fire('Oops...', error.error.message, 'error');
            else if(error.error.errorMessage != undefined)
              Swal.fire('Oops...', error.error.errorMessage, 'error');
            else
              Swal.fire('Oops...', 'Something went wrong!', 'error');
              this.fileMessage = false;
              this.outputBox = false; 
         }
          )
        }
        reader.readAsDataURL(files[0]);
        this.clearSelectedFile();
      }
    }
    else if(fileExtension == "txt" && this.uploadFileData1.size ==0){
      this.fileMessage = false; 
      Swal.fire('Oops...', 'Text file is empty!', 'error');
      this.outputBox = false;
      this.clearSelectedFile();
    }    
    else{
      Swal.fire('Oops...', 'Your file is not supported, Please upload the text file!', 'error');
      this.outputBox = false;
      this.clearSelectedFile();
    }
  }
  ///******* Clear previously selected file */
  clearSelectedFile() {
     this.userFile.nativeElement.value = null;
  }  

///****** Decode hexcode from texbox */
decode(event: any) {
    this.decodeData = JSON.parse(JSON.stringify(event));
  
    var protocol = localStorage.getItem("SelectedProtocol");
//console.log("protocol..."+protocol)

if(this.decodeData.hexDump == null){
  Swal.fire('Oops...', 'No input, Please give the input and try again!', 'error')
  this.outputBox = false;
}

else if(this.decodeData.hexDump !== null && protocol ==null){
  Swal.fire('Oops...', 'Please select the valid protocols from settings tab and try again', 'error')
  this.outputBox = false;
}
// else if(protocol ==null){
//   Swal.fire('Oops...', 'Please select the valid protocols from setting tab and try again!', 'error')
//   this.outputBox = false;
// }
else{
  //debugger
    this.outputBox = false;       
    this.userService.decoder(this.decodeData).subscribe((res: any) => {
      if (res == null || res.errorMessage) {
        this.outputBox = false;
        Swal.fire('Oops...', 'No valid hexdump found, Please try again!', 'error')
      } 
      else {
        Swal.fire('Congratulations', 'Your result is here!', 'success')
        // to get only body from response message
        if(res.response!=undefined){
        res=res.response;
        this.result = res;
      }else{
        res=res;
        this.result = res;
      }
        //console.log("this.result  ",this.result)
        if( this.result.length==undefined){
          this.multiplePacket = false;
          this.addToBox(this.result);
        }else{
          this.outputBox = true; 
          this.tcpDownloadData=res;
            this.packetData = res;
           this.multipleFile = res;
           this.downloadData = res;
           this.fileLength=this.multipleFile.length
            for( var i=0;i < this.fileLength; i++) { 
              this.singlePacket = false;
              this.multiplePacket = true;
            }            
        }
      }

    },
    
      (error: HttpErrorResponse) => {          
       //console.log("error...........  ",error.status)  
       if(error.status==0){
        Swal.fire('Oops...', 'Server error!', 'error');                
       }
       else if(error.error.code == undefined && error.error.errorMessage != undefined)
          Swal.fire('Oops...', error.error.errorMessage, 'error');
        else if(error.error.code == undefined)
          Swal.fire('Oops...', error.error, 'error');
        else if(error.error.code != undefined)
          Swal.fire('Oops...', error.error.message, 'error');
        else if(error.error.errorMessage != undefined)
          Swal.fire('Oops...', error.error.errorMessage, 'error');
        else
          Swal.fire('Oops...', 'Something went wrong!', 'error');
         this.outputBox = false;         
      }
    )
  }
  }

  addToBox(data) {
    data=data.body;
    
    //this.inputForm.reset();
    this.outputBox=false;
    this.outputBox=true;
    this.singlePacket=true;
    this.downloadData = data;
    
    this.packetData = data["IP Packet 1"];
    //console.log("ppppp....."+JSON.stringify(this.packetData));
    const prop = 'Hexdump';
    this.packetData = Object.keys(this.packetData).reduce((object, key) => {
      if (key !== prop) {
        object[key] = this.packetData[key]
      }
      return object
    }, {});   
    // debugger;
    if (this.packetData.TCP) {
      this.outputBox = true;
      this.tcp = true;
      this.udp = false;
      this.sctp = false;
      this.nestedData = this.packetData.TCP;
      const prop = 'TCP';
      this.ipPacketData = Object.keys(this.packetData).reduce((object, key) => {
        if (key !== prop) {
          object[key] = this.packetData[key]
        }
        return object
      }, {});

      this.keys = Object.keys(this.ipPacketData);
      if(this.nestedData.HTTP) {
        this.http = true;
        this.HttpData = this.nestedData.HTTP;
        this.HttpDataKey = Object.keys(this.HttpData);
        const prop1 = 'HTTP';
        this.TCPData = Object.keys(this.packetData.TCP).reduce((object, key) => {
          if (key !== prop1) {
            object[key] = this.packetData.TCP[key]
          }
          return object
        }, {});
  
        this.tcpDataKey = Object.keys(this.TCPData);
      }
      else {
        this.sctp = false;
        this.http = false;
        this.TCPData = this.nestedData;
        this.tcpDataKey = Object.keys(this.TCPData);
      }
    }    
    else if (this.packetData.UDP) {
      this.outputBox = true;
      this.sctp = false;
      this.udp = true;
      this.tcp = false;
      this.nestedData = this.packetData.UDP; 
      const prop3 = 'UDP';
      this.ipPacketData = Object.keys(this.packetData).reduce((object, key) => {
        if (key !== prop3) {
          object[key] = this.packetData[key]
        }
        return object
      }, {});
      
      this.keys = Object.keys(this.ipPacketData);    
      if(this.nestedData.HTTP) {
        this.http = true;
        this.HttpData = this.nestedData.HTTP;
        this.HttpDataKey = Object.keys(this.HttpData);
        const prop4 = 'HTTP';
        this.TCPData = Object.keys(this.packetData.UDP).reduce((object, key) => {
          if (key !== prop4) {
            object[key] = this.packetData.UDP[key]
          }
          return object
        }, {});
        this.tcpDataKey = Object.keys(this.TCPData);
      } else {
        this.http = false;
        this.TCPData = this.nestedData;
        this.tcpDataKey = Object.keys(this.TCPData);
      }
    }
    else if (this.packetData.SCTP) {
      this.sctp = true;
      this.udp = false;
      this.tcp = false;
      this.nestedData = this.packetData.SCTP;    
      const prop3 = 'SCTP';
      this.ipPacketData = Object.keys(this.packetData).reduce((object, key) => {
        if (key !== prop3) {
          object[key] = this.packetData[key]
        }
        return object
      }, {});
      this.keys = Object.keys(this.ipPacketData);
      this.SCTPData = this.nestedData;
      this.sctpDataKey = Object.keys(this.SCTPData);      
    }
    else {
      this.outputBox = true;
      this.ipPacketData = this.packetData;
      this.sctp = false;
      this.udp = false;
      this.tcp = false;
      this.keys = Object.keys(this.ipPacketData);
    }
  }

  addMultpleFile(i,data) {
    this.outputBox=true;   
    this.packetData[i] = data;
    const prop = 'Hexdump';
    this.packetData[i] = Object.keys(data).reduce((object, key) => {
      if (key !== prop) {
        object[key] = data[key]
      }
      return object
    }, {});   
  }
  
  //***** Prevent default shorting order in ng-repeat */
  unsorted(): any { 
  }

  //***to clear form data
  clearFormData(){    
    this.inputForm.reset();
    this.outputBox=false;  
  }
  
  //****download json file */
  downloadJson() {
    //debugger
    this.today = this.datePipe.transform( new Date(),'yyyy-MM-dd_hh-mm-ss');
    var jsonData = this.downloadData;

    //console.log("jsonData...",jsonData)
    // *********Customize naming convention of file
    var customName="",unsupported="";    
  if(jsonData.length==1){
      var version="";   var ipname="",malformed="",notdecoded="",independent="";   
    for (let index = 0; index < this.downloadData.length; index++) {   
       
     jsonData= this.downloadData[index]["IP Packet 1"];   

     console.log("jsonData  "+jsonData);

     //jsonData= this.downloadData[index]["IP Packet 1:SCTP Malformed Packet "]; 
    // [0]["IP Packet 1:Diameter Protocol Malformed Packet "]
    // console.log("pppppp........"+this.downloadData[index]["IP Packet 1:Diameter Protocol Malformed Packet "])

    if(this.downloadData[index]["IP Packet 1:SCTP Malformed Packet "] != undefined){
      jsonData= this.downloadData[index]["IP Packet 1:SCTP Malformed Packet "];
      malformed="M";
    }

    else if(this.downloadData[index]["IP Packet 1:Malformed packet"] != undefined){
      jsonData= this.downloadData[index]["IP Packet 1:Malformed packet"]; 
      malformed="M";    
    }
    else if(this.downloadData[index]["IP Packet 1:TCP Malformed Packet "] != undefined){
      jsonData= this.downloadData[index]["IP Packet 1:TCP Malformed Packet "];
      malformed="M";
    }
    else if(this.downloadData[index]["IP Packet 1:UDP Malformed Packet "] != undefined){
      jsonData= this.downloadData[index]["IP Packet 1:UDP Malformed Packet "];
      malformed="M";
    }
    
    else if(this.downloadData[index]["IP Packet 1:TLS Malformed Packet "] != undefined){
      jsonData= this.downloadData[index]["IP Packet 1:TLS Malformed Packet "];
      malformed="M";
    }
    else if(this.downloadData[index]["IP Packet 1:HTTP Malformed Packet "] != undefined){
      jsonData= this.downloadData[index]["IP Packet 1:HTTP Malformed Packet "];
      malformed="M";
    }
    else if(this.downloadData[index]["IP Packet 1:RTP Malformed Packet "] != undefined){
      jsonData= this.downloadData[index]["IP Packet 1:RTP Malformed Packet "];
      malformed="M";
    }
    else if(this.downloadData[index]["IP Packet 1:DTLS Malformed Packet "] != undefined){
      jsonData= this.downloadData[index]["IP Packet 1:DTLS Malformed Packet "];
      malformed="M";
    }
    else if(this.downloadData[index]["IP Packet 1:SIP Malformed Packet "] != undefined){
      jsonData= this.downloadData[index]["IP Packet 1:SIP Malformed Packet "];
      malformed="M";
    }
    else if(this.downloadData[index]["IP Packet 1:ICMP Malformed Packet "] != undefined){
      jsonData= this.downloadData[index]["IP Packet 1:ICMP Malformed Packet "];
      malformed="M";
    }
    else if(this.downloadData[index]["IP Packet 1:SMTP Malformed Packet "] != undefined){
      jsonData= this.downloadData[index]["IP Packet 1:SMTP Malformed Packet "];
      malformed="M";
    }
    else if(this.downloadData[index]["IP Packet 1:Diameter Protocol Malformed Packet "] != undefined){
      jsonData= this.downloadData[index]["IP Packet 1:Diameter Protocol Malformed Packet "];
      malformed="M";
    } 
    else if(this.downloadData[index]["IP Packet 1:IPv6 Malformed Packet "] != undefined){
      jsonData= this.downloadData[index]["IP Packet 1:IPv6 Malformed Packet "];
      malformed="M";
    } 
    else if(this.downloadData[index]["IP Packet 1:FTP Malformed Packet "] != undefined){
      jsonData= this.downloadData[index]["IP Packet 1:FTP Malformed Packet "];
      malformed="M";
    }  
  
    else if(this.downloadData[index]["IP Packet 1:Unsupported Protocol"] != undefined){
      jsonData= this.downloadData[index]["IP Packet 1:Unsupported Protocol"];
      unsupported="Unsupported"
    } 
    else if(this.downloadData[index]["IP Packet 1:IPv4 Not Decoded"] != undefined){
      jsonData= this.downloadData[index]["IP Packet 1:IPv4 Not Decoded"];    
      notdecoded="N";
    } 
    else if(this.downloadData[index]["IP Packet 1:IPv6 Not Decoded"] != undefined){
      jsonData= this.downloadData[index]["IP Packet 1:IPv6 Not Decoded"]; 
      notdecoded="N";    
    }    
    else if(this.downloadData[index]["IP Packet 1:TCP Not Decoded"] != undefined){
      jsonData= this.downloadData[index]["IP Packet 1:TCP Not Decoded"];  
      notdecoded="N";   
    }
    else if(this.downloadData[index]["IP Packet 1:UDP Not Decoded"] != undefined){
      jsonData= this.downloadData[index]["IP Packet 1:UDP Not Decoded"];  
      notdecoded="N";   
    }  
    else if(this.downloadData[index]["IP Packet 1:SCTP Not Decoded"] != undefined){
      jsonData= this.downloadData[index]["IP Packet 1:SCTP Not Decoded"]; 
      notdecoded="N";    
    }
    else if(this.downloadData[index]["IP Packet 1:ICMP Not Decoded"] != undefined){
      jsonData= this.downloadData[index]["IP Packet 1:ICMP Not Decoded"];  
      notdecoded="N";   
    }
    else if(this.downloadData[index]["IP Packet 1:SMTP Not Decoded"] != undefined){
      jsonData= this.downloadData[index]["IP Packet 1:SMTP Not Decoded"];  
      notdecoded="N";   
    }
    else if(this.downloadData[index]["IP Packet 1:HTTP Not Decoded"] != undefined){
      jsonData= this.downloadData[index]["IP Packet 1:HTTP Not Decoded"]; 
      notdecoded="N";    
    }
    else if(this.downloadData[index]["IP Packet 1:FTP Not Decoded"] != undefined){
      jsonData= this.downloadData[index]["IP Packet 1:FTP Not Decoded"];  
      notdecoded="N";   
    }
    else if(this.downloadData[index]["IP Packet 1:SIP Not Decoded"] != undefined){
      jsonData= this.downloadData[index]["IP Packet 1:SIP Not Decoded"]; 
      notdecoded="N";    
    }
    else if(this.downloadData[index]["IP Packet 1:RTP Not Decoded"] != undefined){
      jsonData= this.downloadData[index]["IP Packet 1:RTP Not Decoded"]; 
      notdecoded="N";    
    }
    else if(this.downloadData[index]["IP Packet 1:TLS Not Decoded"] != undefined){
      jsonData= this.downloadData[index]["IP Packet 1:TLS Not Decoded"];  
      notdecoded="N";   
    }
    
    else if(this.downloadData[index]["IP Packet 1:DTLS Not Decoded"] != undefined){
      jsonData= this.downloadData[index]["IP Packet 1:DTLS Not Decoded"]; 
      notdecoded="N";    
    }
    else if(this.downloadData[index]["IP Packet 1:Diameter Not Decoded"] != undefined){
      jsonData= this.downloadData[index]["IP Packet 1:Diameter Not Decoded"]; 
      notdecoded="N";    
    }
    else if(jsonData["RTP"] != undefined){
      jsonData= jsonData["RTP"];
      independent="RTP";
    }
    else if(jsonData["FTP"] != undefined){
      jsonData= jsonData["FTP"];
      independent="FTP";
    }
    else if(jsonData["Diameter"] != undefined){
      jsonData= jsonData["Diameter"];
      independent="Diameter";
    }
    else if(jsonData["HTTP"] != undefined){
      jsonData= jsonData["HTTP"];
      independent="HTTP";
    }
    else if(this.downloadData[index]["IP"] != undefined){
      jsonData= this.downloadData[index]["IP"];
      console.log("ver>>>>>>  "+jsonData.version+jsonData);
      
//       for (let index2 = 0; index2 < jsonData.length; index2++) { 
// console.log("ppppppp>>>>>>>>>>  "+jsonData[index2].version)
//       }

    }
    else if(jsonData["SIP"] != undefined){
      jsonData= jsonData["SIP"];
      independent="SIP";
    }
    else if(this.downloadData[index].Error != undefined){
      jsonData= this.downloadData[index];
      independent="Error";
    }
    else
      jsonData= this.downloadData[index]["IP Packet 1"];
     //console.log(notdecoded+"....ppppp.....",jsonData)   
     if(jsonData.IPv6 != undefined && jsonData.IPv4 == undefined){
        version=jsonData.IPv6.Version
        ipname="IPv"+version;  
        jsonData=jsonData.IPv6;
     }
     else if(jsonData.IPv4 != undefined && jsonData.IPv6 == undefined){
            version=jsonData.IPv4.version
            jsonData=jsonData.IPv4;            
            ipname="IPv"+version; 

            if( jsonData.IPv6 != undefined && malformed=="M"){
              ipname = ipname+"-IPv"+"6";
              jsonData=jsonData.IPv6;
            } 

        else if( jsonData.IPv6 != undefined){
            ipname = ipname+"-IPv"+jsonData.IPv6.Version;
            jsonData=jsonData.IPv6;
          }     
     }
     else if(this.downloadData[index]["IP"] != undefined){
      for (let index2 = 0; index2 < jsonData.length; index2++) { 
          if(jsonData[index2].version != undefined){
          version=jsonData[index2].version;
          jsonData=jsonData[index2];
          ipname="IPv"+version;
        }
        else if(jsonData[index2].Version != undefined){
          version=jsonData[index2].Version;
          jsonData=jsonData[index2];
          ipname="IPv"+version;
        }
      }

      
   }
     else{
      if(independent!=undefined){      
        customName=independent;
      }else{
       version = jsonData.version;
       ipname="IPv"+version;
      }
     }
       
      if(jsonData.SCTP!=undefined){      
        customName=ipname+"-SCTP";
      }
       
      else if(jsonData.ICMP!=undefined){      
        customName=ipname+"-ICMP";
      }
     
      else if(jsonData.UDP!=undefined){        
        if( jsonData.UDP.DTLS !=undefined)
          customName=ipname+"-UDP-"+"DTLS";
        else if( jsonData.UDP.SIP !=undefined)
          customName=ipname+"-UDP-"+"SIP";
        else if( jsonData.UDP.RTP !=undefined)
          customName=ipname+"-UDP-"+"RTP";
        else
          customName=ipname+"-UDP";
      }else if(jsonData.TCP!=undefined){       
        if( jsonData.TCP.TLS !=undefined)
          customName=ipname+"-TCP-"+"TLS";
        else if( jsonData.TCP.SMTP !=undefined)
          customName=ipname+"-TCP-"+"SMTP";
        else if( jsonData.TCP.SIP !=undefined)
          customName=ipname+"-TCP-"+"SIP";
        else if( jsonData.TCP.HTTP !=undefined)
          customName=ipname+"-TCP-"+"HTTP";
        else if( jsonData.TCP['Diameter Protocol'] !=undefined)
          customName=ipname+"-TCP-"+"Diameter Protocol";
        else if( jsonData.TCP.FTP !=undefined)
          customName=ipname+"-TCP-"+"FTP";
        else
          customName=ipname+"-TCP";
      }
      else {
         if(independent.length > 0 )
           customName = independent;
         else
          customName=ipname;
      }

        if(unsupported=="Unsupported")
          customName=customName+"-"+unsupported
        else if ( notdecoded=="N")
          customName=customName+"("+notdecoded+")"
        else customName;
    }
    }      
     else
        customName="IP-HexDecode";

    var file = JSON.stringify(this.downloadData, null, '\t');
    var element = document.createElement('a');
    element.setAttribute('href', "data:text/json;charset=UTF-8," + encodeURIComponent(file));
    element.setAttribute('download', customName+"("+this.today+")"+".json");

if(malformed=="M")
  element.setAttribute('download', customName+"("+malformed+")("+this.today+")"+".json");
  else element.setAttribute('download', customName+"("+this.today+")"+".json");
    element.style.display = 'grid';
    document.body.appendChild(element);
    element.click(); 
    document.body.removeChild(element);
  }
}
